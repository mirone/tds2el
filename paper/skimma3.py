import string

import h5py
import matplotlib

# matplotlib.use('Qt4Agg')
# from pylab import *
from matplotlib import pyplot as  plt
import numpy as np
import sys
from math import sqrt

h5=h5py.File(sys.argv[1],"r" ) 

sim=h5["sim"]
exp=h5["exp"]
keys = exp.keys()

rhomax = 0.15/sqrt(3.0)

fs_dentro  = 12
fs_fuori = 16
fs_tick=8
items = [

    [ "-1-1-12_1",    50        ],
    [ "-10-16_1",    64        ],
    [ "-1110_1",    64        ],
    [ "-1110_1",    50        ],
    [ "-222_1",    115       ],
    [ "-321_1",    10      ],
    [ "-321_1",    65     ],
    [ "-321_1",    115      ],
    ["-336_1", 64],
    ["-21-15_1",50],
    ["-228_1",74],
    ["-31-10_1",74]
]
f=0.8
fig=plt.figure(dpi=400, figsize = (f*21/2.54,f*29.7/2.54))

for kcount,(chiave,isl) in enumerate(items):
    chiave = "spot_"+chiave
    if chiave not in keys:
        print chiave
        print keys
    ve = exp[chiave]["volume"][:]
    vs = sim[chiave]["volume"][:]

    
    chiave=chiave[5:]
    #pos = chiave.find("_")
    #chiave=chiave[:pos]

    hkl = string.split(chiave,"_")[0]
    if hkl[0]=="-":
        pos1=2
    else:
        pos1 = 1
        
    if hkl[pos1]=="-":
        pos2=pos1+2
    else:
        pos2 = pos1+1
        
    hkl = hkl[:pos1]+","+hkl[pos1:pos2]+","+hkl[pos2:]

    k = kcount/2
    i = kcount%2

    if 1:
    # for k,lista in enumerate([[10,50,60],[64,65,74], [84,115]]):
    #     for i,isl in enumerate(lista):

            rhoz = (isl-64)*rhomax/64
            
            vmin  = min(ve[isl].min(), vs[isl].min())
            vmax  = max(ve[isl].max(), vs[isl].max())
            vmin=0
            Imax=vmax
            Imin = vmin

            
            plt.subplot(6,4, 1+i*2+k*4+1)

            plt.imshow( ve[isl] , vmin=vmin, vmax=vmax,  aspect='auto',origin='lower',extent=( -rhomax,rhomax,-rhomax,rhomax))
            # plt.title(chiave+"/%d"%isl)

            
            ax = plt.gca()
            plt.setp(ax.get_yticklabels(), fontsize=fs_tick)
            plt.setp(ax.get_yticklabels(), rotation=90)
            
            plt.setp(ax.get_xticklabels(), fontsize=fs_tick)

            ax.get_yaxis().set_tick_params(which = "both", direction='out')
            ax.get_xaxis().set_tick_params(which = "both", direction='out')
            #ax.yaxis.set_tick_params(size=5) 
            # if(k==5): plt.xlabel(r'$\rho_x$', fontsize=fs_fuori)

            
            plt.setp(ax.get_yticklabels(), visible=False)
            if(k!=5):
                plt.setp(ax.get_xticklabels(), visible=False)
            
            # plt.title("%4.1e - %4.1e"%(vmin,vmax))

            
            ax.annotate(r"$I_{max}=%3.1f$"%Imax, xy=(-0.08,-0.08), fontsize=fs_dentro, color="white",  bbox = dict(boxstyle = 'round,pad=0.1', fc = 'black', alpha = 0.2),
            )
            
                            # res=self.graph.insertMarker( px, py, "legend", "%d"%(i), color='black', selectable=False, draggable=False, searchFeature=True, 
                            #                xytext = (-20, 0),
                            #                textcoords = 'offset points',
                            #                ha = 'right', va = 'bottom',
                            #                bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.4),
                            #                arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))


            plt.subplot(6,4,1+i*2+k*4)
            plt.imshow( vs[isl], vmin=vmin, vmax=vmax,  aspect='auto',origin='lower',extent=( -rhomax,rhomax,-rhomax,rhomax) )


            ax = plt.gca()
            
            plt.setp(ax.get_yticklabels(), fontsize=fs_tick)
            plt.setp(ax.get_xticklabels(), fontsize=fs_tick)

            if 1:
                if(k==5):
                    if(i==0):
                        plt.xlabel(r'$\rho_x$', fontsize=fs_fuori)
                        ax.xaxis.set_label_coords(1.0, -0.0) 
                    
                else:
                    plt.setp(ax.get_xticklabels(), visible=False)
                if(i==0 ) :
                    if k==5:
                        plt.ylabel(r'$\rho_y$', fontsize=fs_fuori)
                        ax.yaxis.set_label_coords(0.00, 1.0) 
                else:
                    plt.setp(ax.get_yticklabels(), visible=False)
            else:
                if(k==5):
                    pass
                else:
                    plt.setp(ax.get_xticklabels(), visible=False)
                if(i==0) :
                    pass
                else:
                    plt.setp(ax.get_yticklabels(), visible=False)
                

            ax.get_yaxis().set_tick_params(which = "both", direction='out')
            ax.get_xaxis().set_tick_params(which = "both", direction='out')
            ax.yaxis.set_tick_params(size=5) 


            plt.setp(ax.get_yticklabels(), rotation=90)


            
            ax.annotate(r"$\rho_Z=%4.3f$"%rhoz, xy=(-0.08,0.06), fontsize=fs_dentro, color="white",  bbox = dict(boxstyle = 'round,pad=0.1', fc = 'black', alpha = 0.2),
            )
            ax.annotate("hkl="+hkl, xy=(-0.08,-0.08), fontsize=fs_dentro, color="white",  bbox = dict(boxstyle = 'round,pad=0.1', fc = 'black', alpha = 0.2),
            )

            #ax.annotate(r"$I_{min}=%4.3f$"%Imin, xy=(-0.08,-0.08), fontsize=16, color="white",  bbox = dict(boxstyle = 'round,pad=0.1', fc = 'black', alpha = 0.2),)

fig.savefig('foo.png', bbox_inches='tight', dpi=fig.dpi)

            
# figManager = plt.get_current_fig_manager()
#figManager.window.showMaximized()

# plt.show()
