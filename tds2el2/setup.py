import os
import re
import subprocess
import sys

from setuptools import setup

version = [eval(l.split("=")[1]) for l in open(os.path.join(os.path.dirname(
          os.path.abspath(__file__)), "tds2el2", "__init__.py"))
           if l.strip().startswith("version")][0]



setup(
    name="tds2el2",
    version=version,
    author="Alessandro Mirone & Bjorn Wehinger",
    author_email="mirone@esrf.fr",
    description="Elastic constants from thermal diffuse scattering",
    packages = ["tds2el2"+"_v"+version],
    package_dir = {"tds2el2"+"_v"+version: "tds2el2"},
    package_data = {
        "tds2el2"+"_v"+version: [
            "*.ui","*.yaml"
        ],
    },    
    entry_points = {
        'console_scripts': [
            f"tds2el2_v{version}=tds2el2_v{version}.MainWindow:main",
        ],
    },
    long_description="",
    zip_safe=False,
)


