# INSTALLATION BY CONDA

    cd tds2el2/conda
    mamba build .

and then install the package

# QUICK INSTALL BY PIP OR SETUP.PY


First install the python scripts

    cd ..
    pip install .


this install the python scripts in a given directory, inside the conda environment.

Now compile the library

    mkdir build
    cd build
    cmake ../tds2el2/tds2el_c/
    make


Finally you have to copy into the same directory where the scripts are the following file:

    build/spotpicker_cy.so