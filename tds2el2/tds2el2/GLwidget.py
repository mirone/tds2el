from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
# from . import ui
from . import ui
from . import geometry
from .Parameters import Parameters


from silx.gui import qt
from silx.gui.plot3d.SceneWindow import SceneWindow, items
import numpy as np
import sys
from six import StringIO
import traceback

TEST=0



@ui.UILoadable
class GLwidget(qt.QWidget):
    
    selected_point_signal = qt.pyqtSignal(object)
    alignOnPoints_signal  = qt.pyqtSignal(object)
    parsFromVisual_signal = qt.pyqtSignal( object)
    alignByFourier_signal = qt.pyqtSignal( object, object, object)
    parsFromPossibilities_signal = qt.pyqtSignal( object, object, object )
    fit_geo_selected_pars_signal = qt.pyqtSignal( object, object )
    
    last_axisPoint_clicked = None
    myRot = np.array([[1,0,0.0],[0,1,0],[0,0,1]])
    
    def __init__(self, parent=None):
        super(GLwidget, self).__init__(parent)
        self.loadUi()

        sys.excepthook = excepthook
        
        self._clicked = False
        window = SceneWindow()

        sceneWidget = window.getSceneWidget()
        window.getParamTreeView().parent().hide()

        window._sceneGroupResetWidget.parent().hide()
        
        tb = ProjectionToolButton()
        tb.setPlot3DWidget(sceneWidget)
        window.getViewpointToolBar().addWidget(tb)

        sceneWidget.overview.scene.children[0].children[0].marker = 's' ######
        
        sceneWidget.setBackgroundColor((0.8, 0.8, 0.8, 1.))
        sceneWidget.setForegroundColor((0.1, 0.1, 0.1, 1.))
        # sceneWidget.setProjection('orthographic')

        sceneWidget.installEventFilter(self)
        
        scatter3d = sceneWidget.add3DScatter([0.0], [0.0], [0.0], [0.0])
        scatter3d.getColormap().setColormapLUT((
            (0., 0., 0., 1), (0., 0., 0., 1)))
        self.scenewindow = window
        
        sp = qt.QSizePolicy()
        sp.setVerticalPolicy(qt.QSizePolicy.Expanding) #, qt.QSizePolicy.Expanding
        sp.setVerticalStretch(1)
        sp.setHorizontalPolicy(qt.QSizePolicy.Expanding) #, qt.QSizePolicy.Expanding
        sp.setHorizontalStretch(1)
        self.scenewindow.setSizePolicy(sp)
        self.scenewindow.setMinimumSize(800, 800)
        
        self.layout().addWidget( self.scenewindow )

        self.pushButton_alignOnDisplayed.clicked.connect(self.alignOnDisplayedAxis)

        self.whichiswhich = None
        self.whichiswhich_inverted = {}
        self.connectParameters_alignOnPoints()
        self.pushButton_alignOnPoints.clicked.connect( self.onAlignOnPoints_clicked)


        self.pushButton_fitMore.clicked.connect( self.fitMore_clicked)

        
        
        self.pushButton_parsFromVisual.clicked.connect( self.onParsFromVisual_clicked)


        self.pinners = {   self.checkBox_astar:"astar",self.checkBox_bstar:"bstar",self.checkBox_cstar:"cstar"  }
        self.pinners_inverted  = {   "astar": self.checkBox_astar ,"bstar": self.checkBox_bstar  ,"cstar":  self.checkBox_cstar   }
        for cb in self.pinners.keys():
            cb.stateChanged.connect(self.pinVisualAxis )
        self.pinned = { "astar":None,"bstar":None,"cstar":None  }

        self.pinned_aligners = {
            self.pushButton_astarOnVisual:("astar", "vis" ),
            self.pushButton_astarOnVert  :("astar", "ver" ),
            self.pushButton_bstarOnVisual:("bstar", "vis"),
            self.pushButton_bstarOnVert  :("bstar", "ver"),
            self.pushButton_cstarOnVisual:("cstar", "vis"),
            self.pushButton_cstarOnVert  :("cstar", "ver"),
        }
        for cb in self.pinned_aligners.keys():
            cb.clicked.connect(self.alignPinnedAxis )

        self.pushButton_alignOnPinned.clicked.connect(self.alignOnPinnedAxis)
        self.pushButton_pinByFourier.clicked.connect(self.alignUnpinnedByFourier)
        
        self.builded_axis, self.builded_abc, self.builded_Aabc = [],[],[]
        self.comboBox_fourierPossibilities.activated.connect(self.comboBox_fourierPossibilities_EditFinished)

        self.connectParameters_anglesConstraints()


        
    def comboBox_fourierPossibilities_EditFinished(self):
        i = self.comboBox_fourierPossibilities.currentIndex()
        print(" SELEZIONATO " , self.comboBox_fourierPossibilities.currentText())

        abc = self.builded_abc[i]
        Aabc = self.builded_Aabc[i]
        assi  = self.builded_axis[i]
        print(" Axis Lenght\n", abc )
        print(" Angles \n", Aabc )
        print(" Directions \n", assi )

        # Pinno gli assi
        for what, asse in zip( ["astar","bstar","cstar"], assi):
            self.pinned[what] = asse
            self.pinners_inverted[what].setChecked(True)

        # manda allineamento a self.sampleAlignementW  di mainwindow
        
        axis = assi
        X = axis[0]
        Y = axis[1]
        scal = (axis[0]*axis[1]).sum()
        Y = Y - axis[0]*scal
        norm = np.linalg.norm(Y)
        Y = Y/norm
        Z = np.cross(X,Y)
        
        mat = np.array( [X,Y,Z]  ).T
        # mat = np.dot(self.myRot.T,mat)
        angles, quats = geometry.Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat( mat)

        self.parsFromPossibilities_signal.emit(  (angles, quats), abc, Aabc )

        
    def setupChoicesCombo(self):
        labels = []
        for axis, abc, Aabc in zip(   self.builded_axis, self.builded_abc, self.builded_Aabc  ):
            label =""
            for tok in list(abc) :
                label = label +"%.1f_"%tok
            for tok in list( Aabc):
                label = label +"%.2f_"%tok
            labels.append(label[:-1])
        self.comboBox_fourierPossibilities.clear()
        self.comboBox_fourierPossibilities.addItems(labels)

        
    def setFourierPossibilities( self,   builded_axis, builded_abc, builded_Aabc    ):
        self.builded_axis, self.builded_abc, self.builded_Aabc = builded_axis, builded_abc, builded_Aabc
        self.setupChoicesCombo()

    def alignUnpinnedByFourier(self):
        pinnedaxis = []
        for what in  ['astar','bstar','cstar' ]:
            a = self.pinned[what]
            # if a is not None:
            #     a = np.dot(self.myRot.T, a)
            pinnedaxis.append(a)
            
        self.parameters4AnglesConstraints.takeFromOwnGui()
        anglesParObject = self.parameters4AnglesConstraints.getMyParsAsObject()


        ac = anglesParObject
        for tok in [ac.aAA, ac.aBB, ac.aCC]:
            if tok is not None:
                res= tok -int(tok)
                if res==0:

                    msgBox = qt.QMessageBox(None)
                    msgBox.setText("The angle constraint has zero tolerance." )
                    msgBox.setInformativeText("The decimal part of the angle constraint, time 100, is used as tolerance")
                    msgBox.setStandardButtons(  qt. QMessageBox.Ok)
                    msgBox.setDefaultButton( qt.QMessageBox.Ok)
                    ret = msgBox.exec_()
                    
                    return 
        
        
        self.alignByFourier_signal.emit(  pinnedaxis  , self.q3d , anglesParObject ) 

            
        
    def alignOnPinnedAxis( self ) :
        
        axis = [ self.pinned[what] for what in ['astar','bstar','cstar' ]   ]
        if any( [x is None for x in axis]) :
            msgBox = qt.QMessageBox(None)
            msgBox.setText("Not all axis set")
            msgBox.setInformativeText("Alignement on pinned axis was requested but not all thre axis were set")
            msgBox.setStandardButtons(  qt. QMessageBox.Ok)
            msgBox.setDefaultButton( qt.QMessageBox.Ok)
            ret = msgBox.exec_()
            return

        X = axis[0]
        Y = axis[1]
        scal = (axis[0]*axis[1]).sum()
        Y = Y - axis[0]*scal
        norm = np.linalg.norm(Y)
        Y = Y/norm
        Z = np.cross(X,Y)
        
        mat = np.array( [X,Y,Z]  ).T
        # mat = np.dot(self.myRot.T,mat)
        angles, quats = geometry.Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat( mat)

        self.parsFromVisual_signal.emit(  (angles, quats) )
            
    def alignPinnedAxis(self):
        qobj = self.sender()
        what, dire = self.pinned_aligners[qobj]
        if self.pinned[what] is not None:
            axis = self.pinned[what]
            axis = np.dot(self.myRot.T, axis )
            camera =  self.scenewindow.getSceneWidget().viewport.camera
            if dire == "vis" :
                camera.extrinsic.direction = axis
                p = camera.extrinsic.position
                pnorm = np.linalg.norm(p)
                camera.extrinsic.position = -axis*pnorm
            else:
                scal = (camera.extrinsic.direction* axis).sum()
                newdirection = camera.extrinsic.direction - scal*axis
                newdirection = newdirection / np.linalg.norm(newdirection)
                camera.extrinsic.direction = newdirection
                camera.extrinsic.up = axis
            self.scenewindow.getSceneWidget().centerScene()
        else:
            print( "  MA NON E STATO MESSO ")


            
    def pinVisualAxis(self, stato):
        qobj = self.sender()
        what = self.pinners[qobj]
        print( " il checkbox per " , what, " ha cambiato stato , nuovo stato ", stato)
        if not stato :
            self.pinned[what] = None
        else:
            Z = self.scenewindow.getSceneWidget().viewport.camera.extrinsic.direction
            Z = np.dot(self.myRot,Z)
            self.pinned[what] = Z
            print( " ADESSO PINNED ", self.pinned)
            
    def onParsFromVisual_clicked(self  ):
        self.generic_sendSignal_fromVisual( "pars") 


    def fitMore_clicked(self  ):
        print(" in fit more ")
        w = fitAllGeo_hkl()
        w.selected_pars_signal.connect(self.emit_selected_pars_signal)
        w.show()
        self.wstore = w

        
    def emit_selected_pars_signal(self, tok):
        p4alonp =  self.parameters4AlignOnPoints
        p4alonp.takeFromOwnGui()
        
        self.fit_geo_selected_pars_signal.emit(tok,p4alonp )


        
    def onAlignOnPoints_clicked(self  ):
        self.generic_sendSignal_fromVisual( "pars_and_fit") 
        
    def generic_sendSignal_fromVisual(self, todo  ):
        Z = self.scenewindow.getSceneWidget().viewport.camera.extrinsic.direction
        Y = self.scenewindow.getSceneWidget().viewport.camera.extrinsic.up
        X = np.cross( Y,Z  )
        self.parameters4AlignOnPoints.takeFromOwnGui()
        mat = np.array( [X,Y,Z]  ).T
        mat = np.dot(self.myRot,mat)
        angles, quats = geometry.Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat( mat)

        if todo =="pars_and_fit" :
            print(" MANDOOOOOOOOOOOOOOOOOOOOOOOO ",   dir(self.parameters4AlignOnPoints.getMyParsAsObject())  ) 
            self.alignOnPoints_signal.emit( self.parameters4AlignOnPoints.getMyParsAsObject()  )
        elif todo == "pars" :
            self.parsFromVisual_signal.emit(  (angles, quats) )
            

        
    def connectParameters_alignOnPoints(self):
        par_dict = dict([
            ["AA"     ,  self.lineEdit_AA    ],
            ["BB"     ,  self.lineEdit_BB    ],
            ["CC"     ,  self.lineEdit_CC    ],
            ["aAA"   , self.lineEdit_aAA_2  ],
            ["aBB"   , self.lineEdit_aBB_2  ],
            ["aCC"   , self.lineEdit_aCC_2  ],
            ["fix_AA"    ,  self.checkBox_AA    ],
            ["fix_BB"    ,  self.checkBox_BB    ],
            ["fix_CC"     ,  self.checkBox_CC    ],
            ["fix_aAA"    ,  self.checkBox_aAA   ],
            ["fix_aBB"    ,  self.checkBox_aBB   ],
            ["fix_aCC"    ,  self.checkBox_aCC   ],
            ["discard"    , self.lineEdit_discard ]
        ])
        class alignOnPoints_Parameters(Parameters):
             slots = list(par_dict.keys())
             entries= par_dict
             classes = {}
             for name in slots:
                  if "fix" in name:
                       classes[name]=int
                  else:
                       classes[name]=float
        self.parameters4AlignOnPoints =    alignOnPoints_Parameters  ()       

    def connectParameters_anglesConstraints(self):
        par_dict = dict([
            ["aAA"     ,  self.lineEdit_aAA    ],
            ["aBB"     ,  self.lineEdit_aBB    ],
            ["aCC"     ,  self.lineEdit_aCC    ],
        ])
        class anglesConstraints_Parameters(Parameters):
             slots = list(par_dict.keys())
             entries= par_dict
             classes = {}
             for name in slots:
                 classes[name]=float
        self.parameters4AnglesConstraints =  anglesConstraints_Parameters()       
        
    def set_q3d(self, q3d, whichiswhich):
        self.q3d = q3d
        x, y, z = np.array(q3d).T
        self.whichiswhich = whichiswhich
        self.setPoints(  x, y, z )
        self.myRot = np.array([[1,0,0.0],[0,1,0],[0,0,1]])
        
        wi = {}
        for i, (fn,(pos1,pos2) ) in enumerate ( whichiswhich ) :
            wi[(fn,(pos1,pos2))]= i
        self.whichiswhich_inverted = wi

    def alignByAlignementParameters(self, a_pars) :
        print(" in alignByAlignementParameters ")
        print(a_pars)

        q3d, U = geometry.get_Qfin_onlySample(  self.q3d ,   a_pars , alsoU = True )
        x, y, z = np.array(q3d).T
        self.setPoints(  x, y, z , keepvalues = True)
        self.myRot = U
         
    def alignOnDisplayedAxis(self):
        assi, norms = self.getTaggedAxis()
        print( " QUA ")
        invStars, anglesStar, myBravais, abc, anglesReal = geometry.getDescriptionForAxis_fromReciprocalAxis(  assi, norms ) 
        print( " QUI ", abc,anglesReal )

        if None in abc or None in anglesReal:
            msgBox = qt.QMessageBox(None)
            msgBox.setText(" Incomplete Configuration " )
            msgBox.setInformativeText(" You need to tagg more axis ( blue for C, green for b and red for a")
            msgBox.setStandardButtons(  qt. QMessageBox.Ok)
            msgBox.setDefaultButton( qt.QMessageBox.Ok)
            ret = msgBox.exec_()
            return 
        
        bravais, brillouin = geometry.getCellVectors( abc,  anglesReal  ) 

        # questa matrice trasforma gli assi ideali in quelli attuali
        # E' quindi la matrice che da l'orientazione degli assi attuali
           
        rotSample = np.dot(   myBravais.T, np.linalg.inv(  bravais.T    ) )
        # self.setRot(rotSample)

        print(" LA ROTSAMPLE EST ", rotSample)

        quats  = geometry.M2quat( rotSample)
        print(" quats ", quats)
        print("DI RITORNO EST ",  geometry.quat2M(  quats[0], quats[1], quats[2] ) )

    
        angles = geometry.M2r   ( rotSample)
        
        #  angles, quats = geometry.Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat( rotSample)



        
        print( " I QUATS CHE EMETTO SONO ", quats)
        self.parsFromVisual_signal.emit(  (angles, quats) )

    def setRot(self, rotSample):
        widget = self.scenewindow.getSceneWidget()
        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)
        scatter_mask = widget.getSceneGroup().getItems()[1]
        assert isinstance(scatter_mask, items.Scatter3D)

        # xData = scatter.getXData(copy=False).ravel()
        # yData = scatter.getYData(copy=False).ravel()
        # zData = scatter.getZData(copy=False).ravel()
        values = scatter.getValues(copy=False).ravel()
        # values = np.zeros(len(xData), dtype=np.float32)

        self.myRot = rotSample
        # x,y,z =  np.dot( np.linalg.inv(self.myRot) ,  np.array([self.oX,self.oY,self.oZ])  )
        x,y,z =  np.dot( np.linalg.inv(self.myRot) ,  self.q3d.T  )
        
        scatter.setData(x, y, z, values)
        scatter_mask.setData(x, y, z, values)
        self.showAxis()

    def setPoints    (self,x,y,z ,  keepvalues = False  ):
        
        sceneWidget = self.scenewindow.getSceneWidget()
        
        scatter = sceneWidget.getSceneGroup().getItems()[0]
        
        if not keepvalues:
            values = np.zeros_like(x)
        else:
            values = scatter.getValues(copy=False).ravel()
        
        sceneWidget.clearItems()

        # self.oX,self.oY, self.oZ = x,y,z
        # self.rotSample = rotSample
        # x,y,z =  np.tensordot(  np.array([x,y,z]).T, np.linalg.inv(self.rotSample), axes=([-1],[-1])  ).T
        # x,y,z =  np.tensordot(  np.array([self.oX,self.oY,self.oZ]).T, np.linalg.inv(self.rotSample), axes=([-1],[-1])  ).T

        scatter3d = sceneWidget.add3DScatter(x, y, z, values)
        
        scatter3d.getColormap().setColormapLUT((
            (0., 0., 0.,1), (0., 0., 0.,1)))
 
        scatter3d.setSymbol('o')
        scatter3d.setSymbolSize(11)

        scatter3d_mask = sceneWidget.add3DScatter(x, y, z, values)
        sceneWidget.centerScene()

        
        scatter3d_mask.getColormap().setColormapLUT((
            (0., 0., 0., 0.), (1., 0., 1., 1), (1., 0., 0., 1), (0., 1., 0., 1), (0, 0, 1.0, 1) ,  (1, 1, 0.0, 1)   ))
        scatter3d_mask.getColormap().setVRange(0, 5)
        scatter3d_mask.setSymbol('o')
        scatter3d_mask.setSymbolSize(22)

    def eventFilter(self, watched, event):
        if (event.type() == qt.QEvent.MouseButtonPress and
                event.button() == qt.Qt.LeftButton):
            self._clicked = True
        elif event.type() == qt.QEvent.MouseMove:
            self._clicked = False
        elif (event.type() == qt.QEvent.MouseButtonRelease and
                event.button() == qt.Qt.LeftButton and self._clicked):
            self._clicked = False
            self.clicked(watched, event.x(), event.y())
        elif  (event.type() == qt.QEvent.KeyPress) :
            print(" key pressed ", event.key()    )
            print(" key pressed ", event.text()=="a"    )
            # print(" key pressed ", qt.QKeyEvent(event).text()[0][1]    )
        return False
    
    ###    @staticmethod
    def _windowToScene(self, viewport, x, y, ndcZ=0.):
        ndc = viewport.windowToNdc(x, y)
        if ndc is None:
            return None
        else:
            ndc = np.array((ndc[0], ndc[1], ndcZ, 1.), dtype=np.float32)
            camPos = viewport.camera.intrinsic.transformPoint(
                ndc, direct=False, perspectiveDivide=True)
            scenePos = viewport.camera.extrinsic.transformPoint(
                camPos, direct=False)
            return scenePos

    def clicked(self, widget, x, y):
        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)
        
        scatter_mask = widget.getSceneGroup().getItems()[1]
        assert isinstance(scatter_mask, items.Scatter3D)

        xData = scatter.getXData(copy=False).ravel()
        yData = scatter.getYData(copy=False).ravel()
        zData = scatter.getZData(copy=False).ravel()
        values = scatter.getValues(copy=False).ravel()
        # values = np.zeros(len(xData), dtype=np.float32)

        viewport = widget.viewport
        ndcZ = viewport._pickNdcZGL(x, y)
        print("ndcZ", ndcZ)
        if ndcZ != 1:
            pickedPos = self._windowToScene(viewport, x, y, ndcZ)[:3]
            points = np.array((xData, yData, zData)).T
            sqDist = np.sum((points - pickedPos)**2, axis=1)
            index = np.argmin(sqDist)
            print('Picked point', index, points[index])
            corner = self.active_corner()
            if self.whichiswhich is not None and index<len(   self.whichiswhich  ):
                if corner == 0 :
                    print(" da qui, GL, emetto, ", self.whichiswhich[index]  ) 
                    self.selected_point_signal.emit( self.whichiswhich[index]  )
            else:
                print(" whichiswhich is None")
            # values[index] = self.icount
            # self.icount += 1
            print(" corner ist ", corner)
            if corner:
                if values[index] == corner :
                    values[index] = 0
                else:
                    dove = np.where(np.equal(values, corner) )
                    if len(dove[0])> 1:
                        old = self.last_axisPoint_clicked
                        if old is not None and old in dove[0]:
                            values[old]=0
                    values[index] = corner
                    self.last_axisPoint_clicked = index
        scatter.setData(xData, yData, zData, values)
        scatter_mask.setData(xData, yData, zData, values)
        self.showAxis()
        
    def selectPoint(self, imageName, pos   ):
        key = imageName, tuple(pos)
        print(" IN SELECTPOINT ", key)
        if key in self.whichiswhich_inverted:
            index = self.whichiswhich_inverted[key]
        else:
            print( " NON CE ")
            return
        
        widget = self.scenewindow.getSceneWidget()
        
        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)        
        scatter_mask = widget.getSceneGroup().getItems()[1]
        assert isinstance(scatter_mask, items.Scatter3D)

        
        xData = scatter.getXData(copy=False).ravel()
        yData = scatter.getYData(copy=False).ravel()
        zData = scatter.getZData(copy=False).ravel()
        values = scatter.getValues(copy=False).ravel()
        
        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)
        scatter_mask = widget.getSceneGroup().getItems()[1]
        assert isinstance(scatter_mask, items.Scatter3D)

        values[ values==5] = 0
        values[index] = 5
        
        scatter.setData(xData, yData, zData, values)
        scatter_mask.setData(xData, yData, zData, values)
       
        
            
    def getTaggedAxis(self):
        widget = self.scenewindow.getSceneWidget()

        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)
        
        # xData = scatter.getXData(copy=False).ravel()
        # yData = scatter.getYData(copy=False).ravel()
        # zData = scatter.getZData(copy=False).ravel()

        xData, yData, zData = self.q3d.T
        
        # xData = self.oX
        # yData = self.oY
        # zData = self.oZ

        values = scatter.getValues(copy=False).ravel()

        # pos=[None]*4

        selected_axis = [None for i in range(3)]
        
        for corner in range(2,5):
            indexs = np.where(np.equal(values, corner))[0]
            if len(indexs)==2:
                p1p2  = [  np.array(  [  xData[indexs[i] ],    yData[indexs[i] ],   zData [indexs[i] ] ] ) for i in range(2) ] 
                selected_axis[corner-2]  = p1p2
                # pos[corner-1] = np.array(  [  xData[indexs[0] ],    yData[indexs[0] ],   zData [indexs[0] ] ] )

        assi=[None]*3
        norms=[None]*3
        
        for ax in [0,1,2]:
            if selected_axis[ax] is not None:
                aa = selected_axis[ax]
                assi [ax] = aa[1]-aa[0]
                norms[ax] = np.linalg.norm(assi[ax])

        for i in range(3):
            if assi[i] is None:
                return assi, norms
        else:
            if np.linalg.det(assi) < 0 :
                assi[0] = -assi[0]
            return assi, norms
                


            
    def showAxis(self):
        assi, norms = self.getTaggedAxis()
        print(" ASSI ", assi )
        s = self.getTextDescriptionForAxis( assi, norms)
        self.textBrowser.setText(s)

        
    def getTextDescriptionForAxis( self, assi, norms):

        print( "3 QUI ")
        invStars, anglesStar, myBravais, abc, anglesReal = geometry.getDescriptionForAxis_fromReciprocalAxis(  assi, norms ) 
        print ("  ritornato " , invStars, anglesStar, myBravais, abc, anglesReal ) 
        s=""
        # s="Inverse of periods in  reciprocal space\n"
        for i,d in enumerate(invStars):
            if d is None: continue
            s=s+"%s : %f \n"%(  ["1/AA*","1/BB*","1/CC*"][i], d)
            
        for i,a in enumerate( anglesStar ):
            if a is None: continue
            s=s+"%s : %f \n"%(  ["aAA*","aBB*","aCC*"][i], a)
                
        for i,d in enumerate(abc):
            if d is None: continue
            s=s+"%s : %f \n"%(  ["AA","BB","CC"][i], d)

        for i,a in enumerate( anglesReal ):
            if a is None: continue
            s=s+"%s : %f \n"%(  ["aAA","aBB","aCC"][i], a)
        print( s)
        return s
    
    def active_corner(self):
        choosed = np.where( [self.radioButton_None.isChecked(),
                             #self.radioButton_center.isChecked(),
                             0,
                             self.radioButton_a.isChecked(),
                             self.radioButton_b.isChecked(),
                             self.radioButton_c.isChecked()]   )[0]
        print ( " choosed is ", choosed)
        if len(choosed)==0:
            res=0
        else:
            res=choosed[0]
            
        return res



scelteAllGeo = {}
    
@ui.UILoadable
class fitAllGeo_hkl(qt.QDialog):
    selected_pars_signal = qt.pyqtSignal(object)
    def __init__(self  ):
        super( fitAllGeo_hkl  , self).__init__(None)
        self.loadUi()       
        self.setModal(True)
        self.pushButton_fit.clicked.connect(self.prepare_fit)
        self.fillPreselected()


        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()



        add_items = list(geometry.HOLDER.variables) + list(geometry.DETECTOR.variables)
        add_items.remove("phi")
        nadd = len(add_items)
        
        for i,tok in enumerate(add_items) :
            self.layout().addWidget( qt.QCheckBox( tok ) ,     nrow+ (i//ncol) ,i%(ncol))
        

    def keyPressEvent(self, evt):
        if(evt.key() == qt.Qt.Key_Enter or evt.key() == qt.Qt.Key_Return):
            return;
        qt.QDialog.keyPressEvent(self,evt)



        
    def prepare_fit(self):
        global scelteAllGeo
        par_dict = {}
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
          for  icol in range(nrow):
            cell = self.layout().itemAtPosition( irow , icol )
            if cell is not None:
                tok = cell.widget()
                if  isinstance(   tok , qt.QCheckBox ) :
                    name  = str(tok.text())
                    value =  tok.isChecked()
                    par_dict[ name ] = value
                    scelteAllGeo[name]    = value
        print(" QUI SCELTE EST " , scelteAllGeo)
        self.selected_pars_signal.emit( par_dict  )

        
    def fillPreselected(self):
        global scelteAllGeo
        print(" QUA SCELTE EST " , scelteAllGeo)
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
          for  icol in range(nrow):
              
            cell = self.layout().itemAtPosition(irow,icol)
            if cell is not None:
                tok = cell.widget()
                if  isinstance(   tok , qt.QCheckBox ) :
                    name  = str(tok.text())
                    if name in scelteAllGeo:
                        tok.setChecked(   scelteAllGeo[  name   ]   )


        
### Perspective/Ortho ###
import weakref
from silx.gui.plot3d.actions import Plot3DAction
class ProjectionAction(Plot3DAction):
    def __init__(self, parent, projection):
        self._projection = projection.lower()
        super(ProjectionAction, self).__init__(parent)
        self.setCheckable(True)
        self.setText(self._projection.capitalize())
        self.setToolTip('Use %s projection' % self._projection)
        self.toggled.connect(self._toggled)
    def setPlot3DWidget(self, widget):
        if widget is not None:
            self.setChecked(widget.getProjection() == self._projection)
        super(ProjectionAction, self).setPlot3DWidget(widget)
    def _toggled(self, checked=False):
        plot3d = self.getPlot3DWidget()
        if plot3d is not None and checked:
            plot3d.setProjection(self._projection)
class ProjectionActionGroup(qt.QActionGroup):
    def __init__(self, parent):
        super(ProjectionActionGroup, self).__init__(parent)
        self.setExclusive(True)
        self.addAction(ProjectionAction(parent, 'perspective'))
        self.addAction(ProjectionAction(parent, 'orthographic'))
    def setPlot3DWidget(self, widget):
        for action in self.actions():
            action.setPlot3DWidget(widget)
class ProjectionToolButton(qt.QToolButton):
    def __init__(self, parent=None):
        super(ProjectionToolButton, self).__init__(parent)
        self._plot3dRef = None
        self._actionGroup = ProjectionActionGroup(self)
        self._actionGroup.triggered.connect(self._triggered)
        menu = qt.QMenu(self)
        for action in self._actionGroup.actions():
            menu.addAction(action)
        self.setMenu(menu)
        self.setPopupMode(qt.QToolButton.InstantPopup)
        self.setText('Projection')
        self.setToolTip('Set camera projection')
    def _triggered(self, action):
        self.setText(action._projection.capitalize())
    def setPlot3DWidget(self, widget):
        self._plot3DRef = None if widget is None else weakref.ref(widget)
        self._actionGroup.setPlot3DWidget(widget)
        checked = self._actionGroup.checkedAction()
        if checked:
            self._triggered(checked)
    def getPlot3DWidget(self):
        return None if self._plot3DRef is None else self._plot3DRef()
### ###

if hasattr(qt.Qt, 'AA_ShareOpenGLContexts'):
    print('set AA_ShareOpenGLContexts')
    qt.QCoreApplication.setAttribute(qt.Qt.AA_ShareOpenGLContexts)

### Perspective/Ortho ###




separator = '-' * 80
def excepthook(type, value, tracebackobj):
    print( " HOOK ")
    tbinfofile = StringIO()
    traceback.print_tb(tracebackobj, None, tbinfofile)
    # traceback.print_exc()
    # traceback.print_stack(  tracebackobj , file=tbinfofile)

    
    tbinfofile.seek(0)
    tbinfo = tbinfofile.read()
    errmsg = '%s: %s' % (str(type), str(value))
    sections = [separator, errmsg, separator, tbinfo]
    msg = '\n'.join(sections)
    msgBox = qt.QMessageBox(None)
    msgBox.setText("An exception Occurred")
    msgBox.setInformativeText(msg)
    msgBox.setStandardButtons(  qt. QMessageBox.Ok)
    msgBox.setDefaultButton( qt.QMessageBox.Ok)
    ret = msgBox.exec_()
    return



    
if __name__ =="__main__":
        TEST = 1
    
        app=qt.QApplication([])
        w = GLwidget()
        
        points = np.loadtxt(sys.argv[1])
        x, y, z = points.T
        w.setPoints(  x, y, z )
        
        w.show()
        app.exec_()

              
