#!/usr/bin/env bash

echo " IN BUILD.SH "
echo $PWD
echo $PREFIX


if [[ "${target_platform}" == linux-* ]]; then
    echo " IN LINUX "
else
    echo " NOT IN LINUX "
    echo ${target_platform}
fi


if [[ "${target_platform}" != "${build_platform}" ]]; then
    echo " ${target_platform}   different from   ${build_platform}"
#    CMAKE_ARGS="${CMAKE_ARGS} -DProtobuf_PROTOC_EXECUTABLE=$BUILD_PREFIX/bin/protoc"
fi



mkdir -p build
cd build

# export CMAKE_LIBRARY_OUTPUT_DIRECTORY=${SP_DIR}


# cmake -Bbuild -H../pycord -G "Ninja" -DCMAKE_FIND_ROOT_PATH=$PREFIX -DCMAKE_INSTALL_PREFIX=/dev/null  -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=ONLY -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=ONLY

cmake -Bbuild -H../tds2el2/tds2el_c -G "Ninja" -DCMAKE_FIND_ROOT_PATH=$PREFIX -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=${SP_DIR}/${PNAME} -DCMAKE_INSTALL_PREFIX=.  -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=ONLY -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=ONLY -DPYTHON_LIBRARY=${PREFIX}/lib/libpython${PY_VER}.so -DPYTHON_INCLUDE_DIR=${PREFIX}/include/python${PY_VER}/  -DPYTHON_EXECUTABLE=${PREFIX}/bin/python
# -DLIBRARY_OUTPUT_PATH=${SP_DIR}


cmake --build build --target install


cd ..

${PYTHON} -m pip install git+https://gitlab.esrf.fr/mirone/ab2tds.git/

${PYTHON} -m pip install . --no-deps --ignore-installed -vv 
