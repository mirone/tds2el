#!/usr/bin/env python
# -*- coding: utf-8 -*-

#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL2
# Author : 
#             Alessandro Mirone
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import glob
import math
import sys
if(sys.argv[0][-12:]!="sphinx-build"):
    from . import spotpicker_cy

from . import geometry

import h5py

import os
import scipy
import pickle
import multiprocessing
import threading
import h5py 
import copy
# import fftw3f
import pyfftw
 


from math import pi, sin, cos
try :
    import fabio
except :
    print( 'Warning : fabio module could not be initialised')

import numpy as np
import sys, time
import scipy.ndimage as ndi
from . import spotpicker_cy


from threading import Semaphore

class Barrier:
    def __init__(self, n):
        self.n = n
        self.count = 0
        self.mutex = Semaphore(1)
        self.barrier = Semaphore(0)

    def wait(self):
        self.mutex.acquire()
        self.count = self.count + 1
        self.mutex.release()
        if self.count == self.n: self.barrier.release()
        self.barrier.acquire()
        self.barrier.release()



my_fft_plans ={}
my_lock = threading.Lock()


def mylocalmaxs(image ,  min_dist=1 , tabs=None, trel=None , nobord=False ):
    size = 2 * min_dist + 1
    image_max  = np.array(image,"f")
    tmp0 = np.array(image,"f")
    spotpicker_cy.window_maximum(  image_max, tmp0, 0, min_dist  ) 
    spotpicker_cy.window_maximum(  tmp0, image_max, 1, min_dist  )
    spotpicker_cy.window_maximum(  image_max, tmp0, 2, min_dist  ) 
    mask = image == image_max
    if nobord:
        mask[:nobord] = mask[-nobord:] = False
    thresholds = []
    if tabs is None:
        tabs = image.min()
    thresholds.append(tabs)
    if trel is not None:
        thresholds.append(trel * image.max())
    if thresholds:
        mask &= image > max(thresholds)
    coord = np.nonzero(mask)
    coord = np.column_stack(coord)
    return coord

def get_fft_plans( dim1_dim2_myrank , badradius  ):
    dim1,dim2,myrank = dim1_dim2_myrank
    my_lock.acquire()
    # print(" ACQUIRED")
    if (dim1,dim2, myrank) not in my_fft_plans:

        DIM1=1
        while DIM1<dim1*2:
            DIM1*=2
        DIM2=1
        while DIM2<dim2*2:
            DIM2*=2

        ry = np.fft.fftfreq(DIM1,1.0/DIM1) .astype("f")
        rx = np.fft.fftfreq(DIM2,1.0/DIM2) .astype("f")


        fftw_direct =  pyfftw.empty_aligned(  (len(ry), len(rx) )    , dtype='complex64')
        fftw_rec    =  pyfftw.empty_aligned(  (len(ry), len(rx) )    , dtype='complex64')
        
        # fftw_direct=  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")
        # fftw_rec   =  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")


        fft_plan_F  =  pyfftw.builders. fft2( fftw_direct , threads=1)
        fft_plan_B  =  pyfftw.builders.ifft2( fftw_rec , threads=1)

        
        # fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['measure'])
        # fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['measure'])

        disco = (ry*ry)[:,None] + (rx*rx)
        disco = (np.less( disco, badradius* badradius   )).astype("f")

        fftw_direct[:] = disco
        disco_reci = np.array( fft_plan_F())

        my_fft_plans[ (dim1,dim2, myrank)  ] = fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec 

    else:
        fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec = my_fft_plans[ (dim1,dim2, myrank)  ]
    # print(" RELEASE")
    my_lock.release() 

    return fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec




def get_Filter(  Filter_fname, dim1, dim2) :

    if Filter_fname not in  [ None, "None" ]:
        print( 'Loading images filter ...')
        Filter = fabio.open(Filter_fname  ).data.astype("f")
        assert( Filter.shape ==  (dim1,dim2) )
    else:
        Filter = np.ones( (dim1,dim2)  ,"f")

    return Filter




def build_review_stuff(preRes, review_size, dim1, dim2, nblockfl, myrank, nprocs,  flist_alt )  :    
    nimag_lut     = None
    nimag_lut_alt = None

    nimag_lut_alt = None
    inset_template      = None
    inset_template_alt  = None


    inset_template = np.zeros([2*review_size+dim1, 2*review_size+dim2],"f")
    inset_template [:]  = np.nan
    
    if flist_alt is not None:
        inset_template_alt = np.zeros([2*review_size+dim1, 2*review_size+dim2],"f")
        inset_template_alt [:]  = np.nan
        
    
    if preRes is not None:
        
        print(" Configuro LUT ")
        
        nimag_lut = {}
        
        if flist_alt is not None:
            nimag_lut_alt = {}
            inset_template_alt = np.zeros([2*review_size+dim1, 2*review_size+dim2],"f")
            inset_template_alt [:]  = np.nan
        
        for imag, imag_dict in preRes.items():
            for pos, pos_dict in imag_dict.items():
                
                giplan = pos_dict["Gi"]
                if pos_dict["accepted"]:
                    giplan = pos_dict["Gi"]
                    if (giplan//nblockfl)%nprocs != myrank:
                        continue
                    if giplan not in nimag_lut:
                        nimag_lut[giplan] = {}
                    nimag_lut[giplan]  [pos] = np.zeros([2*review_size+1]*3,"f")
                    nimag_lut[giplan]  [pos][:] = np.nan
                    if flist_alt is not None:
                        if giplan not in nimag_lut_alt:
                            nimag_lut_alt[giplan] = {}
                        nimag_lut_alt[giplan]  [pos] = np.zeros([2*review_size+1]*3,"f")
                        nimag_lut_alt[giplan]  [pos][:]  = np.nan


    return nimag_lut, inset_template, nimag_lut_alt, inset_template_alt 





def fill_review_inluts(newim, iim, nimag_lut , newim_alt, nimag_lut_alt,inset_template,inset_template_alt, review_size,
                               Filter,
                               threshold_bragg,
                               badradius,
                               fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec, nblockfl, myrank, nprocs):


    dim1, dim2 = newim.shape
    if nimag_lut is not None:
        if( len(list(nimag_lut.keys())) ):

            newim4view = np.array( newim )
            newim4view[ Filter==0 ] = np.nan
            newim4view[ newim < 0 ]  = np.nan

            if threshold_bragg is not None:
                strongs = np.less( threshold_bragg  ,newim4view)
                ssum = strongs.sum() 

                if badradius is not None   and ssum :
                    if badradius>0.0:
                        isbragg=1

                        # fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec = my_fft_plans[ (dim1,dim2)  ]

                        fftw_direct[:]=0.0

                        fftw_direct[  : newim4view.shape[0]  ,  : newim4view.shape[1]] =  strongs

                        fftw_rec[:] =  fft_plan_F()* disco_reci

                        res_tmp = fft_plan_B()

                        size =  newim4view.size

                        extramask = np.less(0.1, abs(  res_tmp  [  : newim4view.shape[0]  ,  : newim4view.shape[1]] )  )   # /size 

                        newim4view[extramask ] = - newim4view[extramask ]

                        # print( (np.less( -threshold_bragg, newim4view )*np.less( newim4view ,0 )).sum(), " nanned ")

                        newim4view[ np.less( -threshold_bragg, newim4view )*np.less( newim4view ,0 )]  = np.nan

                    else:
                        newim4view[: ] = -np.abs(newim4view)
                        newim4view[ newim4view > -threshold_bragg     ]  = np.nan

                elif ssum:
                    newim4view[ newim4view > threshold_bragg     ]  =  - newim4view[ newim4view > threshold_bragg     ]
            inset_template[  review_size:review_size+dim1 , review_size:review_size+dim2 ] = newim4view


            if nimag_lut_alt is not None:
                inset_template_alt[review_size:review_size+dim1 , review_size:review_size+dim2 ] = newim_alt
                inset_template_alt[  review_size:review_size+dim1 , review_size:review_size+dim2 ][ Filter==0 ]  = np.nan
                inset_template_alt[  review_size:review_size+dim1 , review_size:review_size+dim2 ][ newim_alt < 0 ]  = np.nan


        for ilut in nimag_lut.keys():
            if (ilut//nblockfl)%nprocs != myrank:
                continue
            if abs(iim-ilut)<=review_size:
                for pos in nimag_lut[ilut].keys():
                    inset = inset_template[review_size+pos[0]-review_size:review_size+pos[0]+review_size+1,
                                           review_size+pos[1]-review_size:review_size+pos[1]+review_size+1]

                    nimag_lut[ilut][pos][review_size +(iim-ilut)] =  inset
                    if nimag_lut_alt is not None:                            
                        inset = inset_template_alt[review_size+pos[0]-review_size:review_size+pos[0]+review_size+1,
                                          review_size+pos[1]-review_size:review_size+pos[1]+review_size+1]
                        nimag_lut_alt[ilut][pos][review_size +(iim-ilut)] =  inset




def mainRead(Filter_fname, normalisation,   threshold_bragg,  badradius, flist,flist_alt,
             t_abs, t_rel, binning, peak_size ,review_size, spot_view_size,  medianize, globalize, myrank, nprocs,res, preRes=None,Barrier = None):

    
    master_img = fabio.open(flist[0])  # usiamo questo anche dopo se siamo nel caso h5
    if len(flist) == 1:
        ish5 = True
        nimages = master_img.nframes
        if flist_alt is not None:
            has_alt = true
            master_alt_img = fabio.open(flist_alt[0])  # usiamo questo anche dopo se siamo nel caso h5
    else:
        ish5 = False
        nimages = len(flist)
        if flist_alt is not None:
            has_alt = true
            
        
    DIM1orig, DIM2orig = master_img.data.shape

    data = master_img.data[::binning, ::binning]
    dim1, dim2 = data.shape

    if badradius >0.0:
        fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec = get_fft_plans( (dim1,dim2, myrank), badradius  )
    else:    
        fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec = [None]*5
    Filter = get_Filter(  Filter_fname, dim1, dim2) 

    nblockfl = nimages//nprocs+1

    ## ci sara del lavoro dento a build_review
    nimag_lut, inset_template, nimag_lut_alt, inset_template_alt = build_review_stuff(preRes, review_size, dim1, dim2, nblockfl, myrank, nprocs, flist_alt )
                        
    count = 0 
    count_median = 0
    
    Nring = 2*spot_view_size+1
    middle = spot_view_size


    marge = Nring-middle
    tutti = []

    #######

    if preRes is None:
        # ci  sara del lavoro anche dntro a questa funzione
        find_maxima( flist, flist_alt, res,  peak_size,  threshold_bragg,  spot_view_size, myrank,  nprocs, medianize, t_abs, t_rel, Filter, binning)
        Barrier.wait()

    nblockfl = nimages//nprocs+1
    
    for iim in range(nimages):

    # , fname in enumerate(flist[:]):

        if flist_alt is not None:
            fname_alt = flist_alt[iim]

        marge_filling = marge
        if nimag_lut is not None:
            marge_filling = review_size
            
        if ((iim//nblockfl)%nprocs != myrank)  and   ( ((iim-marge_filling)//nblockfl )  %nprocs != myrank    )  and   ( ((iim+marge_filling) //nblockfl )   %nprocs != myrank    )   :
            continue


        if not ish5:
            fname = flist[iim]
            if normalisation is None:
                newim = (fabio.open(fname).data.astype(np.float32)*Filter)[::binning, ::binning]
                if flist_alt is not None:
                    newim_alt = fabio.open(fname_alt).data.astype(np.float32)*Filter
                else:
                    newim_alt = None
            else:
                nf = normalisation[ fname  ]
                newim = (( fabio.open(fname).data/nf).astype(np.float32)*Filter)[::binning, ::binning]
                if flist_alt is not None:
                    nf = normalisation[ fname_alt ]
                    newim_alt = (fabio.open(fname_alt).data/nf).astype(np.float32)*Filter
                else:
                    newim_alt = None
        else:
            if normalisation is None:
                newim = (master_img.get_frame(iim).data.astype(np.float32)*Filter)[::binning, ::binning]
                if flist_alt is not None:
                    newim_alt = master_alt_img.get_frame(iim).data.astype(np.float32)*Filter
                else:
                    newim_alt = None
            else:
                nf = normalisation[ iim  ]
                newim = ((master_img.get_frame(iim).data/nf).astype(np.float32)*Filter)[::binning, ::binning]
                if flist_alt is not None:
                    nf = normalisation[ fname_alt ]
                    newim_alt = (master_alt_img.get_frame(iim).data/nf).astype(np.float32)*Filter
                else:
                    newim_alt = None
            
                    

            

        if myrank == 0:
            if not ish5:
                print( 'Working on image ', flist[iim] , " MIN MAX " , newim.min(), newim.max()  )
            else:
                print( 'Working on frame ', iim , " MIN MAX " , newim.min(), newim.max())  
        if preRes is  not None:
            fill_review_inluts(newim, iim, nimag_lut , newim_alt, nimag_lut_alt,inset_template,inset_template_alt,  review_size, 
                               Filter,
                               threshold_bragg,
                               badradius,
                               fft_plan_F, fft_plan_B, disco_reci , fftw_direct, fftw_rec, nblockfl, myrank, nprocs
            )

                    
        if ((iim//nblockfl)%nprocs != myrank)  and   ( ((iim-marge)//nblockfl )  %nprocs != myrank    )  and   ( ((iim+marge) //nblockfl )   %nprocs != myrank    )   :
            continue



        if preRes is  None:

            if not ish5:
                print( 'Working on image ', flist[iim] , " MIN MAX " , newim.min(), newim.max()  )
            else:
                print( 'Working on frame ', iim , " MIN MAX " , newim.min(), newim.max()  )

        
            fill_spotviewsize_inres(newim, newim_alt,  res , iim, spot_view_size,
                                    Filter,inset_template,inset_template_alt, review_size,
                                    myrank, nprocs, nblockfl
            )
        


    for imname, im_dict in res.items():
        for pos, pos_dict in im_dict.items():
            pos_dict["review"] = None
            pos_dict["review_alt"] = None
            if pos_dict["accepted"] :
                Gi = pos_dict["Gi"]
                if nimag_lut is not None and  Gi in  nimag_lut:
                    if tuple(pos) in  nimag_lut[Gi]:
                        pos_dict["harvest"] =  nimag_lut[Gi][pos]
                        if nimag_lut_alt is not None:
                            pos_dict["harvest_alt"] =  nimag_lut_alt[Gi][pos]
                            
    return res


def fill_spotviewsize_inres(newim, newim_alt,  res , iim, spot_view_size,
                            Filter,inset_template,inset_template_alt, review_size,
                            myrank, nprocs, nblockfl
     ):


    dim1,dim2 = newim.shape
    if( len(res) ):

        newim4view = np.array( newim )
        newim4view[ Filter==0 ] = 0
        newim4view[ newim < 0 ]  = 0

        if newim_alt is not None:
            
            newim4view_alt              = np.array( newim )
            newim4view_alt[ Filter==0 ] = 0
            newim4view_alt[ newim < 0 ] = 0
            
        inset_template[  review_size:review_size+dim1 , review_size:review_size+dim2 ] = newim4view


        if inset_template_alt is not None:
            inset_template_alt[review_size:review_size+dim1 , review_size:review_size+dim2 ] = newim_alt
            inset_template_alt[  review_size:review_size+dim1 , review_size:review_size+dim2 ][ Filter==0 ]  = 0
            inset_template_alt[  review_size:review_size+dim1 , review_size:review_size+dim2 ][ newim_alt < 0 ]  = 0


    for nimage in res:
        for pos in res[nimage]:
            Giplan = res[nimage][pos]["Gi"]
        
            if (Giplan//nblockfl)%nprocs != myrank:
                continue
            if abs(iim-Giplan)<=spot_view_size:


                pmap, pmap_alt = res[nimage][pos]["pmap"]
                
                inset = inset_template[review_size+pos[0]-spot_view_size:review_size+pos[0]+spot_view_size+1,
                                       review_size+pos[1]-spot_view_size:review_size+pos[1]+spot_view_size+1]

                    
                pmap[spot_view_size +(iim-Giplan)] =  inset
                if pmap_alt is not None:                            
                    inset = inset_template_alt[review_size+pos[0]-spot_view_size:review_size+pos[0]+spot_view_size+1,
                                      review_size+pos[1]-spot_view_size:review_size+pos[1]+spot_view_size+1]

                    # if not isinstance( res[nimage][pos]["pmap"] , np.ndarray) :
                    #     res[nimage][pos]["pmap_alt"] = np.zeros([ 2*spot_view_size+1  ]*3,"f")
                    
                    pmap_alt[spot_view_size +(iim-Giplan)] =  inset





    


def find_maxima( flist, flist_alt,  res,  peak_size, threshold,    spot_view_size, myrank,  nprocs, medianize, t_abs, t_rel, Filter, binning)     :

    master_img = fabio.open(flist[0])  # usiamo questo anche dopo se siamo nel caso h5
    if len(flist) == 1:
        ish5 = True
        nimages = master_img.nframes
        if flist_alt is not None:
            has_alt = true
            master_alt_img = fabio.open(flist_alt[0])  # usiamo questo anche dopo se siamo nel caso h5
    else:
        ish5 = False
        nimages = len(flist)
        if flist_alt is not None:
            has_alt = true
            
        
    DIM1orig, DIM2orig = master_img.data.shape

    data = master_img.data[::binning, ::binning]
    dim1, dim2 = data.shape 
    
    marge = peak_size+1

    tutti = []

    nblockfl = nimages//nprocs+1

    peak_finder_obj = spotpicker_cy.PeakFinder_cy(dim1,dim2, t_abs, peak_size )
    
    for iim in range(nimages): #, fname in enumerate(flist[:]):
        
        if not ish5:
            fname=flist[iim]
            print( fname)
        else:
            print('working on frame', iim)

        if ((iim//nblockfl)%nprocs != myrank)  and   ( ((iim-marge)//nblockfl )  %nprocs != myrank    )  and   ( ((iim+marge) //nblockfl )   %nprocs != myrank    )   :
            continue

        if not ish5:
            newim = (fabio.open(fname).data*Filter)[::binning, ::binning]
        else:
            newim = (master_img.get_frame(iim).data*Filter)[::binning, ::binning]
        newim = newim.astype(np.float32)

        if medianize:
            newim = newim -np.median(newim, axis=0) 
                            
        isattheend = int(iim == nimages - 1)

        
        picchi = peak_finder_obj.find_maxima(newim,  iim , nprocs , myrank ,  isattheend   )

        ## @@ filtra qui 
        
        picchi_again=[]
        tutti = []
        
        for i,j,k in picchi:

            newpos = np.array([iim,j,k])
            if len(tutti) and np.max(np.abs(  np.array(tutti)-newpos), axis=1).min()< peak_size*2:
                continue
            tutti.append([iim,j,k])            
            picchi_again.append( (i,j,k)  )

        picchi = np.array(picchi_again)
            
        if len(picchi) :

            tok = {}

            for pos  in ( picchi ).tolist()  :


                iplan =    min(nimages-1, max(0, int(round(  pos[0]  )) )   )
                posy  =    min(dim1-1, max(0, int(round(  pos[1]*binning  )) )   )
                posx  =    min(dim2-1, max(0, int(round(  pos[2]*binning  )) )   )

                if (iplan//nblockfl)%nprocs != myrank :
                    continue
                
                pmap = np.zeros([2*spot_view_size+1]*3,"f")
                if flist_alt is not None:
                    pmap_alt = np.zeros([2*spot_view_size+1]*3,"f")
                else:
                    pmap_alt = None

                tok[tuple((posy, posx) )] = { "pos":(posy, posx), "Gi":iplan ,  "pmap":[pmap, pmap_alt] , "accepted":True, "mask":None } 

            if not ish5:
                res[flist[iim]]=tok
            else:
                res[iim]=tok

    return res

class myThread (threading.Thread):
   def __init__(self, params, myid , ncores, result, preRes=None, Barrier = None ):
      threading.Thread.__init__(self)
      self.myid = myid
      self.ncores = ncores
      self.params = params
      self.result = result
      self.preRes=preRes
      self.Barrier = Barrier
      
   def run(self):
       
      print( "Starting thread " , self. myid)

      params = self.params
      res=mainRead( params.filter_file, params.normalisation,  params.threshold_bragg,  params.badradius,
                    params.flist,params.flist_alt,  params.threshold_abs, params.threshold_rel, params.bin ,
                    params.peak_size, params.review_size , params.spot_view_size, params.medianize, params.globalize,  self.myid , self.ncores, self.result,
                    preRes=self.preRes, Barrier = self.Barrier)
      
      print( "Exiting thread " + str(self.myid))

def inserisci(target, pos, source):
    d = (source.shape[0]-1)//2

    starty = pos[0]-d
    startx = pos[1]-d

    starty_clip = max(0, starty)
    startx_clip = max(0, startx)
    
    endy = pos[0]+d+1
    endx = pos[1]+d+1

    endy_clip = min( target.shape[0] , endy)
    endx_clip = min( target.shape[1] , endx)
     
    # target[pos[0]-d : pos[0]+d+1   , pos[1]-d : pos[1]+d+1 ]  =1
    
    target[starty_clip:endy_clip, startx_clip:endx_clip] =    source[ starty_clip-starty :  endy_clip-starty ,   startx_clip-startx :   endx_clip-startx   ]


def sort_by_numeric_part(data, prefix, postfix):
    """
    Sort a list of file paths where the numeric part is between the prefix and postfix.
    """
    def extract_numeric_value(file_name):
        try:
            # Remove prefix and postfix to isolate the numeric part
            numeric_part = file_name[len(prefix):-len(postfix)]
            # Convert the numeric part to an integer
            return int(numeric_part)
        except ValueError:
            # Handle cases where the numeric part isn't a valid integer
            return float('inf')  # Assign a large value to push invalid entries to the end

    # Sort the list using the extracted numeric value
    return sorted(data, key=extract_numeric_value)

def fillPlan(readingPars, geoPars, alignPars ,symm_ops, Origin, Xaxis, Yaxis, dQ, npointsX, npointsY , method ,  selected_peak=None ):

    checkNormalisation(readingPars)
    
    Filter_fname = readingPars.filter_file
    if not readingPars.images_prefix.endswith(".h5"):
        flist = glob.glob(readingPars.images_prefix+"*"+readingPars.images_postfix )
        flist = sort_by_numeric_part(flist, readingPars.images_prefix, readingPars.images_postfix)
    else:
        flist = [ readingPars.images_prefix ]

    master_img = fabio.open(flist[0])  # usiamo questo anche dopo se siamo nel caso h5
    if len(flist) == 1:
        ish5 = True
        nimages = master_img.nframes
    else:
        ish5 = False
        nimages = len(flist)
        
    DIM1orig, DIM2orig = master_img.data.shape

    data = master_img.data.astype("f")
    dim1, dim2 = data.shape
    data_sim = np.array(data).astype("f")
    
    if Filter_fname not in  [ None, "None" ]:
        print( 'Loading images filter ...')
        Filter = fabio.open(Filter_fname  ).data
        assert( Filter.shape ==  (dim1,dim2) )
    else:
        Filter = np.ones( (dim1,dim2)  ,"f")

    Q0, Kin , correction =geometry.get_Q0(dim1,dim2, geoPars)


    geoPars.phi = geoPars.start_phi

    tmp = geometry.HOLDER.Prim_Rot_of_RS( geoPars )

    
    # tmp = geometry.Rotation(geoPars.omega , 2)
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.alpha, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.kappa, 2))
    # tmp = np.dot(tmp, geometry.Rotation(-geoPars.alpha, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.beta, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.start_phi, 2))
    ##  tmp = np.dot(tmp, geometry.Rotation(-beta, 1))

    thisQ  = np.tensordot(   Q0, tmp.T,  axes=([-1], [1])  ) . astype("f")
    
    tmp = np.dot(tmp, geometry.Rotation(geoPars.angular_step, 2))

    nextQ  = np.tensordot(   Q0, tmp.T,  axes=([-1], [1])  ) 

    # vettori per  dY
    dq_y = np.zeros_like(thisQ)
    dq_y[:-1,:] = thisQ[1:, :]
    dq_y[:-1,:] = dq_y[:-1,:] - thisQ[:-1,:]
    dq_y[ -1,:] = dq_y[-2,:]
    
    # vettori per  dX
    dq_x = np.zeros_like(thisQ)
    dq_x[:,:-1] = thisQ[:, 1:]
    dq_x[:,:-1] = dq_x[:,:-1] - thisQ[:,:-1]
    dq_x[:, -1] = dq_x[:,-2]
    
    # vettori per  dN
    dq_n = nextQ - thisQ

    # vettori duali

    tmp_matrix_stack = np.zeros(  list(dq_x.shape)+[3]    ,"f"  )
    tmp_matrix_stack[:,:,0,:] = dq_x
    tmp_matrix_stack[:,:,1,:] = dq_y
    tmp_matrix_stack[:,:,2,:] = dq_n

    tmp_det = np.linalg.det(  tmp_matrix_stack   )
    
    dual_x =  np.ascontiguousarray(np.cross (    dq_y, dq_n    ) /tmp_det[:,:,None]).astype("f")
    dual_y =  np.ascontiguousarray(np.cross (    dq_n, dq_x    ) /tmp_det[:,:,None]).astype("f")
    dual_n =  np.ascontiguousarray(np.cross (    dq_x, dq_y    ) /tmp_det[:,:,None]).astype("f")
   
    
    # revert sample alignement
    if not hasattr(alignPars, "Quat_or_angles")  or alignPars.Quat_or_angles      == "angles":
        U = geometry.Snd_Rot_of_RS(  alignPars.r1 , alignPars.r2 , alignPars.r3 )
    else:
        U = geometry.quat2M(   alignPars.r1,  alignPars.r2,  alignPars.r3 )         

        
    result_signal = np.zeros([npointsY, npointsX], "f")
    result_sum = np.zeros([npointsY, npointsX], "f")+np.array([1.0e-32],"f")

    result_sim_signal = None
    result_sim_sum    = None
                    
    for iimage in range(nimages):# , name_image in enumerate( flist  ):

        if selected_peak is not None:
            peak = selected_peak
            Gi = peak["Gi"]

            
            pos  = peak["pos"]

            if "Blob" in peak:            
                blob = peak["Blob"]
                sim,ref = blob.simIntensity
                
                if result_sim_signal is None:
                    result_sim_signal = np.zeros([npointsY, npointsX], "f")
                    result_sim_sum    = np.zeros([npointsY, npointsX], "f")+np.array([1.0e-32],"f")
                    
            else:
                ref = peak["harvest"]
                sim = None

            if 2*abs(iimage-Gi)+1> ref.shape[0]:
                continue

        
            sliceRef =  ref[  (ref.shape[0]-1)//2 +  iimage-Gi   ]
            data[:]=np.nan
            inserisci(  data,      pos, sliceRef)
            
            if sim is not None:
                sliceSim =  sim[  (ref.shape[0]-1)//2 +  iimage-Gi   ]
                data_sim[:]=np.nan
                inserisci(  data_sim ,      pos, sliceSim)

            if not ish5:
                print (" working on ", flist[iimage])
            else:
                print (" working on frame " , iimage)
        
        else:
            if not ish5:
                print(" working on ", flist[iimage])
            else:
                print(" working on frame ", iimage)

            if readingPars.normalisation is None:
                if not ish5:
                    img = fabio.open(flist[iimage])
                else:
                    img = master_img.get_frame(iimage)
                data = (img.data/correction).astype("f")
            else:
                if not ish5:
                    nf = readingPars.normalisation[flist[iimage]]
                    img = fabio.open(flist[iimage])
                else:
                    nf = readingPars.normalisation[iimage]
                    img = master_img.get_frame(iimage)
                data = (img.data/correction/nf).astype("f")


        if selected_peak is not None:
            symm_ops_touse = symm_ops[:1]
        else:
            symm_ops_touse = symm_ops            
    
        for numop, op in enumerate(symm_ops):

            # print(" applico ", numop, op)
            Origin_off = np.dot( U, np.dot(op,Origin )  ) 
            Xaxis_off  = np.dot( U, np.dot(op,Xaxis  )  )  
            Yaxis_off  = np.dot( U, np.dot(op,Yaxis  )  )  

            # Mind that by definition the Rotation method is inverted (hence the minus sign)
            # MIND also that the following line is assuming that phi is along Z
            # But this could be changed in the yaml configuration
            # Even if there s in principle no reason to use anithing else
            # than the z axis, beware
            U_phi = geometry.Rotation(-iimage * geoPars.angular_step, 2)

            Origin_lab = np.dot( U_phi,  Origin_off   ) .astype("f")
            Xaxis_lab  = np.dot( U_phi,  Xaxis_off    ) .astype("f") 
            Yaxis_lab  = np.dot( U_phi,  Yaxis_off    ) .astype("f") 

            if method not in ["nearest", "sinc", "sinc_xyn" ] :
                raise Exception("undefined interpolation method")

            
            spotpicker_cy.fillPlan(    result_signal, result_sum, Origin_lab, Xaxis_lab,  Yaxis_lab.astype("f"), dQ,  data.astype("f"), Filter.astype("f"), thisQ, dual_x, dual_y, dual_n  , method) 
            
            if result_sim_signal is not None:
                spotpicker_cy.fillPlan(    result_sim_signal, result_sim_sum, Origin_lab, Xaxis_lab,  Yaxis_lab, dQ,  data_sim, Filter, thisQ, dual_x, dual_y, dual_n  , method) 



    result_signal[ result_sum ==0  ] =np.nan            
    if result_sim_signal is  None:
        return result_signal/result_sum
    else:
        result_sim_signal[ result_sim_sum ==0  ] =np.nan            
        return np.concatenate( [result_signal/result_sum ,   result_sim_signal/result_sim_sum], axis = 1)
      
def fillVolume(readingPars, geoPars, alignPars ,symm_ops , Origin, Xaxis, Yaxis, Zaxis, dQ, npointsX, npointsY , npointsZ, method, selected_peak=None):

    checkNormalisation(readingPars)
    
    Filter_fname = readingPars.filter_file
    if not readingPars.images_prefix.endswith(".h5"):
        flist = glob.glob(readingPars.images_prefix+"*"+readingPars.images_postfix )
        flist = sort_by_numeric_part(flist, readingPars.images_prefix, readingPars.images_postfix)
    #    flist.sort()
    else:
        flist = [  readingPars.images_prefix ]

    master_img = fabio.open(flist[0])  # usiamo questo anche dopo se siamo nel caso h5
    if len(flist) == 1:
        ish5 = True
        nimages = master_img.nframes

    else:
        ish5 = False
        nimages = len(flist)
    
    data = master_img.data.astype("f")
    data_sim = np.array(data).astype("f")
    
    dim1, dim2 = data.shape
    
    if Filter_fname not in  [ None, "None" ]:
        print( 'Loading images filter ...')
        Filter = fabio.open(Filter_fname  ).data
        assert( Filter.shape ==  (dim1,dim2) )
    else:
        Filter = np.ones( (dim1,dim2)  ,"f")

    Q0, Kin , correction =geometry.get_Q0(dim1,dim2, geoPars)


    geoPars.phi = geoPars.start_phi

    tmp = geometry.HOLDER.Prim_Rot_of_RS( geoPars ).astype("f")    

    
    # tmp = geometry.Rotation(geoPars.omega , 2)
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.alpha, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.kappa, 2))
    # tmp = np.dot(tmp, geometry.Rotation(-geoPars.alpha, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.beta, 1))
    # tmp = np.dot(tmp, geometry.Rotation(geoPars.start_phi, 2))
    #### tmp = np.dot(tmp, geometry.Rotation(-beta, 1))

    thisQ  = np.tensordot(   Q0, tmp.T,  axes=([-1], [1])  ) 
    
    tmp = np.dot(tmp, geometry.Rotation(geoPars.angular_step, 2))

    nextQ  = np.tensordot(   Q0, tmp.T,  axes=([-1], [1])  ) 

    # vettori per  dY
    dq_y = np.zeros_like(thisQ)
    dq_y[:-1,:] = thisQ[1:, :]
    dq_y[:-1,:] = dq_y[:-1,:] - thisQ[:-1,:]
    dq_y[ -1,:] = dq_y[-2,:]
    
    # vettori per  dX
    dq_x = np.zeros_like(thisQ)
    dq_x[:,:-1] = thisQ[:, 1:]
    dq_x[:,:-1] = dq_x[:,:-1] - thisQ[:,:-1]
    dq_x[:, -1] = dq_x[:,-2]
    
    # vettori per  dN
    dq_n = nextQ - thisQ

    # vettori duali

    tmp_matrix_stack = np.zeros(  list(dq_x.shape)+[3]    ,"f"  )
    tmp_matrix_stack[:,:,0,:] = dq_x
    tmp_matrix_stack[:,:,1,:] = dq_y
    tmp_matrix_stack[:,:,2,:] = dq_n

    tmp_det = np.linalg.det(  tmp_matrix_stack   )
    
    dual_x =  np.ascontiguousarray(np.cross (    dq_y, dq_n    ) /tmp_det[:,:,None])
    dual_y =  np.ascontiguousarray(np.cross (    dq_n, dq_x    ) /tmp_det[:,:,None])
    dual_n =  np.ascontiguousarray(np.cross (    dq_x, dq_y    ) /tmp_det[:,:,None])
    
    # revert sample alignement
    if not hasattr(alignPars, "Quat_or_angles")  or alignPars.Quat_or_angles      == "angles":
        U = geometry.Snd_Rot_of_RS(  alignPars.r1 , alignPars.r2 , alignPars.r3 )
    else:
        U =  geometry.quat2M(   alignPars.r1,  alignPars.r2,  alignPars.r3 )         
    
    result_signal = np.zeros([npointsZ, npointsY, npointsX], "f")
    result_sum = np.zeros([npointsZ, npointsY, npointsX], "f")+np.array([1.0e-32],"f")

    result_sim_signal = None
    result_sim_sum    = None
    
    for iimage in range(nimages):# , name_image in enumerate( flist  ):

        if selected_peak is not None:
            peak = selected_peak
            Gi = peak["Gi"]
            pos  = peak["pos"]
            if "Blob" in peak:            
                blob = peak["Blob"]
                sim,ref = blob.simIntensity
                if result_sim_signal is None:
                    result_sim_signal = np.zeros([npointsZ,npointsY, npointsX], "f")
                    result_sim_sum    = np.zeros([npointsZ,npointsY, npointsX], "f")+np.array([1.0e-32],"f")
                    
            else:
                ref = peak["harvest"]
                sim = None

            if 2*abs(iimage-Gi)+1> ref.shape[0]:
                print(" continue on " , name_image)
                continue
            
            sliceRef =  ref[  (ref.shape[0]-1)//2 +  iimage-Gi   ]
            data[:]=np.nan
            inserisci(  data,      pos, sliceRef)
            
            if sim is not None:
                sliceSim =  sim[  (ref.shape[0]-1)//2 +  iimage-Gi   ]
                data_sim[:]=np.nan
                inserisci(  data_sim ,      pos, sliceSim)
        
        else:
            if readingPars.normalisation is None:
                if not ish5:
                    img = fabio.open(flist[iimage])
                else:
                    img = master_img.get_frame(iimage)
                data = (img.data/correction).astype("f")
            else:
                if not ish5:
                    nf = readingPars.normalisation[flist[iimage]]
                    img = fabio.open(flist[iimage])
                else:
                    nf = readingPars.normalisation[iimage]
                    img = master_img.get_frame(iim)
                data = (img.data/correction/nf).astype("f")

        if not ish5:
            print(" working on " , flist[iimage])
        else:
            print(" working on frame ", iimage)

            
        if selected_peak is not None:
            symm_ops_touse = symm_ops[:1]
        else:
            symm_ops_touse = symm_ops
            
        for numop, op in enumerate(symm_ops_touse):
            
            Origin_off = np.dot( U, np.dot(op,Origin )  ) 
            Xaxis_off  = np.dot( U, np.dot(op,Xaxis  )  )  
            Yaxis_off  = np.dot( U, np.dot(op,Yaxis  )  )  
            Zaxis_off  = np.dot( U, np.dot(op,Zaxis  )  )  

            # Mind that by definition the Rotation method is inverted (hence the minus sign)
            # MIND also that the following line is assuming that phi is along Z
            # But this could be changed in the yaml configuration
            # Even if there s in principle no reason to use anithing else
            # than the z axis, beware
            U_phi = geometry.Rotation( - iimage * geoPars.angular_step, 2)
                

            Origin_lab = np.dot( U_phi,  Origin_off   ) .astype("f")
            Xaxis_lab  = np.dot( U_phi,  Xaxis_off    ) .astype("f")
            Yaxis_lab  = np.dot( U_phi,  Yaxis_off    ) .astype("f")
            Zaxis_lab  = np.dot( U_phi,  Zaxis_off    ) .astype("f")

            if method not in ["nearest", "sinc", "sinc_xyn" ] :
                raise Exception("undefined interpolation method")

                
            spotpicker_cy.fillVolume(    result_signal, result_sum, Origin_lab,Xaxis_lab,  Yaxis_lab, Zaxis_lab, float(dQ),  data, Filter.astype("f"), thisQ, dq_x, dq_y, dq_n,  dual_x, dual_y, dual_n , method ) 
            if result_sim_signal is not None:
                spotpicker_cy.fillVolume(    result_sim_signal, result_sim_sum, Origin_lab,Xaxis_lab,  Yaxis_lab, Zaxis_lab, dQ,  data_sim, Filter.astype("f"), thisQ, dq_x, dq_y, dq_n,  dual_x, dual_y, dual_n , method ) 


    result_signal[ result_sum ==0  ] =np.nan            
    if result_sim_signal is None:
        return result_signal/result_sum
    else:
        result_sim_signal[ result_sim_sum ==0  ] =np.nan            
        return  np.concatenate( [result_signal/result_sum ,  result_sim_signal/result_sim_sum], axis=2)


def checkNormalisation(params):
    params.normalisation = None
    if params.normalisation_file is not None and params.normalisation_file!="None":
        print( " reading normalisation file ")
        params.normalisation={}
        sl = open(params.normalisation_file ,"r").read().split("\n")
        for  l in sl:
            fnv = l.split()
            if len(fnv):
                fn,v = fnv
            params.normalisation[fn]=float(v)
        
    
def main(params_arg, res={} , preRes=None):
    params = copy.copy(params_arg)
    Filter_fname = params.filter_file

    if not params.images_prefix.endswith(".h5"):
        flist = glob.glob(params.images_prefix+"*"+params.images_postfix )
        flist = sort_by_numeric_part(flist, params.images_prefix, params.images_postfix)
        #flist.sort()
    else:
        flist = [  params.images_prefix ]


    
    # flist=flist[100:150]
    if params.images_prefix_alt not in [ None, "None"]:

        if not params.images_prefix_alt.endswith(".h5"):
            flist_alt = glob.glob(params.images_prefix_alt+"*"+params.images_postfix )
            flist_alt = sort_by_numeric_part(flist, readingPars.images_prefix, readingPars.images_postfix)
            #flist_alt.sort()
        else:
            flist_alt = [  params.images_prefix ]
               
    else:
        flist_alt = None
    params.bin = 1
    params.medianize = False
    params.globalize = (0,0)
    params.flist = flist
    params.flist_alt = flist_alt

    checkNormalisation(params)
    
    if 1 :

        if params.ncpus not in [None,"None"]:
            ncores = params.ncpus
        else:
            ncores = multiprocessing.cpu_count()
            # ncores = 1

        barrier = Barrier(ncores)
        threads = [     myThread( params, i, ncores, res , preRes=preRes, Barrier = barrier) for i in range(ncores)  ]
        for t in threads:
            t.start()
        for t in threads:
            t.join()
        return res
    else:
        mythr =     myThread( params, 0, 1, res )
        mythr.run()
        
def get_nim_pos(data):
    res=[]
    whichiswhich = []
    
    for imageName, data4im in data.items():
        for pos, dat4pos in data4im.items():
            pos_a=pos            
            accepted = dat4pos["accepted"]
            if accepted:
                Gi    = dat4pos["Gi"]
                pos    = dat4pos["pos"]
                assert( pos[0]==pos_a[0])
                assert( pos[1]==pos_a[1])
                
                res.append([Gi, pos[0], pos[1]] )
                whichiswhich.append( [imageName, pos]    )
    return res, whichiswhich
        
def save_extracted_data(data,   filename ) :
    h5file = h5py.File(filename,"w" )
    accepted_group = h5file.require_group("accepted")
    blocked_group = h5file.require_group("notaccepted")

    for imageName, data4im in data.items():
        for pos, dat4pos in data4im.items():
            posa=pos
            
            accepted = dat4pos["accepted"]
            pmaps    = dat4pos["pmap"]
            Gi    = dat4pos["Gi"]

            pos    = dat4pos["pos"]

            if "hkl" in dat4pos:
                HKL = dat4pos["hkl"]
            else:
                HKL  = None

            
            if accepted :
                mygroup = accepted_group
            else:
                mygroup = blocked_group
            name = str(Gi)+"_" + str(pos[0]) +"_" + str(pos[1])
            
            pos_group =  mygroup.require_group(name)
            pos_group["pmap"] = pmaps[0]
            if pmaps[1] is not None:
                pos_group["pmap_alt"] = pmaps[1]
                
            pos_group["Gi"] = Gi

            pos_group["pos"] = pos
            pos_group["imageName"] = imageName
            if HKL is not None:
                pos_group["hkl"] = HKL
                

            
            if accepted:
                if "harvest" in dat4pos:
                    # if dat4pos["harvest"] is not None:
                        pos_group["harvest"] = dat4pos["harvest"]
                if "harvest_alt" in dat4pos:
                    # if dat4pos["harvest_alt"] is not None:
                        pos_group["harvest_alt"] = dat4pos["harvest_alt"]
    h5file.close()

    
            
def load_extracted_data(   filename ) :
    data={}
    if not os.path.exists(filename):
        return data
    h5file = h5py.File(filename,"r" )
    accepted_group = []
    blocked_group = []
    
    if "accepted" in  h5file:
        accepted_group = h5file["accepted"]
        
    if "notaccepted" in h5file:
        blocked_group = h5file["notaccepted"]
        
    for mygroup,accepted in zip([ accepted_group, blocked_group   ], [True,False] ):
        for pos_name in mygroup:
            pos_group = mygroup[pos_name]
            imageName =pos_group["imageName"][()]
            pos =pos_group["pos"][()]

            Gi =pos_group["Gi"][()]
            if "hkl" in pos_group:
                HKL = pos_group["hkl"][()]
            else:
                HKL = None

            
            pmap =pos_group["pmap"][:]
            if "pmap_alt" in pos_group:
                pmap_alt =pos_group["pmap_alt"][()]
            else:
                pmap_alt = None

            if imageName not in data:
                data[imageName] = {}
            data[imageName][tuple(pos)] = {}
            data[imageName][tuple(pos)]["pmap"]=[pmap,pmap_alt]
            data[imageName][tuple(pos)]["Gi"] = Gi

            data[imageName][tuple(pos)]["pos"] = pos
            data[imageName][tuple(pos)]["accepted"] = accepted
            if HKL is not None:
                data[imageName][tuple(pos)]["hkl"] = HKL
                
            
            if "harvest" in pos_group :
                data[imageName][tuple(pos)]["harvest"] = pos_group["harvest"][:]
            if "harvest_alt" in pos_group :
                data[imageName][tuple(pos)]["harvest_alt"] = pos_group["harvest_alt"][:]
    h5file.close()
    return data

