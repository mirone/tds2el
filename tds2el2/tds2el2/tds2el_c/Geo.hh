#ifndef GEO_HH

#define GEO_HH

class Precalculated {
public:
  Precalculated() {
    Brav=NULL;
    Brill=NULL;
  }
  void free() {
    if ( Brav) {
      delete Brav ;
      delete Brill;
      Brav=NULL;
      Brill = NULL;
    }
  }
  float P0norm[3];
  float DETM[9] ;
  float *Brav;
  float *Brill;
  float brillmax ; 

};


class Geo {
public:
  
  int   orientation_codes;
  float lmbda;
  float dist;
  float pixel_size;
  float det_origin_X;
  float det_origin_Y;
  float angular_step;
  

  int n_rot_beam ;
  int axnum_rot_beam[10];
  float rad_rot_beam[10];

  
  int n_rot_dect ;
  int axnum_rot_dect[10];
  float rad_rot_dect[10];


  float beam_orientation[3];
  float detector_orientations[9];

  
  float start_phi;


  int n_rot_stage ;
  int axnum_rot_stage[10];
  float rad_rot_stage[10];

  
  float pol_degree;
  float pol_x;
  float pol_y;
  float pol_z;

  
  float AA;
  float BB;
  float CC;
  float aAA;
  float aBB;
  float aCC;
  int   Quat_or_angles;
  float r1;
  float r2;
  float r3;

       
  float qmin;
  float qmax;
  
  void setPrimRotOfRs(float * primrotA, float *initial, float phi) ;

  void setSndRotOfRs(float * sndrotA) ;

  void setRotOfRs(float * rot, float phi) ;

  void getQ0( float iy, float ix,  float *q0 ) ;
  void getQ0_debug( float iy, float ix,  float *q0 ) ;


  void precalculate();

  Precalculated prec;


  void getCellVectors(  float *&Brav, float * &Brill ) ; 

  ~Geo();
  
};
void multiply_vect( float *vb, float *M, float*va);
void multiply_vectT( float *vb, float *M, float*va);


void printMatrix( float *M  ) ;
void printVector( float *V  ) ;
void printVector( double *V  ) ;


#endif
