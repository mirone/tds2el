#include"Tensor.hh"
#include<string.h>
#include<assert.h>
#include<string.h>
#include<stdio.h>
#include<math.h>
#include<locale.h>
#include<stdexcept>
Tensor::Tensor() {
  components = NULL;

  this->npolar=300;
  this->nazimuth = 300;
  
  this->clear();
}

Tensor::~Tensor() {
  this->clear();
}

void Tensor::clear() {
  if(components) {
    delete components;
    delete freqs;
    delete vects;
  }
  nvars=0;
  components = NULL;

}

void Tensor::setComponents(int nvars, float * components) {
  this->clear();
  this->nvars=nvars;
  this->components = new float[nvars*36];

  this->freqs = new float [ npolar*nazimuth *3    ];
  this->vects = new float [ npolar*nazimuth *3 *3 ];
  
  memcpy(this->components, components, nvars*36*sizeof(float));
}

void Tensor::setVars(int nvars, float * vars) {
  assert(this->nvars==nvars);

  for(int i=0; i<36; i++) {
    tens[i]=0;
  }
  for(int i=0; i<nvars; i++) {
    for(int k=0; k<36; k++) {
      tens[k] +=  vars[i]*this->components[ i*36  +k ];
    }
  }

  // printf("\n==========================\n");
  // for(int i=0; i<36; i++) {
  //   if(i%6==0) printf("\n");
  //   printf( " %10.4f " ,tens[i]);
    
  // }
  // printf("\n==========================\n");




  
  for(int ipol=0; ipol<this->npolar; ipol++) {
    float p = ipol*M_PI/this->npolar;
    for(int iaz=0; iaz<this->nazimuth; iaz++) {
      float az = iaz*M_PI/this->nazimuth;
      float Q [] = {  sin(p)*cos(az)   , sin(p)*sin(az)          , cos(p)   };
      this->preCalculate(Q, this->freqs+(ipol*nazimuth + iaz )*3,this->vects+(ipol*nazimuth+iaz)*9);
    }
  }
}
 


void Tensor::preCalculate( float *Q, float *myfreqs, float *myvects) {
  double  E1[3*6];
  double  E2[3*3];
  
  for(int i=0; i<18; i++) E1[i]=0;
  for(int irow=0 ; irow<6 ; irow++) {
    for(int i=0; i<3; i++) {
      for(int j=0; j<3; j++) {
	int icomp;
	if(i==j) {
	  icomp=i;
	} else {
	  icomp =  6-(i+j) ; 
	}
	E1[irow*3+i] +=  tens[irow*6+ icomp]*  Q[j] ; 
      }
    }
  }

  for(int i=0; i<9; i++) E2[i]=0;
  for(int icol=0; icol<3; icol++) {
    for(int i=0; i<3; i++) {
      for(int j=0; j<3; j++) {
	int icomp;
	if(i==j) {
	  icomp=i;
	} else {
	  icomp =  6-(i+j) ; 
	}
	E2[i*3+ icol]+=  E1[icomp*3+ icol]*  Q[j]  ; 
	// comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
	// 		,(2,1):3,(0,2):4,(1,0):5}  	  
      }
    }
  }



  
  double c = -(E2[0*3+0]+E2[1*3+1]+ E2[2*3+2]    );
  double b = +(  E2[1*3+1]*E2[2*3+2]- E2[1*3+2]*E2[2*3+1]    +
	    E2[0*3+0]*E2[2*3+2]- E2[0*3+2]*E2[2*3+0]     +
	    E2[0*3+0]*E2[1*3+1]- E2[0*3+1]*E2[1*3+0]     
	    );
  double a = -(E2[0*3+0]*E2[1*3+1]*E2[2*3+2]+
	 E2[0*3+1]*E2[1*3+2]*E2[2*3+0]+
	 E2[0*3+2]*E2[1*3+0]*E2[2*3+1]-
	 E2[1*3+0]*E2[0*3+1]*E2[2*3+2]-
	 E2[1*3+1]*E2[0*3+2]*E2[2*3+0]-
	 E2[1*3+2]*E2[0*3+0]*E2[2*3+1]
	 );


  // printf( " LOCALE %s\n", setlocale(LC_NUMERIC,"C"));
  // printf(" MATRICE DA DIAGONALIZZARE \n");
  // {
  //   printf("[");
  //   for(int i=0; i<3; i++) {
  //     printf("[ ");
  //     for(int j=0; j<3; j++) {
  // 	printf(" %e , ", E2[i*3+j]);
  //     }
  //     printf(" ],");
  //     printf("\n");
  //   }
  //   printf("]");
  // }
  // printf("======================================\n");


  
  
  
  double p = b - c*c /3.0;
  double q = (a  +c*(  c*c*( 2.0/27.0 ) - b/3.0 ) );  

  
  double d  = q*q + p*p*p*4.0/27.0  ;
  if (0< d) d=0;
  d=-d;
    
  d  = sqrt(d);
  p  = pow( (q*q+d*d)/4, 1.0/6.0)*2.0;
    
  double ang  = atan2( d , -q)/3.0;

  double y[3];
  for(int i=0; i<3; i++) {
    y[i] = cos(  ang +i*M_PI*2.0/3.0 )*p-c/3.0;
  }
    
  double tmp;
  if(y[0]>y[1]) {  tmp=y[1];  y[1]= y[0] ; y[0] = tmp; }; 
  if(y[1]>y[2]) {  tmp=y[2];  y[2]= y[1] ; y[1] = tmp; }; 
  if(y[0]>y[1]) {  tmp=y[1];  y[1]= y[0] ; y[0] = tmp; }; 
  
  double maxfreq=y[2];

  int caso = 0;
  
  if( (y[1]-y[0])/maxfreq <1.0e-4) {
    caso+=1;
  }
  
  if( (y[2]-y[1])/maxfreq <1.0e-6) {
    tmp=y[2];  y[2]= y[0] ; y[0] = tmp;
    caso+=1;
  }


  // printf(" CASO %d\n", caso);
  // for(int k=0; k<3; k++) {
  //   printf(" lambda2 = %e\n", y[k]);
  // }

  
  double vect[9];
  double tmpvect[9];

  for(int i=0; i<9; i++) { vect[i]=0.0f;}
  
  vect[0*3+0]=1.0f;
  vect[1*3+1]=1.0f;
  vect[2*3+2]=1.0f;


  if ( caso == 2 ) {

  } else  if( caso==1) {
    
    int imin;
    double vmin;
    int imax;
    double vmax;
    double sum;
    E2[0*3+0] -= y[2];
    E2[1*3+1] -= y[2];
    E2[2*3+2] -= y[2];
    
    for(int i=0; i<3; i++) {
      sum=0.0f;
      for(int j=0; j<3; j++) {
	tmpvect[i*3+j]=0.0f;
	for(int k=0; k<3; k++) {
	  tmpvect[i*3+j] += E2[j*3+k] * vect[i*3+k]; 
	}
	sum += tmpvect[i*3+j] *tmpvect[i*3+j]   ; 
      }
      if(i==0) {
	vmin=sum;
	imin=0;
	vmax=sum;
	imax=0;
      } else {
	if(sum<=vmin) {
	  vmin=sum;
	  imin=i;
	}
	if(sum>vmax) {
	  vmax=sum;
	  imax=i;
	}
      }
      sum=sqrt(sum);
      for(int j=0; j<3; j++) {tmpvect[i*3+j]  /= sum; };
    }
    for(int j=0; j<3; j++) vect[0*3+j] = tmpvect[imax*3+j];
    int ialt = ((imin+imax)*2)%3 ;
    sum=0.0f;
    for(int j=0; j<3; j++)  {
      vect[1*3+j] = tmpvect[ialt*3+j];
      sum+= vect[1*3+j]*vect[0*3+j];
    }
    for(int j=0; j<3; j++)  {
      vect[1*3+j] -= vect[0*3+j]*sum;
    }
    sum=0.0f;
    for(int j=0; j<3; j++)  {
      sum +=  vect[1*3+j]*vect[1*3+j] ;
    }
    sum=sqrt(sum);
    for(int j=0; j<3; j++) { vect[1*3+j]  /= sum; };
    
    vect[2*3+ 0]   =  vect[1*3+ 1]  *  vect[0*3+ 2]  -    vect[1*3+ 2]  *  vect[0*3+ 1] ;
    vect[2*3+ 1]   =  vect[1*3+ 2]  *  vect[0*3+ 0]  -    vect[1*3+ 0]  *  vect[0*3+ 2] ;
    vect[2*3+ 2]   =  vect[1*3+ 0]  *  vect[0*3+ 1]  -    vect[1*3+ 1]  *  vect[0*3+ 0] ;
    
    E2[0*3+0] += y[2];
    E2[1*3+1] += y[2];
    E2[2*3+2] += y[2];
    
  } else if(caso==0)  {
    int imax=0;
    double vmax=0;
    double sum;
    
    for(int n=0; n<3; n++) {
      E2[0*3+0] -= y[n];
      E2[1*3+1] -= y[n];
      E2[2*3+2] -= y[n];
      
      for(int i=0; i<3; i++) {
	int i1,i2;
	i1 = (i+1)%3;
	i2 = (i+2)%3;
	
	sum=0.0f;
	for(int j=0; j<3; j++) {
	  int j1,j2;
	  j1 = (j+1)%3;
	  j2 = (j+2)%3;
	  tmpvect[i*3+j]  = E2[j1*3+ i1]  *  E2[j2*3+ i2]  -    E2[j1*3+ i2]  *  E2[j2*3+ i1] ;
	  sum += tmpvect[i*3+j] *tmpvect[i*3+j]   ; 
	}
	sum=sqrt(sum);
	for(int j=0; j<3; j++) { tmpvect[i*3+j]  /= sum; };
	
	if(i==0) {
	  vmax=sum;
	  imax=0;
	} else {
	  if(sum>vmax) {
	    vmax=sum;
	    imax=i;
	  }
	}

      }
      for(int j=0; j<3; j++) { vect[n*3+j]  = tmpvect[imax*3+j] ; };
      E2[0*3+0] += y[n];
      E2[1*3+1] += y[n];
      E2[2*3+2] += y[n];
    }
  }


  // printf(" VETTORI \n");
  // {
  //   printf("[");
  //   for(int i=0; i<3; i++) {
  //     printf("[ ");
  //     for(int j=0; j<3; j++) {
  // 	printf(" %e , ", vect[i*3+j]);
  //     }
  //     printf(" ],");
  //     printf("\n");
  //   }
  //   printf("]");
  // }
  // printf("======================================\n");
  
  if(0) {
    // test
    for(int n=0; n<3; n++) {
      double sum;
      if(n) {
	E2[0*3+0] += y[n-1];
	E2[1*3+1] += y[n-1];
	E2[2*3+2] += y[n-1];
      }
      E2[0*3+0] -= y[n];
      E2[1*3+1] -= y[n];
      E2[2*3+2] -= y[n];
      sum=0.0;
      for(int j=0; j<3; j++) {
	tmpvect[n*3+j]=0.0f;
	for(int k=0; k<3; k++) {
	  tmpvect[n*3+j] += E2[j*3+k] * vect[n*3+k]; 
	}
	sum += tmpvect[n*3+j] *tmpvect[n*3+j]   ; 
      }
      // printf(" erro r%d %e     ", n, sum ) ; 
      if(sum>1.0e-6) {
	printf(" Problem solving the elastic tensor : error too big %d %e\n", n, sum);
	throw std::runtime_error("  error too high in eigensolver in building the Q grid for acustic waves " ); 
      }
    }
  }

  for(int i=0; i<3; i++) {
    myfreqs[i]=sqrt(fabs(y[i]));
    if( y[i]<0)  myfreqs[i] =  -myfreqs[i]     ;
    for(int k=0; k<3; k++) {
      myvects[i*3+k] = vect[i*3+k];
    }
  }
}

