#include<map>
#include<vector>
#include<mpi.h>


typedef struct ac_struct
{
  float phi;
  float merit ; 
  int ij;  
} AC_Struct;

 
void clonaC(   int * hkls_start ,
	       float *target,
	       int iim ,
	       int * interp_prostart,
	       float *interp_compacted_data,
	       float *data );


void get_Q0C(int dim1, int dim2,
	     double * Q0,
	     double dist,
	     double det_origin_X,
	     double det_origin_Y,
	     double pixel_size,
             double*  det_mat,
             double * p0,
	     double lmbda,
	     double*  MD
	     ) ;
    


void window_maximumC(float *target,float * source,
		     int dire, int mindist, int dz, int dy, int dx);
  
        

class SpotPicker {
 public:

  
  inline int getdq(float *q,float *cvs , float *brls, float *dQhkl ,float *Dhkl ,
		 int * hkl  , float DeltaQ_min , float DeltaQ_max , float &dist2,
		   std::vector<float> & centering4hkl, int ishift, int hascentering, float *eXp, int NeXp) ;

  SpotPicker(int Hmax, int do_elastic, float qcube, int navoid, int * pavoid, float qmax);
  inline int poshkl(int h, int k,int l)  {
    return (l+H)+H2p1*((k+H)+H2p1*(h+H));
  }
  inline int poshkl(int *hkl)  {
    return (hkl[2]+H)+H2p1*((hkl[1]+H)+H2p1*(hkl[0]+H));
  }
  inline void  hkl(int &h, int &k, int &l, int pos)  {
    l   = pos%H2p1 -H;
    pos = pos / H2p1;
    k   = pos%H2p1 -H;
    pos = pos / H2p1;
    h   = pos%H2p1 -H;   
  }

  void harvest(int npoints, int dim1,  float *QQ, float* Kin, 
	       float *cellvectors,
	       float *brills,
	       float *data,  float *Filter , float DeltaQ_min , float DeltaQ_max ,
	       int nharv, int * harvest, float phi, int double_exclusion, float powder_width, 
	       float *Qfin_delt, float *Qfin_phi, float * cellvectors_delta, int nimage, int ntotimages, float threshold,
	       float * centering4hkl, float *eXp, int NeXp) ;
 
  void harvestCube(int npoints,
			     float *QQ,
			     float *data3d,
			     float *Filter );

  void compatta(int all_alone);
  int  interpolatedSpot(int h, int k, int l, int dimVol, float *axis, float *corner, float * Volume, float  beta, int Niter);
  void getDataToFit(int *nspots, int *tofits_spots, int **hkls , int **Npts,
		    float **DQs,  float **Datas , float ** ac_phis  , int ** ac_ijs, int nred,
		    float **tuttiPhi, int **tuttiIJ, float **tuttiDist) ;

  void ac_export(  void * ) ;
  void ac_import(  void * , int) ;
  void CreateAcTypeMPI( MPI_Datatype *AcTypeMPI);

  int H;
  int H2p1;
  std::vector<int> np4hkl;
  int NCENTER_centering ; 
  std::vector<float> centering4hkl;
  std::vector<float> alone4hkl;
  std::vector<int>   Nalone4hkl;
  std::vector<int> takeit;
  std::vector<int> avoid;

  std::vector<float> compacted_data;
  std::vector<int> hkl_start;
  float Qmax;

  std::vector<float> data;
  std::vector<float> idatahkl;

  std::vector<float> interp_data;
  std::vector<int>   interp_np4image;

  std::vector<int>    interp_prostart ;
  std::vector<float>  interp_compacted_data;


 
  std::vector<AC_Struct> AC_data ;

  int do_elastic ;
  float qcube  ;
 
  MPI_Datatype AcTypeMPI;
  MPI_Op AcOp;
  
  int AC_displ[3];
  int has_centering; 

  int MT;
  
  // int Niter;

};
