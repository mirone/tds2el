#include<stdio.h>
#include"Blob.hh"
#include<assert.h>
#include<vector>
#include<math.h>
#include<string.h>
#include"Geo.hh"
#include<stdexcept>

Blob::Blob(int * hkl ,  int Gi , int poy , int pox , int dz, int dy , int dx , float* harvest, Geo * geo, int centering, int NeXp , float *exclusions)  {


  this->otherBlob = NULL; 


  this->eXp = exclusions;
  this->NeXp = NeXp/4;
  
  printf(" CENTERING ALL INIZIO %d\n", centering);

  for(int i=0; i<3; i++) this->hkl[i]  = hkl[i]; 
  this->Gi   = Gi ;
  this->posy = poy; 
  this->posx = pox; 

  this->dims[0] = dz ;
  this->dims[1] = dy ;
  this->dims[2] = dx ;

  this->Tens = NULL;
  
  for(int i=0; i<3; i++) 
    assert(  this->dims[i]%2 == 1    ) ;
  
  this->harvest = harvest ;
  this->my_harvest = NULL ; 
  this->indexes = NULL ; 

  
  this->geo = geo;
  this->pos = NULL ;


  this->setUp(centering);
  printf(" Ci sono  %d  buoni\n",  this->npointsTot );
  if(   this->braggOffset  ) {
    float *p = this->braggOffset  ;
    printf(" Centro Bragg est %e %e %e \n",  p[0], p[1], p[2]) ;
  } else {
    printf(" Niente punto Bragg \n");
  }

  this->sim_intensity = NULL; 

  printf(" CENTERING QUI %d \n",  this->centering ) ;

  corr     = 1 ;
  DcorrDy  = 0;
  DcorrDx  = 0;
  Wcorr    = 1;
  DWcorrDn = 0;
  DWcorrDy = 0;
  DWcorrDx = 0;
  
  DWcorrDnn = 0;
  DWcorrDyy = 0;
  DWcorrDxx = 0;
  
  DWcorrDny = 0;
  DWcorrDyx = 0;
  DWcorrDnx = 0;

}



Blob::~Blob() {
  if(this->pos) {
    //  printf(" cancello blob %d %d %d \n", hkl[0], hkl[1], hkl[2]  ) ;
    delete pos;
  }
  if ( this->indexes ) delete this->indexes;
  if ( this->my_harvest ) delete this->my_harvest;
  this->clear_simulation();
}

void Blob::clear_simulation() {
  if( this->sim_intensity) {
    delete this->sim_intensity ;
    this->sim_intensity=NULL;
  }
}



double  Blob::simulate(float Temp, float second_Temp, int rare) {
  // printf(" IN SIMULATE\n");
  if( this->Tens == NULL ) {
    throw std::runtime_error(" Simulation requested in Blob object but no tensor set for this Blob " ); 
  }
  this->sim_intensity = new float [  this->npointsTot ] ;
  float M[9] ;
  float myq [3];
  float myqO[3];
  
  float *q0s = this->getQ0s();
  
  float Center[3];
  {
    float fhkl[3];
    for(int i=0; i<3; i++) fhkl[i] = this->hkl[i];
    multiply_vectT( Center , this->geo->prec.Brill ,fhkl);
  }
  
  {
   
    if(this->centering) {
      {
	float poss[3];
	float myq[3];
	float M[9] ;	
	poss[0] = (this->Gi   +braggOffset_pixel[0]);	 
	poss[1] = (this->posy +braggOffset_pixel[1]);
	poss[2] = (this->posx +braggOffset_pixel[2]);
	float p[3];
	geo->getQ0( poss[1], poss[2], p  ) ;
	float myphi =  this->geo->start_phi +   poss[0]* this->geo->angular_step       ; 
	this->geo->setRotOfRs(M, myphi) ; 
	multiply_vectT( myq, M,p);
	for(int i=0; i<3; i++) {
	  
	  this->braggOffset[i] = myq[i] - Center[i] ;
	  Center[i] = myq[i];
	}
      }

    }
  }

  double aa=0.0;
  double ab=0.0;
  double ad=0.0;
  double bb=0.0;
  double bd=0.0;
  double dd=0.0;

#pragma omp parallel for schedule(dynamic) reduction(+:aa,ab,ad,bb,bd,dd) private(M,myq, myqO) 
  for(int iz=0; iz<dims[0]; iz++) {
    if (  indexes[iz]<indexes[iz+1] ) {


      float fromC_n = (iz - (this->dims[0]-1.0)/2) / (      (this->dims[0]-1.0)/2    ) ;
      
      float myphi = this->geo->start_phi + (iz+this->Gi - (this->dims[0]-1)/2)*this->geo->angular_step ; 
      this->geo->setRotOfRs(M, myphi) ; 
      
      for(int ip = indexes[iz]; ip<indexes[iz+1]; ip++) {
	if (ip%rare) continue;
	
	int pp = pos[ip];

	// float data = this->harvest[pp];
	float data = this->my_harvest[ip];
	
	int pos2D = pp %( dims[1]*dims[2]  ) ;

	float fromC_y   =   (( pos2D /  dims[2] ) -  (dims[1]-1)/2.0) / ((this->dims[1]-1)/2);
	float fromC_x   =   (( pos2D %  dims[2] ) -  (dims[2]-1)/2.0) / ((this->dims[2]-1)/2);

	float *q0 = q0s + pos2D*3;
	multiply_vectT( myq, M, q0);

	double  summq = 0; 
	
	for(int k=0; k<3; k++) {
	  myqO[k]  = myq[k] ;
	  myq [k] -= Center[k] ;
	  summq += myq[k]*myq[k]; 
	}
	
	if(summq==0) continue;
	
	float weights[4];
	float myfreqs[4*3];
	float myvects[4*3*3];
	
	this->Tens->getInterpData( myq,  weights,myfreqs, myvects);
	// this->Tens->preCalculate( myq, myfreqs, myvects);
	
	double res=0.0;

	float factT1, factT2 ;
	factT1 =  (corr + DcorrDy*fromC_y + DcorrDx*fromC_x  ) *(Wcorr + DWcorrDn*fromC_n + DWcorrDy*fromC_y + DWcorrDx*fromC_x +
								 DWcorrDnn*fromC_n*fromC_n + DWcorrDyy*fromC_y*fromC_y + DWcorrDxx*fromC_x*fromC_x +
								 DWcorrDny*fromC_n*fromC_y + DWcorrDyx*fromC_y*fromC_x + DWcorrDnx*fromC_n*fromC_x 
								 ) ;
	if( second_Temp >0 ) {
	  Blob * p = this->otherBlob ; 
	  factT2  = (p->corr + p->DcorrDy*fromC_y + p->DcorrDx*fromC_x  ) *(p->Wcorr + p->DWcorrDn*fromC_n + p->DWcorrDy*fromC_y + p->DWcorrDx*fromC_x+
									    p->DWcorrDnn*fromC_n*fromC_n + p->DWcorrDyy*fromC_y*fromC_y + p->DWcorrDxx*fromC_x*fromC_x +
									    p->DWcorrDny*fromC_n*fromC_y + p->DWcorrDyx*fromC_y*fromC_x + p->DWcorrDnx*fromC_n*fromC_x 
									    ) ; // small error because not same centering on otherspot
	} else {
	  factT2 = 0 ; 
	}
	
	
	for(int inode=0; inode<4; inode++) {
	  float peso = weights[inode];
	  // printf(" %e ", peso);
	  // if(inode==3) printf("\n");
	  for(int k=0; k<3; k++) {
	    
	    float frq = myfreqs [ inode*3 +k ]; 
	    float *vect = myvects + inode*3*3 +k*3;
	    
	    if(frq>0) {	  
	      double tmp = vect[0]*myqO[0]  + vect[1]*myqO[1]  + vect[2]*myqO[2] ;
	      
	      if ( second_Temp ==0 ) {
		res = res + (tmp*tmp *( cosh (frq/(2*Temp))/frq/ sinh(frq/(2*Temp))   ) * factT1 )*peso  ;
	      } else {
		res = res + (tmp*tmp *   (  -( cosh (frq/(2*Temp       ))/frq/ sinh(frq/(2*Temp       ))   ) * factT1
					    +
					     ( cosh (frq/(2*second_Temp))/frq/ sinh(frq/(2*second_Temp))   ) * factT2
					    ))*peso  ;
	      }
	    }
	  }

	}
	this->sim_intensity[ip] = res;
	// this->sim_intensity[ip] = myq[0];
	// this->sim_intensity[ip] = myq[0]*myq[0] +myq[1]*myq[1] +myq[2]*myq[2]          ;

	aa+=res*res;
	bb+=data*data;
	dd+=1*1;
	
	ab+=res*data;
	ad+=res;
	bd+=data;
	
      } 
    }
  }
  delete q0s;
 

  double det  = aa*dd-ad*ad;
  
  double Ca   = ( dd *ab -ad*bd   ) /det;
  double Cd   = (-ad*ab  +aa *bd  ) /det; 

  if (Cd<0 && second_Temp==0) {
    ad=0;
    bd=0;
            
    det = aa*dd-ad*ad;
    Ca   = ( dd *ab -ad*bd   ) /det;
    Cd   = (-ad*ab  +aa *bd    ) /det;
  }
  double res  = Ca*Ca*aa + Cd*Cd*dd +2*Ca*Cd*ad  + bb  - 2*Ca*ab -2*Cd*bd;
  
  for(int i=0; i<npointsTot; i++) {
    sim_intensity[i] = sim_intensity[i]*Ca + Cd;
  }

  return res;

  
}


void  Blob::getDataForOld(int &myNpt, float *& myhkl, float * &mydata, float *& myqs, float *& myfacts) {

  myNpt = this->npointsTot  ;

  myhkl   = new float[3];

  mydata  = new float[this->npointsTot ];
  myqs    = new float[this->npointsTot *3];
  myfacts = new float[this->npointsTot *2 ];
  
  for(int i=0; i<3; i++) myhkl[i] = this->hkl[i];
  
  float *q0s = this->getQ0s();



  
  float Center[3];
  {
    float fhkl[3];
    for(int i=0; i<3; i++) fhkl[i] = this->hkl[i];
    multiply_vectT( Center , this->geo->prec.Brill ,fhkl);
  }
  {
    if(this->centering) {
      {
	float poss[3];
	float myq[3];
	float M[9] ;
	poss[0] = (this->Gi   +braggOffset_pixel[0]);	 
	poss[1] = (this->posy +braggOffset_pixel[1]);
	poss[2] = (this->posx +braggOffset_pixel[2]);
	float p[3];
	geo->getQ0( poss[1], poss[2], p  ) ;
	float myphi =  this->geo->start_phi +   poss[0]* this->geo->angular_step       ; 
	this->geo->setRotOfRs(M, myphi) ; 
	multiply_vectT( myq, M,p);	
	for(int i=0; i<3; i++) {
	  
	  this->braggOffset[i] = myq[i] - Center[i] ;
	  Center[i] = myq[i];
	}
      }
    }
  }
  float myq[3];
  float M[9];
  for(int iz=0; iz<dims[0]; iz++) {
    if (  indexes[iz]<indexes[iz+1] ) {
      float fromC_n = (iz - (this->dims[0]-1)/2.)/ ((this->dims[0]-1)/2.0);
      float myphi = this->geo->start_phi + (iz+this->Gi - (this->dims[0]-1)/2)*this->geo->angular_step ; 
      this->geo->setRotOfRs(M, myphi) ; 
      for(int ip = indexes[iz]; ip<indexes[iz+1]; ip++) {	
	int pp = pos[ip];

	mydata[ip] = this->my_harvest[ip];
	
	int pos2D = pp %( dims[1]*dims[2]  ) ;

	float fromC_y =   (( pos2D /  dims[2] ) -  (dims[1]-1)/2.0) / ((this->dims[1]-1)/2.0);
	float fromC_x =   (( pos2D %  dims[2] ) -  (dims[2]-1)/2.0) / ((this->dims[2]-1)/2.0);

	float *q0 = q0s + pos2D*3;
	multiply_vectT( myq, M, q0);


	for(int k=0; k<3; k++) myqs[3*ip+k] = myq[k]-Center[k];


	float factT1, factT2 ;
	factT1 =  (corr + DcorrDy*fromC_y + DcorrDx*fromC_x  ) *(Wcorr + DWcorrDn*fromC_n + DWcorrDy*fromC_y + DWcorrDx*fromC_x  +
								 DWcorrDnn*fromC_n*fromC_n + DWcorrDyy*fromC_y*fromC_y + DWcorrDxx*fromC_x*fromC_x +
								 DWcorrDny*fromC_n*fromC_y + DWcorrDyx*fromC_y*fromC_x + DWcorrDnx*fromC_n*fromC_x ) ;
	if( this->otherBlob   ) {
	  Blob * p = this->otherBlob ; 
	  factT2  = (p->corr + p->DcorrDy*fromC_y + p->DcorrDx*fromC_x  ) *(p->Wcorr + p->DWcorrDn*fromC_n + p->DWcorrDy*fromC_y + p->DWcorrDx*fromC_x  +
									    p->DWcorrDnn*fromC_n*fromC_n + p->DWcorrDyy*fromC_y*fromC_y + p->DWcorrDxx*fromC_x*fromC_x +
									    p->DWcorrDny*fromC_n*fromC_y + p->DWcorrDyx*fromC_y*fromC_x + p->DWcorrDnx*fromC_n*fromC_x ) ; // small error because not same centering on otherspot
	} else {
	  factT2 = 0 ; 
	}
	
	myfacts[ip                      ] =  factT1 ; 
	myfacts[ip  + this->npointsTot  ] =  factT2 ; 
	// myfacts[ip                      ] =  1.0 ; 
	// myfacts[ip  + this->npointsTot  ] =  1.0 ; 
      }
    }
  }
  printf(" w000 IN getdata \n");
  printf(  "  w000 --- corr , DcorrDy , DcorrDx, Wcorr , DWcorrDn , DWcorrDy , DWcorrDx     %e %e %e    %e %e %e %e  \n",    corr , DcorrDy , DcorrDx, Wcorr , DWcorrDn , DWcorrDy , DWcorrDx  ) ;
  if(	   this->otherBlob   ) {

    Blob * p = this->otherBlob ; 
    assert(  this->hkl[0] == p->hkl[0]) ;
    assert(  this->hkl[1] == p->hkl[1]) ;
    assert(  this->hkl[2] == p->hkl[2]) ;
    
    printf(  " w000 --- p-> corr , DcorrDy , DcorrDx, Wcorr , DWcorrDn , DWcorrDy , DWcorrDx     %e %e %e    %e %e %e %e  \n",    p->corr , p->DcorrDy , p->DcorrDx, p->Wcorr , p->DWcorrDn , p->DWcorrDy , p->DWcorrDx  ) ;
  }
  delete q0s;
 
}







void  Blob::getQfromCenter(int n,int iy,int ix, float *QQ) {
  float M[9] ;
 
  
  float *q0s = this->getQ0s();
  
  {
    float myphi = this->geo->start_phi + (     this->Gi +n )*this->geo->angular_step ;
    
    float *q0 = q0s + ( ( (dims[1]-1)/2  + iy  )*dims[2]  +  (dims[2]-1)/2  +ix )*3;
    
    this->geo->setRotOfRs(M , myphi) ;
    
    multiply_vectT(   QQ, M, q0);

  }
  delete q0s;
};




int checkExclusion(float *dQhkl, int NeXp, float *eXp, float  DeltaQ_min) {

  int OKeXp=1;
  for(int ieXp=0; ieXp<NeXp; ieXp++) {
    if(eXp[3+4*ieXp] >0 ) {
      float XX=0;
      for(int i=0; i<3;i++) {
	XX +=   dQhkl[i]*eXp[i +4*ieXp]  ; 
      }
      if(!( fabs(XX)>eXp[3+4*ieXp]* fabs(DeltaQ_min) || DeltaQ_min == 0  )  ) {
	OKeXp = 0;
      }
    } else {
      float XX=0;
      float scalprod=0;
      float d=0;
      float norm=0;
      for(int i=0; i<3;i++) {
	float tmp = eXp[i +4*ieXp] ;
	norm += tmp*tmp ; 
      }
      norm = sqrt(norm);
      
      for(int i=0; i<3;i++) {
	scalprod +=   dQhkl[i]*eXp[i +4*ieXp]  ; 
      }
      for(int i=0; i<3;i++) {
	d = dQhkl[i] - scalprod*eXp[i +4*ieXp]/norm;
	XX +=   d*d  ; 
      }
      if(!( sqrt(XX)>fabs(eXp[3+4*ieXp])* fabs(DeltaQ_min) || DeltaQ_min == 0  )  ) {
	OKeXp = 0;
      }
    }
  }
  return OKeXp; 
}


void Blob::stickIntensity( float * sim,  float * ref)  {
  assert( sim_intensity ) ;
  for(int iz=0; iz<dims[0]; iz++) {
    for(int ip = indexes[iz]; ip<indexes[iz+1]; ip++) {
      int pp = pos[ip];
      // ref[pp] = harvest[pp];
      ref[pp] = my_harvest[ip];
      sim[pp] = sim_intensity[ip];
    }
  }
}


void Blob::setTensor( Tensor * tens) {

  this->Tens = tens;
}


void Blob::setUp(int centering) {  // centro se c'e' centro  e tabella corrispondenze a dati buoni

  this->centering = centering;

  braggOffset = NULL ;  // inizialmente nullo, settato se si trovano punti oltre threshold ( negativi )

  double poss[3] ;
  double sum=0.0;
  
  for(int i=0; i<3; i++) poss[i]=0;
  
  for(int i0 = 0; i0< this->dims[0]; i0++) {
    for(int i1 = 0; i1< this->dims[1]; i1++) {
      for(int i2 = 0; i2< this->dims[2]; i2++) {
	int ii =    (i0*this->dims[1]+i1)*this->dims[2]+i2; 
	if ( harvest[ ii ]<0) {
	  poss[0] += i0* (- harvest[ii]);
	  poss[1] += i1* (- harvest[ii]);
	  poss[2] += i2* (- harvest[ii]);
	  sum     +=     (- harvest[ii]);
	}
      }
    }
  }

  float M[9] ;
  float myq[3];
  float myqdiff[3];
  float Fhkl[3];
  float Qcent[3];
  float Dhkl[3];
  
  if(sum==0.0) {
    if ( this->braggOffset ) {
      delete this->braggOffset ;
    }
    
    this->braggOffset = NULL ; 
    
  } else {
    if ( this->braggOffset == NULL) {
      this->braggOffset = new float[3];
    }
    for(int k=0; k<3; k++) {
      poss[k] = poss[k]/sum;
    }
    braggOffset_pixel[0] = (poss[0] - (this->dims[0]-1)/2);	 
    braggOffset_pixel[1] = (poss[1] - (this->dims[1]-1)/2);
    braggOffset_pixel[2] = (poss[2] - (this->dims[2]-1)/2);
    
    poss[0] = (poss[0]+this->Gi - (this->dims[0]-1)/2);	 
    poss[1] = (poss[1]+this->posy - (this->dims[1]-1)/2);
    poss[2] = (poss[2]+this->posx - (this->dims[2]-1)/2);
    
    float p[3];
    geo->getQ0( poss[1], poss[2], p  ) ;
    float myphi =  this->geo->start_phi +   poss[0]* this->geo->angular_step       ;
 
    this->geo->setRotOfRs(M, myphi) ;
    multiply_vectT( myq, M,p);

    float fhkl[3];
    for(int i=0; i<3; i++) fhkl[i] = this->hkl[i];
    multiply_vectT( Qcent , this->geo->prec.Brill ,fhkl);
    
    for(int i=0; i<3; i++) {
      this->braggOffset[i] = myq[i] - Qcent[i] ;
    }
    
  }

  this->npointsTot = 0 ; // numero di tutti i buoni punti
  
  if ( this->indexes) delete this->indexes;
  this->indexes    =  new int [  this->dims[0]+1   ] ;    // da dove partono  i punti per la proiezione iesima ( attorno a Gi )
  
  std::vector<int> Vpos;
  std::vector<float> Vmy_harvest;


  float *q0s = this->getQ0s();
  
  for(int i0 = 0; i0< this->dims[0]; i0++) {

    this->indexes[i0] = this->npointsTot ;
    
    float myphi =  this->geo->start_phi +   (i0+this->Gi - (this->dims[0]-1)/2)* this->geo->angular_step       ; 
     
    this->geo->setRotOfRs(M, myphi) ; 
    
    for(int i1 = 0; i1< this->dims[1]; i1++) {
 
      
      for(int i2 = 0; i2< this->dims[2]; i2++) {
	
	int ii =    (i0*this->dims[1]+i1)*this->dims[2]+i2; 
	if ( !isnan(harvest[ ii ]) && harvest[ ii ]>=0 ) {

	  float *q0 = q0s + ( i1*dims[2] + i2  )*3 ; 
	  
	  multiply_vectT( myq, M, q0);
  
	  multiply_vect( Fhkl , this->geo->prec.Brav , myq );

	  
	  for(int i=0; i<3; i++) {
	    Dhkl[i] = Fhkl[i] - this->hkl[i] ;
	  }

	  
	  multiply_vectT( myqdiff , this->geo->prec.Brill , Dhkl );

	  
	  if(   this->braggOffset  && centering) {
	    for(int i=0; i<3; i++) {
	      myqdiff[i] = myqdiff[i]-this->braggOffset[i];
	    }
	  }
	  sum=0.0;
	  for(int i=0; i<3; i++) {
	    sum += myqdiff[i]*myqdiff[i];
	  }
	  // printf(" sqrt(sum) , this->geo->prec.brillmax/this->geo->prec.brillmax, this->geo->qmin %e %e %e\n",   sqrt(sum) , this->geo->prec.brillmax/this->geo->prec.brillmax, this->geo->qmin ) ;
	  if( sum/this->geo->prec.brillmax/this->geo->prec.brillmax >= this->geo->qmin*this->geo->qmin   && sum/this->geo->prec.brillmax/this->geo->prec.brillmax < this->geo->qmax*this->geo->qmax  ) {

	    if( !checkExclusion( myqdiff  , this->NeXp, this->eXp,   this->geo->prec.brillmax*this->geo->qmin       )) {
	      continue;
	    }
	    
	    Vpos.push_back(ii) ; 
	    Vmy_harvest.push_back( harvest[ii] ) ; 
	    this->npointsTot++;
	  }
	}
      }
    }
  }
  this->indexes[ this->dims[0]] = this->npointsTot ;
  
  if ( this->pos ) delete this->pos ; 
  pos = new int[Vpos.size()]      ;  // dove si trova in harvest
  memcpy(pos, &(Vpos[0]), sizeof(int)*Vpos.size());
  
  if ( this->my_harvest ) delete this->my_harvest ; 
  my_harvest = new float[Vmy_harvest.size()]      ;  
  memcpy(my_harvest, &(Vmy_harvest[0]), sizeof(float)*Vmy_harvest.size());
  
  delete q0s ; 
}



void Blob::merge(Blob *otherBlob) {


  this->otherBlob = otherBlob ;
  
  double sum=0.0;
  

  float M[9] ;
  float myq[3];
  float myqdiff[3];
  float Fhkl[3];

  float Dhkl[3];
  

  this->npointsTot = 0 ; // numero di tutti i buoni punti
  if (this->indexes)  delete this->indexes ; 
  this->indexes    =  new int [  this->dims[0]+1   ] ;    // da dove partono  i punti per la proiezione iesima ( attorno a Gi )
  
  std::vector<int> Vpos;
  std::vector<float> Vmy_harvest;

  float *q0s = this->getQ0s();


  float Center[3];
  {
    float fhkl[3];
    for(int i=0; i<3; i++) fhkl[i] = this->hkl[i];
    multiply_vectT( Center , this->geo->prec.Brill ,fhkl);
  }

  //float MAX=0.0, MIN=100.0;
  
  for(int i0 = 0; i0< this->dims[0]; i0++) {

    this->indexes[i0] = this->npointsTot ;
    
    float myphi =  this->geo->start_phi +   (i0+this->Gi - (this->dims[0]-1)/2)* this->geo->angular_step       ; 
     
    this->geo->setRotOfRs(M, myphi) ; 
    
    for(int i1 = 0; i1< this->dims[1]; i1++) {
 
      
      for(int i2 = 0; i2< this->dims[2]; i2++) {
	
	int ii =    (i0*this->dims[1]+i1)*this->dims[2]+i2; 
	if ( !isnan(harvest[ ii ]) && harvest[ ii ]>=0 ) {

	  float *q0 = q0s + ( i1*dims[2] + i2  )*3 ; 
	  
	  multiply_vectT( myq, M, q0);

	  if(1) {
	    for(int i=0; i<3; i++) {
	      myqdiff[i] = myq[i]-Center[i] ;
	    }
	  } else {
	  
	    multiply_vect( Fhkl , this->geo->prec.Brav , myq );
	    
	    for(int i=0; i<3; i++) {
	      Dhkl[i] = Fhkl[i]-this->hkl[i];
	    }
	    
	    multiply_vectT( myqdiff , this->geo->prec.Brill , Dhkl );
	  }
	  
	  if(   this->braggOffset  && centering) {
	    for(int i=0; i<3; i++) {
	      myqdiff[i] = myqdiff[i]-this->braggOffset[i];
	    }
	  }
	  sum=0.0;
	  for(int i=0; i<3; i++) {
	    sum += myqdiff[i]*myqdiff[i];
	  }
	  
	  if( sum/this->geo->prec.brillmax/this->geo->prec.brillmax >= this->geo->qmin*this->geo->qmin   && sum/this->geo->prec.brillmax/this->geo->prec.brillmax < this->geo->qmax*this->geo->qmax  ) {
	    // debug debug
	    // float other_data = otherBlob->interpolateAtQ( i0 +Gi -(dims[0]-1)/2  ,i1+posy -(dims[1]-1)/2 ,i2+posx -(dims[2]-1)/2 ,  myq    ) ;
	    // float other_data = 1.01*harvest[ii];



	    if( !checkExclusion( myqdiff  , this->NeXp, this->eXp,   this->geo->prec.brillmax*this->geo->qmin       )) {
	      continue;
	    }
	    



	    
	    float other_data = otherBlob->interpolateAtQ( i0,i1,i2,  myq    ) ;
	    if (other_data>=0.0) {

	      // if(MAX<sum) MAX=sum;
	      // if(MIN>sum) MIN=sum;
	      
	      Vpos.push_back(ii) ; 
	      Vmy_harvest.push_back( other_data-harvest[ii] ) ; 
	      this->npointsTot++;
	    }
	  }
	}
      }
    }
  }
  // printf(" MAX MIN ==== %e %e \n", MAX, MIN);
  this->indexes[ this->dims[0]] = this->npointsTot ;
  
  if ( this->pos ) delete this->pos ; 
  pos = new int[Vpos.size()]      ;  // dove si trova in harvest
  memcpy(pos, &(Vpos[0]), sizeof(int)*Vpos.size());
  
  if ( this->my_harvest ) delete this->my_harvest ; 
  my_harvest = new float[Vmy_harvest.size()]      ;  // dove si trova in harvest
  memcpy(my_harvest, &(Vmy_harvest[0]), sizeof(float)*Vmy_harvest.size());
  
  delete q0s ; 
}


float Blob::interpolateAtQ( int  i0,int i1,int i2, float *  Qother    ) {



  // {  // DEBUGGGGGGGGGG
  //   i0 = i0 -Gi +(dims[0]-1)/2 ;
  //   i1 = i1 -posy +(dims[1]-1)/2 ;
  //   i2 = i2 -posx +(dims[2]-1)/2 ;
  //   if(i0<0 || i0>= dims[0] ) return -1;
  //   if(i1<0 || i1>= dims[1] ) return -1;
  //   if(i2<0 || i2>= dims[2] ) return -1;
  //   int ii =    ( (i0    ) *this->dims[1]+    (i1)      )*this->dims[2]   +   (i2)    ; 
  //   float data  =  harvest[ ii ] ;
  //   if (  isnan(harvest[ ii ]) || harvest[ ii ]<0 ) {
  //     return -1; 
  //   }
  //   return data ;
  // }
  
  
  float py[2], px[2] ;
  float M[9] ;
  
  py[0] = posy + i1 - (dims[1]-1)/2 ; 
  px[0] = posx + i2 - (dims[2]-1)/2 ;
  
  py[1] = posy + i1 - (dims[1]-1)/2 +1; 
  px[1] = posx + i2 - (dims[2]-1)/2 +1;

  float q0[3][3];
  geo->getQ0( py[0], px[0],((float*)  q0 ) +0) ; //Origin
  geo->getQ0( py[0], px[1],((float*)  q0 ) +3) ; // DX
  geo->getQ0( py[1], px[0],((float*)  q0 ) +6) ; // DY
    
  float myphi =  this->geo->start_phi +   (i0+this->Gi - (this->dims[0]-1)/2)* this->geo->angular_step       ; 
     
  this->geo->setRotOfRs(M, myphi) ;

  float myq [4] [3] ;
  
  multiply_vectT( ( (float*)  myq ) +0 , M,((float*)   q0 ) +0 );	 // Origin
  multiply_vectT( ( (float*)  myq ) +3 , M,((float*)   q0 ) +3 );	 // Dx
  multiply_vectT( ( (float*)  myq ) +6 , M,((float*)   q0 ) +6 );	 // Dy

  this->geo->setRotOfRs(M, myphi + this->geo->angular_step) ;            // Dz
  
  multiply_vectT(( (float*)  myq ) + 9 , M, ( (float*) q0)  + 0 );       // Dz


  float prim[3][3];

  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      prim[i][j] = myq[i+1][j]-myq[0][j];   //  x,y,z
    }
  }
  double det = 0;
  for (int i=0; i<3; i++) {
    det +=  prim[ 0+i ][ 0 ] * prim[ (1+i)%3 ][ 1 ] * prim[ (2+i)%3 ][2] ; 
    det -=  prim[ 0+i ][ 2 ] * prim[ (1+i)%3 ][ 1 ] * prim[ (2+i)%3 ][0] ; 
  }
  
  float dual[3][3];
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      dual[i][j] = (prim[(i+1)%3][(j+1)%3]*prim[(i+2)%3][(j+2)%3]-
		    prim[(i+1)%3][(j+2)%3]*prim[(i+2)%3][(j+1)%3])/det ;
    }
  }

  float diff[3];
  for (int i=0; i<3; i++) {
    diff[i] =  Qother[i] - myq[0][i] ; 
  }
  float dI[3] ; 
  for (int i=0; i<3; i++) {
    dI[i]=0.0;
    for (int j=0; j<3; j++) {
      dI[i] +=   dual[i][j] *diff[j] ; 
    }
  }

  float newI[3] ;
  
  newI[0] = i0 + dI[2] ;   // z
  newI[1] = i1 + dI[1] ;   // y 
  newI[2] = i2 + dI[0] ;   // x

  
  float facts[3][2] ;

  int ni[3];
  
  for (int i=0; i<3; i++) ni[i] = (int ) newI[i] ;


  for (int i=0; i<3; i++) {
    if (newI[i]<0 || ni[i]+1>= this->dims[i] ) {
      return -1;
    }
  } 


  
  for (int i=0; i<3; i++) {
    facts[i][1] =   newI[i] - ni[i] ;
    facts[i][0] =   1 - facts[i][1] ;
  }
  
  float res = 0.0 ;
  for(int k0=0; k0<2; k0++) {
    for(int k1=0; k1<2; k1++) {
      for(int k2=0; k2<2; k2++) {
	
	int ii =    ( (ni[0]+k0) *this->dims[1]+    (ni[1]+k1)      )*this->dims[2]   +   (ni[2]+k2)    ; 

	float data  =  harvest[ ii ] ;

	if (  isnan(harvest[ ii ]) || harvest[ ii ]<0 ) {
	  return -1; 
	}
	res +=   data * facts[  0 ][ k0 ] * facts[ 1 ][ k1 ]* facts[ 2 ][ k2 ] 	;
      }
    }
  }
  return res; 
}







float * Blob::getQ0s()  {
  float *res = new float [  dims[1]*dims[2] *3];
  
  float py, px ; 

  for(int iy=0; iy<dims[1]; iy++) {
    py = posy + iy - (dims[1]-1)/2 ; 
    for(int ix=0; ix<dims[2]; ix++) {
      px = posx + ix - (dims[2]-1)/2 ;

      geo->getQ0( py, px, res+( iy*dims[2]+ix  )*3  ) ;

    }
  }
  return res; 
}
