#include<math.h>
#include<stdio.h>
#include<string.h>

#include"Geo.hh"
#include<stdexcept>


#include <cstdlib>  // Per getenv
#include <string>   // Per std::string

int getTds2el2HackAxis() {
    // Ottieni il valore della variabile d'ambiente
    const char* value = std::getenv("TDS2EL2_HACK_AXIS");
    
    // Verifica se la variabile è definita
    if (value == nullptr) {
        return -1; // Restituisci -1 se non è definita
    }
    
    // Converte il valore in stringa per una verifica più facile
    std::string valueStr = value;
    
    // Controlla se il valore è '0', '1' o '2'
    if (valueStr == "0") return 0;
    if (valueStr == "1") return 1;
    if (valueStr == "2") return 2;

    return -1; // Restituisci -1 se il valore non è valido
}


//  1
//     cc  ss
//    -ss  cc

// Note : rotation is taken on the right because we are inverting the rotation



#define AA(I,J) A[(I)*3+J]
#define BB(I,J) B[(I)*3+J]


// Here below, I0 I1 I2 are meant to be cyclic, like 1 2 0 or 2 1 0 or 0 1 2 ( right hand rule)
// This ROTATION operation is meant to rotate reference systems. Not vectors. 
// It results in multiplying a matrix  B = dot( A , Transposed_of(rotation_around( I0, anngle)) )
// If init==0, then instead of A, the identity matrix I is used.
// Therefore if init==0 the result ( in B ) is the rotation around I0 by -angle ( mind the minus).
// The minus comes from the transposition, that for orthonormal matrices is equivalente to the inverse.

#define ROTATION(I0,I1,I2)						\
  void Rotation_ ## I0    (float *B, float *A,   float angle, int init=0) { \
  angle = M_PI*angle/180.0f ;					\
									\
float cc = cos(angle);							\
float ss = sin(angle);							\
									\
									\
if (init==0) {								\
  BB(I0,I0)  =  AA(I0,I0);						\
  BB(I1,I0)  =  AA(I1,I0);						\
  BB(I2,I0)  =  AA(I2,I0);						\
									\
  BB(I0,I1)  =          + AA(I0,I1)*  cc   -      AA(I0,I2)*ss;		\
  BB(I1,I1)  =          + AA(I1,I1)*  cc   -      AA(I1,I2)*ss;		\
  BB(I2,I1)  =          + AA(I2,I1)*  cc   -      AA(I2,I2)*ss;		\
									\
  BB(I0,I2)  =            AA(I0,I1)*  ss   +      AA(I0,I2)*cc;		\
  BB(I1,I2)  =            AA(I1,I1)*  ss   +      AA(I1,I2)*cc;		\
  BB(I2,I2)  =            AA(I2,I1)*  ss   +      AA(I2,I2)*cc;		\
 } else {								\
  BB(I0,I0)  =  1.0;							\
  BB(I1,I0)  =  0.0;							\
  BB(I2,I0)  =  0.0;							\
  									\
  BB(I0,I1)  =             0.0   ;					\
  BB(I1,I1)  =             cc    ;					\
  BB(I2,I1)  =             -ss   ;					\
  									\
  BB(I0,I2)  =            0.0  ;					\
  BB(I1,I2)  =            ss   ;					\
  BB(I2,I2)  =            cc   ;					\
 }									\
}


ROTATION(0,1,2);
ROTATION(1,2,0);
ROTATION(2,0,1);




#undef AA
#undef BB

void multiply_3x3( float *c, float *a, float*b) {

  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      c[3*i+j]=0.0;
      for(int k=0; k<3; k++) {
	c[3*i+j] +=  a[3*i +k] * b[ 3*k +j]            ;
      }
    }
  }
}
void transpose_3x3( float *a) {
  float tmp;
  for(int i=0; i<3; i++) {
    for(int j=i+1; j<3; j++) {
      tmp = a[3*i+j];
      a[3*i+j] = a[3*j+i];
      a[3*j+i] = tmp ; 
    }
  }
}



void multiply_vect( float *vb, float *M, float*va) {

  for(int i=0; i<3; i++) {
    vb[i]=0.0;
    for(int k=0; k<3; k++) {
      vb[i] +=  M[3*i +k] * va[ k]            ;
    }
  }
}

void multiply_vectT( float *vb, float *M, float*va) {

  for(int i=0; i<3; i++) {
    vb[i]=0.0;
    for(int k=0; k<3; k++) {
      vb[i] +=  M[3*k +i] * va[ k]            ;
    }
  }
}

void setM( float *M, int code, float psiz) {
  /*
    For code<4, the matrix (in  which is applied to detector coordinates is 
    (-1)**c     0 
    0           (-1)**(c/2)
    This matrix acts on a vector x,y with x running on the fast dimension of the detector.
    For code>=4
    0 (-1)**c 
    (-1)**(c/2) 0
  */

  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      M[3*i+j]=0.0;
    }
  }
  M[0] = 1.0;
  if(code<4)  {
    M[3*1+1 ] = pow(-1,code)   * psiz; 
    M[3*2+2 ] = pow(-1,code/2) * psiz; 
  } else {		       
    M[3*1+2 ] = pow(-1,code   )* psiz; 
    M[3*2+1 ] = pow(-1,code/2 )* psiz; 
  }
}



Geo::~Geo() {
  // printf(" in distruttore Geo, per ora non faccio niente\n");

}



void Rot_dispatcher( int axnum, float * tmp_b, float * tmp_a, float angle )  {

  if( axnum < 0 ) {
    angle = -angle ;
  }
  
  if ( abs(axnum)==1 ) {
    Rotation_0(tmp_b, tmp_a,  angle  , 0);
  } else  if (  abs(axnum)==2 ) {
    Rotation_1(tmp_b, tmp_a,  angle  , 0);
  } else  if (  abs(axnum)==3 ) {
    Rotation_2(tmp_b, tmp_a,  angle  , 0);
  } else {
    throw std::runtime_error(" Rotation code not recognised " ); 
  }
}


void Geo::setPrimRotOfRs(float * primrotA, float *initial, float myphi)  {
  float tmp_a[9];
  float tmp_b[9];

  if( initial == NULL ) {
    memset(tmp_a, 0, 9*sizeof(float) ) ;
    tmp_a[0]=tmp_a[4]=tmp_a[8]=1.0;
  } else {
    memcpy(tmp_a, initial, 9*sizeof(float) ) ;
  }
  
  for( int i = 0; i< n_rot_stage ; i++) {
    float angle = rad_rot_stage[i];
    if( angle != angle) angle = myphi;
    Rot_dispatcher( axnum_rot_stage[i],  tmp_b, tmp_a, -angle ) ;
    memcpy(tmp_a, tmp_b, 9*sizeof(float) ) ;
  }
  memcpy(primrotA, tmp_a, 9*sizeof(float) ) ;

  
  // tmp = Rotation(omega + omega_offset, 2)
  // tmp = np.dot(tmp, Rotation(alpha, 1))
  // tmp = np.dot(tmp, Rotation(kappa, 2))
  // tmp = np.dot(tmp, Rotation(-alpha, 1))
  // tmp = np.dot(tmp, Rotation(beta, 1))
  // tmp = np.dot(tmp, Rotation(phi, 2))
  // tmp = np.dot(tmp, Rotation(-beta, 1))

}


void Geo::setRotOfRs(float * rot, float myphi)  {
  
  float tmp_a[9];
  float tmp_b[9];
  this->setSndRotOfRs(tmp_b);
  
  this->setPrimRotOfRs( tmp_a , NULL , myphi ) ;
 
  multiply_3x3( rot,    tmp_a, tmp_b  )  ;
  
}





void Geo::setSndRotOfRs(float * sndRot)  {
  float tmp_a[9];
  float tmp_b[9];


  int swapper = getTds2el2HackAxis();

  if (this->Quat_or_angles == 1 )  {
    int init=1;
    Rotation_2( tmp_a   , tmp_b,   this->r3  , init);
    init = 0;
    Rotation_1( tmp_b   , tmp_a,   this->r2  , init);
    Rotation_0( sndRot , tmp_b,   this->r1 , init);


    // tmp = Rotation(r3, 2)
    //   tmp = np.dot(tmp, Rotation(r2, 1))
    //   tmp = np.dot(tmp, Rotation(r1, 0))
    // return tmp

  } else {
    double x = this->r1 ; 
    double y = this->r2 ; 
    double z = this->r3 ;

    double d ; 
    d = x*x+y*y+z*z;
      
    if( d>1) {
      d=sqrt(d);
      x/=d;
      y/=d;
      z/=d;
    }
    d = x*x+y*y+z*z ; 
    if( d>1.0)  {
      d=1.0;
    }
    
    
    double  a ; 
    a=sqrt(1.0-d);
    
    sndRot[3 *0 + 0 ] =  a*a +x*x -y*y-z*z ;  sndRot[3 *0 + 1 ] =          2*x*y +2*a*z   ;  sndRot[3 *0 + 2 ] =           2*x*z -2*a*y    ;  
    sndRot[3 *1 + 0 ] =       2*x*y -2*a*z ;  sndRot[3 *1 + 1 ] =    a*a -x*x + y*y-z*z   ;  sndRot[3 *1 + 2 ] =           2*y*z +2*a*x    ;  
    sndRot[3 *2 + 0 ] =       2*x*z +2*a*y ;  sndRot[3 *2 + 1 ] =          2*y*z -2*a*x   ;  sndRot[3 *2 + 2 ] =   a*a -x*x -y*y  + z*z    ;  
    
    // res= np.array(
    // 		  [
    // 		   [  a*a +x*x -y*y-z*z  ,            2*x*y +2*a*z          ,              2*x*z -2*a*y                  ] ,
    // 		   [        2*x*y -2*a*z ,       a*a -x*x + y*y-z*z           ,            2*y*z +2*a*x               ] ,
    // 		   [      2*x*z +2*a*y   ,             2*y*z -2*a*x          ,     a*a -x*x -y*y  + z*z             ] 
    
    // 		   ]
    // 		  )
    
    
  }

  if ( swapper >=0) {
    for(int i=0; i<3; i++) {
      sndRot[3 *i + swapper ] = - sndRot[3 *i + swapper ] ;
    }
  }
  
}


void printMatrix( float *M  ) {
  for(int i=0; i<3; i++) {
    printf(" %e        %e     %e\n", M[3*i+0],M[3*i+1],M[3*i+2] ) ;
  }
  
}
void printVector( float *M  ) {
  for(int i=0; i<1; i++) {
    printf(" %15.10e     %15.10e     %15.10e\n", M[3*i+0],M[3*i+1],M[3*i+2] ) ;
  }
  
}
void printVector( double *M  ) {
  for(int i=0; i<1; i++) {
    printf(" %15.10e        %15.10e     %15.0e\n", M[3*i+0],M[3*i+1],M[3*i+2] ) ;
  }
  
}

void Geo::getQ0( float iy, float ix,  float *q0 ) {
  float p[3] ;
  float prot[3];
  // @@@ This method is not yet adapted to the new general geometrical scheme.
  // it has to be adapted or the simulation are wrong
  p[0] = + this->dist ;  // attento al segno
  p[1] =  iy - this->det_origin_Y ; 
  p[2] =  ix - this->det_origin_X ; 
  
  multiply_vect( prot , this->prec.DETM,  p  ) ;

  double norm = prot[0]*prot[0]+prot[1]*prot[1]+prot[2]*prot[2];
  norm = sqrt(norm);

  
  prot[0] /= norm ; 
  prot[1] /= norm ; 
  prot[2] /= norm ; 
  
  q0[0] = (prot[0] - this->prec.P0norm[0] )/this->lmbda ; 
  q0[1] = (prot[1] - this->prec.P0norm[1] )/this->lmbda ; 
  q0[2] = (prot[2] - this->prec.P0norm[2] )/this->lmbda ;

}


void Geo::precalculate()  {

  float tmp_a[9];
  float tmp_b[9];
  
  { // beam
    memset(tmp_a, 0, 9*sizeof(float) ) ;

    tmp_a[0] = beam_orientation[0];
    tmp_a[1] = beam_orientation[1];
    tmp_a[2] = beam_orientation[2];

    for( int i = 0; i< n_rot_beam ; i++) {
      float angle = rad_rot_beam[i];
      // because the dispatcher operates on the right of tmp_a with the transposed rotation matrix.
      Rot_dispatcher( axnum_rot_beam[i],  tmp_b, tmp_a, angle ) ;

      memcpy(tmp_a, tmp_b, 9*sizeof(float) ) ;
    }
    // take the row not the column
    // ( we have used Rot_dispatcher which is done for the rotation of the rference system
    // and therefore has operated on the right
    this->prec.P0norm[0]  =     tmp_a[3*0+0]; //* this->dist ; 
    this->prec.P0norm[1]  =     tmp_a[3*0+1]; //* this->dist         ; 
    this->prec.P0norm[2]  =     tmp_a[3*0+2]; //* this->dist            ; 
  }
  { 
    memset(tmp_a, 0, 9*sizeof(float) ) ;
    for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {
	// put the vector on the rows, not on the columns
	// Once again we use the dispatcher whih works in transposed mode
    	tmp_a[ j*3+i] = this->detector_orientations[ j*3+i  ];
      }
    }
 
    for( int i = 0; i< n_rot_dect ; i++) {
      float angle = rad_rot_dect[i];
      Rot_dispatcher( axnum_rot_dect[i],  tmp_b, tmp_a, angle ) ;
      memcpy(tmp_a, tmp_b, 9*sizeof(float) ) ;
    }


    
    {
      // this setM is only for compatibility mode with the old version
      // which was using the swapping of detector coordinates.
      // In practice it will be effective only with the tutorial example ( orientation_code=7)
      // For the future it will be always orientation_code = 0 ( identity)
      // However the information pixel_size is passed here
      // and this has to be kept
      setM( tmp_b, this->orientation_codes , this->pixel_size) ;
    }
  }

  {
   transpose_3x3( tmp_a   )  ;
  }
  multiply_3x3(this->prec.DETM  , tmp_a, tmp_b  ) ;

  
    // # """ Primary projection of pixel coordinates (X,Y) to the reciprocal space. """
    // B = Rotation(b2, 1) # Beam tilt matrix
    // p0 = np.dot(B, [ dist, 0, 0 ])
    //   MD = [MD0,MD1, MD2, MD3, MD4 , MD5, MD6 , MD7 ][paramsExp.orientation_codes]
    // def DET(theta, theta_offset, d2, d1):
    // # """ 
    // # Rotation matrix for the detector 
    // # theta is the nominal theta value and d1,D2 are the tilts of detector
    // # """
    // tmp = Rotation(theta + theta_offset, 2)
    // tmp = np.dot(tmp, Rotation(d2, 1))
    // tmp = np.dot(tmp, Rotation(d1, 0))
    // return tmp
    //     P_total_tmp[ 1:3]=  (np.tensordot(P_total_tmp[1:3]  * np.array(paramsExp.pixel_size)    ,MD , axes=([0], [1])))
    // P_total_tmp = np.tensordot(P_total_tmp, DET(paramsExp.theta, paramsExp.theta_offset, paramsExp.d2, paramsExp.d1), axes=([0], [1]))


  this->prec.free();
  this->getCellVectors(  this->prec.Brav, this->prec.Brill ) ; 


  this->prec.brillmax=0.0;
  for(int i=0; i<3; i++) {
    float sum=0.0;
    for(int k=0; k<3; k++) {
      sum += this->prec.Brill[i*3+k   ] * this->prec.Brill[i*3+k] ; 
    }
    if ( this->prec.brillmax < sqrt( sum  ) ) {
      this->prec.brillmax = sqrt( sum  ) ;
    }
  }
}


void Arb_Rot( float angle, float *rot_axis, float *res, float *v) {
  float *u;
  float tmp[3];

  angle = angle *M_PI/180.0;
  
  if( res==v) {
    u= tmp;
  } else {
    u = res;
  }
  
  float x,y,z;
  x = rot_axis[0];
  y = rot_axis[1];
  z = rot_axis[2];

  float rot_mat[] = {
    1 + (1-cos(angle))*(x*x-1)         , -z*sin(angle)+(1-cos(angle))*x*y ,     y*sin(angle)+(1-cos(angle))*x*z ,
    z*sin(angle)+(1-cos(angle))*x*y , 1 + (1-cos(angle))*(y*y-1)          ,    -x*sin(angle)+(1-cos(angle))*y*z ,
    -y*sin(angle)+(1-cos(angle))*x*z,  x*sin(angle)+(1-cos(angle))*y*z,   1 + (1-cos(angle))*(z*z-1) 
  };
  multiply_vect( u,  rot_mat, v) ;
  if( res==v) {
    for(int i=0; i<3; i++) {
      res[i] = u[i];
    }
  }
}

void sscal( float f, float *x) {
  for(int i=0; i<3; i++) {
    x[i] *= f; 
  }
}

int ciclicity(int a,int b,int c) {
  if( a==b ||  a==c || c==b) {
    return 0;
  }
  
  if(  (b-a)*(c-b)*(a-c) <0  ){
    return 1;
  } else {
    return -1   ;
  }
}


void  totalAntisymm( float * Brill,  float *Brav)  {


  float antisymm[3][3][3];
  
  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      for(int k=0; k<3; k++) {
	antisymm[i][j][k] =   ciclicity(i,j,k) ; 
      }
    }
  }

  float cellVolume = 0.0 ;


  float *xa = Brav+0;
  float *xb = Brav+3;
  float *xc = Brav+6;
  
  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      for(int k=0; k<3; k++) {
	cellVolume +=      antisymm[i][j][k]  * xa[i]*xb[j]*xc[k] ; 
      }
    }
  }
  
  memset(Brill, 0, 9*sizeof(float));
  
  
  for(int i=0; i<3; i++) {
    for(int j=0; j<3; j++) {
      for(int k=0; k<3; k++) {
	for(int i0=0; i0<3; i0++) {
	  for(int j0=0; j0<3; j0++) {
	    for(int k0=0; k0<3; k0++) {
	      Brill[ 3*i0 +i  ] += antisymm[i0][j0][k0] * antisymm[i][j][k]  * Brav[ 3*j0 +j  ]  * Brav[ 3*k0 +k  ] /cellVolume/2; 
	    }
	  }
	}
      }
    }
  }
}

float arcCos( float *u, float *v) {

  float a = u[0]*v[0]+u[1]*v[1]+u[2]*v[2];
  float nu = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
  float nv = sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);

  a=a/nu/nv ;
  if (a> 1) a =  1;
  if (a<-1) a = -1;
  
  return acos(  a ) ;
}

float norm(float *u)  {
  float nu = sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
  return nu;
}

void Geo::getCellVectors(  float *&Brav, float * &Brill )  {


  float xa[] = {1.0,0.0,0.0 };
  float xb[3];
  float xc[3];

  float Z[] = {0.0,0.0,  1.0}; 
  float Y[] = {0.0,1.0,  0.0}; 
  float X[] = {1.0,0.0,  0.0}; 

 
  Arb_Rot(  this->aCC,      Z   , xb,  X );
  Arb_Rot(  -this->aBB,     Y   , xc,  X ); 
  

  float sa;
  sa = ( cos( this->aBB*M_PI/180 )*cos( this->aCC*M_PI/180 )-cos( this->aAA*M_PI/180 ));
  sa = sa/(sin( this->aBB*M_PI/180 )*sin( this->aCC*M_PI/180 ));
  float omegaa = asin( sa  ) ; 

  Arb_Rot( omegaa  ,     X   , xc,  xc ); 

  sscal(AA, xa) ;
  sscal(BB, xb) ;
  sscal(CC, xc) ;

  Brill = new float [9];
  Brav  = new float [9];

  memcpy( Brav+0, xa, 3*sizeof(float)) ; 
  memcpy( Brav+3, xb, 3*sizeof(float)) ; 
  memcpy( Brav+6, xc, 3*sizeof(float)) ; 

  memset(Brill, 0, 9*sizeof(float));


  totalAntisymm(  Brill,  Brav ) ;
  

  
  float alphastar   =   arcCos(  Brill+3*1,  Brill+3*2   ) *180/M_PI;
  float betastar    =   arcCos(  Brill+3*0,  Brill+3*2   ) *180/M_PI;
  float gammastar   =   arcCos(  Brill+3*0,  Brill+3*1   ) *180/M_PI;

  float Astar   =   norm(  Brill+3*0);
  float Bstar   =   norm(  Brill+3*1);
  float Cstar   =   norm(  Brill+3*2);


  Arb_Rot(  0.0      ,      Z   , xa,  X );
  Arb_Rot(  gammastar    ,      Z   , xb,  X );
  Arb_Rot( -betastar     ,      Y   , xc,  X ); 
  
  sa = ( cos( betastar*M_PI/180 )*cos( gammastar*M_PI/180 )-cos( alphastar*M_PI/180 ));
  sa = sa/(sin( betastar*M_PI/180 )*sin( gammastar*M_PI/180 ));
  omegaa = asin( sa  );
  
  Arb_Rot( omegaa  ,     X   , xc,  xc ); 
  
  sscal(Astar, xa) ;
  sscal(Bstar, xb) ;
  sscal(Cstar, xc) ;

  memcpy( Brill+0, xa, 3*sizeof(float)) ; 
  memcpy( Brill+3, xb, 3*sizeof(float)) ; 
  memcpy( Brill+6, xc, 3*sizeof(float)) ; 

  memset(Brav , 0, 9*sizeof(float));


  
  totalAntisymm(    Brav, Brill ) ;

}




