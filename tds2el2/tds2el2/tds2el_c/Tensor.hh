#ifndef TENS_HH

#define TENS_HH

#include<math.h>
#include<stdio.h>
class Tensor {
public:

  Tensor();
  void clear();

  void setComponents( int nvars, float *components);
  void setVars      ( int nvars, float *vars);
  void preCalculate(float *Q, float *myfreqs, float *myvects);

  
  ~Tensor();
  
  
  int nvars;
  float * components;
  
  float tens[36];
  
  
  int npolar;
  int nazimuth;
  float *freqs ;
  float *vects ; 


  inline void addtoInterp(int npos, float * weights,  float * myfreqs,  float *  myvects , int ip, int ia,  float w  )  {
    weights[npos] = w;
    for(int i=0; i<3; i++) {
      myfreqs [npos*3+i] = this->freqs[(ip*nazimuth + ia )*3  +i];
    }
    for(int i=0; i<9; i++) myvects [npos*3*3+i] = this->vects[(ip*nazimuth + ia )*9  +i];
  }
  
  inline void getInterpData( float *Q, float *weights,
			     float *myfreqs, float *myvects){
    
    double sum = Q[0]*Q[0]+ Q[1]*Q[1]+ Q[2]*Q[2];
    float scal = sqrt(sum);
    
    float myq[3];
   for(int i=0; i<3; i++) myq[i] = Q[i]/scal ;

    
    float mypol = acosf( myq[2] ) ;
    float myaz  = atan2f( myq[1], myq[0]  ) ;
    
    if(myaz<0) {
      myaz = myaz+M_PI;
      mypol = ((float) M_PI)-mypol ; 
    }
    if ( mypol >= (float) M_PI || mypol<0  ) {
      mypol=0;
    }
    if( myaz <0  ) {
      myaz=0;
    }
    if( myaz >=  (float) M_PI ) {
      myaz=  ((float) M_PI )*(0.999999)    ;
    }
    
    float fpol =  (  mypol*npolar   / M_PI   ) ; 
    float faz  =  (  myaz *nazimuth / M_PI   ) ; 
    
    int ipol0 = (int)  fpol  ;
    int iaz0  = (int)   faz  ; 
    
    fpol = fpol -ipol0;
    faz  = faz  -iaz0 ;
    
    int ipol1 = ipol0+1  ;
    int iaz1  = iaz0 +1  ;
    
    if(ipol1 == this->npolar) ipol1 = 0;
    
    int invertpol = 0;
    
    if(iaz1==this->nazimuth) {
      iaz1 = 0;
      invertpol=1;
    }
    addtoInterp(0,  weights,   myfreqs,   myvects , ipol0, iaz0, (1-fpol)*(1-faz)   ) ;
    addtoInterp(1,  weights,   myfreqs,   myvects , ipol1, iaz0, (  fpol)*(1-faz)   ) ;
    
    if(invertpol) {
      ipol0 = npolar - ipol0;
      if(ipol0==npolar) ipol0 = 0;
      ipol1 = npolar - ipol1;
      if(ipol1==npolar) ipol1 = 0;
    }
    
    
    addtoInterp(2,  weights,   myfreqs,   myvects , ipol0, iaz1, (1-fpol)*(  faz)   ) ;
    addtoInterp(3,  weights,   myfreqs,   myvects , ipol1, iaz1, (  fpol)*(  faz)   ) ;
    
    for(int i=0; i<4*3; i++) {
      myfreqs[i] *= scal ; 
    }
      
  }  
  
  
  

};

#endif
