from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from . import ui
from silx.gui import qt

import numbers
import re
import yaml
import yaml.resolver
from six import u
import collections

import h5py
import sys

yaml.resolver.Resolver
Resolver = yaml.resolver.Resolver
Resolver.add_implicit_resolver(
        u'tag:yaml.org,2002:float',
        re.compile(u(r"""^(?:[-+]?(?:[0-9][0-9_]*)(\.[0-9_]*)?(?:[eE][-+]?[0-9]+)?
                    |\.[0-9_]+(?:[eE][-+][0-9]+)?
                    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\.[0-9_]*
                    |[-+]?\.(?:inf|Inf|INF)
                    |\.(?:nan|NaN|NAN))$"""), re.X),
        list(u'-+0123456789.'))

#################################################################
##  THIS redefinition of yaml is used to keep the entry ordering
## when accessing yamlData keys
##
#yaml_anydict.py
import yaml
from yaml.representer import Representer
from yaml.constructor import Constructor, MappingNode, ConstructorError
def dump_anydict_as_map( anydict):
    yaml.add_representer( anydict, _represent_dictorder)
def _represent_dictorder( self, data):
    return self.represent_mapping('tag:yaml.org,2002:map', data.items() )
class Loader_map_as_anydict( object):
    'inherit + Loader'
    anydict = None      #override
    @classmethod        #and call this
    def load_map_as_anydict( klas):
        yaml.add_constructor( 'tag:yaml.org,2002:map', klas.construct_yaml_map)
    'copied from constructor.BaseConstructor, replacing {} with self.anydict()'
    def construct_mapping(self, node, deep=False):
        if not isinstance(node, MappingNode):
            raise ConstructorError(None, None,
                    "expected a mapping node, but found %s" % node.id,
                    node.start_mark)
        mapping = self.anydict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError as exc:
                raise ConstructorError("while constructing a mapping", node.start_mark,
                        "found unacceptable key (%s)" % exc, key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping
    def construct_yaml_map( self, node):
        data = self.anydict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)
        
class myOrderedDict (collections.OrderedDict):
    def __setitem__(self,a,b):
        ## print "cucu",a,b
        if type(a)==type("") and a in self:
            self[a+"_tagkajs"]=b 
        else:
            ## print super(myOrderedDict, self)
            super(myOrderedDict, self).__setitem__(a,b )
            
def cleaned(key):
    while key[-8:]=="_tagkajs":
        key=key[:-8]
    return key
      
dictOrder = myOrderedDict

class Loader( Loader_map_as_anydict, yaml.Loader):
    anydict = dictOrder
Loader.load_map_as_anydict()
dump_anydict_as_map( dictOrder)
##
## END of yaml redefinition
###############################



# class Parameters(qt.QObject):
class Parameters(qt.QObject):
    classes = {'x':float}
    slots = list(classes.keys())
    entr={}
    entries_reversed = {}
    def __init__(self):
        super(Parameters, self).__init__()  
     
        for name in self.slots:
            object.__setattr__(self,name,None)
        for name,qgui in self.entries.items():
            self.entries_reversed[qgui] = name
            if  isinstance(  qgui  , qt.QLineEdit ):
                qgui.editingFinished.connect(self.lineEditFinished)
            elif  isinstance(  qgui  , qt.QComboBox ):
                qgui.activated.connect(self.ComboEditFinished)
            elif  isinstance(  qgui  , qt.QCheckBox ):
                qgui.stateChanged.connect(self.checkBoxChanged)
            elif  isinstance(  qgui  , qt.QRadioButton ):
                qgui.toggled.connect(self.checkBoxChanged)

    def checkBoxChanged(self):
        print(" in checkBoxChanged" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        tmp = qobj.isChecked()
        try:
            tmp=int(tmp)
        except:
            pass
        setattr(self, name, tmp)
        
    def ComboEditFinished(self):
        print(" in ComboEditFinished" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        txt = str(qobj.currentText())
        tmp = txt
        try:
            tmp=int(tmp)
        except:
            pass

        setattr(self, name, tmp)


    def lineEditFinished(self):
        print(" in lineEditFinished" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        txt = str(qobj.text()).strip()
        if txt=="":
            txt="None"
        tmp = txt
        if self.classes[name]  is not str:
            print( " EXECUTING ", "tmp="+txt)
            d_globals= {}
            d_locals = {}
            exec("tmp="+txt,d_globals, d_locals)
            tmp = d_locals["tmp"]
            if tmp == "":
                tmp = None
            if tmp is not None:

                if not isinstance(  tmp , enlarge_possible_classes(self.classes[name]) ):
                    msg = " Error: the value given to variable " + name +" should be of class "+str(self.classes[name])
                    msg=msg+ " Now it is of type " + str(type(tmp))
                    qobj.setText("None")
                    raise Exception( msg)
        setattr(self, name, tmp)


    
    def __setattr__(self, name,val):

        if name != "__METAOBJECT__" :

            if not( name in self.slots):
                raise Exception(" for object %s name %s is not valid "%( self, name))


            
            if val is not None and not isinstance( val, enlarge_possible_classes(self.classes[name])    ) :
                raise Exception(" for object %s name %s property must be of the class %s. It was of type %s instead "%( self, name,self.classes[name] , type(val)   ))
        object.__setattr__(self,name,val)
    def setGui(self, entries):
        object.__setattr__(self,"entries",entries)

    def evaluate(self, txt):
        print(" EVALUATE ", txt)
        glob ={}
        locs ={"txt":txt}
        exec("tok=%s"%txt, glob, locs)
        return locs["tok"]

    def takeFromOwnGui(self):
        for name, gui_input in self.entries.items():
            
            if isinstance( gui_input , qt.QLineEdit):
                tmp = str(gui_input.text()) .strip()
                if tmp=="": tmp = "None"
                if self.classes[name] != str:
                    self.__setattr__(name, self.evaluate(   tmp             ))
                else:
                    self.__setattr__(name,    tmp      )
            elif   isinstance( gui_input , qt.QComboBox)   :
                tmp = str(gui_input.currentText()) .strip()
                if tmp=="": tmp = "None"
                try:
                    tmp=int(tmp)
                except:
                    pass
                
                self.__setattr__(name,   tmp        )
            elif   isinstance( gui_input , qt.QCheckBox) or  isinstance( gui_input , qt.QRadioButton)  :
                tmp = gui_input.isChecked()
                tmp=int(tmp)
                self.__setattr__(name,   tmp        )


    def exportToOwnGui(self):
        for name in self.slots:
            print( " EXPORT ", name , str(getattr(self, name)) )
            print( self.entries)
            if name in self.entries:
                if isinstance( self.entries[name], qt.QLineEdit   ):
                    self.entries[name].setText( str(getattr(self, name)) )
                elif isinstance( self.entries[name], qt.QComboBox) :
                    ind = self.entries[name].findText( str(getattr(self, name)) )
                    self.entries[name].setCurrentIndex( ind )
                elif isinstance( self.entries[name]  , qt.QCheckBox) or  isinstance( self.entries[name] , qt.QRadioButton)   :
                    try:
                        tmp = int(getattr(self, name))
                    except:
                        tmp = 0
                    self.entries[name].setChecked( tmp)
                    

    def takeFromYamlText(self,s):
       data = yaml.load(s, Loader=Loader)
       for name in self.slots:
           tok = data[name]
           if self.classes[name] != str:
               self.__setattr__(name, self.evaluate(   tok       )             )
           else:
               self.__setattr__(name,  tok      )
               
               
    def takeFromDict(self,data):
       for name in self.slots:
           if name in data:
               tok = data[name]
           else:
               tok = None
           print( name, tok )
           if self.classes[name] != str:
               self.__setattr__(name, self.evaluate(   tok       )             )
           else:
               self.__setattr__(name,   tok  )
               

    def exportToYamlText(self,prepend):
       s=""
       for name in self.slots:
           s=s+prepend+name+" : " + str(   getattr(self, name)     )+"\n"
       return s

    def exportToDict(self):
       s={}
       for name in self.slots:
           s[name]=  getattr(self, name)     
       return s
   
    def importFromDict(self, dizio):
       for name in self.slots:
           if name in dizio:
               self.__setattr__( name, dizio[name]  )     
       return s

    def getMyParsAsObject(self):
        dizio = self.exportToDict()
        mypars =  type('MyObjectPourDecrireLesPars', (object,), dizio)
        return mypars

def enlarge_possible_classes(myclass):
    possibles = (myclass, )
    if myclass is float:
        possibles = (float, int)
    return possibles
