from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


def setCyFromDict( OB,       dizio, lista=None )            :
    for key,ob in dizio.items():
        if lista is  None or key in lista:
            if ob is not None:
                if(hasattr(OB, key) ):
                    setattr( OB, key, ob)
                else:
                    print(f"key {key} missing from {OB} in setCyFromDict" ) 

