import sys      
from six import StringIO
import traceback

separator = '-' * 80
def excepthook(type, value, tracebackobj):
    print( " HOOK ")
    tbinfofile = StringIO()
    traceback.print_tb(tracebackobj, None, tbinfofile)
    # traceback.print_exc()
    # traceback.print_stack(  tracebackobj , file=tbinfofile)    
    tbinfofile.seek(0)
    tbinfo = tbinfofile.read()
    errmsg = '%s: %s' % (str(type), str(value))
    sections = [separator, errmsg, separator, tbinfo]
    msg = '\n'.join(sections)
    print(" -----------------  +++++++++++++++++++ ")
    print( msg)
    return

sys.excepthook = excepthook
l=["O"]
atom_name = "b'O'"


l.index(atom_name.replace("b'","").replace("'",""))


