from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import math
import copy
from scipy import optimize
import os
import sys
import getopt

from .Parameters import  Parameters, Loader  # Loader must be loaded before using it in dataset
from six import u
import re

##########################################################
import yaml
import yaml.resolver
Resolver = yaml.resolver.Resolver

#~~~~# the following are yaml technicalities which allow to directly transform yaml files into python numerical objects
Resolver.add_implicit_resolver(
        u'tag:yaml.org,2002:float',
        re.compile(u(r"""^(?:[-+]?(?:[0-9][0-9_]*)(\.[0-9_]*)?(?:[eE][-+]?[0-9]+)?
                    |\.[0-9_]+(?:[eE][-+][0-9]+)?
                    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\.[0-9_]*
                    |[-+]?\.(?:inf|Inf|INF)
                    |\.(?:nan|NaN|NAN))$"""), re.X),
        list(u'-+0123456789.'))
##########################################################


dirname = os.path.dirname(__file__)


    
opts, args = getopt.getopt(sys.argv[1:],"c:")

CONFIG=None
original_configuration = False
if len(opts):
    for op in opts:
        if '-c' == op[0]:
            CONFIG = yaml.load(open(op[1],"r"), Loader)
            
if CONFIG is None:
    original_configuration = True
    CONFIG = yaml.load(open(os.path.join(dirname,"CONFIG.yaml"),"r"), Loader)


print (CONFIG)
class Holder:
    """ This is the sample holder class.
    Its job consists in rotating  the reference system from the lab to the possibly aligned sample one.
    It can reproduce any geometry as dictated by "config" argument of __init__
    """
    def __init__(self,config):
        # the holder_dict contains all the specifications for the holder
        holder_dict = config["setup"]["holder"]
        self.axis=[]  # will be a list  of 3d vectors, taken as rotation axis.
        # In principle the Arb_Rot function accept any axis in input, not even normalised
        # However the C++ part implementation still relies on each vector of self.axis
        # to contain only one non-zero element which can be 1 or -1
        
        self.angles=[] # this will be a list of names, as given in the yaml description.
        # The name appears in the parameters list. When a rotation is asked the numerical
        # value is obtained from the parameters object asking for the member having that given name
        
        self.variables = [] # This account for the list of variables, each accounted only once.
        # In fact the same variable can appear twice or mode in the self.angles list, when
        # the same parameter governes more than one axis.
        
        while True:
            # considering all the rotation expressed
            # in the description of the holder
            if "rot" in holder_dict:
                holder_dict =  holder_dict["rot"] 
                myaxis = np.array( holder_dict["axis"],"f" )
                myangle = holder_dict["angle"]
                self.axis.append  ( myaxis    )
                self.angles.append( myangle )
                if isinstance( myangle,str ) :
                    # account it only once each variable name.
                    # It will be put in the parameters objects
                    if myangle not in self.variables:
                        self.variables.append( myangle   )
            else:
                break
                    
        assert(self.angles[-1] == "phi" )
                    
    def Prim_Rot_of_RS(self, params):
        # This gives the matrix which rotates the laboratory reference system
        # into the sample holder reference system.
        # Its invers will be used to rotate a Q given in the laboratory system
        # into the transform Q given in the sample holder system.
        Res = np.array([ [1,0,0],[0,1,0],[0,0,1]],"f")
        for  ax,an in zip(self.axis, self.angles):
            if isinstance( an , (float,int) ):
                da = an
            else:
                da =  getattr(params, an)
            myrot = Arb_Rot( da, ax      )
            Res = np.dot( Res, myrot)
            
        # myrot = Arb_Rot( params.phi  ,  np.array( [   0,0,-1] ,"f"  )     )
        # Res = np.dot( Res, myrot)

        return Res
    
class Detector:
    def __init__(self,config):
        # the detector_dict contains all the specifications for the holder
        detector_dict = config["setup"]["detector"]
        # same logic applies here as for the holder, concerning axis, angles, and variables
        self.axis=[]
        self.angles=[]
        self.variables = []

        # Direction must be perpendicular to the two orientation axis , and must be
        # aligned with the beam, same direction as photons, when all the angle of the detector
        # (the angles here named) are 0. Direction must be thinked of as the line starting from the sample and perpendicular to the detector.
        # The intersection point corresponds to the tds2el2 parameters det_origin_X and det_origin_Y.
        # The system formed by thre axis : direction, orientation[0], orientation[1] is rotated by the rotation specified in the yaml file
        # 
        self.direction = np.array(  detector_dict["direction"]       ,"f")

        # the orientation is composed of two 3D axis, the first for the x direction of the detector
        # the other for the vertical y which in practical cases does not need o be vertical
        # but aligned as specified in yaml.
        self.orientation = np.array(  detector_dict["orientation"]       ,"f")
        
        while True:
            # considering all the rotation expressed
            # in the description of the Detector
            if "rot" in detector_dict:
                detector_dict =  detector_dict["rot"] 
                myaxis = np.array( detector_dict["axis"],"f" )
                myangle = detector_dict["angle"]
                self.axis.append  ( myaxis    )
                self.angles.append( myangle )
                if isinstance( myangle,str ) :
                    if myangle not in self.variables:
                        self.variables.append( myangle   )
            else:
                break
            
    def DET(self, params):
        """ This function returns a matrix which is the identity
        when all the rotation angle are zero.
        In the general case it is the rotation  matrix of the detector
        """
        Res = np.array([ [1,0,0],[0,1,0],[0,0,1]],"f")
        for  ax,an in zip(self.axis, self.angles):
            if isinstance( an , (float, int) ):
                da = an
            else:
                da =  getattr(params, an)
            myrot = Arb_Rot( da, ax      )
            Res = np.dot( Res, myrot)

        return Res


class Beam:
    def __init__(self,config):
        """ This class gives the beam direction
        in particular minusP0 is used which gives 
        minus p0 (p0 is the beam direction), so when it is summed to the exit direction, and the result divided by lambda 
        we get DQ given by the sample
        """
        beam_dict = config["setup"]["beam"]
        self.axis=[]
        self.angles=[]
        self.variables = []

        # this direction must be aligned with the beam
        self.direction = np.array(  beam_dict["direction"]       ,"f")
        
        while True:
            if "rot" in beam_dict:
                beam_dict =  beam_dict["rot"] 
                myaxis = np.array( beam_dict["axis"],"f" )
                myangle = beam_dict["angle"]
                self.axis.append  ( myaxis    )
                self.angles.append( myangle )
                if isinstance( myangle,str ) :
                    if myangle not in self.variables:
                        self.variables.append( myangle   )
            else:
                break
            
    def minusP0(self, params):
        res = np.array(np.array(self.direction,"f"),"f")
        for  ax,an in zip(self.axis[::-1], self.angles[::-1]):
            if isinstance( an , (float, int) ):
                da = an
            else:
                da =  getattr(params, an)
            myrot = Arb_Rot( da, ax      )
            res = np.dot(  myrot,res)
        norm = np.linalg.norm(res)
        return -res/norm   # minus, so when it is summed to the exit direction, and the result divided by lambda we get DQ given by the sample


    
HOLDER   = Holder  (CONFIG)
DETECTOR = Detector(CONFIG)
BEAM = Beam(CONFIG)
    
                    
###############
##
## the MD swap the detector axis. MD0 is always used now, the other are left so that the example of the viedo
## can still run with the new code.
##
MD0 = np.array([[1, 0], [0, 1]  ], dtype=np.int32)
MD1 = np.array([[-1, 0], [0, 1] ], dtype=np.int32)
MD2 = np.array([[1, 0], [0, -1] ], dtype=np.int32)
MD3 = np.array([[-1, 0], [0, -1]], dtype=np.int32)
MD4 = np.array([[0, 1], [1, 0]  ], dtype=np.int32)
MD5 = np.array([[0, -1], [1, 0] ], dtype=np.int32)
MD6 = np.array([[0, 1], [-1, 0] ], dtype=np.int32)
MD7 = np.array([[0, -1], [-1, 0]], dtype=np.int32)

MDs_by_code = [MD0, MD1, MD2, MD3, MD4,  MD5, MD6, MD7]


def get_tds2el2_hack_axis():
    # Leggi la variabile d'ambiente
    value = os.getenv('TDS2EL2_HACK_AXIS')
    
    # Converte il valore in un intero se possibile, altrimenti assegna None
    if value in ['0', '1', '2']:
        print(" HACK value" )
        return int(value)
    else:
        print(" HACK NO")
        return None


def modulus(a):
    # this could be replace by linalg.norm
    return np.sqrt((a*a).sum())


def ciclicity(a,b,c):
  # this is the kroenecker symbol
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1   

def arcCos(   a,b   ):
    c = (a*b).sum()
    c=c/modulus(a)
    c=c/modulus(b)
    return np.arccos(c)
    

def anyIsNone(L):
    # could be replaced. There is a numpy method doing this
    for l in L:
        if l is None:
            return True
    return False

def cell_2_cell( assi):
    assi = np.array(assi)
    if anyIsNone(assi):
        return None,None
    invA = np.linalg.inv(assi.T)
    norms = [np.linalg.norm(a) for a in invA]
    return invA, norms

def Arb_Rot(angle, rot_axis):
    # An arbitrary rotation around an axis.
    # It respects the usaul convention. A positive angle
    # means turning a clockwise rotation around the axis lookin in the direction of the axis.

    angle = np.radians(angle)
    assert(len(rot_axis)==3)

    rot_axis = np.array(rot_axis)/(  np.linalg.norm(  np.array(rot_axis,"d")))
    
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat


def fromBrillToBravaisVectors(abc,Aabc):
    AA,BB,CC = abc
    aAA, aBB, aCC = Aabc

    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( aCC,np.array([0.0,0.0,  1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-aBB,np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( aBB*math.pi/180 )*math.cos( aCC*math.pi/180 )-math.cos( aAA*math.pi/180 ))
    sa = sa/(math.sin( aBB*math.pi/180 )*math.sin( aCC*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 
    
    xa  = xa*AA
    xb  = xb*BB
    xc  = xc*CC
    cellvectors = np.array([ xa,xb,xc ] )
    
    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Bravaisvectors=np.zeros([3,3], np.float32)
    Bravaisvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Bravaisvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Bravaisvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alpha   =   arcCos(  Bravaisvectors[1],  Bravaisvectors[2]   ) *180/math.pi
    beta    =   arcCos(  Bravaisvectors[0],  Bravaisvectors[2]   ) *180/math.pi
    gamma   =   arcCos(  Bravaisvectors[0],  Bravaisvectors[1]   ) *180/math.pi

    A,  B,  C  =  map( modulus,  [ Bravaisvectors[0],  Bravaisvectors[1],  Bravaisvectors[2] ]   )

    return (A,B,C), (alpha,beta, gamma)

    
def getCellVectors(abc,Aabc):
    AA,BB,CC = abc
    aAA, aBB, aCC = Aabc

    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( aCC,np.array([0.0,0.0,  1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-aBB,np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( aBB*math.pi/180 )*math.cos( aCC*math.pi/180 )-math.cos( aAA*math.pi/180 ))
    sa = sa/(math.sin( aBB*math.pi/180 )*math.sin( aCC*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 
    
    xa  = xa*AA
    xb  = xb*BB
    xc  = xc*CC
    cellvectors = np.array([ xa,xb,xc ] )
    
    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   )
 
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( -betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/(math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar

    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume

    # Brillvectors[:] *=-1
    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")

def getDescriptionForAxis_fromReciprocalAxis(  assi, norms):

    invStars = [None]*3
    myBrill = np.array(assi)
    for i,(v,n) in enumerate(zip(assi, norms)):
        if v is None: continue
        d = 1.0/ n
        v[:]=v*d
        invStars[i] = d

    anglesStar=[None]*3
        
    for i,v in enumerate(assi):
        V1 = assi[(i+1)%3]
        V2 = assi[(i+2)%3]
        if V1 is not None and V2 is not None:
            angle = np.rad2deg( np.arccos((V1*V2).sum()) )
            anglesStar[i] = angle
                
    myBravais, rnorms = cell_2_cell(myBrill)
    
    abc = [None]*3
        
    if myBravais is not None:
        rassi  = np.array(myBravais)

        for i,(v,n) in enumerate(zip(rassi, rnorms)):
            d = 1.0/ n
            v[:]=v*d
            abc[i] = n
            

    anglesReal=[None]*3
            
    if myBravais is not None:
        for i,v in enumerate(rassi):
            V1 = rassi[(i+1)%3]
            V2 = rassi[(i+2)%3]
            angle = np.rad2deg( np.arccos((V1*V2).sum()) )
            anglesReal[i] = angle
            
    return invStars, anglesStar, myBravais,  abc, anglesReal

        
def peaks2Q(picchi, params_a, npoints=0, alsoOriginal=False) :
    params = copy.deepcopy(params_a)
    qtotal=[]
    dists = []
    for  picco in picchi :
        iim = picco[0]
        pos = picco[1:]
        params.phi = params.start_phi + iim*params.angular_step
        posY, posX = pos
        Qfin = get_Qfin_nosample(get_Q0bypos(posY, posX , params ) , params)
        qtotal.append(Qfin)
        dists.append(  (Qfin*Qfin).sum() )
        
    if npoints:
        inds = np.argsort(dists)
        qtotal=np.array(qtotal)[  inds[:npoints] ]
        picchiOriginal = [  picchi[k] for k in    inds[:npoints]   ]
        
    else:
        
        qtotal=np.array(qtotal)
        picchiOriginal = picchi
        
        
    if alsoOriginal:
        return qtotal, picchiOriginal
    else:
        return qtotal
        
        
def W2Q(i_window, y_window,  x_window, paramsExp_a, paramsAlign) :
    paramsExp = copy.deepcopy(paramsExp_a)
    qtotal=[]
    dists = []

    

    Q0_w = get_Q0( y_window.max()- y_window.min()   +1     ,x_window.max()- x_window.min()+1, paramsExp  ,  y_window.min(), x_window.min() )[0]


    Q0_w = Q0_w[ y_window - y_window.min(),] [:, x_window - x_window.min() ]
    

    
    for i_angle in  i_window : 
        paramsExp.phi = paramsExp.start_phi + i_angle*paramsExp.angular_step
        Qfin = get_Qfin_nosample( Q0_w , paramsExp)
        Qfin = get_Qfin_onlySample( Qfin, paramsAlign ) 

        qtotal.append(Qfin)

    return np.array(qtotal)
        


def Rotation(angle, rotation_axis=0):
    # this gives the rotations for the coordinates when angle and rotation_axis epxress the rotation of the reference system
    # In practice it is the inverse of what you expect.
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val


#--------------------------------------------------------------------------------------------------------


def   get_Qfin( Q0,  
               paramsExp):
    """ Given an array Q0 of 3D vectors, each 3D vector is brought first into the RS ( Reference System)
    of the holder. The it is brought in the reference system of the crystal lattice.
    The matrices R and U that epress the rotation of the holder and of the crystal, are inverted taking the transpose.
    """

    # R = Prim_Rot_of_RS(paramsExp.omega, paramsExp.phi, paramsExp.kappa, paramsExp.alpha, paramsExp.beta)
    R = HOLDER.Prim_Rot_of_RS(paramsExp)
    
    if not hasattr(paramsExp, "USEQUAT")  or paramsExp.USEQUAT == False:
        U = Snd_Rot_of_RS(paramsExp.r1, paramsExp.r2, paramsExp.r3)
    else:
         U = quat2M( paramsExp.r1, paramsExp.r2, paramsExp.r3 )

    ######################################################
    # Q0 is here an array of 3D vectors.
    # With tensor dot we apply R.T to each 3D vector,
    # This because of the axes choice.
    # R.T is the inverse of R for unitary matrices.
    # 
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    return Qfin



def   get_Qfin_onlySample( Q,  
                           paramsSample, alsoU = False):
    if not hasattr(paramsSample, "Quat_or_angles")  or paramsSample.Quat_or_angles      == "angles":
        U = Snd_Rot_of_RS(paramsSample.r1, paramsSample.r2, paramsSample.r3)
    else:

         U = quat2M( paramsSample.r1, paramsSample.r2, paramsSample.r3 )         
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    if alsoU:
        return Qfin,U
    else:
        return Qfin

def   get_Qfin_onlySample_quats( Q,  r1,r2,r3):
    U = quat2M( r1, r2, r3 )         
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    return Qfin


def   get_Qfin_nosample( Q0,  
               paramsExp):

    # R = Prim_Rot_of_RS(paramsExp.omega, paramsExp.phi, paramsExp.kappa, paramsExp.alpha, paramsExp.beta)
    
    R = HOLDER.Prim_Rot_of_RS(paramsExp)


    
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    Qfin = Q
    return Qfin

#--------------------------------------------------------------------------------------------------------    

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    # r1=-r1
    # r2=-r2
    # r3=-r3
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))

    swapper = get_tds2el2_hack_axis()
    if swapper is not None:
        tmp[:, swapper ] = -tmp[:, swapper ]

    
    return tmp

#--------------------------------------------------------------------------------------------------------

def Prim_Rot_of_RS(omega, phi, kappa, alpha, beta):
    # """ 
    # Primary rotation of reciprocal space. 
    # Omega, kappa, phi are the nominal values of the angle
    # """
    # omega=-omega
    # phi  = -phi
    # kappa = -kappa
    # alpha=-alpha
    # beta=-beta

    tmp = Rotation(omega , 2)
    tmp = np.dot(tmp, Rotation(alpha, 1))
    tmp = np.dot(tmp, Rotation(kappa, 2))
    tmp = np.dot(tmp, Rotation(-alpha, 1))
    tmp = np.dot(tmp, Rotation(beta, 1))
    tmp = np.dot(tmp, Rotation(phi, 2))
    tmp = np.dot(tmp, Rotation(-beta, 1))
    return tmp

# -----------------------------------------------------------------------------------------------------------

def get_Q0bypos(posY,posX, paramsExp, alsoCorr = False) :
    p0 = BEAM.minusP0(paramsExp)

    MD = DETECTOR.orientation

    extra_MD = MDs_by_code[paramsExp.orientation_codes]
    MD=np.dot(extra_MD, MD )
    
    Q0 = np.zeros( [3], dtype=np.float32)
    
    A,B =  paramsExp.det_origin_X, paramsExp.det_origin_Y

    coords = np.array( [  paramsExp.dist     ,  posX-A,  posY-B   ]         ,"f")
    
    coords[1:3] = coords[1:3]  *np.array(paramsExp.pixel_size) 

    # P_total_tmp[ 1:3]=  (np.tensordot(P_total_tmp[1:3]  * np.array(paramsExp.pixel_size)    ,MD , axes=([0], [1])))
    
    P_total_tmp =  np.tensordot(  coords   ,  np.array(  [   DETECTOR.direction ,  MD[0]  , MD[1] ]   ) , axes=([0], [0])        )

    P_total_tmp = np.tensordot(P_total_tmp, DETECTOR.DET(paramsExp) , axes=([0], [1]))
    
    P_total_tmp_modulus = np.sqrt(np.sum(P_total_tmp * P_total_tmp, axis= -1))
    
    Q0_tmp = P_total_tmp / P_total_tmp_modulus
    
    Q0 = ((Q0_tmp + p0 ) / paramsExp.lmbda).astype(np.float32)

    if not alsoCorr:
        return Q0

    v_in  = p0 
    v_out =  Q0_tmp


    normal_to_pol = np.array(   [    paramsExp.pol_x,  paramsExp.pol_y  ,  paramsExp.pol_z      ]        ,"f")
    normal_to_pol = normal_to_pol /  np.linalg.norm(   normal_to_pol    )
    
    cross_vin_pol = np.cross(v_in,  normal_to_pol, axis=0)
    cross_vin_pol = cross_vin_pol/np.linalg.norm(cross_vin_pol)
    
    p0xn_out  = ( cross_vin_pol *v_out).sum(axis=-1)
    
    n_out     =    (v_out*normal_to_pol ).sum(axis=-1)
    
    correction =  paramsExp.pol_degree*(1- p0xn_out*p0xn_out) +(1-paramsExp.pol_degree)*(1-n_out*n_out)

    C3 = (paramsExp.dist ** 3) /  (P_total_tmp_modulus**3)
    correction = correction *C3
    if np.isnan( correction):
        message = """ Something is wrong with the polarisation correction. Control polarisation in the input"""
        raise ValueError(message)
        
    return Q0, correction

def get_Q0(dim1,dim2, paramsExp, offset1=0, offset2 = 0) :
    p0 = BEAM.minusP0(paramsExp)

    MD = DETECTOR.orientation

    extra_MD = MDs_by_code[paramsExp.orientation_codes]
    MD=np.dot(extra_MD, MD )
    
    Q0 = np.zeros((dim1, dim2, 3), dtype=np.float32)


    
    coords = np.zeros((dim1, dim2 , 3), dtype=np.float32)
    
    coords[:, :, 0 ] =  paramsExp.dist

    A,B =  paramsExp.det_origin_X  ,     paramsExp.det_origin_Y

    coords[:, :, 1 ] = (np.arange(dim2)+offset2 -   A   )[None,:]
    coords[:, :, 2 ] = (np.arange(dim1)+offset1 -   B    )[:,None]
    
    coords[:,:,1:3] = coords[:,:,1:3 ]  *np.array(paramsExp.pixel_size)

    P_total_tmp =  np.tensordot(     coords        ,  np.array(  [   DETECTOR.direction ,  MD[0]  , MD[1] ]   ) , axes=([-1], [0])        )
    
    P_total_tmp = np.tensordot(P_total_tmp,   DETECTOR.DET(paramsExp) , axes=([2], [1]))
    
    P_total_tmp_modulus = np.sqrt(np.sum(P_total_tmp * P_total_tmp, axis= -1))
    Q0_tmp = P_total_tmp.T / P_total_tmp_modulus.T
    
    Q0 = ((Q0_tmp.T + p0 ) / paramsExp.lmbda).astype(np.float32)
    v_in  = p0 
    v_out =  Q0_tmp.T

    ## todo : rotate also nomal_to_pol as in minusP0()
    
    normal_to_pol = np.array(   [    paramsExp.pol_x,  paramsExp.pol_y  ,  paramsExp.pol_z      ]        ,"f")
    normal_to_pol = normal_to_pol /  np.linalg.norm(   normal_to_pol    )
    
    cross_vin_pol = np.cross(v_in,  normal_to_pol, axis=0)
    cross_vin_pol = cross_vin_pol/np.linalg.norm(cross_vin_pol)
    
    p0xn_out  = ( cross_vin_pol *v_out).sum(axis=-1)
    
    n_out     =    (v_out*normal_to_pol ).sum(axis=-1)
    
    correction =  paramsExp.pol_degree*(1- p0xn_out*p0xn_out) +(1-paramsExp.pol_degree)*(1-n_out*n_out)
        
    C3 = (paramsExp.dist ** 3 / (  (P_total_tmp* P_total_tmp).sum(axis=-1)   ) ** (3/2.)).astype(np.float32)
    print(" C3 shape ", C3.shape)
    print(" correction  shape ", correction.shape)
    correction = correction *C3

    #    Q0, Kin , correction   
    return (Q0).astype("f"), -(p0  / paramsExp.lmbda).astype("f")   , correction.astype("f")




#-------------------------------------------------------------------------------------------------------------------------

def  Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat(rottodo):
    """ The argument is the rotation which bring the reference axis to the sample axis
        and the result is a tuple (angles, quats) containing the parameters
        for the rotation which converts the  coordinates in the original reference system
        to the coordinates in the sample system
    """
    assert( np.linalg.det(rottodo) > 0)

    M =  rottodo
    if math.fabs(M[2,0])!=1.0:
        r1 = math.atan2(M[2,1],M[2,2])
        if  (math.fabs(M[2,1]/math.sin(r1))>math.fabs(M[2,2]/math.cos(r1))):
            r2 = math.atan2( -M[2,0], M[2,1]/math.sin(r1)  )
        else:
            r2 = math.atan2( -M[2,0], M[2,2]/math.cos(r1)  )
        r3 = math.atan2(M[1,0],M[0,0])
    else:
        raise Exception( " ce cas reste a programmer, http://www.chrobotics.com/library/understanding-euler-angles ")
    r3,r2,r1 = np.degrees( [ -r3,-r2,-r1 ] )
    quat = r2quat( r1,r2,r3 )
    return ( (r1,r2,r3), quat)


def M2r(rottodo):
    assert( np.linalg.det(rottodo) > 0)
    (r1,r2,r3), quat = Mat2SampleAxis_to_Mat2SampleCoordinates_as_Angles_and_Quat(rottodo)
    return r1,r2,r3
#-----------------------------------------------

def alignByFourier( pinnedaxis  , q3d  , angleConstraints ):
    ac = angleConstraints
    if True:

        return Fourier( pinnedaxis ,  q3d ,  [ac.aAA, ac.aBB, ac.aCC] )
    else:
        
        builded_axis = []
        builded_abc    = []
        builded_Aabc    = []

        cc,cb,ca = None, None, None
        if ac.aAA is not None:
            ca = math.cos(  np.deg2rad(  ac.aAA  ) ) 
        if ac.aBB is not None:
            cb = math.cos(  np.deg2rad(  ac.aBB  ) ) 
        if ac.aCC is not None:
            cc = math.cos(  np.deg2rad(  ac.aCC  ) ) 

        # print(" AA AB AC ", cc,  cb, ca)
        
        ds_list,ss_list, abc_list = analyse_maxima_fourier.Fourier( q3d     , cc = cc, cb = cb, ca=ca)

        
        for ds, ss, abc in zip( np.array(ds_list), np.array(ss_list), np.array(abc_list) ) :
            X,Y,Z = ds
            aAA = np.degrees(math.acos(  np.dot(Y,Z )   ))
            aBB = np.degrees(math.acos(  np.dot(X,Z )   ))
            aCC = np.degrees(math.acos(  np.dot(X,Y )   ))

            A = ds[0]
            B = ds[1] - np.dot(ds[1],ds[0])*ds[0]
            B = B/np.linalg.norm(B)
            C = np.cross(A,B)

            if np.dot(C, ds[2])<0:
                # print " CAMBIO " 
                Z = -Z
                aAA = np.degrees(math.acos(  np.dot(Y,Z )   ))
                aBB = np.degrees(math.acos(  np.dot(X,Z )   ))
                aCC = np.degrees(math.acos(  np.dot(X,Y )   ))
                
            builded_abc .append(   abc     )
            builded_Aabc.append(   [aAA, aBB, aCC]    )
            builded_axis.append(  [X,Y,Z]  )
        return builded_axis, builded_abc, builded_Aabc
    


       
def Fit_AllGeo_hkl ( picchi  ,  geo_pars ,  ali_pars ,  par_dict ,  discard  =   0.0):

    initial_vars = []
    whatToSet = []

    for name, value in par_dict.items():
        if value:
            if hasattr( geo_pars, name):
                initial_vars.append( getattr( geo_pars,name )   )
                whatToSet.append(   [    geo_pars     ,   name     ]      )
            if hasattr( ali_pars, name):
                initial_vars.append( getattr ( ali_pars,name)   )
                whatToSet.append(   [     ali_pars    ,    name    ]      )


    class  Functor_AllGeo_hkl_error:
        def __init__(self, picchi , geo_pars ,ali_pars ,   whatToSet    , discard=0):
            self.picchi = picchi
            self.geo_pars = geo_pars
            self.ali_pars = ali_pars
            self.whatToSet = whatToSet
            self.discard = discard

        def __call__(self,x):

            for v, w2s in zip(x, self.whatToSet):
                setattr(w2s[0], w2s[1], v)
                
            q3d =  peaks2Q(self.picchi, self.geo_pars )

            abc, Aabc  = ( ali_pars.AA,  ali_pars.BB,  ali_pars.CC     ), ( ali_pars.aAA,  ali_pars.aBB,  ali_pars.aCC )
            cellvectors, brillvectors = getCellVectors(abc,Aabc)

            r1,r2,r3 =   ali_pars.r1,  ali_pars.r2,  ali_pars.r3

            Qfin = get_Qfin_onlySample_quats(  q3d ,   r1,r2,r3 )
            hkl  = np.tensordot(Qfin,cellvectors,  axes=[-1,-1] )
            errc =  np.round(hkl) - hkl

            errc_sum = (errc*errc).sum(axis=-1)
            if self.discard != 0:
                errc_sum.sort()
                N = len(errc_sum)
                keep = int(N*(1-self.discard))
                errc_sum = errc_sum[:keep]

            print( " ERROR ", errc)
            return (errc_sum).sum()

    funct = Functor_AllGeo_hkl_error(picchi , geo_pars ,ali_pars ,   whatToSet    , discard=discard )

    result = optimize.minimize(funct, initial_vars,  method='Nelder-Mead')
    
    print("RISULTATO ALLINEAMENTO ALLGEO", result)
    x = result.x
    for v, w2s in zip(x, whatToSet):
        setattr(w2s[0], w2s[1], v)
    
def  Fit_hkl ( q3d=None  ,
               abc = None,
               fix_abc = None,
               Aabc = None,
               fix_Aabc = None,
               quats = None, discard = 0):
    
    # print( " SONO in Fit_hkl")
    # print( " q3d shape", q3d.shape)
    # print( " quats ", quats )
    # print( " abx " , abc )
    # print(" fix_abc ", fix_abc)
    # print(" Aabc ", Aabc )
    # print(" fix_Aabc ", fix_Aabc)

    totvars = np.array(list(abc) + list(Aabc) + list(quats)+[0.01,0.01,0.01] )
    takeit = np.array([ tok ==0 for tok in list(fix_abc) + list(fix_Aabc) + [0,0,0]  +[0,0,0] ])*1

    Nvars = takeit.sum()
    
    initialsvars = totvars[ takeit == 1  ]
    wherevars    = np.where( takeit ==1 )

    class  Functor_hkl_error:
        def __init__(self, q3d, totvars, wherevars, discard=0):
            self.q3d = q3d
            self.wherevars = wherevars
            self.totvars = np.array(totvars)
            self.discard=discard
        def __call__(self,x):
            self.totvars[wherevars ] = x
            abc, Aabc, zero = self.totvars[:3], self.totvars[3:6], self.totvars[-3:]
            cellvectors, brillvectors = getCellVectors(abc,Aabc)
            r1,r2,r3 = self.totvars[6:9]
            Qfin = get_Qfin_onlySample_quats(  self.q3d ,   r1,r2,r3 )+np.array(zero)
            hkl  = np.tensordot(Qfin,cellvectors,  axes=[-1,-1] )
            # print ( " np.round(hkl)  " ,  hkl ) 
            errc =  np.round(hkl) - hkl

            errc_sum = (errc*errc).sum(axis=-1)
            if self.discard != 0:
                errc_sum.sort()
                N = len(errc_sum)
                keep = int(N*(1-self.discard))
                errc_sum = errc_sum[:keep]
            
            # print( errc)
            return (errc_sum).sum()

    funct = Functor_hkl_error( q3d, totvars, wherevars, discard = discard)

    result = optimize.minimize(funct, initialsvars,  method='Nelder-Mead') #  options={"maxiter":10000,"fatol":1.0e-6})
    print("RISULTATO ALLINEAMENTO ", result)
    x = result.x
    totvars[wherevars ] = x
    abc   = totvars[:3]
    Aabc  = totvars[3:6]
    quats = totvars[6:9]
    return abc,Aabc, quats
    


#----------------------------------------------------------------

def M2quat(M):
    assert( np.linalg.det(M) > 0)

    Q=np.zeros([4,4],"d")

    t = M[0,0]+M[1,1]+M[2,2]
    
    for i in range(3):
        Q[i,i]= 2*M[i,i] - t
        for j in range(3):
            if j!=i:
                Q[i,j] = M[i,j]+M[j,i]

        Q[i,3] = M[(i+1)%3,  (i+2)%3 ] - M[ (i+2)%3,  (i+1)%3 ] 
        Q[3,i] = M[(i+1)%3,  (i+2)%3 ] - M[ (i+2)%3,  (i+1)%3 ] 
    Q[3,3]= t

    Q=Q/3

    evals, evects = np.linalg.eigh(Q)

 
    imax = np.argmax((evals))
    res =  evects.T[ imax]
    res = res[:3]*np.sign( res[3]   ) 
    return res

def r2quat(q1,q2,q3):
    M = Snd_Rot_of_RS( q1,q2,q3 )
    quat = M2quat(M)
    # print(" M2quat ", M, quat)
    return quat

def quat2r(r1,r2,r3):
    M = quat2M(r1,r2,r3)

    # print(" quat2M ", r1,r2,r3,  M)

    r1,r2,r3 = M2r(M)
    return r1,r2,r3



def quat2M(x,y,z):
    d = x*x+y*y+z*z
    if d>1:
        d=math.sqrt(d)
        x/=d
        y/=d
        z/=d
    # print x,y,z
    d = x*x+y*y+z*z
    if d>1.0:
        d=1.0
    a=math.sqrt(1.0-d)
    
    res= np.array(
        [
            [  a*a +x*x -y*y-z*z  ,             2*x*y +2*a*z          ,       2*x*z -2*a*y                  ] ,
            [        2*x*y -2*a*z ,      a*a -x*x + y*y-z*z           ,      2*y*z +2*a*x               ] ,
            [      2*x*z +2*a*y   ,             2*y*z -2*a*x          ,     a*a -x*x -y*y  + z*z             ] 

        ]
    )

    res = res.astype("f")
    
    swapper = get_tds2el2_hack_axis()
    if swapper is not None:
        res[:, swapper ] = -res[:, swapper]

    
    return res




def get_typical_firstneighbour_distance(points):
    dall = []
    for i1 in range(len(points)):
        i1_dists = []
        for i2 in range( len(points)):
            d = np.linalg.norm(points[i2]-points[i1])
            i1_dists.append(d)
        i1_dists.sort()
        for i in range(1,len(i1_dists)):
            if i1_dists[i] > 4*i1_dists[i-1]:
                dfirst = i1_dists[i]
                break
        else:
            dfirst = i1_dists[0]
        dall.append(dfirst)
    return np.median(dall)


def sphere_sampling ( N ):
  #  Reference:
  #
  #    Richard Swinbank, James Purser,
  #    Fibonacci grids: A novel approach to global modelling,
  #    Quarterly Journal of the Royal Meteorological Society,
  #    Volume 132, Number 619, July 2006 Part B, pages 1769-1793.
  #
  numoro = 1.618033988749895
  ps =  np.linspace(0.0, 1.0, num=N, endpoint=False)
  zs = 1-2*ps
  r=np.sqrt(  1-zs*zs   )
  xs = np.cos( numoro * 2 * np.pi * ps*N ) *r
  ys = np.sin( numoro * 2 * np.pi * ps*N ) *r
  res = np.zeros([N,3],"f")
  res[:,2]=zs
  res[:,1]=ys
  res[:,0]=xs
  return res



def nearest_map( directions ) :
    nearest = []
    for i,d in enumerate(directions):
         diff = directions-d
         diff = np.linalg.norm(diff, axis=-1)
         order = np.argsort(diff)

         diff = directions+d
         diff = np.linalg.norm(diff, axis=-1)
         order2 = np.argsort(diff)
         nearest.append( np.concatenate([order[1:6],order2[:6]]) )
    return nearest

def get_fft(PP, bins, irtipo ):
     irtipo=irtipo
     freqs = np.fft.fftfreq(len(bins)-1)+0.000001
     h = np.histogram(  PP  ,  bins=bins      )[0]
     ft = np.fft.fft(h)
     ft[:irtipo*2 ] = 0
     ft[-irtipo*2: ] = 0
     ft  =  np.abs(ft/np.sqrt(np.abs(freqs)))
     return ft

def get_free_slot( assi  ):
    slot_pos = None
    for i in range(3):
        if assi[i] is None:
            slot_pos = i
            
    return slot_pos
    
 
def angle_compatible(direction,  assi, aConstr ):
    slot_pos = get_free_slot(assi)

    if slot_pos is None:
        raise Exception(" problem : 3 pinned axis already found, but still trying to fix another")
    
    slot_pos_1 = None
    slot_pos_2 = None

    if assi[  (slot_pos+1)%3 ] is not None:
        slot_pos_1 = (slot_pos+1)%3
        
    if assi[  (slot_pos+2)%3 ] is not None:
        slot_pos_2 = (slot_pos+2)%3
    
    if  slot_pos_1 is None and  slot_pos_2 is None :
        # print(" no slots da comparare ritorno True ")
        return True

    for slot_other in [ slot_pos_1,  slot_pos_2  ]:
        if slot_other is not None:
            # -1 0 1    ...  -1*(-1_0) -> 1 ; -1*(-1+1)->0 ; -1*(0+1)->-1
            slot_angle =  1 + -1 * ( slot_pos - 1  + slot_other - 1 )
            if aConstr[slot_angle] is not None:
                anom = int(aConstr[slot_angle])
                tol = (aConstr[slot_angle] - anom)*100
                cc =  np.dot( assi[slot_other]  , direction)
                a  =  np.rad2deg(np.arccos(np.clip(cc,-1,1)))

                # print(" per mettere in slot ", slot_pos,"  farei angolo ", a ," con slot ", slot_other, " da comparare a ",  anom, tol ) 
                if a < anom-tol or a> anom+tol :
                    # print(" fuori tolleranza False")
                    return False

    if None not in [slot_pos_1 , slot_pos_2 ]:
        assi[slot_pos] = direction
        det =  np.linalg.det(assi)
        assi[slot_pos] = None
        if (det) < 0.2:
            # print(" falso per volume ")
            return False
        
                
    # print(" dentro tolleranze  True ")

    return True


        
def check_maximum_compatibility( i,  building_axis,   maxpos_perdir,    anglesconstraints, directions,  nearest   ):
    if angle_compatible(directions[i] , building_axis , anglesconstraints ):
        m,pos = maxpos_perdir[i]
        for k in nearest[i] :
            if m<maxpos_perdir[k][0]  and angle_compatible( directions[k],  building_axis, anglesconstraints  ) :
                return False
        else:
            return True
    else:
        return False


def    backforward_directions(assi, assi_alldirections, pos=0):
    if pos == 3 :
        assi_alldirections.append(assi)
    else:
        if assi[pos] is None:
            backforward_directions(assi, assi_alldirections, pos=pos+1)
        else:
            for senso in [-1,1]:
                newassi = copy.deepcopy(assi)
                newassi[pos] = senso*assi[pos]
                backforward_directions(newassi, assi_alldirections, pos=pos+1)
            
        

def recorsiva_build( building_axis, building_mp,  maxpos_perdir,    anglesconstraints, directions,   nearest,    builded_axis,  builded_mp   , depth =0  ):

    if get_free_slot( building_axis ) is None:
        # print(" AGGIUNTA ")
        builded_axis.append(  building_axis )
        builded_mp   .append(  building_mp    )
        return
    
    for i in range(len(directions)):
        
        if check_maximum_compatibility( i,  building_axis,   maxpos_perdir,    anglesconstraints, directions,  nearest   ):

            # print(" TROVATO UN CANDIDATO A DEPTH", depth, " i ", i)
            
            target_slot = get_free_slot(  building_axis       )
            building_axis_next = copy.deepcopy(  building_axis   )
            building_axis_next[target_slot] = np.array(directions[i])
            
            building_mp_next = copy.deepcopy( building_mp     )
            building_mp_next.append(  np.array(maxpos_perdir[i]) )
            recorsiva_build( building_axis_next,  building_mp_next,  maxpos_perdir,    anglesconstraints, directions,  nearest,  builded_axis,  builded_mp   , depth=depth+1  )

def Fourier( pinnedassi,  points,  anglesconstraints ):
    # print(" in FOURIER ")
    rtipo = get_typical_firstneighbour_distance(points)
    # print(" STABILISCO DIREZIONI ")
    Ndirs = 10000

    if   os.path.exists( os.path.join(os.path.dirname(os.path.abspath(__file__)), "sampling_directions_%d.txt"%Ndirs  )):
        
        fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), "sampling_directions_%d.txt"%Ndirs  )
        directions = np.loadtxt(fn)
        
        fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), "nearest_directions_map_%d.txt"%Ndirs  )
        nearest  = np.loadtxt(fn,dtype="i")

        
    else:
        directions = sphere_sampling( Ndirs  )
        nearest = nearest_map(directions)
        np.savetxt("sampling_directions_%d.txt"%Ndirs,  directions)
        np.savetxt("nearest_directions_map_%d.txt"%Ndirs,  nearest)

    
    Rmax  = np.linalg.norm( points ,axis=-1).max()  *2 # *2 is the padding factor
    PP =    np.tensordot(directions , points, [[-1],[-1]]) 
    bins = np.linspace(-Rmax, Rmax, num=2015 , endpoint = True )
    irtipo = int(Rmax/ rtipo)
    freqs = np.fft.fftfreq(len(bins)-1)+0.000001
    
    maxpos_perdir=[]

    for i in range(len(PP)):        
        ft=get_fft(PP[i],bins ,irtipo )
        imax = np.argmax(np.abs(ft[:len(ft)//2]))
        maxpos_perdir.append(  (
            abs(ft[imax]),
            (1.0/(2*Rmax))*  imax ) 
        )
        
        
    assi = [tok for tok in pinnedassi]
    assi_alldirections = []
    backforward_directions(assi, assi_alldirections)

    #####################################################################################################################
    if 0:
        for i in range(3):
            if pinnedassi[i] is not None:
                PPtmp =    np.tensordot( pinnedassi[i] , points, [[-1],[-1]]) 
                ft=get_fft(PPtmp,bins ,irtipo )
                imax = np.argmax(np.abs(ft[:len(ft)//2]))
                pos = (1.0/(2*Rmax))*  imax 
                f=open("pinned_%d.txt"%i,"w")
                f.write("#      D  %f     imax %d    " %(pos , imax) )
                for t in ft:
                    f.write(" %e \n" %  abs(t) ) 
    #########################################################################################################################
                    
    for assi in assi_alldirections : 
        building_axis = copy.deepcopy(assi)
        building_mp = []
        builded_axis = []
        builded_mp    = []
        # print("CHIAMO RECORSIVA ")
        recorsiva_build( building_axis, building_mp,   maxpos_perdir,    anglesconstraints, directions,   nearest,  builded_axis,  builded_mp     )

    assert ( len(builded_axis) == len( builded_mp) )
    builded_axis, builded_mp  = map( np.array, (     builded_axis, builded_mp        ))
   
    # FARE IN MODO DA METTERE ANCHE GLI ASSI CHE SONO GIA IN PINNED PER I MAXPOS !!!
    print(   " builded_mp.shape  " , builded_mp.shape)
    if builded_mp.shape[1] != 3 :
        sh  =  builded_mp.shape
        new_builded_mp = np.zeros( [sh[0], 3, sh[2]],    builded_mp.dtype   )
        icount=0
        for i in range(3):
            if assi[i] is None:
                new_builded_mp[:, i, :] = builded_mp[:, icount, :]
                icount += 1
            else:
                new_builded_mp[:, i, 0] = 0

                PPtmp =    np.tensordot(assi [i] , points, [[-1],[-1]]) 

                ft=get_fft(PPtmp,bins ,irtipo )
                imax = np.argmax(np.abs(ft[:len(ft)//2]))
                new_builded_mp[:, i, 1] = (1.0/(2*Rmax))*  imax 
        
        builded_mp  = new_builded_mp
    
    criterium = np.sum( builded_mp[:,:,0] , axis=-1  )  # somma i massimi dei 3 assi 
    order = np.argsort( -criterium  )
    
    builded_axis =  builded_axis [order]
    builded_mp   =  builded_mp   [order]
    tmp_builded_abc  =  builded_mp[:, :,1]
    builded_abc  =  []
    builded_Aabc = []
    
    for ds, (a,b,c) in zip(builded_axis,  tmp_builded_abc   ):

        a=1.0/a
        b=1.0/b
        c=1.0/c
        
        X,Y,Z = ds
        aAA = np.degrees(math.acos(  np.dot(Y,Z )   ))
        aBB = np.degrees(math.acos(  np.dot(X,Z )   ))
        aCC = np.degrees(math.acos(  np.dot(X,Y )   ))

        (a,b,c) ,  (aAA,aBB,aCC)  = fromBrillToBravaisVectors( (a,b,c) ,  (aAA,aBB,aCC)  )
        
        builded_Aabc . append( list(map(float,[aAA, aBB, aCC])) ) 
        builded_abc . append( list(map(float,[a,b,c])) ) 

    return builded_axis, builded_abc, builded_Aabc


# print " LOADING " 
# points = numpy.loadtxt(str(sys.argv[2]))
# print " chiamo gfourier " 
# ds,ss, abc  = Fourier(points,0,0,0)




def existing_combination(candidate_op,op_list,sigma):
    for op in op_list:
        absdiff = abs(candidate_op - op)
        if absdiff.sum()<sigma:
            return True
    return False


def genlist2oplist(genlist, cellvects, dtype=np.float32):
    base_op_list = [ascii_gen2operator(gen, cellvects ) for gen in genlist]
    I = np.identity(3)
    combined_op_list = [I]
    while 1:
        new_add_in_cycle = False
        for op in combined_op_list:
            for base_op in base_op_list:
                newop = np.dot(op,base_op)
                if not existing_combination(newop,combined_op_list,0.01):
                    combined_op_list.append(newop)
                    new_add_in_cycle = True
        if not new_add_in_cycle:
            break
    return np.array(combined_op_list).astype(dtype)


def Mirror(mirror_axis):
    x,y,z = {"x":[-1,1,1], "y":[1,-1,1], "z":[1,1,-1],0:[-1,1,1], 1:[1,-1,1], 2:[1,1,-1]}[mirror_axis]
    mat = np.array([[x,0,0],[0,y,0],[0,0,z]])
    return mat 


__I = np.identity(3)

gendict = {  '1':__I,
             '1~':- __I,
             '2(x)':Arb_Rot(180,[1,0,0]),
             '2(y)':Arb_Rot(180,[0,1,0]),
             '2(z)':Arb_Rot(180,[0,0,1]),
             '4(x)':Arb_Rot(90,[1,0,0]),
             '4(y)':Arb_Rot(90,[0,1,0]),
             '2(110)':np.array([[0,1,0],[1,0,0],[0,0,-1]]),
             '3(z)':Arb_Rot(120,[0,0,1]),
             '3(111)':np.array([[0,1,0],[0,0,1],[1,0,0]]),
             '4(z)':Arb_Rot(90,[0,0,1]),
             # '4(z)~':- __I*Arb_Rot(90,[0,0,1]),
             'm(x)':Mirror(0),
             'm(y)':Mirror(1),
             'm(z)':Mirror(2),
             'm(100)':  np.array([[-1,1,0],[0,1,0],[0,0,1]])   ,
             'm(010)':  np.array([[1,0,0],[1,-1,0],[0,0,1]])   ,
             'm(110)':  np.array([[0,-1,0],[-1,0,0],[0,0,1]])  
}


def ascii_gen2operator(generator, cell_vects):
    print( generator)
    I = np.identity(3)

    # print(" ascii_gen2operator  " , cell_vects ) 
    
    
    for name in list(gendict.keys()) :
        orig_name = name
        name = name.replace(")","_")
        name = name.replace("(","_")
        name = name.replace("~","_inv_")
        name = "gen_"+name
        gendict[name] = gendict[orig_name]

    
    mat = gendict[generator]
    pos = generator.find("_")
    if pos!=-1:
        if "1" in generator[pos:]:

            toRec = np.array( [  tok  for tok in cell_vects ]  )
            fromRec = np.linalg.inv(toRec)
            # basis = np.array( [  tok/np.sqrt( (tok*tok).sum())    for tok in cell_vects ]  )
            # mat = np.dot( basis.T  ,   np.dot(mat, np.linalg.inv(basis.T)   )  )
            mat = np.dot( fromRec  ,   np.dot(mat, toRec  )  )

            
    # np.savetxt(sys.stdout,mat,fmt="%e")
    # print " " 
    return mat



def get_elastic_simmetry(all_ops):
    components = [(0,0),(1,1),(2,2),(1,2),(2,0),(0,1)]
    comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
                  ,(2,1):3,(0,2):4,(1,0):5}  
    res={}
    transf = []
    for op in all_ops:
        # np.savetxt(sys.stdout,op,fmt="%e")
        # print ""

        # if   op[2,2]!=1.0     or  abs(np.linalg.det(op)-1)>1.0e-5   :
        #     continue

        # if  abs( op[0,0]-1.0) >1.0e-3  and  abs( op[0,0]+0.5) >1.0e-3:
        #     continue

        mat = np.zeros([6,6],"d")
        for i, (k,l) in enumerate(components):
            dispatch_k = op[:,k]
            dispatch_l = op[:,l]
            for K,cK in enumerate(dispatch_k ) :
                for L,cL in enumerate(dispatch_l ) :
                    fact=1.0
                    if (K!=L):
                        fact=fact/2.0
                    if (k!=l):
                        fact=fact*2.0
                    mat[comppos[(K,L)], i ] += cK*cL * fact
        transf.append(mat)

    ricerca = [(i,i) for i in range(6)]
    for i in range(5):
        for j in range(i+1,6):
            ricerca.append((i,j))

    # NMC=100000
    for (i,j) in ricerca[:]:
        matrici = np.zeros([6,6])
        matrici[i,j]=1
        matrici[j,i]=1
        trace_matrici =  np.zeros([6,6],"d")
        for rot in transf:
            add =  np.tensordot(  matrici , rot,([-2],[-1]))
            add =  np.tensordot(  add , rot,([-2],[-1]))
            trace_matrici[:] +=  add
        trace_matrici = (np.reshape(trace_matrici, [36]))

        
        for vect,I in res.values():
            trace_matrici -= vect*trace_matrici[I]

        if abs(trace_matrici[ i*6+j ])>1.0e-2:
            vect =  trace_matrici/trace_matrici[ i*6+j ]
            I =  i*6+j
            for vv,II in res.values():
                n0 =  np.less(1.0e-2, abs(vv)).sum()
                nuovo  =  vv - vect * vv[I]
                n1 =  np.less(1.0e-2, abs(nuovo)).sum()
                if n1<n0:
                    vv[:]=nuovo
            res[(i,j)]=vect, I


    displays = [[[] for j in range(6)] for i in range(6) ]


    ndisplaylines=0
    maxlen=0
    for vect,II in res.values():
        i,j = II//6 +1,  II%6 +1
        for K,v in enumerate(vect):
            if(abs(v)>1.0e-2):
                k,l = K//6 ,  K%6 
                add = "%f A%d%d"%(v,i,j)  
                displays[k][l].append(    add     )
                # displays[l][k] = displays[k][l]
                ndisplaylines = max(  ndisplaylines, len(  displays[k][l]))
                maxlen = max( maxlen, len(add)   ) 
           

    tensore_string = ""
                
    for i in range(6):
        for il in range(ndisplaylines):
            linea=""
            for j in range(6):
                add=""
                if il <  len(displays[i][j]):
                    add= displays[i][j] [il]
                add = add+" "*( maxlen-len(add))
                if  j<5:
                    add = add +" | "
                linea = linea + add
            # print ( linea)
            tensore_string = tensore_string + linea + "\n"
        if i < 5:
            # print( "-"*(len(linea)))
            tensore_string = tensore_string + ( "-"*(len(linea))) +"\n"

    nomi_variabili=[]
    vettori_variabili=[]
    # print( ricerca)
    for i,j in ricerca:
        if (i,j) in res:
            nome = "A%d%d"%(i+1,j+1)
            vettore,I =  res[(i,j)]

            nomi_variabili.append(nome)
            vettori_variabili.append(vettore)
        

    # print( " THE ELASTIC TENSOR VARIABLES ARE ")
    # print( nomi_variabili)
    return nomi_variabili,np.array(vettori_variabili), tensore_string
