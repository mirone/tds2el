from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
def readPars(parfile):

    params = {}
    f = open(parfile, "rb")
    content = f.read()
    content = content.split(b"\n")
    f.close()
    for i,line in enumerate(content):
        if b"CELL INFORMATION" in line:

            l = content[i+1].split()
            params["AA"],params["BB"], params["CC"]    = [  float(l[k]) for k in [1,5,9]  ]
            l = content[i+2].split()

            params["aAA"],params["aBB"],params["aCC"] = [  float(l[k]) for k in [1,5,9]  ]

            
        if b"ALPHA (DEG)" in line:

            params["alpha"] = float( line.split()   [4])
            params["beta"]  = float( line.split()   [7])
            
        if b"X-RAY BEAM ORIENTATION (DEG)" in line:
            
            params["beam_tilt_angle"] =  float( line.split()   [7])

            # params["beam_b3"] = float( line.split()   [9])
            
        if b"SOFTWARE ZEROCORRECTION (DEG)" in line:
            params["omega_offset"] = float( line.split()   [6])
            params["theta_offset"] = float( line.split()   [8])

            # params["kappa_offset"] = kappa_0
            # params["phi_offset"]   = phi_0
            
        if b"PIXELSIZE" in line:
            params["pixel_size"] = float( line.split()   [-1])
            
        if b"DETECTOR ROTATION (DEG)" in line:
            params["d1"] = float( line.split()   [6])
            params["d2"] = float( line.split()   [8])
            # params["det_tilt_d3"] = float( line.split()   [10])
            
        if b"DETECTOR DISTANCE (MM)" in line:
            params["dist"] = float(  line.split() [-1])
            
        if b"DETECTOR ZERO" in line:
            params["det_origin_X"] = float( line.split()   [-3])
            params["det_origin_Y"] = float( line.split()   [-1])
            
        if line.startswith(b"CRYSTALLOGRAPHY UB"):
            
            l = line.split()
            ub = l[2:]
            ub = [float(x) for x in ub]
            UB = np.asarray(ub)
            UB = UB.reshape((3,3))
            params["UB"] = UB
            
        if line.startswith(b"CRYSTALLOGRAPHY WAVELENGTH"):
            params["lmbda"] = float(line.split()[2])
            
    params["UB"] = params["UB"]/params["lmbda"]

    return params


 
