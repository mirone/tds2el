from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from . import ui

import sys, getopt

from silx.gui import qt

import silx.gui.data
import silx.gui.data.DataViewerFrame

import numpy as np
import sys
from six import StringIO
import traceback

from .Parameters import  Parameters, Loader  # Loader must be loaded before using it in dataset
from . import extraction

from silx.gui.plot import PlotWidget
import silx.sx
from . import geometry
from . import fromCrysalis
from . import ccp4writer

from . import hdf5dialog
import os
import h5py
from . import spotpicker_cy
from . import utils
import gc
import copy
import math
from scipy import optimize
import ab2tds.dabax as dabax
import shlex

from . import Dataset
#  from .Dataset import Parameters


from silx.gui.dialog.GroupDialog import GroupDialog
from silx.gui.dialog.DataFileDialog import DataFileDialog

from silx.image.marchingsquares import MarchingSquaresMergeImpl as MarchSq

timer = qt.QTimer()
observed = None

class MyStack(silx.sx.StackView):
    def __init__(self, *args, **argws):
        super(MyStack , self).__init__(*args,**argws)

        self.tb = self.addToolBar("Simuls" )
        
        self.cb = qt.QComboBox( )
        self.cb.addItems(["exp all", "exp/simu","exp used", "simu"] )
        self.tb.addWidget(self.cb)

        self.sigPlaneSelectionChanged.connect(self.onPlaneSelection)  #.emit(idx)

    
    def setStack_s(self,d, components = None):
        self.components = components
        self.setStack(d)
        
    def onPlaneSelection(self,idx):
        print("onPlaneSelection" , idx )
        if self.components is not None:
            a,b = self.components
            if idx==0:  # 1-2
                self.setStack(  np.concatenate( [a, b], axis=1)  )
                
            if idx==1:  # 0-2
                self.setStack(  np.concatenate( [a, b], axis=0)  )

            if idx==2:  # 1-2
                self.setStack(  np.concatenate( [a, b], axis=0)  )

            
        
    def closeEvent(self, event):
        self.hide()
        event.ignore()

# class myScrollArea(qt.QScrollArea):
#     def centraWidget(...

from .GLwidget import GLwidget 

def check_python_filter( hkl , python_filter):
    g={}
    assert(len(hkl)==3)
    l= dict( zip( ["h", "k", "l"], hkl ) )

    if python_filter is None or python_filter == "None" or str(python_filter).strip()=="":
        return True
    
    return eval(python_filter, g,l)

    

@ui.UILoadable
class moviesShower(qt.QDockWidget):
    wscope = None
    selected_spot_signal = qt.pyqtSignal(object, object)

    def spotSelectionDone(self, pmapd):
        peak = pmapd.peak
                
        self.selected_spot_signal.emit( peak["fn"] , tuple(peak["pos"]) )
    
    # def __init__(self,extracted_data,
    #              expGeo_p,
    #              smpAl_p,
    #              fit_p,
    #              parent=None, second_extracted_data = None, first_second_correspondance = None ):
        
    def __init__(self,dataset,
                 parent=None, second_dataset = None, first_second_correspondance = None ):
        
        super(moviesShower, self).__init__(parent)
        self.loadUi()
        
        self.dataset  = dataset
        self.second_dataset  = second_dataset
        
        self.first_second_correspondance  = first_second_correspondance
        
        
        self.pmaps_dict = {}

        self.tabWidget.clear()

        
        self.createTab("accepted")
        self.createTab("blocked")

        if self.wscope is None:
            moviesShower.wscope = MyStack()
            moviesShower.wscope.setStack_s(np.random.random(64**3).reshape(64, 64, 64))
            moviesShower.wscope.setAttribute(qt.Qt.WA_QuitOnClose, False)

        moviesShower.wscope.hide()

        # moviesShower.wscope._browser.valueChanged[int].connect(self.onPlanSelection)
        moviesShower.wscope.sigFrameChanged.connect(self.onPlanSelection)
           
        moviesShower.wscope.cb.currentIndexChanged[int].connect(self.on_cb_changed)

        timer.start(100)

        self._isVisualisingSimu = 0

        ## mettere come property isVisualisingSimu
        

    @property
    def isVisualisingSimu(self):
        return self._isVisualisingSimu
    
    @isVisualisingSimu.setter
    def isVisualisingSimu(self, x):
        self._isVisualisingSimu = x
        moviesShower.wscope.cb.setCurrentIndex(x)

    def on_cb_changed(self, index):
        self._isVisualisingSimu = index
        self.applyChangeSimRef()
        
    def ToggleSimRef(self):
        
        self.isVisualisingSimu = (        self.isVisualisingSimu + 1)%4
        self.applyChangeSimRef()
        
    def applyChangeSimRef(self):
        
        if "Blob" not in  moviesShower.wscope.peak:
            self.isVisualisingSimu = 0
        else:
            blob = moviesShower.wscope.peak["Blob"]
            sim_ref = blob.simIntensity
            if sim_ref is None:
                self.isVisualisingSimu = 0
                
        if   self.isVisualisingSimu == 0 :
            if self.wscope.second_peak is None:
                moviesShower.wscope.setStack_s(   np.abs( moviesShower.wscope.peak["harvest"])  )
            else:

                ref1 = np.abs( moviesShower.wscope.peak["harvest"])
                ref2 = np.abs( moviesShower.wscope.second_peak["harvest"])
                moviesShower.wscope.setStack_s(   np.concatenate( [ref1,ref2], axis=1    ), [ref1, ref2]  )
            
        elif   self.isVisualisingSimu == 3 :
            
            blob = moviesShower.wscope.peak["Blob"]
            sim,ref = blob.simIntensity
            moviesShower.wscope.setStack_s(  np.array( sim) ) 
            
        elif   self.isVisualisingSimu == 2 :
            
            blob = moviesShower.wscope.peak["Blob"]
            sim,ref = blob.simIntensity
            moviesShower.wscope.setStack_s(  np.array( ref) )
                
        elif   self.isVisualisingSimu == 1 :

            blob = moviesShower.wscope.peak["Blob"]
            sim,ref = blob.simIntensity

            moviesShower.wscope.setStack_s(  np.concatenate( [sim, ref], axis=1), [sim,ref] )
            

    # @@@@@@@ intercettare cambio perspective per ridisporre sim_ref in caso isvisualisingsimu==3
        
    def changeOfPlane(self, iplan):
        print("CHANGE iplan", iplan)
        
    def onPlanSelection(self, num):
        # print( " in plan selection ", num)
        # print(  "  moviesShower.wscope.qminmax_selection ",    moviesShower.wscope.qminmax_selection )
        if not  self.isVisualisingSimu:
            pass
            # return
        
        plot = moviesShower.wscope.getPlot()
        plot.remove(kind="curve")

        ori = moviesShower.wscope._perspective
        num = num % (    self.stack.shape[ori]         ) 
        
        for qmm, color, legend  in zip( [moviesShower.wscope.qminmax_selection,moviesShower.wscope.qminmaxC_selection], ["red","blue"], ["","_centered"] ):


            if qmm is not None:


                ori = moviesShower.wscope._perspective

                if  ori == 0:
                    qmm_plane = qmm[num,:,:]
                    splane = self.stack[num,:,:]
                    
                elif ori == 1 :
                    qmm_plane = qmm[:,num,:]
                    splane = self.stack[:,num,:]
                elif ori == 2:
                    qmm_plane = qmm[:,:, num]
                    splane = self.stack[:,:, num]

                qmm_plane =  qmm_plane * 1.0

                ## np.save( "plan.npy", qmm) 

                ms = MarchSq( qmm_plane)
                
                polygons = ms.find_contours(level=self.dataset.Pfit.qmax )

                SHFT = 0.5
                for k,p in enumerate(polygons):
                    plot.addCurve(x=p[:,1]+SHFT, y= p[:,0]+SHFT , legend = "QMAX_%s"%k+legend, replace=False,color=color)
                    if self.isVisualisingSimu==1:
                        plot.addCurve(x=p[:,1]+SHFT, y= p[:,0]+SHFT+qmm_plane.shape[0] , legend = "QMAX_%s_bis"%k+legend, replace=False,color=color)
                        
                polygons = ms.find_contours(level=self.dataset.Pfit.qmin )

                for k,p in enumerate(polygons):
                    plot.addCurve(x=p[:,1]+SHFT, y= p[:,0]+SHFT , legend = "QMIN_%s"%k+legend, replace=False,color=color)
                    if self.isVisualisingSimu==1:
                        plot.addCurve(x=p[:,1]+SHFT, y= p[:,0]+SHFT +qmm_plane.shape[0] , legend = "QMIN_%s_bis"%k+legend, replace=False,color=color)
                                        
                # tmp = np.less(   qmm_plane,      self.fit_p.qmax    )*1.0
                # crit = tmp[:,1:] - tmp[:,:-1]
                # y1,x1 = np.where(crit>0)    
                # plot.addCurve(x=x1, y= y1-0.5, legend = "QMAX1"+legend, replace=False,color=color)

                # y1,x1 = np.where(crit<0)    
                # plot.addCurve(x=x1, y= y1-0.5, legend = "QMAX2"+legend, replace=False,color=color)


                # crit = tmp[1:,:] - tmp[:-1,:]
                # y1,x1 = np.where(crit>0)
                # ordine = np.argsort( x1  )
                # x1=x1[ordine]
                # y1=y1[ordine]
                # plot.addCurve(x=x1-0.5, y= y1, legend = "QMAX3"+legend, replace=False,color=color)
                    
            
        print( " selezionato n plan ", num)
        
    def selectSpot(self,   args ) :

        print(" SELECT SPOT")
        (name_image, pos) = args
        print(" in selectSpot ricevo ",  name_image, pos    ) 
        if (name_image, tuple(pos)) in self.pmaps_dict:
            pmpd = self.pmaps_dict[  (name_image, tuple(pos))    ]


            if "harvest" in pmpd.peak:
                stack = pmpd.peak["harvest"]
            else :
                stack = None            
            
            Gi = pmpd.peak["Gi"]

                    
                    
            pmpd.doSpotSelection(Gi, pos, stack )
            self.stack = stack
            self.peak = pmpd.peak
            
        else:
            print(" MA NON C'E'")
            print ( list(self.pmaps_dict.keys())  ) 
            
    def createTab(self,what):
        area = qt.QScrollArea()
        scrolledWidget = qt.QWidget()
        scrolledGrid   = qt.QGridLayout()
        
        self.tabWidget.addTab( area, what  )
        icount = 0
        
        for imageName, data4im in self.dataset.extracted_data.items():
            for pos, data4pos in data4im.items():
                print( " mostro ", imageName, pos)
                if data4pos["accepted"] == {"accepted":True, "blocked":False}[what] :

                    
                    data4pos["fn"] = imageName
                    
                    second_data4pos = None
                    if self.second_dataset is not None:
                        sim, spos = self.first_second_correspondance[  (imageName,pos)   ]
                        second_data4pos = self.second_dataset.extracted_data[sim][spos]
                    
                    
                    newpmpd = PmapDisplayer(data4pos, imageName, self, second_peak = second_data4pos)
                    
                    newpmpd.centerme_signal.connect( area.ensureWidgetVisible )
                    newpmpd.centerme_signal.connect( self.spotSelectionDone )

                    # newpmpd.expGeo_p =  self.dataset.PexperimentGeo
                    # newpmpd.smpAl_p  =  self.dataset.Palignement
                    # newpmpd.fit_p    =  self.dataset.Pfit
                    newpmpd.dataset =  self.dataset
                    newpmpd.second_dataset =  self.second_dataset
                    
                    self.pmaps_dict[  ( imageName, tuple(pos)  )    ] = newpmpd

                    scrolledGrid.addWidget( newpmpd  , icount//4, icount%4)
                    icount += 1
        scrolledWidget.setLayout(scrolledGrid)
        area.setWidget(scrolledWidget)
        sizepolicy = qt.QSizePolicy.Fixed
        area.setSizePolicy(sizepolicy, sizepolicy)
          

def convertArrayToQImage(image):
    image = np.array(
        image,
        copy=False,
        order='C',
        dtype=np.uint8)
    qimage = qt.QImage(
        image.data,
        image.shape[1],
        image.shape[0],
        image.strides[0],  # bytesPerLine
        qt.QImage.Format_RGB888).copy()       
    return qimage

class PmapDisplayer(qt.QFrame):
    centerme_signal = qt.pyqtSignal(object)
    def __init__(self, peak, fn, parent, second_peak = None):
        qt.QFrame.__init__(self)

        sizepolicy = qt.QSizePolicy.Expanding
        sizepolicy = qt.QSizePolicy.Fixed
        self.setSizePolicy(sizepolicy, sizepolicy)
        
        self.peak=peak
        self.second_peak=second_peak
        
        self.parent = parent
        try:
            pmaps, pmaps_alt = peak["pmap"]
        except:
            pmaps = peak["pmap"]
            pmaps_alt = None

        self.setAutoFillBackground(True)

        self.pmaps = []
        for p in pmaps:
            ima  = convertArrayToQImage(p)
            self.pmaps.append(qt.QPixmap(ima))
        self.resize(self.pmaps[0].width(), self.pmaps[0].height())
        print( " size " , self.pmaps[0].width(), self.pmaps[0].height())
        
        self.pmaps_alt = []
        if pmaps_alt is not None:
            for p in pmaps_alt:
                ima  = onvertArrayToQImage(p)
                self.pmaps_alt.append(qt.QPixmap(ima))
        
        self.grid = qt.QGridLayout(self)
        
        self.label = qt.QLabel(  )
        self.label.setSizePolicy(sizepolicy, sizepolicy)
        
        if "hkl" in peak:
            print( "%-- s y,x = %s"%(peak["fn"], peak["pos"] ))            
            self.label.setToolTip("%s y,x = %s   hkl=%s"%(peak["fn"], peak["pos"], peak["hkl"] )  )
        else:
            print( "%s y,x = %s"%(peak["fn"], peak["pos"] ))
            self.label.setToolTip("%s y,x = %s"%(peak["fn"], peak["pos"] )  )
        
        self.checkbox = qt.QCheckBox()
        self.checkbox.setChecked(  self.peak["accepted"]  )
        
        self.checkbox.stateChanged.connect(self.changecheck)
        self.checkbox.setSizePolicy(sizepolicy, sizepolicy)
        
        if "hkl_rounded<" in peak:
            self.grid.addWidget(self.label,0, 0, 2,1)
            # self.tag_w = qt.QWidget()
            # self.tag_w.setSizePolicy(sizepolicy, sizepolicy)
            # self.grid.addWidget(self.tag_w,0,1)
            # self.grid2 =  qt.QGridLayout(self.tag_w)
            self.tag = qt.QLabel("%s"%  peak["hkl_rounded"] )
            # self.tag.setSizePolicy(sizepolicy, sizepolicy)
            # self.grid2.addWidget(self.checkbox,0,0)
            #self.grid2.addWidget(self.tag,1,0)
            self.grid.addWidget(self.checkbox,0,1)
            self.grid.addWidget(self.tag     ,1,1)
            
        else:
            self.grid.addWidget(self.label,0, 0, 1,1)
            self.grid.addWidget(self.checkbox,0,1)
            

        
        if len(self.pmaps_alt):
            self.label_alt = qt.QLabel(  )
            self.label_alt.setSizePolicy(sizepolicy, sizepolicy)
            self.label_alt.resize(3*self.pmaps[0].width(), 3*self.pmaps[0].height())            
            self.grid.addWidget(self.label_alt,0,2)

            fn = peak["fn"]

            if "hkl" in peak:
                print( "%-- s y,x = %s"%(peak["fn"], peak["pos"] ))
                self.label_alt.setToolTip("%s y,x = %s   hkl=%s"%("alt of "+fn, peak["pos"], peak["hkl"] )  )
            else:
                print( "%s y,x = %s"%(peak["fn"], peak["pos"] ))
                self.label_alt.setToolTip("%s y,x = %s"%("alt of "+fn, peak["pos"] )  )
        print(" RESIZE ", self.pmaps[0].width())
        self.label.resize(3*self.pmaps[0].width(), 3*self.pmaps[0].height())            
        # self.label_alt.resize(3*self.pmaps[0].width(), 3*self.pmaps[0].height())            

                
        self.imgnumber=0
        self.changeFrame()
        

        # self.label.setPixmap(self.pmaps[0].scaled( self.label.width(), self.label.height(), qt.Qt.KeepAspectRatio))

        
        timer.timeout.connect(self.changeFrame)
        self.fixborder()

    def mousePressEvent(self, QMouseEvent):
        print( "PRESS " , QMouseEvent.pos())
        print( "checkbos pos ", self.checkbox.pos())

        m_pos = QMouseEvent.pos().x()
        c_pos = self.checkbox.pos().x()

        pos = self.peak["pos"]
        Gi  = self.peak["Gi"]

        stack = None
        
        if m_pos< c_pos:
            if "harvest" in self.peak:
                stack = self.peak["harvest"]


        else :
            if "harvest_alt" in self.peak:
                stack = self.peak["harvest_alt"]
        self.doSpotSelection(Gi, pos, stack  )


        
    def doSpotSelection(self, Gi, pos, stack ):
        
        for peak, dataset in zip( [self.peak, self.second_peak],[self.dataset, self.second_dataset]   ):
            if dataset is None:
                continue
            qminmax_selection = None
            qminmaxC_selection = None
            if peak is None:
                continue
            if  "hkl" in  peak    :
                if dataset is self.dataset  and   self.second_dataset is None :
                    dataset.Pfit.takeFromOwnGui()
                if dataset.Pfit.qmin is not None and dataset.Pfit.qmax is not None:
                    if stack is None:
                        message = """This Stack is None, you probably still need to run a collection of data"""
                        raise RuntimeError(message)
                    
                    dimz, dimy, dimx = stack.shape
                    assert(dimz%2==1)
                    assert(dimy%2==1)
                    assert(dimx%2==1)
                    assert(dimz==dimx)
                    assert(dimz==dimy)

                    i_window = Gi     + np.arange( -(dimz//2),  (dimz//2)+1 )
                    y_window = pos[0] + np.arange( -(dimy//2),  (dimy//2)+1 )
                    x_window = pos[1] + np.arange( -(dimx//2),  (dimx//2)+1 )

                    Q = geometry.W2Q( i_window, y_window,  x_window, dataset.PexperimentGeo.getMyParsAsObject()   , dataset.Palignement   )

                    P = dataset.Palignement 
                    cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))

                    ref = max(  map( np.linalg.norm,   brillV)  )

                    h,k,l = peak["hkl"]
                    Center = np.dot(      np.array([h,k,l]),   brillV      ) 

                    Qdiff =  Q - Center
                    dist  =  np.sqrt( (  Qdiff*Qdiff ).sum(axis=-1) )
                    qminmax_selection  = dist/ref

                    if "Blob" in peak:
                        blob = peak["Blob"]
                        offset = -blob.braggOffset
                        if offset is not None:
                            Qdiff =  Q - (Center+offset)
                            dist  =  np.sqrt( (  Qdiff*Qdiff ).sum(axis=-1) )
                            qminmaxC_selection  = dist/ref

                        ## simInt_refInt = blob.simIntensity
            if dataset is self.dataset:
                moviesShower.wscope.qminmax_selection = qminmax_selection
                moviesShower.wscope.qminmaxC_selection = qminmaxC_selection
            else:
                moviesShower.wscope.second_qminmax_selection = qminmax_selection
                moviesShower.wscope.second_qminmaxC_selection = qminmaxC_selection
        
        moviesShower.wscope.peak = self.peak
        moviesShower.wscope.second_peak = self.second_peak
        
        ## moviesShower.wscope.simInt_refInt  = simInt_refInt
        
        
        if stack is not None:

            self.isVisualisingSimu  = 0 # provare qui chiamando toggle per conservare settaggio di picco precedente
            moviesShower.wscope.setStack_s(np.abs(stack))
            moviesShower.stack = stack
            self.parent.isVisualisingSimu = 0


            self.parent.applyChangeSimRef()
            
            # self.parent.ToggleSimRef(settedVisu=self.parent.isVisualisingSimu)

            # if   qminmax_selection is not None:
            #     moviesShower.wscope.setStack(qminmax_selection)


            
            moviesShower.wscope.show()
            
            global observed
            if observed is not None:
                try:
                    observed.fixborder()
                except:
                    pass
        observed = self
        
        self.checkbox.setStyleSheet("margin:1px; border:1px solid rgb(255, 0, 0); ")
        print(" CENTERME SIGNAL EMIT ")
        self.centerme_signal.emit(self)
        
            
    def changecheck(self, status):
        self.peak["accepted"] = (status!=0)
        self.fixborder()
        
    def fixborder(self):
        if self.peak["accepted"] :
            self.checkbox.setStyleSheet("margin:1px; border:1px solid rgb(0, 255, 0);")
        else:
            self.checkbox.setStyleSheet("margin:1px; border:1px solid rgb(200, 200, 200); ")
        
    def changeFrame(self):
        if not self.peak["accepted"]:
            self.imgnumber = len(self.pmaps)//2

        self.label.setPixmap(self.pmaps[self.imgnumber%len(self.pmaps)].scaled( self.label.width(), self.label.height(), qt.Qt.KeepAspectRatio))

        if len(self.pmaps_alt):
            self.label_alt.setPixmap(self.pmaps_alt[self.imgnumber%len(self.pmaps)].scaled( self.label.width(), self.label.height(), qt.Qt.KeepAspectRatio))
        self.imgnumber+=1

           
@ui.UILoadable
class MainWindow(qt.QMainWindow):
    get_q3d_signal = qt.pyqtSignal(object,object)
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.loadUi()

        self.dataset = None
        self.second_dataset = None
        self.first_second_correspondance = None
        
        sys.excepthook = excepthook
        
        self.tabWidget.clear()



        ### diventera setDataset
        if self.dataset is not None:
            self.dataset.detach()
        
        self.dataset = Dataset.Dataset()
        
        self.extractionW = extractionParsWidget()
        self.extractionW .attachDataset(self.dataset)
        
        self.experimentGeoW = experimentGeoParsWidget()
        self.experimentGeoW .attachDataset(self.dataset)
        
        self.sampleAlignementW = sampleAlignementWidget()
        self.sampleAlignementW.attachDataset(self.dataset)
        
        self.generatorsW = generatorsParsWidget()
        self.generatorsW.attachDataset(self.dataset)

        self.fitW = fitParsWidget()
        self.fitW.attachDataset(self.dataset)
        
        self.elasticW = elasticParsWidget()
        
        self.elasticW.attachDataset(self.dataset)
        
        self.dwW = dwParsWidget( None )
        self.dwW.attachDataset(self.dataset)


        
        self.pvRenderW = planDefinitionPars(self)

        object.__setattr__(self.pvRenderW.parameters,"npoints",200)
        # self.pvRenderW.parameters.exportToOwnGui()
        
        self.pvRenderW.planDefinition_signal.connect( self.renderPlan)
        self.pvRenderW.volumeDefinition_signal.connect( self.renderVolume)

        self.fitW.pushButton_setUpFitNonCentered.clicked.connect( self.setupFitNonCentered )
        self.fitW.pushButton_setUpFitCentered.clicked.connect( self.setupFitCentered )
        self.fitW.pushButton_setUpTensor.clicked.connect( self.setupTensor )
        self.fitW.pushButton_simulate.clicked.connect( self.simulate )
        self.fitW.pushButton_fit.clicked.connect( self.fit )


        
        self.tabWidget.addTab( self.extractionW  ,"Data Extraction")
        self.tabWidget.addTab( self.experimentGeoW  ,"Experiment Geometry")
        self.tabWidget.addTab( self.sampleAlignementW  ,"Sample Alignement")
        self.tabWidget.addTab( self.generatorsW  ,"Symm. Generators")
        self.tabWidget.addTab( self.fitW  ,"Fit")
        self.tabWidget.addTab( self.elasticW  ,"Tensor")
        self.tabWidget.addTab( self.dwW  ,"Deb.Wal. Scat.Facts.")

        self.actionSave_Configuration.triggered.connect(self.saveConfigurationData)
        self.actionLoad_Configuration.triggered.connect(self.loadConfigurationData)
        self.actionSave_confOnly.triggered.connect(self.saveConfigurationOnly)
        self.actionLoad_confOnly.triggered.connect(self.loadConfigurationOnly)
        self.actionRead_fromCrysalisFile.triggered.connect(self.readFromCrysalys)
        
        self.actionSpots_movies.triggered.connect(self.addMovies)
        self.actionGL_viewer.triggered.connect(self.addGLview)


        self.actionPlan_Volume_Render.triggered.connect(self.showPVrender)
        
        self.sampleAlignementW.tagAndFilter_signal.connect(   self.tagAndFilterExtractedData )

        self.mS = None
        self.GL = None

        sys.excepthook = excepthook

    def showPVrender(self):
        # self.pvRenderW.updateBlobs()
        self.pvRenderW.show()
        if self.pvRenderW.showCount==0:
            self.pvRenderW.showCount+=1
            p=self.pvRenderW.parameters
            p.npoints=200
            p.xH = 0.5
            p.xK = 0.0
            p.xL = 0.0
            p.yH = 0.0
            p.yK = 0.5
            p.yL = 0.0
            p.zH = 0.0
            p.zK = 0.0
            p.zL = 0.5
            self.pvRenderW.parameters.exportToOwnGui()
        
    def connect_GL_movies(self):
        if None not in [  self.mS , self.GL   ]:
            
            self.GL.selected_point_signal.connect( self.mS.selectSpot   )
            self.mS.selected_spot_signal.connect( self.GL.selectPoint   )
            
        if self.GL is not None:
            self.sampleAlignementW.doAlignementWithThisPars_signal.connect( self.GL.alignByAlignementParameters)
            self.GL.alignOnPoints_signal.connect(self.Fit_hkl )

            self.GL.parsFromVisual_signal.connect(self.sampleAlignementW.setRot )



            self.GL.parsFromPossibilities_signal.connect( self.sampleAlignementW.setRotABCs )
                # .emit(  (angles, quats), abc, Aabc )

            
            for varname in ["AA","BB","CC","aAA","aBB","aCC",]:
                if  getattr( self.dataset.Palignement, varname ) is not None:
                    setattr( self.GL.parameters4AlignOnPoints, varname ,  getattr( self.dataset.Palignement, varname)  )
            self.GL.parameters4AlignOnPoints.exportToOwnGui()

            self.GL.alignByFourier_signal.connect(self.alignByFourier)

            self.GL.fit_geo_selected_pars_signal.connect( self.Fit_hkl_selected_pars  )
            
    def alignByFourier(self,  pinnedaxis  , q3d, anglesConstraints):
        builded_axis, builded_abc, builded_Aabc  = geometry.alignByFourier( pinnedaxis  , q3d  , anglesConstraints )
        assert ( len(builded_axis) == len( builded_abc) )
        self.GL.setFourierPossibilities(    builded_axis, builded_abc, builded_Aabc    )
        
    def addMovies(self):
        if self.mS is not None:
            self.removeDockWidget (self.mS)
            del self.mS
            self.mS = None

                               
        self.mS = moviesShower(self.dataset , second_dataset = self.second_dataset   , first_second_correspondance = self.first_second_correspondance     )
        self.dw_m = self.addDockWidget ( qt.Qt.LeftDockWidgetArea, self.mS )
        self.connect_GL_movies()
        
    def addGLview(self):
        # if self.GL is not None:
        #     # self.removeDockWidget (self.GL)
        #     del self.GL
        #     self.GL = None
        
        self.GL = GLwidget()
        self.connect_GL_movies()
        
        self.experimentGeoW.transform_signal.connect(  self.transform   )
        x, y, z = np.array([
            [0,0,0],
            [1,0,0],
            [0,1,0],
            [0,0,1]
        ]).T
        self.GL.setPoints(  x, y, z )

        # self.GL.show()
        
        dw = qt.QDockWidget()
        dw.setWidget(self.GL)

        dw.show()

        self.dw=dw
        
        # self.dw_gl = self.addDockWidget ( qt.Qt.RightDockWidgetArea, dw )
        self.get_q3d_signal.connect(  self.GL.set_q3d   )
        
    def Fit_hkl_selected_pars(self, par_dict, p4alonp):

        self.dataset.Palignement.takeFromOwnGui()
        ali_pars = self.dataset.Palignement
        
        self.dataset.PexperimentGeo.takeFromOwnGui()
        geo_pars = self.dataset.PexperimentGeo
        
        q1,q2,q3 = ali_pars.r1, ali_pars.r2, ali_pars.r3
        if ali_pars.Quat_or_angles == "angles":
            q1,q2,q3 = geometry.r2quat(q1,q2,q3)
            ali_pars.Quat_or_angles = "quaternions"
            ali_pars.exportToOwnGui()
            ali_pars.r1, ali_pars.r2, ali_pars.r3 = q1,q2,q3 
            ali_pars.exportToOwnGui()
            
        if p4alonp.discard is None:
            discard = 0.0
        else:
            discard = p4alonp.discard
            if discard<0 or discard>1.0:
                raise Exception( "discard factor must be within 0 and 1")

        picchi, whichiswhich =  extraction.get_nim_pos(  self.dataset.extracted_data  )
        maxN         = self.experimentGeoW.NLim

        geo_pars_A = geo_pars.getMyParsAsObject()
        ali_pars_A = ali_pars.getMyParsAsObject()

        q3d, picchi = geometry.peaks2Q(picchi, geo_pars_A, npoints = maxN, alsoOriginal = True)
        
        geometry.Fit_AllGeo_hkl ( picchi  ,  geo_pars_A , ali_pars_A ,par_dict , discard =  discard)

        for name,v in par_dict.items():
            if v :
                if hasattr(geo_pars, name) :
                    setattr(geo_pars, name, getattr(geo_pars_A, name))
                if hasattr(ali_pars, name) :
                    setattr(ali_pars, name, getattr(ali_pars_A, name))

        geo_pars.exportToOwnGui()
        ali_pars.exportToOwnGui()


                           
    def Fit_hkl(self,cell_params):
        self.dataset.Palignement.takeFromOwnGui()
        ali_pars = self.dataset.Palignement

        useangle=1
        q1,q2,q3 = ali_pars.r1, ali_pars.r2, ali_pars.r3
        if ali_pars.Quat_or_angles == "angles":
            q1,q2,q3 = geometry.r2quat(q1,q2,q3)
            useangle=0

        if cell_params.discard is None:
            discard = 0.0
        else:
            discard = cell_params.discard
            if discard<0 or discard>1.0:
                raise Exception( "discard factor must be within 0 and 1")
            
        ####
        if not hasattr(  self.GL   ,"q3d"):
            self.generic_msg( "No data to fit", "First you need to send points to GL by the 'transform to GL' button")
            return
        q3d = self.GL.q3d
        abc, Aabc, quats = ( 
            geometry.Fit_hkl ( q3d=q3d  ,
                               abc =( cell_params.AA, cell_params.BB, cell_params.CC  ),
                               fix_abc = ( cell_params.fix_AA, cell_params.fix_BB, cell_params.fix_CC  ),
                               Aabc = ( cell_params.aAA, cell_params.aBB, cell_params.aCC  ),
                               fix_Aabc = ( cell_params.fix_aAA, cell_params.fix_aBB, cell_params.fix_aCC  ),
                               quats = (q1,q2,q3), discard =  discard
            )
        )
        r1,r2,r3 = quats
        if useangle == 0:
            r1,r2,r3 = geometry.quat2r(r1,r2,r3)

        ali_pars.r1, ali_pars.r2, ali_pars.r3 = r1,r2,r3

        ali_pars.AA, ali_pars.BB, ali_pars.CC = abc
        ali_pars.exportToOwnGui()

        ali_pars.aAA, ali_pars.aBB, ali_pars.aCC = Aabc
        ali_pars.exportToOwnGui()

        
        cell_paramsW = self.GL.parameters4AlignOnPoints
        
        cell_paramsW.AA , cell_paramsW.BB , cell_paramsW.CC    = abc
        cell_paramsW.aAA, cell_paramsW.aBB, cell_paramsW.aCC   = Aabc

        cell_paramsW.exportToOwnGui()
        
# abs(h)<=4 and abs(k)<=4 and  abs(l)<=4  and h%2==0 and k%2==0 and l%2==0

    def tagAndFilterExtractedData(self, tolerance, python_filter):
        
        picchi, whichiswhich =  extraction.get_nim_pos(  self.dataset.extracted_data  )
        exp_geo_pars = self.dataset.PexperimentGeo.getMyParsAsObject()
        maxN         = self.experimentGeoW.NLim
        q3d = geometry.peaks2Q(picchi, exp_geo_pars)
        
        
        a_pars =  self.dataset.Palignement.getMyParsAsObject()      

        abc, Aabc  = ( a_pars.AA,  a_pars.BB,  a_pars.CC     ), ( a_pars.aAA,  a_pars.aBB,  a_pars.aCC )
        cellvectors, brillvectors = geometry.getCellVectors(abc,Aabc)
        

        r1,r2,r3 =   a_pars.r1,  a_pars.r2,  a_pars.r3
        
        if a_pars.Quat_or_angles == "angles" :
            r1,r2,r3 = geometry.r2quat(r1,r2,r3)
        Qfin = geometry.get_Qfin_onlySample_quats(       q3d ,   r1,r2,r3 )


        
        hkl  = np.tensordot(Qfin,cellvectors,  axes=[-1,-1] )
        rhkl = np.round(hkl) 
        err =  np.max( np.abs ( rhkl - hkl), axis = -1 )
        for a,b,c in zip(   hkl, rhkl, err    ) :
            print(" FILTRO ", a,b,c, tolerance)
        if tolerance == 0:
            tolerance = 0.05
        for i , ( imageName, pos   ) in enumerate(whichiswhich):
            if err[i]> tolerance  or not check_python_filter( (rhkl[i]).astype("i"), python_filter) :
                del self.dataset.extracted_data[imageName][(pos[0], pos[1])]
                if len( self.dataset.extracted_data[imageName]   )  == 0 :
                    del self.dataset.extracted_data[imageName]
            else:
                hkl = (rhkl[i]).astype("i")
                self.dataset.extracted_data[imageName][(pos[0], pos[1])]["hkl"] = hkl
    
        
    def transform(self):
        picchi, whichiswhich =  extraction.get_nim_pos(  self.dataset.extracted_data  )
        exp_geo_pars = self.dataset.PexperimentGeo.getMyParsAsObject()
        maxN         = self.experimentGeoW.NLim
        q3d = geometry.peaks2Q(picchi, exp_geo_pars, npoints = maxN)
        self.get_q3d_signal.emit(  q3d , whichiswhich  )



    def  readFromCrysalys(self):
        filename = qt.QFileDialog.getOpenFileName(None, "select", )
        if isinstance(filename, tuple):
            filename = filename[0]
        filename=str(filename)

        Cpars = fromCrysalis.readPars( filename ) 
        self.sampleAlignementW.getFromCrys(Cpars)
        self.experimentGeoW.getFromCrys(Cpars)
        
    def saveConfigurationOnly(self):
        self.saveConfiguration( False)
        
    def saveConfigurationData(self):
        self.saveConfiguration( True)
        
    def saveConfiguration(self, dataalso=True):
        filename = qt.QFileDialog.getSaveFileName(None, "select" )
        if isinstance(filename, tuple):
            filename = filename[0]
        print( " RETURN " , filename)
        filename=str(filename)
        if len(filename)==0:
             return



        txt = self.dataset.getConfAsYaml()
         
        self.dataset.PexperimentGeo.takeFromOwnGui()
        experiment_geo_parameter_yaml       =  self.dataset.PexperimentGeo.exportToYamlText(prepend=" "*4)
        txt = txt + "experiment_geo :\n" + experiment_geo_parameter_yaml + "\n"

        self.dataset.Palignement.takeFromOwnGui()
        sample_alignement_parameter_yaml       =  self.dataset.Palignement.exportToYamlText(prepend=" "*4)
        txt = txt + "sample_alignement :\n" + sample_alignement_parameter_yaml + "\n"

        
        self.dataset.Pgenerators.takeFromOwnGui()
        generators_parameter_yaml       =  self.dataset.Pgenerators.exportToYamlText(prepend=" "*4)
        txt = txt + "generators :\n" + generators_parameter_yaml + "\n"


        
        self.pvRenderW.parameters.takeFromOwnGui()
        pvRender_parameter_yaml       =  self.pvRenderW.parameters.exportToYamlText(prepend=" "*4)
        txt = txt + "pvRender :\n" + pvRender_parameter_yaml + "\n"        

        self.dataset.Pfit.takeFromOwnGui()
        fit_parameter_yaml       =  self.dataset.Pfit.exportToYamlText(prepend=" "*4)
        txt = txt + "fit :\n" + fit_parameter_yaml + "\n"
        
        self.dataset.Pelastic.takeFromOwnGui()
        elastic_parameter_yaml       =  self.dataset.Pelastic.exportToYamlText(prepend=" "*4)
        txt = txt + "tensor :\n" + elastic_parameter_yaml + "\n"

        # lascia questo per ultimo

        dw_parameter_yaml       =  self.dataset.Pdw.exportToYamlText(prepend=" "*4)
        if len(dw_parameter_yaml) :
            txt = txt + "dw : \n" + dw_parameter_yaml + "\n"
        
        open(filename,"w").write(txt)
        print(" SALVO ", dataalso)
        
        if dataalso:
            extraction.save_extracted_data(self.dataset.extracted_data,   filename+"_extracted_data.h5"  ) 

    def loadConfigurationOnly(self):
        self.loadConfiguration( False)
        
    def loadConfigurationData(self):
        self.loadConfiguration( True)

    def loadConfiguration(self, dataalso=True):
        
        filename = qt.QFileDialog.getOpenFileName(None, "select", )
        if isinstance(filename, tuple):
            filename = filename[0]

        filename=str(filename)
        if len(filename)==0: return
        self.loadParFile(filename, dataalso=dataalso)
        
    def loadParFile(self, filename, dataalso=True, second_file = None, max_dist = None):
        d = Dataset.yaml.load(open(filename,"r"), Dataset.Loader)
        self.dataset.takeConfFromDict(d)
        
        self.elasticW.init_pars_W()
        self.dwW.update()
         
        if "pvRender" in d:
            self.pvRenderW.parameters.takeFromDict(d["pvRender"])
            self.pvRenderW.parameters.exportToOwnGui()

            
        if dataalso:
            self.dataset.extracted_data = extraction.load_extracted_data( filename+"_extracted_data.h5"  ) 
            
        self.second_dataset = None
        if second_file is not None:
            self.second_dataset = Dataset.Dataset()

            second_d = Dataset.yaml.load(open(second_file,"r"), Dataset.Loader)
            self.second_dataset.takeConfFromDict(second_d)
            
            self.second_dataset.extracted_data = extraction.load_extracted_data( second_file+"_extracted_data.h5"  ) 

            self.first_second_correspondance = {}

            
            for imageName, data4im in self.dataset.extracted_data.items():
                for pos, data4pos in data4im.items():
                    
                    if data4pos["accepted"] == True:
                        for sec_imageName, sec_data4im in self.second_dataset.extracted_data.items():
                            for sec_pos, sec_data4pos in sec_data4im.items():
                                if sec_data4pos["accepted"] == True:
                                    Gi = data4pos["Gi"]
                                    sec_Gi = sec_data4pos["Gi"]

                                    
                                    if (abs( Gi-sec_Gi   ) <= max_dist) and (abs(pos[0]-sec_pos[0]) <= max_dist) and (abs(pos[1] -sec_pos[1]) <= max_dist) :
                                        self.first_second_correspondance[  (imageName,pos)   ] = (sec_imageName,sec_pos)
                                        break
                                        
            for imageName, data4im in list(self.dataset.extracted_data.items()):
                for pos, data4pos in list(data4im.items()):
                    if (imageName,pos) not in self.first_second_correspondance:
                        del  self.dataset.extracted_data [ imageName] [ pos ]
                        
                if len( self.dataset.extracted_data[imageName]  ) == 0 :
                    del self.dataset.extracted_data[imageName]

            for k1,k2 in self.first_second_correspondance.items():
                print( k1, k2 )
                    
    
    def renderPlan(self, planPars ):
        readingPars = self.dataset.Pextraction.getMyParsAsObject()
        geoPars = self.dataset.PexperimentGeo.getMyParsAsObject()
        alignPars = self.dataset.Palignement.getMyParsAsObject()
        symmPars = [ tok    for tok in  self.dataset.Pgenerators.slots  if getattr(self.dataset.Pgenerators, tok )]  

        P = alignPars
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))

        pP = planPars


        cH_cK_cL =  np.array([pP.centerH, pP.centerK, pP.centerL])
        selected_spot  = None

        
        if pP.checkBox_actOnSim is not None and pP.checkBox_actOnSim:
            if  hasattr( moviesShower.wscope,"peak")  and    "hkl" in  moviesShower.wscope.peak:
                # c=StringIO(( unicode(pP.comboBox_selectSpot)    ).replace("[","").replace("]",""))
                # cH_cK_cL =  loadtxt(c,delimiter=",")
                cH_cK_cL = moviesShower.wscope.peak["hkl"]
                selected_spot  = moviesShower.wscope.peak
                
        Center = np.dot(   cH_cK_cL  ,   brillV      ) 

        Xaxis  =  np.dot(  np.array(  [ pP.xH, pP.xK, pP.xL ] )  ,    brillV )
        Yaxis  =  np.dot(  np.array(  [ pP.yH, pP.yK, pP.yL ] )  ,    brillV )


        
        if abs(np.dot(Yaxis,Xaxis)) > np.dot(Xaxis,Xaxis) :
            Xaxis = Xaxs * abs(np.dot(Yaxis,Xaxis)) / np.dot(Xaxis,Xaxis)
        
        normX  = np.linalg.norm(Xaxis)
        
        dQ = 2*normX/(pP.npoints-1)
        npointsX = pP.npoints

        Xaxis = Xaxis / normX
    
        Yaxis = Yaxis - Xaxis *np.dot(  Yaxis, Xaxis   )
        
        normY = np.linalg.norm(Yaxis)
        npointsY = int(   round(2*normY/dQ) +1  )
        Yaxis =  Yaxis/normY

        Origin = Center - Xaxis*dQ/2.0*npointsX - Yaxis*dQ/2.0*npointsY

        P = alignPars
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))
        
        oplist = geometry.genlist2oplist(symmPars, cellV)

        method="nearest"
        if pP.rb_sinc :
            method="sinc"
        if pP.rb_sinc_xyn :
            method="sinc_xyn"
        
       
        image = extraction.fillPlan(readingPars, geoPars, alignPars , oplist,Origin, Xaxis, Yaxis, dQ, npointsX, npointsY, method,   selected_spot)  

        self.pvRenderW.dvf.setData(image)


    def  getTensorStructure(self):
        genPars =  self.dataset.Pgenerators
        
        symmPars = [ tok    for tok in  genPars.slots  if getattr(genPars, tok )]
        alignPars = self.dataset.Palignement.getMyParsAsObject()
        P = alignPars
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))
        oplist = geometry.genlist2oplist(symmPars,cellV )
        nomi_variabili,vettori_componenti, tensore_string  = geometry.get_elastic_simmetry(  oplist  )
        return nomi_variabili,vettori_componenti, tensore_string 

    def  getTensorVars(self,   nomi_variabili)   :

        elParameters =  self.dataset.Pelastic

        assert( set(elParameters.slots) == set(nomi_variabili))
        
        res = []
        for n in nomi_variabili:
            res.append( getattr( elParameters, n )  )
            # print( n, " --> ", getattr( elParameters, n ) ) 
        return np.array(res)

    def setupFitCentered(self):
        self.setupFit( centering = 1 )
        self.BlobsAreCentered = 1

    def setupFitNonCentered(self):
        self.setupFit( centering = 0 )
        self.BlobsAreCentered = 0


    def generic_msg(self, msg1, msg2):
        msgBox = qt.QMessageBox(None)
        msgBox.setText(msg1)
        msgBox.setInformativeText(msg2)
        msgBox.setStandardButtons(  qt. QMessageBox.Ok)
        msgBox.setDefaultButton( qt.QMessageBox.Ok)
        ret = msgBox.exec_()
        
    def noBlobs_msg(self):
        msgBox = qt.QMessageBox(None)
        msgBox.setText(" No blobs have been treated " )
        msgBox.setInformativeText("Check if you have performed the necessary steps. In particular tagging the spots in the alignement tab")
        msgBox.setStandardButtons(  qt. QMessageBox.Ok)
        msgBox.setDefaultButton( qt.QMessageBox.Ok)
        ret = msgBox.exec_()
        
    def noGeo_msg(self):
        msgBox = qt.QMessageBox(None)
        msgBox.setText(" Incomplete Configuration " )
        msgBox.setInformativeText(" you need to perform setup steps")
        msgBox.setStandardButtons(  qt. QMessageBox.Ok)
        msgBox.setDefaultButton( qt.QMessageBox.Ok)
        ret = msgBox.exec_()
        
    def noTensor_msg(self):
        msgBox = qt.QMessageBox(None)
        msgBox.setText(" Tensor not Configuration " )
        msgBox.setInformativeText(" you need to perform setup steps")
        msgBox.setStandardButtons(  qt. QMessageBox.Ok)
        msgBox.setDefaultButton( qt.QMessageBox.Ok)
        ret = msgBox.exec_()           
       

    def fit(self):
        print( " in fit ")

        data = self.dataset.extracted_data
        Nblobs = 0
        for imagename, imspots in data.items():
            for pos, peak in imspots.items():
                if peak["accepted"] and  "Blob" in peak:
                    Nblobs+=1
                    
        if Nblobs == 0:
            self.noBlobs_msg()
            return
        
        if self.BlobsAreCentered  or self.second_dataset is not  None:
            w = choiceVarsFitCentered(self.nomi_variabili_tensor )
        else:
            w = choiceVarsFit(self.nomi_variabili_tensor )
        w.selected_pars_signal.connect(self.fitElastic)
        w.show()
        self.wstore_fit = w

    def fitElastic(self, par_dict):
        print( " ricevuto ", par_dict)
        initial_vars = []
        whatToSet = []

        # alignPars    = copy.deepcopy(self.dataset.Palignement)
        # elParameters = copy.deepcopy(self.dataset.Pelastic         )
        # fitPars      = copy.deepcopy(self.dataset.Pfit             )
        # geoPars      = copy.deepcopy(self.dataset.PexperimentGeo   )
        
        alignPars    = (self.dataset.Palignement)
        elParameters = (self.dataset.Pelastic         )
        fitPars      = (self.dataset.Pfit             )
        if self.second_dataset is not None:
            second_fitPars =  self.second_dataset.Pfit
        else:
            second_fitPars =None

        
        geoPars      = (self.dataset.PexperimentGeo   )


        varSources = []
        if par_dict["constraints"]!="":
            lexer = shlex.shlex(par_dict["constraints"])
            for name in lexer:
                for container in  [geoPars, alignPars, elParameters]:
                    if hasattr( container, name):
                        varSources.append(   [    container     ,   name     ]      )
        
        for name, value in par_dict.items():
            if name != "constraints":
                if value:
                    for container in  [geoPars, alignPars, elParameters]:
                        if hasattr( container, name):
                            initial_vars.append( getattr( container,name )   )
                            whatToSet.append(   [    container     ,   name     ]      )

        class  Functor_Elastic:
            def __init__(self, extractedData , geo_pars ,ali_pars , el_pars, fit_pars,second_fit_pars,
                         whatToSet , geo_cy,  tens_cy  , nomi_variabili_tensor, blobsAreCentered,
                         varSources,   constraints):

                self.geo_pars = geo_pars
                self.ali_pars = ali_pars
                self.el_pars = el_pars
                self.fit_pars = fit_pars
                self.second_fit_pars = second_fit_pars
                
                self.whatToSet = whatToSet

                self.geo_cy  = geo_cy
                self.tens_cy = tens_cy
                self.nomi_variabili_tensor = nomi_variabili_tensor

                self.extractedData = extractedData

                self.BlobsAreCentered = blobsAreCentered
                self.varSources       = varSources
                self.constraints      = constraints
                
            def __call__(self,x):


                GETOLD = 0

                
                for v, w2s in zip(x, self.whatToSet):


                    print( w2s[1] , " --> ", v ) 
                    setattr(w2s[0], w2s[1], v)

                penality = 0
                if self.constraints!="":
                    namespace={}
                    namespaceG={}
                    for source,name  in self.varSources:
                        namespace[name] = getattr(source,name)
                        
                    lines = list(self.constraints.split("\n"))
                    
                    for l in lines:
                        if not( ">" in l or "<" in l):
                            exec(l, namespaceG, namespace)
                    
                    for l in lines:
                        if ">" in l or "<" in l:
                            if not eval(l, namespaceG, namespace):
                                penality+=1.0e20
                    
                    for source,name in  self.varSources:
                        setattr(source,name, namespace[name])


                    
                utils.setCyFromDict( self.geo_cy,      self.geo_pars.exportToDict())
                utils.setCyFromDict( self.geo_cy,       self.ali_pars.exportToDict() )

                self.geo_cy.precalculate()

                elParameters =  self.el_pars

                assert( set(elParameters.slots) == set(self.nomi_variabili_tensor))
                
                tvars = []
                for n in self.nomi_variabili_tensor:
                    tvars.append( getattr( self.el_pars, n )  )
                    # print( n, " --> ", getattr( self.el_pars , n ) ) 
                tvars =  np.array(tvars,"f")

                self.tens_cy.setTensorVars(  tvars )

                temp    = self.fit_pars.Temperature
                if self.second_fit_pars is not None:
                    second_temp = self.second_fit_pars.Temperature
                else:
                    second_temp = None

                
                density = self.fit_pars.Density
                
                data = self.extractedData

                KELVIN2ERG = 1.380648780669e-16
                HNOBAR_CGS   = 6.626069e-27  ## /(2.0*math.pi)
                DENSITY    = density
                # 1 Pa = J/m3   ==>    erg/cm3  = 0.1 J/m3   ==>  1GPa = 10**10 erg/cm3
                GPA2CGS  = 1.0e+10
                A2CM  = 1.0e-8      # dans le calcul de la frequence on utilise des Qs qui sont en A**-1
                FACTOR4T =  A2CM*  math.sqrt(DENSITY/GPA2CGS)*  KELVIN2ERG / (    HNOBAR_CGS)

                res=penality


                if GETOLD:
                    Npt_l = []
                    hkl_l = []
                    data_l = []
                    qs_l = []
                    facts1_l = []
                    facts2_l = []

                    
                
                for imagename, imspots in data.items():
                    for pos, peak in imspots.items():
                        if peak["accepted"] and "Blob" in peak:
                            blob = peak["Blob"]
                            
                            blob.clearSimulation()
                            if self.BlobsAreCentered :
                                if blob.braggOffset is None:
                                    continue

                            if self.fit_pars.Rarefaction is None:
                                rarefaction = 1
                            else:
                                rarefaction = int(self.fit_pars.Rarefaction)

                            if rarefaction<1:
                                rarefaction = 1


                            if GETOLD:
                                
                                Npt,hkls_py, data_py, qs_py, facts_py =  blob.getDataForOld( )
                                hkls_py, data_py, qs_py, facts_py = map(np.array, [hkls_py, data_py, qs_py, facts_py])
                                
                                qs_py.shape =   len(qs_py )//3   , 3
                                
                                Npt_l.append(Npt )
                                hkl_l.append(hkls_py)
                                data_l.append(data_py)
                                qs_l.append(qs_py)
                                facts1_l.append(facts_py[ :len( facts_py) //2   ])
                                facts2_l.append(facts_py[ len( facts_py) //2  :  ])

                                
                            if second_temp is None:
                                res = res + blob.simulate(FACTOR4T*temp , rarefaction )
                            else:
                                res = res + blob.simulate2T(FACTOR4T*temp, FACTOR4T*second_temp , rarefaction )
                                

                if GETOLD:
                    f = h5py.File("olddata.h5","w")
                    f["Npts"] = np.array( Npt_l ) 
                    f["hkls"] = np.array( hkl_l )
                    f["datas"] = np.concatenate(  data_l ) 
                    f["qs"] = np.concatenate(  qs_l , axis = 0)
                    
                    f["facts1"] = np.concatenate(  facts1_l ) 
                    f["facts2"] = np.concatenate(  facts2_l ) 
                    f.close()
                    raise
                
                            
                print("ERROR ", res)
                return res

        if not hasattr(self, "geo_cy"):
            self.noGeo_msg()
            return
        
        if not hasattr(self, "tensor_cy"):
            self.noTensor_msg()
            return

            
        funct = Functor_Elastic(self.dataset.extracted_data , geoPars ,alignPars ,
                                elParameters, fitPars, second_fitPars ,   whatToSet , self.geo_cy,
                                self.tensor_cy  , self.nomi_variabili_tensor, self.BlobsAreCentered,
                                varSources , 
                                par_dict["constraints"]
        )
        # print( " NOMI VARIABILI TENSOR " , self.nomi_variabili_tensor  ) 
        # tests = [
        #     [{"A11":305.0,"A44":154.0,"A12":90.0}[tok] for c,tok in  whatToSet],
        #     [{"A11":305.0,"A44":54.0,"A12":90.0}[tok] for  c,tok in  whatToSet],
        #     [{"A11":305.0,"A44":154.0,"A12":190.0}[tok] for  c,tok in  whatToSet],
        # ]
        # print (" ERRORI ")
        # for t in tests:
        #     print(" SETTO ", t)
        #     res = funct(t)
        #     print (" RESULT ", res)

        # raise
        try:
            result = optimize.minimize(funct, initial_vars,  method='Nelder-Mead')
            
            print("RISULTATO FIT ELASTIC CONSTANTS", result)
            x = result.x

            geoPars      = self.dataset.PexperimentGeo   
            alignPars    = self.dataset.Palignement
            elParameters = self.dataset.Pelastic         
            
            for val,(cnter ,name) in zip(x, whatToSet):
                # print (   val,(cnter ,name) ) 
                for container in  [geoPars, alignPars, elParameters]:
                    if hasattr( container, name):
                        setattr( container,name ,val)
                    
        except:
            #traceback.print_stack()
            traceback.print_exc()
            pass
        
        for container in  [geoPars, alignPars, elParameters]:
            container.exportToOwnGui()

        # for source, target in zip ( [geoPars,
        #                              alignPars,
        #                              elParameters  ] ,
        #                             [self.dataset.PexperimentGeo,
        #                              self.dataset.Palignement,
        #                              self.dataset.Pelastic]  ) :
        #     for n in source.slots:
        #         val = getattr( source,n)
        #         setattr(target,n,val)
        #     target.exportToOwnGui()

            
    def simulate(self) :

        #senza geo
        #  controllare qua


        print(" in simulate")

        if not hasattr(self, "geo_cy"):
            self.noGeo_msg()
            return

        if not hasattr(self, "tensor_cy"):
            self.noTensor_msg()
            return

        if self.second_dataset is not None:
            second_fitPars =  self.second_dataset.Pfit
        else:
            second_fitPars =None

        
        utils.setCyFromDict( self.geo_cy,  self.dataset.PexperimentGeo.exportToDict(),  self.dataset.PexperimentGeo.immutable_slots)
        
        self.geo_cy.setup_beam( *( self.dataset.PexperimentGeo.get_axes_angles("beam")) ) 
        self.geo_cy.setup_stage( *( self.dataset.PexperimentGeo.get_axes_angles("holder")) ) 
        self.geo_cy.setup_dect( *( self.dataset.PexperimentGeo.get_axes_angles("detector")) ) 
        
        utils.setCyFromDict( self.geo_cy,       self.dataset.Palignement.exportToDict() )
        # utils.setCyFromDict( geo_cy,       self.dataset.Pfit.exportToDict() )
        
        self.geo_cy.precalculate()

        
        tensor_cy = self.tensor_cy
        tvars = self.getTensorVars( self.nomi_variabili_tensor  )
        tensor_cy.setTensorVars(  np.array(tvars,"f")  )
         
        data = self.dataset.extracted_data

        temp = self.dataset.Pfit.Temperature
        density = self.dataset.Pfit.Density

        if second_fitPars is not None:
            second_temp = second_fitPars.Temperature
        else:
            second_temp = None

        
        KELVIN2ERG = 1.380648780669e-16
        HNOBAR_CGS   = 6.626069e-27  ## /(2.0*math.pi)
        DENSITY    = density
        # 1 Pa = J/m3   ==>    erg/cm3  = 0.1 J/m3   ==>  1GPa = 10**10 erg/cm3
        GPA2CGS  = 1.0e+10
        A2CM  = 1.0e-8      # dans le calcul de la frequence on utilise des Qs qui sont en A**-1
        FACTOR4T =  A2CM*  math.sqrt(DENSITY/GPA2CGS)*  KELVIN2ERG / (    HNOBAR_CGS)

        Nblobs = 0
        for imagename, imspots in data.items():
            for pos, peak in imspots.items():
                if peak["accepted"] and  "Blob" in peak:
                    blob = peak["Blob"]

                    Nblobs+=1
                    blob.clearSimulation()
                    
                    if self.BlobsAreCentered :
                        if blob.braggOffset is None:
                            continue
                    if second_temp is None:
                        blob.simulate(FACTOR4T*temp )
                    else:
                        blob.simulate2T(FACTOR4T*temp, FACTOR4T*second_temp )


        print(" Treated ", Nblobs , " blobs ")

        if Nblobs == 0:
            self.noBlobs_msg()
        
    # e siamo pronti a visualizzare
        
    def setupTensor(self):        
        nomi_variabili,vettori_componenti, tensore_string = self.getTensorStructure()
        
        tensor_cy = spotpicker_cy.Tensor_cy()
        tensor_cy.setTensorComponents( np.array(vettori_componenti,"f")   )

        assert( set(nomi_variabili) == set( self.dataset.Pelastic.slots )  )
 
        tvars = self.getTensorVars(  nomi_variabili )
        print(" SETTO TENSOR VARS ", tvars);
        tensor_cy.setTensorVars(  np.array(tvars,"f")  )
        
        self.setTensorToBlobs(tensor_cy)
        self.tensor_cy = tensor_cy
        
        self.nomi_variabili_tensor  = nomi_variabili

        
        print(" FINITO ")
        # raise

    def setTensorToBlobs(self,  tensor_cy ):
        data = self.dataset.extracted_data
        for imagename, imspots in data.items():
            for pos, peak in imspots.items():
                if "Blob" in peak:
                    blob =  peak["Blob"]
                    blob.setTensor  ( tensor_cy) 

    def removeBlobs(self):
        data = self.dataset.extracted_data
        for imagename, imspots in data.items():
            for pos, peak in imspots.items():
                if "Blob" in peak:
                    del peak["Blob"]

    def setupFit(self, centering = 0):
        print( " in setup fit " )


        for dataset in [self.dataset, self.second_dataset]:

            if dataset is None:
                continue
            
            geoPars = dataset.PexperimentGeo.getMyParsAsObject()
            alignPars = dataset.Palignement.getMyParsAsObject()

            # fitPars   = self.dataset.Pfit.getMyParsAsObject()
            # debyewPars    = [self.dwW.DWs   ,  self.dwW.atomNames   ,  self.dwW.atomPositions   ]

            geo_cy = spotpicker_cy.Geo_cy()
            geo_cy.orientation_codes = dataset.PexperimentGeo.orientation_codes

            utils.setCyFromDict( geo_cy,      dataset.PexperimentGeo.exportToDict(),  dataset.PexperimentGeo.immutable_slots)

        
            geo_cy.setup_beam( *(dataset.PexperimentGeo.get_axes_angles("beam")) ) 
            geo_cy.setup_stage( *(dataset.PexperimentGeo.get_axes_angles("holder")) ) 
            geo_cy.setup_dect( *(dataset.PexperimentGeo.get_axes_angles("detector")) ) 

            utils.setCyFromDict( geo_cy,       dataset.Palignement.exportToDict() )
            utils.setCyFromDict( geo_cy,       dataset.Pfit.exportToDict(), ["qmin", "qmax"] )

            geo_cy.setup_orientations(
                geometry.BEAM.direction,
                np.array(
                    [
                        geometry.DETECTOR.direction,
                        geometry.DETECTOR.orientation[1],
                        geometry.DETECTOR.orientation[0],
                    ]
                )
            )
            
            geo_cy.precalculate()


            

            if dataset is self.dataset:
                self.geo_cy = geo_cy
            else:
                self.second_geo_cy = geo_cy
        
        self.removeBlobs()
        
        picchi, whichiswhich =  extraction.get_nim_pos(  self.dataset.extracted_data  )
        for  (Gi, posy, posx),   (  imagename, pos)  in zip(picchi, whichiswhich) :
            
            picco = self.dataset.extracted_data[imagename][(posy,posx)]
            if not picco["accepted"] :
                continue

            if self.second_dataset is not None:
                
                if (imagename, tuple(pos)) not in self.first_second_correspondance:
                    continue
                
                sec_imagename, sec_pos = self.first_second_correspondance[(imagename,tuple(pos))]

                sec_picco = self.second_dataset.extracted_data[sec_imagename][tuple(sec_pos)]

                if not sec_picco["accepted"] :
                    continue
            

            if 'hkl' not in picco:
                msgBox = qt.QMessageBox(None)
                msgBox.setText(" HKL tag not available")
                msgBox.setInformativeText("Previous to doing fits you have to run the tagging button in the Sample Alignement tab")
                msgBox.setStandardButtons(  qt. QMessageBox.Ok)
                msgBox.setDefaultButton( qt.QMessageBox.Ok)
                ret = msgBox.exec_()
                break



            if  self.dataset.Pfit.Exclusions is None or self.dataset.Pfit.Exclusions == "None":
                exclusions =     np.array([0.0,0,1.0,1.0e-20],"f")
            else:
                exclusions =     np.array( self.dataset.Pfit.Exclusions  ,"f")
                
            
            newBlob = spotpicker_cy.Blob_cy(   picco['hkl'], Gi , posy,posx, picco['harvest']  , self.geo_cy, centering, exclusions)
            
            if centering and newBlob.braggOffset is None:
                continue



            
            if self.second_dataset is not None:

                sec_newBlob = spotpicker_cy.Blob_cy(   sec_picco['hkl'], sec_picco["Gi"] , sec_pos[0] , sec_pos[1] , sec_picco['harvest']  , self.second_geo_cy, centering, exclusions)
                
                if centering and sec_newBlob.braggOffset is None:
                    continue
            else:
                sec_newBlob = None
            
            self.dataset.extracted_data [imagename][(pos[0], pos[1])]["Blob"] = newBlob
            self.dataset.extracted_data [imagename][(pos[0], pos[1])]["sec_Blob"] = sec_newBlob # to avoid garbage collection

            # print ( " ______________________________________________________________________w000_______")
            for dataset , blob in zip( [self.dataset, self.second_dataset], [ newBlob, sec_newBlob ]):
                
                if dataset is None:
                    continue


                bd0 = (blob.dim0-1)//2
                bd1 = (blob.dim1-1)//2
                bd2 = (blob.dim2-1)//2


                
                ###############################################""
                ## setting up correction
                #
                q0tmp, corr00 =  geometry.get_Q0bypos(posy   ,posx   ,  dataset.PexperimentGeo      , alsoCorr = True)
                q0tmp, corr10 =  geometry.get_Q0bypos(posy+bd1,posx   ,  dataset.PexperimentGeo      , alsoCorr = True)
                q0tmp, corr01 =  geometry.get_Q0bypos(posy   ,posx+bd2,  dataset.PexperimentGeo      , alsoCorr = True)
                blob.corr = corr00
                blob.DcorrDy = (corr10-corr00)
                blob.DcorrDx = (corr01-corr00)

                ###############################################################

                ###################################################
                # setting up DW and scattering factors
                #
                if dataset.Pdw.DWs is not None:


                    
                    q000 = blob.getQfromCenter( 0,0,0)
                    
                    q100 = blob.getQfromCenter( bd0,0  ,0  )
                    q010 = blob.getQfromCenter( 0  ,bd1,0  )
                    q001 = blob.getQfromCenter( 0  ,0  ,bd2)
                    
                    q110 = blob.getQfromCenter( bd0,bd1,0)
                    q011 = blob.getQfromCenter( 0  ,bd1,bd2)
                    q101 = blob.getQfromCenter( bd0,0  ,bd2)

                    qm00 = blob.getQfromCenter( -bd0,0  ,0  )
                    q0m0 = blob.getQfromCenter( 0  ,-bd1,0  )
                    q00m = blob.getQfromCenter( 0  ,0  ,-bd2)
                    
                    qmm0 = blob.getQfromCenter( -bd0,-bd1,0)
                    q0mm = blob.getQfromCenter( 0  ,-bd1,-bd2)
                    qm0m = blob.getQfromCenter( -bd0,0  ,-bd2)


                    q1m0 = blob.getQfromCenter(  bd0,-bd1,0)
                    q01m = blob.getQfromCenter( 0  , bd1,-bd2)
                    q10m = blob.getQfromCenter(  bd0,0  ,-bd2)

                    qm10 = blob.getQfromCenter( -bd0, bd1,0)
                    q0m1 = blob.getQfromCenter( 0  ,-bd1, bd2)
                    qm01 = blob.getQfromCenter( -bd0,0  , bd2)
                    

                    w000 = Get_dwscatt_fact(q000, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    
                    w001 = Get_dwscatt_fact(q001, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w010 = Get_dwscatt_fact(q010, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w100 = Get_dwscatt_fact(q100, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    w00m = Get_dwscatt_fact(q00m, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w0m0 = Get_dwscatt_fact(q0m0, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    wm00 = Get_dwscatt_fact(qm00, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    wmm0 = Get_dwscatt_fact(qmm0, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w0mm = Get_dwscatt_fact(q0mm, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    wm0m = Get_dwscatt_fact(qm0m, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    w110 = Get_dwscatt_fact(q110, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w011 = Get_dwscatt_fact(q011, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w101 = Get_dwscatt_fact(q101, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    
                    w1m0 = Get_dwscatt_fact(q1m0, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w01m = Get_dwscatt_fact(q01m, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w10m = Get_dwscatt_fact(q10m, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    wm10 = Get_dwscatt_fact(qm10, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    w0m1 = Get_dwscatt_fact(q0m1, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)
                    wm01 = Get_dwscatt_fact(qm01, dataset.Pdw.DWs,  dataset.Pdw.atomNames , dataset.Pdw.atomPositions, dataset.PexperimentGeo.lmbda)

                    # print ( " w000 " , w000 ) 
                   
                    blob.Wcorr = w000
                    
                    blob.DWcorrDn = (w100-wm00)/2.0
                    blob.DWcorrDy = (w010-w0m0)/2.0
                    blob.DWcorrDx = (w001-w00m)/2.0

                    blob.DWcorrDnn = (w100+wm00-w000)/2.0
                    blob.DWcorrDyy = (w010+w0m0-w000)/2.0
                    blob.DWcorrDxx = (w001+w00m-w000)/2.0

                    blob.DWcorrDny = ( (w110 + wmm0  -w1m0 - wm10  )/4.0     )
                    blob.DWcorrDyx = ( (w011 + w0mm  -w01m - w0m1  )/4.0     )
                    blob.DWcorrDnx = ( (w101 + wm0m  -w10m - wm01  )/4.0     )

                    
                else:
                    blob.Wcorr    = 1.0
                    blob.DWcorrDn = 0.0
                    blob.DWcorrDy = 0.0
                    blob.DWcorrDx = 0.0

                    blob.DWcorrDnn = 0.0
                    blob.DWcorrDyy = 0.0
                    blob.DWcorrDxx = 0.0

                    blob.DWcorrDny = 0.0
                    blob.DWcorrDyx = 0.0
                    blob.DWcorrDnx = 0.0

            if self.second_dataset is not None:
                newBlob.merge2T( sec_newBlob  ) 

            # newBlob.showDW()

#        for imagename in self.dataset.extracted_data:
#            
#            dds = self.dataset.extracted_data[imagename]
#            for pos in dds:
#                d4s = dds[pos]
#                b = d4s["Blob"]
#                b.showDW()

                


                
            
        
    def renderVolume(self, planPars ):
        readingPars = self.dataset.Pextraction.getMyParsAsObject()
        geoPars = self.dataset.PexperimentGeo.getMyParsAsObject()
        alignPars = self.dataset.Palignement.getMyParsAsObject()
        symmPars = [ tok    for tok in  self.dataset.Pgenerators.slots  if getattr(self.dataset.Pgenerators, tok )]  

        P = alignPars
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))

        pP = planPars
        cH_cK_cL =  np.array([pP.centerH, pP.centerK, pP.centerL])


        selected_spot  = None
 
        if pP.checkBox_actOnSim is not None and pP.checkBox_actOnSim:
            if  hasattr( moviesShower.wscope,"peak")  and    "hkl" in  moviesShower.wscope.peak:
                # c=StringIO(( unicode(pP.comboBox_selectSpot)    ).replace("[","").replace("]",""))
                # cH_cK_cL =  loadtxt(c,delimiter=",")
                cH_cK_cL = moviesShower.wscope.peak["hkl"]
                selected_spot  = moviesShower.wscope.peak

        print(cH_cK_cL ) 
        print( brillV) 
        Center = np.dot(   cH_cK_cL  ,   brillV      ) 
       
        # Center = np.dot(      np.array([pP.centerH, pP.centerK, pP.centerL]),   brillV      ) 

        Xaxis  =  np.dot(  np.array(  [ pP.xH, pP.xK, pP.xL ] )  ,    brillV )
        Yaxis  =  np.dot(  np.array(  [ pP.yH, pP.yK, pP.yL ] )  ,    brillV )
        Zaxis  =  np.dot(  np.array(  [ pP.zH, pP.zK, pP.zL ] )  ,    brillV )

        if abs(np.dot(Yaxis,Xaxis)) > np.dot(Xaxis,Xaxis) :
            Xaxis = Xaxis * abs(np.dot(Yaxis,Xaxis)) / np.dot(Xaxis,Xaxis)
            
        if abs(np.dot(Zaxis,Xaxis)) > np.dot(Xaxis,Xaxis) :
            Xaxis = Xaxis * abs(np.dot(Zaxis,Xaxis)) / np.dot(Xaxis,Xaxis)
            
        if abs(np.dot(Zaxis,Yaxis)) > np.dot(Yaxis,Yaxis) :
            Yaxis = Yaxis * abs(np.dot(Zaxis,Yaxis)) / np.dot(Yaxis,Yaxis)


        print(" ASSI ADESSO ", Xaxis,  Yaxis,   Zaxis  ) 


            
        normX  = np.linalg.norm(Xaxis)

        dQ = 2*normX/(pP.npoints-1)
        npointsX = pP.npoints

        Xaxis = Xaxis / normX

        print(" ASSI ADESSO ", Xaxis,  Yaxis,   Zaxis  ) 
       

        Yaxis = Yaxis - Xaxis *np.dot(  Yaxis, Xaxis   )

        normY = np.linalg.norm(Yaxis)
        npointsY = int(   round(2*normY/dQ) +1  )
        
        Yaxis = Yaxis / normY
        
        print(" ASSI ADESSO ", Xaxis,  Yaxis,   Zaxis  ) 

        Zaxis = Zaxis - Xaxis *np.dot(  Zaxis, Xaxis   )
        Zaxis = Zaxis - Yaxis *np.dot(  Zaxis, Yaxis   )

        normZ = np.linalg.norm(Zaxis)

        print(" ASSI ADESSO ", Xaxis,  Yaxis,   Zaxis  ) 

        
        npointsZ = int(   round(2*normZ/dQ) +1  )
        
        Zaxis = Zaxis / normZ

        Origin = Center - Xaxis*dQ/2.0*npointsX - Yaxis*dQ/2.0*npointsY- Zaxis*dQ/2.0*npointsZ



        P = alignPars
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))

        oplist =  geometry.genlist2oplist(symmPars, cellV)
        # oplist =  geometry.genlist2oplist(symmPars, brillV)

                
        method="nearest"
        if pP.rb_sinc :
            method="sinc"
        if pP.rb_sinc_xyn :
            method="sinc_xyn"

        Volume = extraction.fillVolume(readingPars, geoPars, alignPars , oplist , Origin, Xaxis, Yaxis, Zaxis, dQ, npointsX, npointsY,  npointsZ, method, selected_spot)

        print(" Ottenuto ", Volume.shape)
        
        self.pvRenderW.dvf.setData(Volume)

        # v = self.pvRenderW.dvf.__dataViewer.__views[0]
        # v.widget._browser.valueChanged[int].connect(self.pvRenderW.dvf.changeOfPlane)

        
@ui.UILoadable
class planDefinitionPars(qt.QSplitter):
    planDefinition_signal = qt.pyqtSignal(object)
    volumeDefinition_signal = qt.pyqtSignal(object)
    def __init__(self, mw):
        super( planDefinitionPars    , self).__init__(None)
        self.loadUi()
        self.showCount=0
        self.setChildrenCollapsible(False)
        self.MW = mw
        self.dvf =         silx.gui.data.DataViewerFrame.DataViewerFrame(self)
      
        self.dvf.setVisible(True)
        
        # self.layout().addWidget(self.dvf)
        self.addWidget(self.dvf)
        
        # self.setStretchFactor(0, int stretch)
        
        a=np.arange(10000)
        a.shape=100,100
        self.dvf.setData(a)

        names = ["centerH", "centerK", "centerL",
                 "xH", "xK", "xL",
                 "yH", "yK", "yL",
                 "zH", "zK", "zL",
                 "npoints","rb_nearest", "rb_sinc", "rb_sinc_xyn",
                 "checkBox_actOnSim"
        ]
        par_dict = {}
        for n in names:
            par_dict[n] = getattr(self,n)
            
        class planDefParameters(Parameters):
             slots = list(par_dict.keys())
             entries= par_dict
             classes = {}
             for name in slots:
                 if name not in  ["npoints" ,  "rb_nearest", "rb_sinc", "rb_sinc_xyn"  ,"checkBox_actOnSim"]:
                     classes[name]=float
                 else:
                     classes[name]=int

             
        self.parameters =      planDefParameters()

        
        self.pushButton_plan.clicked.connect(self.renderPlan)
        self.pushButton_volume.clicked.connect(self.renderVolume)
        self.pushButton_volumeSave.clicked.connect(self.saveVolume)

    # def updateBlobs(self):

    #     data = self.MW.dataset.extracted_data
    #     blobs = []
    #     for imagename, imspots in data.items():
    #         for pos, peak in imspots.items():
    #             if peak["accepted"] and  "Blob" in peak and 'hkl'  in peak:
    #                 blobs.append(  str(peak["hkl"])      )
                    
        
    # def changeOfPlane(self, num ):
    #     print( " segnale : " , num)

    def saveVolume(self):
        data = self.dvf.data()
        assert(len(data.shape)==3)
        filename = qt.QFileDialog.getSaveFileName(None, "select a file to save Volume", )
        if isinstance(filename, tuple):
            filename = filename[0]
        filename=str(filename)
        if len(filename)==0:
            return
        ccp4writer.write_ccp4_grid_data(data, filename)
        
    def renderPlan(self):
        print( " in render plan " )
        self.planDefinition_signal.emit(  self.parameters.getMyParsAsObject()    )

    def renderVolume(self):
        print( " in render Volume " )
        self.volumeDefinition_signal.emit(  self.parameters.getMyParsAsObject()    )

@ui.UILoadable
class fitParsWidget(qt.QWidget):
    def __init__(self, parent=None):
        super(fitParsWidget , self).__init__(parent)
        self.loadUi()
        
    def attachDataset(self, dataset):
        self.dataset = dataset
        par_dict = {}
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
            posslot = self.layout().itemAtPosition(irow,1)
            if posslot is None:
                continue
            tok = posslot.widget()
            if  isinstance(   tok , qt.QLineEdit ) or  isinstance(   tok , qt.QComboBox ):
                name = str(self.layout().itemAtPosition(irow,0).widget().text()).strip()
                name = name.replace(" ","_")
                print ( name)
                print(   self.dataset.Pfit.slots)
                assert( name in self.dataset.Pfit.slots)
                par_dict[ name ] = tok
        dataset.Pfit.initEntries( par_dict)
                


@ui.UILoadable
class dwParsWidget(qt.QWidget):
    
    def __init__(self, parent=None):
        super( dwParsWidget, self).__init__(parent)
        self.loadUi()
    def attachDataset(self, dataset):
        
        self.pushButton_load.clicked.connect(self.loadWidget)
        self.pushButton_clear.clicked.connect(self.clearWidget)
        
        self.textBrowser.setReadOnly(True)
        
        self.dataset       = dataset
        self.update()


    def clearWidget(self):
        self.dataset.Pdw.clear()
        self.textBrowser.setPlainText("")
        
    def loadWidget(self):
        fn,gn = hdf5_filedialog_ab2tds()
        print (fn,gn)
        
        error = self.dataset.Pdw_load(fn,gn)
        if error:
            msgBox = qt.QMessageBox ()
            msgBox.setText("The unit cell read from ab initio output does not seem compatible with tds2el cell");
            msgBox.setInformativeText("This may occour when the primitive cell is used in ab calculation. Please note that the Debye Waller tensors and atomic positions will not be rotatted to bring them in the tds2el reference. This may be not necessary if the same convention are applied. TDS2EL uses Giacovazzi's one where astar is along x.");
            msgBox.setStandardButtons(qt.QMessageBox.Ok );
            msgBox.setDefaultButton(qt.QMessageBox.Ok);
            ret = msgBox.exec_();            
        self.update()

    def update(self):
        s = self.dataset.Pdw.getInfoString()
        self.settext(s)
        
    def settext(self,s):
                        
        font =  qt.QFont ("Monospace");
        font.setStyleHint(qt.QFont.TypeWriter);
        self.textBrowser.setFont(font);
        self.textBrowser.setText(s)
  
        
@ui.UILoadable
class elasticParsWidget(qt.QWidget):
    def __init__(self, parent=None, alignPars=None,generatorsPars =None ):
        super( elasticParsWidget, self).__init__(parent)
        self.loadUi()
        self.textBrowser.setReadOnly(True)
        
        self.pushButton_setTensor.clicked.connect(self.OnInit_Pars) 
    def attachDataset(self, dataset):

        self.dataset= dataset

        self.init_pars_W()
                                                                          
    def OnInit_Pars(self):
        self.dataset.init_elastic_pars()
        self.init_pars_W()

    def init_pars_W(self):
        nrow = self.gridLayout_docker.rowCount()
        ncol = self.gridLayout_docker.columnCount()
        for  irow in range(nrow):
            for icol in range(ncol):
                tok = self.gridLayout_docker.itemAtPosition(irow,icol)
                if tok is not None:
                    tok = tok.widget()
                    self.gridLayout_docker.removeWidget(tok)
                    tok.deleteLater()
                    del tok

        par_dict = {}

        nomi_variabili = list( self.dataset.Pelastic.slots ) 

        nomi_variabili.sort()
        
        for irow,nome in enumerate(nomi_variabili):
            self.gridLayout_docker.addWidget(qt.QLabel( nome) ,irow,0)
            tok = qt.QLineEdit()

            val  = getattr( self.dataset.Pelastic, nome    )
            
            if val is not None:
                tok.setText(  str(val)  )
            
            self.gridLayout_docker.addWidget( tok  ,irow,1)
            par_dict[nome] = tok

        self.dataset.Pelastic.initEntries( par_dict)
        tstring = None
        if hasattr( self.dataset.Pelastic, "tensore_string"  ) :
            tstring  = getattr( self.dataset.Pelastic, "tensore_string"    )
            
        if tstring is None:
            tstring = ""

        font =  qt.QFont ("Monospace");
        font.setStyleHint(qt.QFont.TypeWriter);
        self.textBrowser.setFont(font);
        self.textBrowser.setPlainText(str(tstring))
        
@ui.UILoadable
class extractionParsWidget(qt.QWidget):
    def __init__(self, parent=None):
        super(extractionParsWidget , self).__init__(parent)
        self.loadUi()
        

    def attachDataset(self, dataset):
        
        par_dict = {}
        
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
            tok = self.layout().itemAtPosition(irow,1).widget()
            
            if  isinstance(   tok , qt.QLineEdit ) or  isinstance(   tok , qt.QComboBox ):
                
                name = str(self.layout().itemAtPosition(irow,0).widget().text()).strip()
                name = name.replace(" ","_")
                
                assert( name in dataset.Pextraction.slots)

                par_dict[ name ] = tok

        dataset.Pextraction .initEntries(par_dict)              
                
        self.dataset = dataset
        
        self.pushButton_extract.clicked.connect(self.dataset.extract)
        self.pushButton_collectAround.clicked.connect(self.dataset.collectAround)

        ##### self.extracted_data = {}   va in dataset !!!!!!!!



@ui.UILoadable
class generatorsParsWidget(qt.QWidget):
    def __init__(self, parent=None):
        super(generatorsParsWidget , self).__init__(parent)
        self.loadUi()
        
    def attachDataset(self, dataset):
        par_dict = {}
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
          for  icol in range(ncol):
            
            tok = self.layout().itemAtPosition(irow,icol).widget()
            if  isinstance(   tok , qt.QCheckBox ):
                name = str(tok.text()).strip()
                name = name.replace(")","_")
                name = name.replace("(","_")
                name = name.replace("~","_inv_")
                name = "gen_"+name
               
                assert( name in dataset.Pgenerators.slots     )

                par_dict[ name ] = tok

                
        dataset.Pgenerators.initEntries(   par_dict  )
        self.dataset=dataset

@ui.UILoadable
class sampleAlignementWidget(qt.QWidget):
    
    doAlignementWithThisPars_signal = qt.pyqtSignal(object)
    tagAndFilter_signal             = qt.pyqtSignal(object, object)
    
    def __init__(self, parent=None):
        super( sampleAlignementWidget, self).__init__(parent)
        self.loadUi()

    def attachDataset(self, dataset):
        
        par_dict = {}
        nrow = self.widget.layout().rowCount()
        ncol = self.widget.layout().columnCount()

        for  irow in range(nrow):
            w = self.widget.layout().itemAtPosition(irow,1)
            if w is not None:
                tok = w.widget()
                if  isinstance(   tok , qt.QLineEdit ) or  isinstance(   tok , qt.QComboBox ):
                    print (    tok  )
                    name = str(self.widget.layout().itemAtPosition(irow,0).widget().text()).strip()
                    
                    name = name.replace(" ","_")
                    print( name )
                    assert( name in dataset.Palignement.slots     )

                    par_dict[ name ] = tok


        dataset.Palignement.initEntries(   par_dict  )
        self.dataset=dataset
        
        toggle = par_dict["Quat_or_angles"]

        try:
            toggle.currentIndexChanged[str].disconnect()
        except:
            pass

        toggle.currentIndexChanged[str].connect(dataset.Palignement.toggle)
        
        self.pushButton_align.clicked.connect(self.onAlign)
        
        class tagAndFilterParameters(Parameters):
            slots = ["hkl_tolerance","python_filter"]
            entries= { "hkl_tolerance" : self.lineEdit_hklTolerance ,"python_filter":  self.lineEdit_python_filter  }
            classes = { "hkl_tolerance" : float , "python_filter" : str  }

        self. toleranceParameters = tagAndFilterParameters()
        self.pushButton_tagAndFilter.clicked.connect(self.onTagAndFilterClicked)
                           
    def onTagAndFilterClicked(self):
        msgBox = qt.QMessageBox ()
        msgBox.setText("You are going to filter the spots ");
        msgBox.setInformativeText("This will discard completely "
                                  " those not satisfying the (close to)"
                                  "integer hkl condition");
        msgBox.setStandardButtons(qt.QMessageBox.Ok |  qt.QMessageBox.Cancel);
        msgBox.setDefaultButton(qt.QMessageBox.Cancel);
        ret = msgBox.exec_();
        if not ret==qt.QMessageBox.Ok:
            return
        self.toleranceParameters.takeFromOwnGui()
        self.tagAndFilter_signal.emit( self.toleranceParameters.hkl_tolerance ,  self.toleranceParameters.python_filter )

                           
    def getFromCrys(self,pars):
        
        print( " ricevuto " , list(pars.keys()))
        
        for tag in ['AA', 'BB', 'CC', 'aAA', 'aBB', 'aCC']:
            if tag in pars:
                setattr( self.dataset.Palignement  ,     tag,  pars[tag]  )
                
        print( " UB " , pars["UB"] )
        P = self.dataset.Palignement
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))

        print("Brill " , brillV, " det cell " , np.linalg.det(cellV ),  " det brill " ,   np.linalg.det( brillV  )    )
        print( " det UB " , np.linalg.det(pars["UB"]  ))
          
        # rotSample = np.dot(   myBravais.T, np.linalg.inv(  bravais.T    ) )
        
        rotSample = np.dot(  pars["UB"], np.linalg.inv(  brillV.T    ) )
        
        print(" LA ROTSAMPLE EST ", rotSample)
        
        quats  = geometry.M2quat( rotSample)
        print(" quats ", quats)
        print("DI RITORNO EST ",  geometry.quat2M(  quats[0], quats[1], quats[2] ) )
        angles = geometry.M2r   ( rotSample)

        print(" Test unitarieta  ",   np.dot(   rotSample  , rotSample.T   )  ) 
        
        P = self.dataset.Palignement
        if self.dataset.Palignement.Quat_or_angles == "quaternions" :
            P.r1 , P.r2, P.r3  = quats
        else:
            P.r1 , P.r2, P.r3  = angles
        
        self.dataset.Palignement.exportToOwnGui()        

        

        
    def onAlign(self):
        pars =  self.dataset.Palignement.getMyParsAsObject()
        self.doAlignementWithThisPars_signal.emit(pars)

    def setRotABCs( self, anglesquat, abc, Aabc):
        self.setRot(anglesquat)
        a,b,c = abc
        aa,ab,ac = Aabc
        self.dataset.Palignement.AA = a
        self.dataset.Palignement.BB = b
        self.dataset.Palignement.CC = c
        self.dataset.Palignement.aAA = aa
        self.dataset.Palignement.aBB = ab
        self.dataset.Palignement.aCC = ac
            
        self.dataset.Palignement.exportToOwnGui()
        
                    
    def setRot(self, anglesquat):
        
        (a1,a2,a3), (q1,q2,q3) = anglesquat

        self.dataset.Palignement.takeFromOwnGui()
        
        if self.dataset.Palignement.Quat_or_angles == "quaternions" :
            
            self.dataset.Palignement.r1 = q1
            self.dataset.Palignement.r2 = q2
            self.dataset.Palignement.r3 = q3
        else :
            self.dataset.Palignement.r1 = a1
            self.dataset.Palignement.r2 = a2
            self.dataset.Palignement.r3 = a3
            
        self.dataset.Palignement.exportToOwnGui()
        
            


        
@ui.UILoadable
class experimentGeoParsWidget(qt.QWidget):
    transform_signal = qt.pyqtSignal(object)
    
    def __init__(self, parent=None):
        super(experimentGeoParsWidget , self).__init__(parent)
        self.loadUi()
        irowmax = 0
        nrow = self.layout().rowCount()
        for  irow in range(nrow):
            tok = self.layout().itemAtPosition(irow,1).widget()
            if  isinstance(   tok , qt.QLineEdit ) or  isinstance(   tok , qt.QComboBox ):
                if irow > irowmax :
                    irowmax = irow


        add_items = list(geometry.HOLDER.variables) + list(geometry.DETECTOR.variables)+ list(geometry.BEAM.variables)
        add_items.remove("phi")
        nadd = len(add_items)
        for I in range(nrow,irowmax,-1):
            if self.layout().itemAtPosition(I,0) is not None:
                w = self.layout().itemAtPosition(I,0).widget()
                self.layout().removeItem( self.layout().itemAtPosition(I,0) );
                self.layout().addWidget( w  ,    I+nadd ,0)
                
            if self.layout().itemAtPosition(I,1) is not None:
                w = self.layout().itemAtPosition(I,1).widget()
                self.layout().removeItem( self.layout().itemAtPosition(I,1)  );
                self.layout().addWidget(  w ,    I+nadd ,1)

        
        
        for i,tok in enumerate(add_items) :
            self.layout().addWidget(  qt.QLabel(tok) ,     irowmax+1+i ,0)
            self.layout().addWidget(  qt.QLineEdit(),   irowmax+1+i ,1)
        
    def attachDataset(self, dataset):
        
        par_dict = {}
        nrow = self.layout().rowCount()
        ncol = self.layout().columnCount()
        for  irow in range(nrow):
            print(" IROW ", irow, nrow)
            tok = self.layout().itemAtPosition(irow,1).widget()
            
            if  isinstance(   tok , qt.QLineEdit ) or  isinstance(   tok , qt.QComboBox ):
                name = str(self.layout().itemAtPosition(irow,0).widget().text()).strip()                
                name = name.replace(" ","_")

                assert( name in dataset.PexperimentGeo.slots)

 
                par_dict[ name ] = tok

        dataset.PexperimentGeo.initEntries(   par_dict  )
        self.dataset = dataset
        
        self.pushButton_transformToGLviewer.clicked.connect(self.transform)
        self.NLim = None
        self.lineEdit_nspots4GL.editingFinished.connect(self.NLim_EditFinished)

    def getFromCrys(self,pars):
        print( " ricevuto " , list(pars.keys()))
        for tag in self.dataset.PexperimentGeo.slots:
            if tag in pars:
                setattr(  self.dataset.PexperimentGeo ,     tag,  pars[tag]  )
        self.dataset.PexperimentGeo.exportToOwnGui()        
        
        
    def transform(self):
        print(" TRANSFORM")
        self.transform_signal.emit(self)
        
    def NLim_EditFinished(self):
        qobj = self.sender()
        txt = str(qobj.text()).strip()
        if txt=="":
            txt="None"
        tmp = txt
        d_globals= {}
        d_locals = {}
        exec("tmp="+txt,d_globals, d_locals)
        tmp = d_locals["tmp"]
        
        if tmp == "":
            tmp = None
            
        if tmp is not None:
            if not isinstance(  tmp , int  ):
                msg = " Error: the value given to variable which limits the number of spots. It  should be of class int"
                msg=msg+ " Now it is of type " + str(type(tmp))
                qobj.setText("None")
                raise Exception( msg)
        self.NLim = tmp


scelteVarsFit = {}
    
@ui.UILoadable
class choiceVarsFit(qt.QDialog):
    selected_pars_signal = qt.pyqtSignal(object)
    
    def __init__(self , elVars, addconfigvars=True ):
        
        super( choiceVarsFit  , self).__init__(None)
        self.loadUi()       
        self.setModal(True)
        self.pushButton_fit.clicked.connect(self.prepare_fit)
        self.fillPreselected(elVars)

        if addconfigvars:
            nrow = self.widget.layout().rowCount()
            ncol = self.widget.layout().columnCount()
            
            add_items = list(geometry.HOLDER.variables) + list(geometry.DETECTOR.variables)+ list(geometry.BEAM.variables)
            add_items.remove("phi")
            nadd = len(add_items)
            for i,tok in enumerate(add_items) :
                self.widget.layout().addWidget( qt.QCheckBox( tok ) ,     nrow+ (i//ncol) ,i%(ncol))
                

    def keyPressEvent(self, evt):
        if(evt.key() == qt.Qt.Key_Enter or evt.key() == qt.Qt.Key_Return):
            return;
        qt.QDialog.keyPressEvent(self,evt)

        
    def prepare_fit(self):
        global scelteVarsFit
        par_dict = {}
        nrow = self.widget.layout().rowCount()
        ncol = self.widget.layout().columnCount()
        for  irow in range(nrow):
          for  icol in range(nrow):
            cell = self.widget.layout().itemAtPosition( irow , icol )
            if cell is not None:
                tok = cell.widget()
                if  isinstance(   tok , qt.QCheckBox ) :
                    name  = str(tok.text())
                    value =  tok.isChecked()
                    par_dict[ name ] = value
                    scelteVarsFit[name]    = value

        value = self.constraints.toPlainText()
        par_dict[ "constraints"  ] = value
        scelteVarsFit["constraints" ]    = value
                    
        self.selected_pars_signal.emit( par_dict  )

        
    def fillPreselected(self, elVars):
        global scelteVarsFit
        print(" QUA SCELTE EST " , scelteVarsFit)
        nrow = self.widget.layout().rowCount()
        ncol = self.widget.layout().columnCount()
        
        for i,name in enumerate(elVars):
            irow = nrow + i//ncol
            icol = i%ncol
            cbox = qt.QCheckBox(name)
            self.widget.layout().addWidget( cbox, irow,icol) 

        nrow = self.widget.layout().rowCount()
        ncol = self.widget.layout().columnCount()

        
        for  irow in range(nrow):
          for  icol in range(nrow):
              
            cell = self.widget.layout().itemAtPosition(irow,icol)
            if cell is not None:
                tok = cell.widget()
                if  isinstance(   tok , qt.QCheckBox ) :
                    name  = str(tok.text())
                    if name in scelteVarsFit:
                        tok.setChecked(   scelteVarsFit[  name   ]   )


                        
        font =  qt.QFont ("Monospace");
        font.setStyleHint(qt.QFont.TypeWriter);
        self.constraints.setFont(font);

        if "constraints" in scelteVarsFit:
            self.constraints.setPlainText(scelteVarsFit[  "constraints"    ] )


@ui.UILoadable
class choiceVarsFitCentered(choiceVarsFit):
    selected_pars_signal = qt.pyqtSignal(object)
    def __init__(self , elVars ):
        super( choiceVarsFitCentered  , self).__init__(elVars, addconfigvars=False)
        
        
separator = '-' * 80
def excepthook(type, value, tracebackobj):
    print( " HOOK ")
    tbinfofile = StringIO()
    traceback.print_tb(tracebackobj, None, tbinfofile)
    traceback.print_exc()
    traceback.print_stack(  tracebackobj , file=tbinfofile)    
    tbinfofile.seek(0)
    tbinfo = tbinfofile.read()
    errmsg = '%s: %s' % (str(type), str(value))
    
    sections = [separator, errmsg, separator, tbinfo]
    print(separator)
    print(errmsg)
    print(separator)
    print(tbinfo)
    msg = '\n'.join(sections)
    msgBox = qt.QMessageBox(None)
    msgBox.setText("An exception Occurred")
    msgBox.setInformativeText(msg)
    msgBox.setStandardButtons(  qt. QMessageBox.Ok)
    msgBox.setDefaultButton( qt.QMessageBox.Ok)
    ret = msgBox.exec_()
    return

def main():
    if hasattr(qt.Qt, 'AA_ShareOpenGLContexts'):
        print('set AA_ShareOpenGLContexts')
        qt.QCoreApplication.setAttribute(qt.Qt.AA_ShareOpenGLContexts)  ######
        
    global app
    app = qt.QApplication.instance()
    
    if app is None:
        app=qt.QApplication([])
        
    w = MainWindow()

    
    opts, args = getopt.getopt(sys.argv[1:],"c:")
    
    if len(args)>0 :
        if len(args)==1:
            w.loadParFile( args[0] )
        else:
            w.loadParFile( args[0], second_file = args[1], max_dist = int( args[2]  ) )
            
            w.tabWidget.removeTab( w.tabWidget.indexOf(  w.extractionW ) )
            w.tabWidget.removeTab( w.tabWidget.indexOf(  w.experimentGeoW ) )
            w.tabWidget.removeTab( w.tabWidget.indexOf(  w.sampleAlignementW ) )
            w.tabWidget.removeTab( w.tabWidget.indexOf(  w.generatorsW ) )
            w.tabWidget.removeTab( w.tabWidget.indexOf(  w.dwW ) )


            
        w.setAttribute(qt.Qt.WA_QuitOnClose, True)
    w.show()
    app.setQuitOnLastWindowClosed(True);
    app.exec_()

def hdf5_filedialog_ab2tds( ):
    
    d=DataFileDialog()
    
    # d.setFilterMode()# DataFileDialog.FilterMode.ExistingData)
    d.setFilterMode(DataFileDialog.FilterMode.ExistingDataset)
    result = d.exec_()
    
    if result:
        url = d.selectedUrl()
        pos = url.find("?")
        url = url[pos+1:]
        print ( " RITORNO ", url)
        return d.selectedFile(), url
    else:
        return None, None

    
    filename =  qt.QFileDialog.getOpenFileName(None,'Open hdf5 file and then choose groupname',filter="ab2tds (*md5*)\nall files ( * )"  )
    if isinstance(filename, tuple):
        filename = filename[0]
    filename=str(filename)
    
    if len(filename) :
        d=DataDialog()
        d.addFile(  unicode(filename)   )
        d.setModal(True)
        res = d.exec_()
        if res :
            return filename, d.getSelectedDataUrl().data_path()
        else:
            return None, None 
        
        ret =None
        if os.path.exists(filename):
            storage=[None]
            storage=[""]
            window = hdf5dialog.hdf5dialog(filenames = [unicode(filename)], storage = storage)
            window.setModal(True)
            ret = window.exec_()
        if ret:
            name =  storage[0]
            return filename, name
        else:
            return None , None
    else:
        return None, None



def Theta(Q, Lambda  ):
    dum = np.sqrt( np.sum(Q*Q, axis=-1)) * Lambda/2.0/math.pi/2.0
    return np.arcsin( dum  )




def Get_dwscatt_fact(q, DWs,  atomNames , positions, lmbda):
    q=q*2*math.pi
    WW3x3s = DWs  # una matrice 3X3 per ogni atomo
    a = np.tensordot(  q  ,  WW3x3s ,  ([-1], [-1])  )  # a adesso est [ Natoms , 3  ]
    a = ( q[None,:]  *  a    ).sum(axis=-1)    # a adesso est [  Natoms]
    tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
    scatterers = [  tf0.Element( string_check(aname) ) for aname in  atomNames ]
    sctfacts   = np.array( [ scatterer.f0Lambda(lmbda , Theta( q , lmbda  )   )
                                for scatterer in scatterers ]  ).astype(np.complex64)
    
    factors = np.exp( -   1.0j *    np.tensordot( q,  positions, axes=([-1],[-1]) )           )   #  Natoms
    
    sctfacts = factors *  sctfacts     
    a=(np.exp(-a) *  sctfacts ).sum(axis=-1)  # a adesso est []


    a= (a*a.conjugate()).real
    
    return a



def string_check(name):
    if isinstance(name, str):
        return name
    elif isinstance(name, bytes):
        return name.decode("utf-8")
    else:
        message = """ name class is neither str not bytes"""
        raise ValueError(message)



if __name__ =="__main__":
    main()

              
