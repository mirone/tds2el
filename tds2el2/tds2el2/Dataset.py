from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from . import ui
from silx.gui import qt
from silx.gui.plot import PlotWidget
import silx
from silx.gui.colors import  Colormap

from . import geometry
import numpy as np

import numbers
import re
import yaml
import yaml.resolver
from six import u
import collections

import h5py
import sys
from . import extraction
from six import StringIO


yaml.resolver.Resolver
Resolver = yaml.resolver.Resolver
Resolver.add_implicit_resolver(
        u'tag:yaml.org,2002:float',
        re.compile(u(r"""^(?:[-+]?(?:[0-9][0-9_]*)(\.[0-9_]*)?(?:[eE][-+]?[0-9]+)?
                    |\.[0-9_]+(?:[eE][-+][0-9]+)?
                    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\.[0-9_]*
                    |[-+]?\.(?:inf|Inf|INF)
                    |\.(?:nan|NaN|NAN))$"""), re.X),
        list(u'-+0123456789.'))

#################################################################
##  THIS redefinition of yaml is used to keep the entry ordering
## when accessing yamlData keys
##
#yaml_anydict.py
import yaml
from yaml.representer import Representer
from yaml.constructor import Constructor, MappingNode, ConstructorError
def dump_anydict_as_map( anydict):
    yaml.add_representer( anydict, _represent_dictorder)
def _represent_dictorder( self, data):
    return self.represent_mapping('tag:yaml.org,2002:map', data.items() )
class Loader_map_as_anydict( object):
    'inherit + Loader'
    anydict = None      #override
    @classmethod        #and call this
    def load_map_as_anydict( klas):
        yaml.add_constructor( 'tag:yaml.org,2002:map', klas.construct_yaml_map)
    'copied from constructor.BaseConstructor, replacing {} with self.anydict()'
    def construct_mapping(self, node, deep=False):
        if not isinstance(node, MappingNode):
            raise ConstructorError(None, None,
                    "expected a mapping node, but found %s" % node.id,
                    node.start_mark)
        mapping = self.anydict()
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            try:
                hash(key)
            except TypeError as exc:
                raise ConstructorError("while constructing a mapping", node.start_mark,
                        "found unacceptable key (%s)" % exc, key_node.start_mark)
            value = self.construct_object(value_node, deep=deep)
            mapping[key] = value
        return mapping
    def construct_yaml_map( self, node):
        data = self.anydict()
        yield data
        value = self.construct_mapping(node)
        data.update(value)
        
class myOrderedDict (collections.OrderedDict):
    def __setitem__(self,a,b):
        ## print "cucu",a,b
        if type(a)==type("") and a in self:
            self[a+"_tagkajs"]=b 
        else:
            ## print super(myOrderedDict, self)
            super(myOrderedDict, self).__setitem__(a,b )
            
def cleaned(key):
    while key[-8:]=="_tagkajs":
        key=key[:-8]
    return key
      
dictOrder = myOrderedDict


from .Parameters import Loader

# class Loader( Loader_map_as_anydict, yaml.Loader):
#     anydict = dictOrder
# Loader.load_map_as_anydict()

dump_anydict_as_map( dictOrder)
##
## END of yaml redefinition
###############################





class ExclusionClass(list):
    ctc = list
    @staticmethod
    def analyse(txt, qobj):
        
        d_globals= {}
        d_locals = {}
        exec("tmp="+txt,d_globals, d_locals)
        tmp = d_locals["tmp"]
        if tmp == "":
            tmp = None
        if tmp is not None:
            if not isinstance(  tmp , list ):
                msg = " Error: the value given to variable " + name +" should be of class "+str(self.classes[name])
                msg=msg+ " Now it is of type " + str(type(tmp))
                qobj.setText("None")
                raise Exception( msg)
            if len(tmp)%4 :
                msg = " Error: the variable exceptions if initialised must contain a multiple of 4 floats "
                qobj.setText("None")
                raise Exception( msg)
            for tok in tmp:
                if not isinstance(tok,(float, int)):
                    msg = " Error: the variable exceptions if initialised must contain all floats "
                    qobj.setText("None")
                    raise Exception( msg)
                    
        return ExclusionClass(tmp)
    
SpecialClasses = [ExclusionClass]



# class Parameters(qt.QObject):
class Parameters(qt.QObject):
    # the __setattr__ function of self is under control to limit
    # the settable names. See __setattr__ of class Parameters.
    # Where necessary we bypass it by directly calling the method of the super class

    # classes = {'x':float}
    # slots = list(classes.keys())

    def __init__(self):
        super(Parameters, self).__init__()  
        object.__setattr__(self,"entries_reversed" ,  {})

    def initSlots(self, slots, classes):
        object.__setattr__(self,"slots" ,  slots)
        object.__setattr__(self,"classes" ,  classes)
        
        for name in self.slots:
            object.__setattr__(self,name,None)
            
    def initEntries(self, entries):
        object.__setattr__(self,"entries",entries)
        assert( set(self.slots) == set(entries.keys()))
        for name,qgui in self.entries.items():
            self.entries_reversed[qgui] = name
            
            if  isinstance(  qgui  , qt.QLineEdit ):
                try:
                    qgui.editingFinished.disconnect()
                    print( " DISCONNESSO ")
                except:
                    pass
                qgui.editingFinished.connect(self.lineEditFinished)
            elif  isinstance(  qgui  , qt.QComboBox ):
                try:
                    qgui.activated.disconnect()
                    print( " DISCONNESSO ")
                except:
                    pass
                qgui.activated.connect(self.ComboEditFinished)
            elif  isinstance(  qgui  , qt.QCheckBox ):
                try:
                    qgui.stateChanged.disconnect()
                    print( " DISCONNESSO ")
                except:
                    pass
                
                qgui.stateChanged.connect(self.checkBoxChanged)
            elif  isinstance(  qgui  , qt.QRadioButton ):
                try:
                    qgui.toggled.disconnect()                
                    print( " DISCONNESSO ")
                except:
                    pass
                qgui.toggled.connect(self.checkBoxChanged)

    def checkBoxChanged(self):
        print(" in checkBoxChanged" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        tmp = qobj.isChecked()
        try:
            tmp=int(tmp)
        except:
            pass
        setattr(self, name, tmp)
        
    def ComboEditFinished(self):
        print(" in ComboEditFinished" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        txt = str(qobj.currentText())
        tmp = txt
        try:
            tmp=int(tmp)
        except:
            pass

        setattr(self, name, tmp)


    def lineEditFinished(self):
        print(" in lineEditFinished" )
        qobj = self.sender()
        name = self.entries_reversed[qobj]
        txt = str(qobj.text()).strip()
        if txt=="":
            txt="None"
        tmp = txt
        
        if self.classes[name] in SpecialClasses:
            
            tmp = self.classes[name].analyse(tmp,qobj)
        
        elif self.classes[name]  is not str:
            print( " EXECUTING ", "tmp="+txt)
            d_globals= {}
            d_locals = {}
            exec("tmp="+txt,d_globals, d_locals)
            tmp = d_locals["tmp"]
            if tmp == "":
                tmp = None
            if tmp is not None:
                if not isinstance(  tmp , self.classes[name]  ):
                    msg = " Error: the value given to variable " + name +" should be of class "+str(self.classes[name])
                    msg=msg+ " Now it is of type " + str(type(tmp))
                    qobj.setText("None")
                    raise Exception( msg)
        setattr(self, name, tmp)


    
    def __setattr__(self, name,val):
        
        if name != "__METAOBJECT__" :
            if not( name in self.slots):
                raise Exception(" for object %s name %s is not valid "%( self, name))

            if self.classes[name]  in SpecialClasses:
                ctc = self.classes[name].ctc
            else:
                ctc = self.classes[name]
            if val is not None and not isinstance( val, ctc    ) :
                raise Exception(" for object %s name %s property must be of the class %s. It was of type %s instead "%( self, name,self.classes[name] , type(val)   ))
        object.__setattr__(self,name,val)





        
    def setGui(self, entries):
        object.__setattr__(self,"entries",entries)

    def evaluate(self, txt):
        print(" EVALUATE ", txt)
        glob ={}
        locs ={"txt":txt}
        exec("tok=%s"%txt, glob, locs)
        return locs["tok"]

    def takeFromOwnGui(self):
        for name, gui_input in self.entries.items():
            
            if isinstance( gui_input , qt.QLineEdit):
                tmp = str(gui_input.text()) .strip()
                if tmp=="": tmp = "None"
                if self.classes[name] != str:
                    self.__setattr__(name, self.evaluate(   tmp             ))
                else:
                    self.__setattr__(name,    tmp      )
            elif   isinstance( gui_input , qt.QComboBox)   :
                tmp = str(gui_input.currentText()) .strip()
                if tmp=="": tmp = "None"
                try:
                    tmp=int(tmp)
                except:
                    pass
                
                self.__setattr__(name,   tmp        )
            elif   isinstance( gui_input , qt.QCheckBox) or  isinstance( gui_input , qt.QRadioButton)  :
                tmp = gui_input.isChecked()
                tmp=int(tmp)
                self.__setattr__(name,   tmp        )


    def exportToOwnGui(self):
        if not hasattr(self, "entries") :
            return
        for name in self.slots:
            print( " EXPORT ", name , str(getattr(self, name)) )
            if name in self.entries:
                if isinstance( self.entries[name], qt.QLineEdit   ):
                    self.entries[name].setText( str(getattr(self, name)) )
                elif isinstance( self.entries[name], qt.QComboBox) :
                    ind = self.entries[name].findText( str(getattr(self, name)) )
                    self.entries[name].setCurrentIndex( ind )
                elif isinstance( self.entries[name]  , qt.QCheckBox) or  isinstance( self.entries[name] , qt.QRadioButton)   :
                    try:
                        tmp = int(getattr(self, name))
                    except:
                        tmp = 0
                    self.entries[name].setChecked( tmp)
                    

    def takeFromYamlText(self,s):
       data = yaml.load(s, Loader=Loader)
       for name in self.slots:
           tok = data[name]
           if self.classes[name] != str:
               self.__setattr__(name, self.evaluate(   tok       )             )
           else:
               self.__setattr__(name,  tok      )
               
               
    def takeFromDict(self,data):
       for name in self.slots:
           if name in data:
               tok = data[name]
           else:
               tok = None
           print( name, tok )
           if self.classes[name] != str:
               self.__setattr__(name, self.evaluate(   tok       )             )
           else:
               self.__setattr__(name,   tok  )
               

    def exportToYamlText(self,prepend):
       s=""
       for name in self.slots:
           s=s+prepend+name+" : " + str(   getattr(self, name)     )+"\n"
       return s

    def exportToDict(self):
       s={}
       for name in self.slots:
           s[name]=  getattr(self, name)     
       return s
   
    def importFromDict(self, dizio):
       for name in self.slots:
           if name in dizio:
               self.__setattr__( name, dizio[name]  )     
       return s

    def getMyParsAsObject(self):
        dizio = self.exportToDict()
        mypars =  type('MyObjectPourDecrireLesPars', (object,), dizio)
        return mypars


class ExtractionParameters(Parameters):
    def __init__(self):
        super(ExtractionParameters, self).__init__()
        slots = ['filter_file', 'review_size', 'images_prefix', 'images_postfix',
                 'threshold_rel', 'peak_size', 'threshold_abs', 'spot_view_size',
                 'images_prefix_alt','ncpus',
                 'threshold_bragg', 'badradius', "normalisation_file" ]
        classes = {}
        for name in slots:
            if "image" in name or "file" in name:
                classes[name]=str
            elif "threshold" in name:
                classes[name]=float
            elif "radius" in name:
                classes[name]=float
            elif "size" in name or "ncpus" in name:
                classes[name]=int
        
        self.initSlots(slots, classes)


class experimentGeoPars(Parameters):
    def __init__(self):
        super( experimentGeoPars , self).__init__()
        slots = ['orientation_codes','lmbda', 'dist', 'pixel_size', 'det_origin_X',
                         'det_origin_Y', 'angular_step', 
                         'start_phi','pol_degree', 'pol_x','pol_y','pol_z' ]


        # the __setattr__ function of self is under control to limit
        # the settable names. See __setattr__ of class Parameters.
        # Where necessary we bypass it by directly calling the method of the super class
        object.__setattr__(self, "immutable_slots",tuple(slots))
 
        add_items = list(geometry.BEAM.variables)+list(geometry.HOLDER.variables)+list(geometry.DETECTOR.variables)
        add_items.remove("phi")

        slots=slots+add_items
        

        classes = {}
        for name in slots:
              if "codes" in name:
                   classes[name]=int
              else :
                   classes[name]=float

        self.initSlots(slots, classes)

    def get_axes_angles(self, what=""):
        axis_index =[]
        angles  = []

        my_dict = self.exportToDict()

        hdict = dict(
            beam = geometry.BEAM,
            holder = geometry.HOLDER,
            detector = geometry.DETECTOR
        )
        
        for ax, var in zip( hdict[what].axis, hdict[what].angles ) :
            assert isinstance( var, str), f"Check you yaml configuration file, the var name should be a string, I get {var} "
            aind =  (abs(np.array(ax).astype("i")) *np.array([1,2,3])).sum()
            assert abs(np.array(ax)).sum() == 1  and  ( aind in [1,2,3] ),  f"Check you yaml configuration file, the axis should be all 0 expect on component equal to 1 or -1 "

            fattore = np.array(ax).sum()
            axis_index.append(aind*fattore)

            if var == "phi":
                angles.append(np.nan)
            else:                
                angles.append(  my_dict[var]  ) 
        return np.array(axis_index,dtype=np.int32), np.array(angles,dtype=np.float32)

class fitPars(Parameters):
    def __init__(self):
        super( fitPars , self).__init__()
        slots =    [ "qmin", "qmax", "Temperature", "Density","Rarefaction","Exclusions"]   
        classes = {}
        for name in slots:
            if name == "Rarefaction" :
                classes[name]=int
            elif name =="Exclusions":
                classes[name]=ExclusionClass
            else:
                classes[name]=float
        self.initSlots(slots, classes)


        
class alignementPars(Parameters):
    def __init__(self):
        super( alignementPars , self).__init__()
        slots = ['AA', 'BB', 'CC', 'aAA', 'aBB', 'aCC', 'Quat_or_angles', 'r1', 'r2','r3']

        classes = {}
        for name in slots:
            if "Quat" in name:
                classes[name]=str
            else:
                classes[name]=float
                
        self.initSlots(slots, classes)

    def toggle(self, choice):

        r1,r2,r3 = self.r1, self.r2, self.r3

        if None in [r1,r2,r3]:
            return

        if choice == "quaternions" :
            print( " TOGGLE to quaternions ")
            r1,r2,r3 = geometry.r2quat(r1,r2,r3)
        else:
            print( " TOGGLE to angles ")
            r1,r2,r3 = geometry.quat2r(r1,r2,r3)
            
        self.r1, self.r2, self.r3 = r1,r2,r3
        
        self.Quat_or_angles = str(choice)
        
        self.exportToOwnGui()
                    


        
class generatorsPars(Parameters):
    def __init__(self):
        super( generatorsPars , self).__init__()
        gendict = geometry.gendict
        
        slots = []
        
        for name in list(gendict.keys()) :
            if name != "1":
                orig_name = name
                name = name.replace(")","_")
                name = name.replace("(","_")
                name = name.replace("~","_inv_")
                name = "gen_"+name
                slots.append(name)
        
        classes = {}
        for name in slots:
            classes[name]=int
                
        self.initSlots(slots, classes)



def a2s(  A, fmt=".4%e" ) :
    if A is None or A=="None":
        return ""
    
    if len(A.shape)==2:
         return a2s_2d(  A, fmt )
     
    if len(A.shape)==3:
         return a2s_3d(  A, fmt )

def a2s_2d(  A, fmt ):
    sa="[\n"
    sb="]\n"

    for l in A:
        sa=sa+"["
        for t in l:
            sa=sa+fmt%t +","
        sa=sa[:-1]+"],\n"
    sa=sa[:-2]+"\n"
    return sa+sb

def a2s_3d(  A, fmt ):
    sa="[\n"
    sb="]\n"

    for l in A:
        sa=sa+a2s_2d(l, fmt)+","
    return sa[:-1]+sb


        
class dwPars:
    def __init__(self):
        self.clear()

    def clear(self):
        self.DWs           = None
        self.atomNames     = None
        self.atomPositions = None

    def setInfos(self, DWs, atomNames, atomPositions):
        self.DWs           = DWs            
        self.atomNames     = atomNames    
        self.atomPositions = atomPositions


        
    def getInfoString(self):
        s = StringIO()

        print (self.DWs) 
        print (type(self.DWs)) 
        sdw="DWs : " +   a2s( self.DWs , fmt = "%.4e"  )       +"\n"


        san="atomNames : " + repr(self.atomNames)+"\n"

        sap="atomPositions : "   +   a2s( self.atomPositions , fmt = "%.4e"  )       +"\n"

        s= sdw+san+sap
        
        return s



    def initFromDict( self, dizio )  :
        if dizio["DWs"] != "None" and dizio["DWs"] is not None:
            DWs = np.array(dizio["DWs"])
        else:
            DWs = None
            
        atomNames = dizio["atomNames"]
        if dizio["atomPositions"] != "None" and dizio["atomPositions"] is not None:
            atomPositions = np.array(dizio["atomPositions"])
        else:
            atomPositions = None
        self.setInfos(DWs,atomNames, atomPositions )


        
    def exportToYamlText(self,prepend=" "*4):
        txt = self.getInfoString()
        res=""
        for line in txt.split("\n"):
            res=res+prepend+line+"\n"
        return res

    
        
class elasticPars(Parameters):
    def __init__(self):
        super( elasticPars , self).__init__()
        slots = []
        classes=[]
        self.initSlots(slots, classes)
        
    def initFromDict(self, dizio):
        slots = [tok for tok in dizio.keys() if tok !="tensore_string" ]
        print(" SLOTS " ,  slots ) 
        classes = {}
        for name in slots:
            classes[name]=float

        self.initSlots(slots, classes)

        for k in slots:
            setattr(self,k, dizio[k])
            
        if "tensore_string" in dizio:
            object.__setattr__(self,"tensore_string" ,  dizio["tensore_string"] )
           
    def exportToYamlText(self,prepend):
        s=""
        for name in self.slots:
            s=s+prepend+name+" : " + str(   getattr(self, name)     )+"\n"
        if hasattr( self, "tensore_string") :
            val = getattr(self, "tensore_string")
            if val is not None:
                val = str(val)
                s=s+prepend+"tensore_string"+" :  |\n"
                for l in val.split("\n"):
                    s = s + prepend + "  " +l +"\n"
        return s


            
            
class Dataset:
    def __init__(self):
        self.Pextraction = ExtractionParameters()
        self.extracted_data = {}

        self.PexperimentGeo = experimentGeoPars()
        self.Palignement    = alignementPars()
        self.Pgenerators    = generatorsPars()
        self.Pfit           = fitPars()
        self.Pelastic       = elasticPars()
        self.Pdw            = dwPars()

        
    def Pdw_load(self, fn,gn):
        h5 = h5py.File(fn,"r")
        DWs = h5[gn][:]
        atomNames = h5["/atomNames"][()]
        if atomNames is not None:
            atomNames = [tok.decode("utf-8") if  isinstance(tok, bytes)  else tok  for tok in atomNames]
        atomReducedPositions = h5["/atomReducedPositions"][:]
        cellVects  = h5["cellVects"][:]
        P = self.Palignement
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))    
        vects_el2tds =  cellV .T
        vects_ab2tds =  cellVects.T
        A = np.dot(     vects_ab2tds         ,   np.linalg.inv(vects_el2tds) )
        test = np.dot(A.T,A)-np.eye(3)
        error = 0
        if not  abs(test).max()<0.01 :
            print (cellVects)
            print ( cellV)
            A=None
            error = 1
        else:
            ## find the closest rotation matrix
            quats = geometry.M2quat(A)
            A     = geometry.quat2M(*quats)
        
        natoms = len( atomNames )
        assert(DWs.shape[0]==natoms)
        atomPositions  = np.dot(atomReducedPositions, cellVects)
        if A is not None:
            for i in range(natoms):
                DWs[i] =   np.dot(  A.T,    np.dot( DWs[i], A  )  ) 
            atomPositions  = np.einsum( "ij,kj->ki"   ,  A.T  ,  atomPositions  )
        self.Pdw.setInfos(DWs, atomNames, atomPositions)




        

        
    def detach(self):
        self.Pextraction .entries = {}
        self.Palignement .entries = {}
        self.Pgenerators .entries = {}
 
    def extract(self):
        # dizio = self.parameters.exportToDict()
        # mypars =  type('MyObjectPourDecrireLesPars', (object,), dizio)

        self.Pextraction.takeFromOwnGui()
        mypars = self.Pextraction.getMyParsAsObject()

        if len(self.extracted_data):
            msgBox = qt.QMessageBox ()
            msgBox.setText("You are going to reacquire the spots ");
            msgBox.setInformativeText("This will reset the previous one. Do you want to proceed?");
            msgBox.setStandardButtons(qt.QMessageBox.Ok |  qt.QMessageBox.Cancel);
            msgBox.setDefaultButton(qt.QMessageBox.Cancel);
            ret = msgBox.exec_();
            if not ret==qt.QMessageBox.Ok:
                return

            self.extracted_data ={}
        
        extraction.main(mypars, self.extracted_data)

        dostop=0
        app = qt.QApplication.instance()
        if app is None:
            app=qt.QApplication([])
            dostop = 1 

        create_pixmap_4_extracted_data(self.extracted_data,   app ) 
        print( self.extracted_data.keys())


        if dostop:
            app.quit()


        
    def collectAround(self):
        dizio = self.Pextraction.exportToDict()
        mypars =  type('MyObjectPourDecrireLesPars', (object,), dizio)

        extraction.main(mypars, self.extracted_data,preRes=self.extracted_data)
        print( self.extracted_data.keys())

    def takeConfFromDict(self, d):       
        self.Pextraction.takeFromDict(d["extraction_pars"])
        self.Pextraction.exportToOwnGui()        

        if "experiment_geo" in d:
            self.PexperimentGeo.takeFromDict(d["experiment_geo"])
            self.PexperimentGeo.exportToOwnGui()

        if "sample_alignement" in d:
            self.Palignement.takeFromDict(d["sample_alignement"])
            self.Palignement.exportToOwnGui()

        if "generators" in d:
            self.Pgenerators.takeFromDict(d["generators"])
            self.Pgenerators.exportToOwnGui()
 
        if "fit" in d:
            self.Pfit.takeFromDict(d["fit"])
            self.Pfit.exportToOwnGui()

        if "tensor" in d:
            if d["tensor"] is not None:
                self.Pelastic.initFromDict( d["tensor"] )

        if "dw" in d:
            print ( d["dw"] )
            if d["dw"] is not None:
                self.Pdw.initFromDict( d["dw"]  )          


            
        
    def getConfAsYaml(self):

        self.Pextraction.takeFromOwnGui()
        extraction_parameter_yaml       =  self.Pextraction.exportToYamlText(prepend=" "*4)
        txt = "extraction_pars :\n" + extraction_parameter_yaml + "\n"

        return txt
                

            
    def init_elastic_pars(self):
        symmPars = [ tok    for tok in  self.Pgenerators.slots  if getattr(self.Pgenerators, tok )]
        P = self.Palignement
        cellV, brillV = geometry.getCellVectors(   ( P.AA,P.BB, P.CC ),   (  P.aAA,P.aBB, P.aCC   ))
        oplist = geometry.genlist2oplist(symmPars, cellV)
        nomi_variabili,vettori_componenti, tensore_string  = geometry.get_elastic_simmetry(  oplist  )
        
        dizio = {}
        for n in nomi_variabili:
            dizio[n]= None

        dizio["tensore_string"] = tensore_string
            
        self.Pelastic.initFromDict( dizio)

    
        
        
def array2pixmap(centered_ring, app):

    if centered_ring is None:
        return centered_ring
    
    tmpw=PlotWidget()
    adata=[]
    side = centered_ring.shape[0]

    for II in range( centered_ring.shape[0] ):

        cm = Colormap( name='viridis', normalization='log'   )
        rgbA = cm.applyToData(centered_ring[II])
        adata.append(rgbA[:, :, :3])

        
        # tmpw.addImage(centered_ring[II], colormap={'name':'viridis', 'autoscale':True, 'normalization':'log'} )
        
        # tmpw._backend.ax.set_position([0, 0, 1, 1])
        # tmpw._backend.ax2.set_position([0, 0, 1, 1])
        # tmpw._backend.ax2.set_axis_off()
        # tmpw._backend.ax.set_axis_off()
        # tmpw.setGeometry(0,0, side , side  )
        # tmpw.show()                
        # tmpw.hide()
        # app.processEvents()
        # if hasattr(  qt.QWidget, "grab"    ):
        #     pixmap = qt.QWidget.grab(tmpw)
        # else:
        #     pixmap = qt.QPixmap.grabWidget(tmpw)
        
        # adata.append( convertQImageToArray(pixmap.toImage()) )
        
    return adata


def convertQImageToArray(image):

    if image.format() != qt.QImage.Format_RGB888:
        image = image.convertToFormat(qt.QImage.Format_RGB888)

    ptr = image.bits()
    if qt.BINDING not in ('PySide', 'PySide2'):
        ptr.setsize(image.byteCount())
        if qt.BINDING == 'PyQt4' and sys.version_info[0] == 2:
            ptr = ptr.asstring()
    elif sys.version_info[0] == 3:  # PySide with Python3
        ptr = ptr.tobytes()

    array = np.fromstring(ptr, dtype=np.uint8)

    array = array.reshape(image.height(), -1)[:, :image.width() * 3]
    array.shape = image.height(), image.width(), 3

    return array



def create_pixmap_4_extracted_data(data,   app ) :
    for imageName, data4im in data.items():
        for pos, data4pos in data4im.items():
            pmaps    = data4pos["pmap"]

            pmaps[0]  = array2pixmap(pmaps[0], app)
            pmaps[1]  = array2pixmap(pmaps[1], app)

