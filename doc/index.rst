.. tds2el documentation master file, created by
   sphinx-quickstart on Tue Dec  3 11:49:00 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to tds2el's documentation!
==================================

Tds2el is an open source package for quantitative and model-free analysis of thermal diffuse x-ray scattering from single crystals.
The program allows to extract the full elasticity tensor from the measured scattering intensity distribution as described in Ref [1].
The main functionalities are:

- Fit of elastic constants for arbitrary crystal symmetry with interactive selection of scattering intensities
- Simulation of scattering intensities for given elastic tensor
- 3D reconstruction of scattering intensities and visualisation compatible with Chimera and PyMca
- Peak hunting on large data sets
- Refinement and visualisation of scattering geometry and lattice parameters compatible with CrysAlis and PyMOL

For the measurement strategy and formalism please refer to Ref [1] and references therein.

Alessandro Mirone and Bjorn Wehinger
Grenoble and Geneva 2017


*  **TDS2EL2** is the complete rewrite of the original tds2el code, with the addition of a powerful all-in-one GUI which guides you through all the different steps: data collection, peak finding, sample alignement, experiment geometry refinement, reciprocal space reconstruction, fit of elastic constants, fit of elastic constants plus alignement parameters plus  experiment geometry  and 2Temperature-subtraction fits.
   The installation ( explained in the installation section) install both the old and the new version.


Reference :
  
  * [1]  :download:`PDF <paper.pdf>` Bjorn Wehinger, Alessandro Mirone, Michael Krisch, and Alexei Bosak, Full Elasticity Tensor from Thermal Diffuse Scattering, Phys. Rev. Lett. 118, 035502 (2017).
  
  * https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.118.035502
    
Contents:

.. toctree::
   :maxdepth: 3

   installation
   TDS2EL2
   TDS2EL

	      
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Licence
-------

TDS2EL  is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) 
any later version.

TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
Suite 330, Boston, MA 02111-1307, USA.

TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
and cannot be used as a free plugin for a non-free program. 

Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
is a problem for you.

