import datetime
import fabio
import glob
from scipy.interpolate import interp1d
import time
import string
import os

fl_flux={}
fl_temp={}

sum=0.0

nt=0

for T in ["90","120"] :

    fl = glob.glob("../../data/%s/mgo__0001p_0*.cbf"%T)
    
    fl_flux[T]={}
    fl_temp[T]={}

    for fn in fl:
        print fn
        img = fabio.open( fn,"r")
        Flux = float([ t for t in string.split(img.header["_array_data.header_contents"],"\n") if "Flux" in t ][0].split()[2])
        Temp = float([ t for t in string.split(img.header["_array_data.header_contents"],"\n") if "Temperature" in t ][0].split()[2])


        fl_flux[T][fn] =   Flux
        fl_temp[T][fn] =   Temp
        sum += Flux
        nt  += 1

averag = sum/ nt


for T in ["90","120"] :
    fl = glob.glob("../../data/%s/mgo__0001p_0*.cbf"%T)
    fl.sort()
    
    output = open("normalisation_%s.txt"%T,"w")
    for fn in fl:
        output.write("%s   %e\n"%(fn, fl_flux[T][fn]/averag) )
    output.close()
    output = open("temperature_%s.txt"%T,"w")
    for fn in fl:
        output.write("%s   %e\n"%(fn, fl_temp[T][fn]) )
    output.close()

