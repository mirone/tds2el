import h5py
import matplotlib
matplotlib.use('Qt4Agg')
from pylab import *
import numpy as np
import sys

h5=h5py.File(sys.argv[1],"r" ) 

sim=h5["sim"]
exp=h5["exp"]
keys = exp.keys()

for chiave in keys:
    ve = exp[chiave]["volume"][:]
    vs = sim[chiave]["volume"][:]
    fig=plt.figure()
    
    chiave=chiave[5:]
    #pos = chiave.find("_")
    #chiave=chiave[:pos]
    for k,lista in enumerate([[10,50,60],[64,65,74], [84,115]]):
        for i,isl in enumerate(lista):

            vmin  = min(ve[isl].min(), vs[isl].min())
            vmax  = max(ve[isl].max(), vs[isl].max())
            
            plt.subplot(3,6, 1+i*6+k*2)
            plt.imshow( ve[isl] , vmin=vmin, vmax=vmax )
            
            plt.title(chiave+"/%d"%isl)

            
            plt.subplot(3,6,1+i*6+k*2+1)
            plt.imshow( vs[isl], vmin=vmin, vmax=vmax )
            plt.title("%4.1e - %4.1e"%(vmin,vmax))
            
    figManager = plt.get_current_fig_manager()
    figManager.window.showMaximized()
    plt.show()
