import datetime
import fabio
import glob
from scipy.interpolate import interp1d
import time
import string
import os
def get_timeintensity_interpolator(filename):
    times = []
    ints  = []
    s=open(filename,"r").read()
    if len(s)<200:
        return string.atof(s)
    sl = string.split(s,"\n")
    for l in sl[3:]:
        if "Err" in l or "Advi" in l:
            continue
        l=string.split(l)
        if len(l)==0: break
        day,month, year = string.split(l[0],"/")
        hours,mins,secs = string.split(l[1],":")
        dt =  datetime.datetime(int(year), int(month), int(day), int(hours), int(mins), int(secs))
        s = time.mktime(dt.timetuple())
        # print s, float( l[2])
        times.append(s)
        ints.append(float( l[2] ))
    return interp1d(times, ints)

interpolator = get_timeintensity_interpolator("INTENSITY.dat")
output = open("normalisation.txt","w")
fl = glob.glob("data/Calc*cbf")
fl_dic={}
sum=0.0

for fn in fl:
    print fn
#    im = fabio.open( fn,"r")
#    tempo = im.getheader()["_array_data.header_contents"].split("\n")[1][2:]
#    print tempo
#0         1         2
#012345678901234567890    
#2015/Nov/01 23:12:52
    # monthToNum = {'Nov':11}
    # year, month,day,hours,mins,secs = tempo[:4], monthToNum[tempo[5:8]]   , tempo[9:11],tempo[12:14], tempo[15:17], tempo[18:]
    # dt =  time.mktime( datetime.datetime(int(year), int(month), int(day), int(hours), int(mins), int(float(secs))). timetuple()  )

    st=os.stat(fn)    
    mtime=st.st_mtime

    fl_dic[fn] =  interpolator(mtime)
    sum += fl_dic[fn]


sum = sum/ len(fl)
fl.sort()
for fn in fl:
    output.write("%s   %e\n"%(fn, fl_dic[fn]/sum) )

    
