Generic Goniometer
==================


By default the goniometer is given by this yaml description ::

  setup :
     holder :
       rot :
         axis  : [0,0,1]
         angle : omega      
         rot   :
           axis  : [0,1,0]
           angle : alpha
           rot :
             axis : [0,0,1]
             angle : kappa
             rot:
                axis : [0,-1,0]
                angle : alpha
                rot:
                   axis : [0,1,0]
                   angle : beta
                   rot:
                      axis : [0,0,1]
                      angle : phi
     detector :
         direction : [-1,0,0]
         orientation   :   [  [0,1,0],[0,0,1]  ]
         rot :
            axis : [0,0,-1]
            angle : theta
            rot :
               axis : [0,-1,0]
               angle : d2
               rot :
                 axis : [-1,0,0]
                 angle : d1
     beam :
        direction  : [-1,0,0]
        rot :
            axis : [0,-1,0]
            angle : beam_tilt_angle

this corresponds to the pictures in geometry.pdf. This description is contained by default in CONFIG.yaml and you dont have to worry about. If you need another configuration you can take CONFIG.yaml from the sources, edit it and then launch tds2el2 giving the path to your file in this way ::

  tds2el2_v1 -c myCONFIG.yaml arg1 arg2....

  
         
     





  
