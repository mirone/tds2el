Code Invocation
===============



There are several scripts. For the new version there just one : tds2el2_v1 and is documented by video tutorials.

The original version consists instead of several script that need to be runned in order. In particular  The last one is
tds2el and   is runned several times and can do many many things according to the options selected in the  input file :

      * to do a coarse interpolation of the volume on a cubic grid ( just for confort ofvisualisation )

      * To do the harvesting of the spots around the Bragg peaks and store them into a hdf5 file

      * to further work on the harvest for:

          * fine interpolating around selected Bragg peaks for visualising the data and discover what's going on

          * fitting the elastic constants

          * finely retuning r1,r2,r3 and the cell parameters AA, BB, CC

          * doing both the two previous action  at the same time

          * visualising the calculated diffused arounf peaks ( actually all the visualisation consist in writing a cubic grid to a file )



How can I call the scripts?
---------------------------

  * By virtualEnv installation all the script are in you path as explained in section "installation" once you have sourced the activate script.

  * The Arguments : all scripts takes arguments in the form of name of files containing options
    Go to the sections dedicated to the scripts for details.  
