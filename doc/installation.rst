Installation
============

  * If you install from a Debian package you can skip the following points, install it , and then go directly to the code invocation section

  * Using Git, sources can be retrived with the following commands:
    * public access ::

        git clone  https://gitlab.esrf.fr/mirone/tds2el.git 

    * private access ::

        git clone git@gitlab.esrf.fr:mirone/tds2el.git

   Master branch now contains also the more recent developments. in particular TDS2EL2 the new version with the workflow integrated in a unique GUI.

  * The installation process install both tds2el ( the original program of the prl paper ) and tds2el2.
  * The program have been tested on python2.
  * There are a number of dependencies. The following ones can be easily installed through your linux distribution ::

     cython
     h5py
     scipy
     fabio
     PyOpenGL
     pyyaml
     pyfftw
     matplotlib  
     pymca5
      
  * If you want to use the old version you need also ::
  
      pyopencl    
      mpi4py      
    
  * All the above dependencies can be easily found in a recent Debian distribution. Beside these packages you also needs to install
    locally, in your home directory tree, some other packages using the instruction *python setup.py install --user* which install directly the
    packages in your path, or the instruction *python setup.py install --prefix=/some/where* abd then set your PATH and PYTHONPATH to acced the packages.
    these packages are
  
        * *Silx* that you can get by ::

            git clone https://github.com/silx-kit/silx

	* *Ab2tds*

             git clone https://gitlab.esrf.fr/mirone/ab2tds 
 
  If you are behind a firewall it may happen that you need to manage your proxy variables  ::

       export http_proxy=http://XXX.YYY.ZZZ:1234
       export https_proxy=https://XXX.YYY.ZZZ:1234
       export no_proxy="localhost,.esrf.fr"

  * skip this for the new version.
    
    * For the old tds2el version, 3D rendering of spots in reciprocal space is based on pymol. You may not need it if you dont want to perform such visualisation.
      On many linux distribution pymol is already available as a package and you can use it. If your distribution has no pymol,  
      and if  you want ot use it, you have to install it. To do so ::

       sudo apt-get install libglew-dev       # for the old version. Install libs needed to compile pymol
       sudo apt-get install libglm-dev        # for the old version. Install libs needed to compile pymol
       sudo apt-get install freeglut3-dev     # for the old version. Install libs needed to compile pymol
       git clone https://github.com/schrodinger/pymol-open-source.git    # for the old version. To install pymol
       cd pymol-open-source                                              # for the old version. To install pymol
       python setup.py build install  --user                                   # for the old version. To install pymol
       
    
  Now go into the tds2el directory and run the installation command ::

     python setup.py install --user
    
  Then you are ready to run the code. 

  * For the documentation go, from the source root directory, to doc subdirectory and ::

      make html

  it will create the html pages into _build/html subdirectory.
