Installation
============

  * If you install from a Debian package you can skip the following points, install it , and then go directly to the code invocation section

  * Using Git, sources can be retrived with the following commands:
    * public access ::

        git clone  https://gitlab.esrf.fr/mirone/tds2el.git 

    * private access ::

        git clone git@gitlab.esrf.fr:mirone/tds2el.git

  * Branch version_1  is meant to work with the paper examples
    and being usable. For a more updated branch use *master* branch. To retrieve such version sources, after cloning, go to tds2el directory and ::

      git checkout version_1

  But master will follow better the more recent developments. in particular TDS2EL2 the new version with the workgflow integrated in a unique GUI.

  * The installation process install both tds2el ( the original program of the prl paper ) and tds2el2.
  * The recomended installation is through the virtual environment environment. Alternatively a local installation may work ::

        virtualenv --python=python3 ~/software/VE_python3

  for a python3 environment, or ::

      virtualenv --python=python2 ~/software/VE_python2

  So far the tds2el2 ( the new code) works on both 2 and 3. The old code works instead only on python2.
  Be sure that you unset spurious environment variables ::

      unset LD_LIBRARY_PATH
      unset LDFLAGS
      unset CPPFLAGS
      unset PYTHONPATH

  you will need  to load external packages, therefore manage the firewall variables, at esrf ::

       export http_proxy=http://XXX.YYY.ZZZ:1234
       export https_proxy=https://XXX.YYY.ZZZ:1234
       (At ESRF ask to the TID for the exact location of the proxy)
       export no_proxy="localhost,.esrf.fr"

  Activate the virtualEnv ::

     source ~/software/VE_python(2,3)/bin/activate # choose between 2 or 3 and remove ()

  Upgrade your pip ::

     pip install pip --upgrade
     pip install setuptools --upgrade
     
  Now you need to install a number of dependencies. If you plan to use only the new tds2el2 code ::

     pip install git+https://github.com/silx-kit/silx   # get latest version of silx
     
     # for python3 install pyqt5
        pip install pyqt5
     # or install this on python2
        pip  install pySide2
	
     pip install cython
     pip install h5py
     pip install scipy
     pip install fabio
     pip install PyOpenGL
     pip install pyyaml
     pip install pyfftw
     
  Extra Packages

    * ab2tds
     
  This is all for tds2el2.  If you also need to use the old tds2el code ::
     
     pip install pyopencl    # necessary if you plan to use the old version. not used in tds2el2
     pip install matplotlib  # for the old version
     pip install pymca5      # for the old version
     pip install mpi4py      # for the old version

  Still for the old version, 3D rendering of spots in reciprocal space is based on pymol. You may not need it if you dont want to perform such visualisation.
  On many linux distribution pymol is already available as a package and you can use it if you do a local installation of tds2el. We are here documenting
  the virtualEnv installation so,
  If you want it, you have to install it in your virtual environment. To do so ::

    sudo apt-get install libglew-dev       # for the old version. Install libs needed to compile pymol
    sudo apt-get install libglm-dev        # for the old version. Install libs needed to compile pymol
    sudo apt-get install freeglut3-dev     # for the old version. Install libs needed to compile pymol
    cd ~/software                                                     # for the old version. To install pymol
    git clone https://github.com/schrodinger/pymol-open-source.git    # for the old version. To install pymol
    cd pymol-open-source                                              # for the old version. To install pymol
    python setup.py build install                                     # for the old version. To install pymol

    
  Now go into the tds2el directory and run the installation command ::

     python setup.py install
    
  Then you are ready to run the code. If you have sourced ::
     
      source ~/software/VE_python(2,3)/bin/activate  # choose 2 or 3 and remove ()

  you have the executable in the path.

  * For the documentation go, from the source root directory, to doc subdirectory and ::

      make html

  it will create the html pages into _build/html subdirectory.
