TDS2EL2
=======

Here we illustrate the new TDS2EL2 code on the two cases of the PRL paper : Calcite and MgO
The goniometer, neam and detector description is now given in CONFIG.yaml file.
For generic goniometers you can adapt the goniometer, detector, and beam description following the explanations in this link :doc:`Generic Goniometer <./generic_goniometer>` .
Mind that now the orientation of the detector, if different from default one,  must be given through reediting of the CONFIG.yaml file.

* Calcite

  To reproduce the manipulations illustrated in the following videos download the data
   :download:`calcite.tgz <examples/calcite/calcite.tgz>`
  and the configuration file
   :download:`CALCITE <examples/calcite/CALCITE>`
  and the normalisation coefficients
   :download:`normalisation.txt <examples/calcite/normalisation.txt>`
	     
  in the same directory. Then untar the data. You will get a subdirectory data/
  with many files inside.
  Now you are ready to watch and reproduce the  video.
  Go into the directory where you have untarred *calcite.tgz*, the same where you have saved *CALCITE*  and run TDS2EL2 ::

    tds2el2_v1 CALCITE

  You can now follow the videos. 


  * Peak finding and alignement

    .. raw:: html
	   
       <video  width="600" height="400"   controls>
       <source src="videos/peak_finding_and_first_alignement.webm" type="video/webm">
       Your Browser does not support the video tag.
       </video>
       
  * Fit of the elastic constants

    .. raw:: html
	   
       <video  width="600" height="400"   controls>
       <source src="videos/peak_fitels.webm" type="video/webm">
       Your Browser does not support the video tag.
       </video>
       
  * 3D reciprocal space reconstruction with interpolation

    .. raw:: html
	   
       <video  width="600" height="400"   controls>
       <source src="videos/interp.webm" type="video/webm">
       Your Browser does not support the video tag.
       </video>

* MgO

  To reproduce the manipulations illustrated in the following videos download the data
   :download:`MgO_data.tgz <./examples/MgO/MgO_data.tgz>`
  and the input files
   :download:`mgo_inputs_tds2el2.tgz   <./examples/MgO/mgo_inputs_tds2el2.tgz>`
	     
  Download and untar them in some directory. Now you have a sub-directory *MgO/data* with data files for two temperatures,
  and a sub-directory *MgO/analysis/90/* with input files.
  In order to use Debye Waller factors estimation you need to add  in this directory  the phonons file 
  *phon_e888_co900_dos.phonon.md5_code=05e9b8e2d2e2ff82f3d30660528cb1d6* that you can extract from
   :download:`MgO_input.tgz   <./examples/MgO/MgO_input.tgz>`
	     
  Now go into *MgO/analysis/90/* and run TDS2EL2 ::

    tds2el2_v1 MGO_90_allineato

  You can now follow the videos
  
  * Fit of the elastic constants of MgO with two temperatures subtraction.

    .. raw:: html
	   
       <video  width="600" height="400"   controls>
       <source src="videos/mgofits.webm" type="video/webm">
       Your Browser does not support the video tag.
       </video>
 
