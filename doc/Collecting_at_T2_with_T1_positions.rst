Collecting at T2 with T1 positions
==================================

When fitting data at two different temperatures, it is important that
the data at T2 have the same relative position respect to the spot center, in reciprocal space,
as the data at T1.

In the tutorial on the MgO case we have explained how to use
the cloning script. This script take as input
a file collected at T1 which has been generated to contain, beside the data,
also  the the information on how to clone by interpolation the same Q points
for the data at T2.

This generation was triggered by specifyng the DELTA dictionary.
This dictionary contains the difference in alignement
between T2 and T1.
Here we report the concerned part of the MgO tutorial, with additional information
to do the cloning also with on-Bragg recentering :


Cloning by a mere geometrical alignement
----------------------------------------


Suppose that you have aligned both datasets at T1 and T2 with the parameter files
tds2el_90.par aand  ../120/tds2el_120.par
You can obtain the differences in alignement with this command  ::


   tds2el_getDelta_v1 tds2el_90.par  ../120/tds2el_120.par

to get this output ::

   DELTA={
    "r1":-9.99999999973e-07,
   }

in the case they only differe by a minimal amount in r1 variable.
All the other variables which have changed between T1 and T2 will appear in DELTA.  
   
the *DELTA* dictionary contains the differences which, added to the *90* K parameters,
give the *120K* ones. We create now a new input file by adding the dictionary 
definition at the end of *tds2el_90.par*  (copy and past, at the end of the par file,  DELTA={...} as
from the output of tds2el_getDelta)
and redo the harvesting on *collection.h5*. ( it is already done if you
have used the par file as it is)
Now, thank to the DELTA option, the collected file will contain, beside the
usual harvested data, additional informations that will be used
to interpolate the *120K*  dataset to obtain interpolated datapoints that correspond
to the same Q points at which we have the data points in the *90* K case.

We have now, in *collection.h5* file, the interpolation informations for each data point: they are
obtained calculating
for each *90K* data point the combination of pixels *120* K data that have to be taken in order
to reproduce the same *90* K Q point
using the *120* K  dataset whose alignement  differs by DELTA.

With this information we can clone *collection.h5* using  data for the *120* K dataset.
We use for this the :download:`*clone_120.inp*   <examples/MgO/analysis/120/clone_120.inp>`
input file ::

    images_prefix         =  "../../data/120/mgo__0001p_0"
    filter_file           ="../filter.edf"
    file2becloned = "../90/collection.h5"
    clonefile = "collection_120_cloned.h5"
    normalisation_file    = "../90/normalisation_120.txt"
    threshold = 1000
    badradius = 0 


The command is, to be given in *analysis/120* ::

  tds2el_cloneinterp_v1 clone_120.inp
    
here *images_prefix* points to *120* K directory, the interpolating information is read from *file2becloned*,
the result goes to *clonefile*. The interpolation  is applied on the *120* K datapoints,
after normalisation by *normalisation_file*.
The regions  above *threshold* points are not  used
in the fit. If *badradius* is zero  a whole image is discarded if one of its pixels is higher that *threshold*.
Otherwise for each pixel above-thrshold all pixels within badradius are rejected.

Cloning by considering on-Bragg centering
-----------------------------------------

We have also seen in the tutorial on the Calcite case that when the Bragg spot positions are precise enough
their position (calculated as the baricenter of those points inside Q_min with intensity higher than threshold)
can be used to redefine the center of each spot.

In this case beside providing DELTA
a list of two centering file must be provided.
The workflow is the following

  * a harvesting for T1
  * a harvesting for T2
  * then a recentered havesting for T1 havong set in the input  ::

  
     DELTA={ "r1":0.000000001, or something else }
     centering_file =["collection_for_T1.h5", "collection_for_T2.h5"]


the generated file can then be cloned by giving its name as file2becloned to the cloning script.     
