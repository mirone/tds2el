%\documentclass[aps,prl,preprint,superscriptaddress]{revtex4-1}
\documentclass[aps,prl,superscriptaddress,twocolumn]{revtex4-1}
%\documentclass[prl,10pt,twocolumn,notitlepage,floatfix,footinbib,superscriptaddress,showkeys,aps]{revtex4-1}
% Packages, Macros and speciallizied commands
\usepackage{macros_BW}

%\usepackage{lineno}
%\linenumbers

\begin{document}


\title{High-pressure elasticity and crystal structure from a single measurement}

\author{Bj\"orn Wehinger}
\email[]{bjorn.wehinger@unige.ch}
\affiliation{Department of Quantum Matter Physics, University of Geneva, 24, Quai Ernest Ansermet, CH-1211 Gen\`eve, Switzerland}

\author{Alessandro Mirone}
\email[]{mirone@esrf.fr}
\affiliation{European Synchrotron Radiation Facility, 71, Avenue des Martyrs, F-38000 Grenoble, France}

\date{\today}

\begin{abstract}
We report on the determination of high-pressure elasticity from thermal diffuse x-ray scattering of magnesium oxide at pressures up to 44 GPa. We find that the full elasticity tensor as a function of pressure can be extracted from a single crystal diffraction experiment. This enables the simultaneous measurement of elasticity and crystal structure from a single experiment and thus marks a decisive step towards establishing reliable calibration for extreme pressures. Elastic constants can be extracted with this method from small crystals of arbitrary symmetry, shape and optical properties and will allow for significant progress in our understanding of the elastic behaviour of planets interior at geophysically relevant conditions, structural phase transitions and fundamental interactions of phonons with other quasi-particles.
\end{abstract}

\maketitle

%\section{Introduction}
Accurate pressure calibration is a central topic in high-pressure science and a challenging problem for static pressures in the mbar range. State-of-the-art experiments above 100\,GPa static pressure can be conducted on single crystals and provide  precise results on the crystal structure and equation of state \cite{mcmahon_structure_2007,dewaele_equations_2004}. The combined use of very small crystals with Helium loaded diamond anvil cells assure excellent hydrostaticity and enable high-precision measurements using x-ray diffraction. The limiting factor for even higher accuracy in such experiments often is the uncertainty on absolute pressure. While shock-wave experiments allow for a direct measurement of pressure and provide equation of states with an accuracy of 1-3\,\% at high temperatures \cite{yokoo_ultrahigh-pressure_2009,smith_ramp_2014}, static pressure experiments in the mbar range using diamond anvil cells require the use of a secondary pressure scale that is calibrated against shock-wave experiments \cite{dorogokupets_equations_2007,sokolova_self-consistent_2013,holmes_equation_1989}. Highly accurate calibration is limited by the fact, that the temperatures used in static pressure experiments are often very different from shock-wave experiments which in turn requires the use of additional approximations for the calibration. Furthermore, using a secondary pressure scale requires the pressure calibrant to be at exactly the same conditions as the sample itself. This becomes experimentally very challenging and critical for pressures above 100\,GPa where the volume on which excellent hydrostaticity is guarantied becomes very small. Establishing a primary pressure scale is thus very desirable. Such can be obtained by the combined use of structural and elastic properties and direct calibration up to 55 GPa was obtained from magnesium oxide by combining structural data from x-ray diffraction with the elasticity tensor from Brillouin light scattering \cite{zha_elasticity_2000}. The obtained accuracy is rather good (about 1\,\%) and helped to improve the calibration of the Ruby fluorescence. The drawbacks are the difficulties of extending high-pressure Brillouin light scattering to opaque materials and the complexity of the combined experimental setup. A suitable alternative for the high-pressure measurement of the elastic properties is inealstic x-ray scattering that can be applied to high temperatures as well \cite{antonangeli_simultaneous_2012}. However, such experiments require very sophisticated spectrometer and are very time consuming for systems of low symmetry where different orientations of the crystal need to be applied. Recent progress on the quantitative analysis of thermal diffuse scattering \cite{wehinger_full_2017} shows that the full elasticity tensor can actually be obtained from an x-ray diffraction experiment. This allows the simultaneous measurement of the crystal structure and elasticity in a single experiment. The information on the full elasticity tensor at geophysical relevant pressures furthermore contains the key information for connecting sound velocities of minerals to seismic wave propagation which in-turn allows for conclusive statements on composition and conditions of the interior of planets \cite{antonangeli_composition_2010, antonangeli_toward_2015}. In condensed matter physics, pressure-induced changes in the elasticity tensor capture subtle details of the interplay of phonons with other quasiparticals. These include electronic topological phase transition, electron-phonon coupling in superconductors and spin-phonon coupling in quantum spin systems.  
Precise measurements of the elasticity tensor in the vicinity of structural phase transition allow for a detailed understanding of the underlying mechanism. This is in particular true for high-pressure phase transition where the treatment of finite strain leads to a consistent Landau theory \cite{troster_fully_2014}.
Here, we show that the full elasticity tensor can be obtained in single crystal x-ray diffraction experiments at high pressures, thus enabling the establishment of a primary pressure scale from a single measurement for static pressures. Our model-free analysis of thermal diffuse scattering allows for experiments on single crystals of arbitrary symmetry and shape regardless of optical properties. We benchmark our approach on magnesium oxide for static pressures up to 44\,GPa using a diamond anvil cell.

%\section{Results}

\figuremacroW{MgO-high-pressure-diffuse}{Diffuse scattering of MgO at high pressure and room temperature. Graphical rendering of measured (top panels) and fitted (bottom panels) of diffuse scattering in the vicinity of the $[3\,0\,0]$ reflection at indicated external pressure and $q_z$ = 0.126\,r.l.u.. Areas with zero intensity (dark blue) are affected by elastic diffuse scattering and masked. Plotted intensities are obtained from the irregular experimental grid by an inverted interpolation using four iterations and Thikonov regularisation.}{1.0}

Thermal diffuse scattering from MgO was measured at the high-pressure beamline ID27 at the European Synchrotron Radiation Facility. High-quality samples were prepared by mechanical cutting and gentle etching. A single crystal of size 50$\times$50$\times$20\,$\mu$m$^3$ was placed in a membrane driven diamond anvil cell with 70\,$^\circ$ opening. Natural diamonds of 1.6\,mm thickness and culets of 450\,$\mu$m in diameter were used with stainless steel as gasket. Helium served as pressure medium to ensure optimal hydrostaticity and minimal contribution to the scattering. Fluorescence from two Ruby crystals was used for independent pressure measurement. Monochromatized x rays of wavelength $\lambda$ = 0.62\,\AA\xspace and a spot size of 14$\times$20\,$\mu$m$^2$ were used and scattering intensities were collected in transmission geometry with a noise-free single photon counting hybrid detector equipped with 300\,$\mu$m thick Si pixels of size 172$\times$172\,$\mu$m$^2$ (PILATUS-300K from Dectris, Switzerland) placed 130.291\,mm behind the sample. The cell was rotated orthogonal to the incoming x rays with angular steps of 0.1\,$^\circ$ exploiting the full angular range of the cell opening. The direct beam behind the sample was blocked by a Ta beam stopper with conical opening. In order to insure a monotonous absorption of scattering from the diamonds we first performed a careful absorption measurement to determine the angular ranges for which neither incoming nor scattered x rays satisfied the Bragg condition of the diamonds. 
The diffraction geometry, orientation matrix and cell volume were refined by a Fourier analysis of all collected MgO Bragg reflections. 
For fitting the four rank elasticity tensor  $c_{ijlm}$, we have selected four weak reflections with minimal contribution to elastic diffuse scattering. Data were analyzed using the recently established method for determination of the full elasticity tensor \cite{wehinger_full_2017}. The reciprocal volume to be analyzed is limited according to two conditions. First, the dispersion of the acoustic phonons dominating the thermal diffuse scattering must fulfill the elastic approximation. We thus excluded the region with reduced wavevectors $q > 0.18$ reciprocal lattice units (r.l.u.). Second, to minimize the contribution of elastic scattering, we excluded regions very close to the Bragg reflections. The observed scattering is the convolution of diffuse scattering from diamond and MgO. For our high-pressure experiment we find the exclusion according $q < 0.08$\,r.l.u. appropriate. Diffuse scattering due to stacking faults in the sample contributes as well to the elastic part. Such contribution is localized along the high symmetry direction and can be excluded by carefully masking the scattering along these directions in reciprocal space. Scattering along three main crystallographic directions were masked with slabs of thicknesses $d_x = d_y = 0.104$ and $d_z = 0.036$\,r.l.u. 

%rods with radii $r_x = r_y = 0.052$ and $r_z = 0.018$ to be testet!!!!!!!

We assume the validity of the adiabatic and harmonic approximations and compute scattering intensities for single phonon processes \cite{xu_zkri_2005,bosak_jap_2015}. For the selected regions in reciprocal space the thermal diffuse scattering is dominated by acoustic phonons which in turn follow the elastic wave equation
%
\begin{equation}
\label{eq:el_motion}
\rho \omega^2 u_{i} = c_{ijlm} k_j k_l u_m,
\end{equation}
%
with $\rho$ the mass density, $ \bs{k} = k \bs{n} $ the wave vector and $ \omega $ the frequency of the elastic waves \cite{fedorov_PP_1968} in good approximation \cite{wehinger_full_2017}. Eq. (\ref{eq:el_motion}) is solved for the given crystal symmetry and $c_{ijlm}$ by solving the minimization problem 
\begin{equation}
\label{eq:monotemp}
c, {\bs{b}},{\bs{g}} =  \underset{c^\prime , {\bs{b}^\prime},{\bs{g}^\prime}}{\operatorname{argmin}} \left( \sum_{ {\bs{Q}}}  \left( I^{calc}_{{\bs{Q}}, T}( c^\prime, {\bs{b}^\prime} ,{\bs{g}^\prime}) -  I^{exp}_{{\bs{Q}}, T} \right)^2\right), 
\end{equation}  
%
where  $I^{exp}_{{\bs Q}, T} $ is the measured scattering intensity at temperature $T$ over a set of reciprocal space points  $ \bs{Q} \in \left\{ Q_{exp}  \right\}$ in the proximity to Bragg reflections, $I^{calc}_{{\bs{Q}}, T}$ is the calculated intensity.
The elastic tensor $c$, background $\bs{b}$ and renormalization factor $\bs{g}$ are the fit parameters, see Ref. \cite{wehinger_full_2017} for details.
The diffuse scattering contribution from diamonds is treated as linear in the vicinity of each Bragg reflection and taken into account by $\bs{b}$. $\bs{g}$ takes into account absorption, polarization and geometrical corrections for both, sample and diamonds. Scattering intensities on individual pixels were fitted simultaneously, with the exact diffraction geometry and lattice parameters constraint to the results of the Fourier analysis.

\figuremacro{MgO-high-pressure-elasticity}{Elastic moduli for MgO at room temperature as a function of external pressure. Symbols with error bars were obtained from fits to diffuse scattering intensities in the vicinity of reflections $[\overline{2}\,0\,\overline{2}]$, $[3\,0\,0]$, $[3\,0\,1]$ and $[4\,0\,0]$. Filled area between solid lines are fits to Brilluoin scattering results from Ref. \cite{zha_elasticity_2000}. Pressure was determined from Ruby fluorescence.}{1.0}

Experimental and fitted scattering intensities for MgO measured at room temperatures and pressures up to 44\,GPa are shown in Fig. \ref{fig:MgO-high-pressure-diffuse}. Scattering intensities decay smoothly with increasing $q$. The shape of the intensity distribution changes slightly with increasing pressure. By fitting high-pressure scattering intensities collected at a single temperatures we obtained the full elasticity tensor upon a single scaling factor. Absolute values of $c_{ijlm}$ are obtained by normalization to the bulk modulus for MgO \cite{zha_elasticity_2000}. The pressure evolution of the elastic modulii is shown in Fig. \ref{fig:MgO-high-pressure-elasticity}, were we compare the results of our study to previous results obtained from fits to Brillouin scattering \cite{zha_elasticity_2000}. The values obtained from fitting the diffuse scattering intensity distribution all increase with pressure and follow the different slopes and curvatures as established previously within errors. 
The error bars on the elastic moduli increase with pressure. The reason is decreasing crystal quality which leads to stronger contribution of elastic diffuse scattering. 

\figuremacro{MgO-elasticity-vol-dep}{Volume-dependence of the elastic moduli for MgO at room temperature. Symbols with error bars were obtained from fits to diffuse scattering intensities in the vicinity of selected reflections (see caption of Fig. \ref{fig:MgO-high-pressure-elasticity}. The cell volume was determined by Fourier analysis of the ensemble of collected Bragg reflections with an error of $\sigma_V/V = 6 \times 10^{-3}$. Filled area between solid lines a fits to Brilluoin scattering results from Ref. \cite{zha_elasticity_2000}.}{1.0}

The volume-dependence of the elastic moduli is shown in Fig. \ref{fig:MgO-elasticity-vol-dep}. The unit cell volume was obtained at each pressure point from a Fourier analysis of the ensemble of collected MgO reflections. 

% Expand here: evolution of errors, crystal quality, fitting: estimate total number of pixels used for fit, 

%\section{Discussion}
The rigorous treatment of diffuse scattering measured at high pressures using diamond anvil cells allows for the extraction of the full elasticity tensor. The fits are sensitive to the correct choice of the region in reciprocal space to be analyzed. The upper limit $q_{max}$ needs to be chosen system-dependent such, that the elastic approximation is valid, e.g. the dispersion of acoustic phonons is linear in $q$ and contribution of optical phonons can be treated as constant. Both conditions can be verified either by \textit{ab initio} calculations or iterative fits with increasing $q_{max}$. Here, we used the conditions established at ambient pressure \cite{wehinger_full_2017}. Masking of elastic scattering needs to be established iteratively for $q_{min}$ and the diameter of the slabs. Diffuse scattering due to stacking faults contribute along high symmetry directions and even small contributions can be excluded by carefully masking slabs along these directions in reciprocal space. Alternatively, small contributions of elastic diffuse scattering can be subtracted by exploiting the temperature dependence of thermal diffuse scattering which differs significantly from the temperature dependence of elastic scattering of both the sample and environment. Analyzing the intensity differences measured at two or more slightly different temperatures can increase the precision of the measurements significantly and allows to retrieve absolute values of the full elasticity tensor \cite{wehinger_full_2017}. Such experiments can be performed at high pressure and we expect improvements to similar accuracy as obtained at ambient pressure. In measurements with increased statistics and momentum resolution it will be possible to probe small temperature differences which allow to study the evolution of the elasticity tensor over a wide range of temperature. Determination of absolute values becomes relevant if the bulk modulus is unknown. In the current experiment we also analyzed some symmetry forbidden reflections. Although their presence implies a violation of the crystal symmetry due to imperfections, the symmetry of the diffuse scattering can be constrained in good approximation to the symmetry of the perfect crystal structure. Diffuse scattering from the diamonds and sample environment was treated as linear contribution. This approximation is justified by the fact that the dominant Compton scattering from diamond and contribution from air scattering varies very little across the small regions of interest. In order to minimize the systematic error on the elastic moduli determined from high-pressure TDS studies it is therefor important to carefully evaluate the region in reciprocal space to be analyzed. Because of the convolution the lower limit $q_{min}$ needs to be larger than for ambient measurements without diamonds in the sample environment. A fully consistent study requires a low pressure comparison with an ambient measurement. Deonvolution is an alternative method for the treatment of other contributions to diffuse scattering and to reduce $q_{min}$. The drawback of this approach is the required high statistics on the scattering intensities.

A primary pressure scale can be obtained from the volume-dependence of the elastic modulii (Fig. \ref{fig:MgO-elasticity-vol-dep}) using the thermodynamic relation 
\begin{equation}
\label{eq:td}
K_T = -V  \left( \frac{\partial P}{\partial V} \right)_T, 
\end{equation}
where $K_T$ is the isothermal bulk modulus which can be obtained from the elastic moduli \cite{hill_pps_1952}. Assuming $K_T$ and $V$ to be measured as function of some empirical pressure parameter $\Pi$, the integration of Eq. \ref{eq:td} along an isotherm yields the thermodynamic pressure P with
\begin{equation}
\label{eq:P}
P(\Pi) = - \int^{\Pi}_{0} \frac{K_T}{V}\left( \frac{\partial V}{\partial \Pi} \right)_T d \Pi .
\end{equation}
$\Pi$ can thus be calibrated in terms of absolute pressure if $K_T$ and $V$ are measured over a range of pressure points. For estimating the accuracy of this approach using the proposed methodology we replace the integral in Eq. \ref{eq:P} by a sum 
\begin{equation}
\label{eq:Psum}
P = - \sum_{i=1}^{n} K_i \left(1 - \frac{V_{i-1}}{V_i} \right),
\end{equation}
where 
$K_i$ and $V_i$ are the $n$ individual measurements of $K_T$ and $V$ at discrete values of $\Pi$.
For closely spaced measurements ( $V_{i-1}/V_i \approx 1$) and assuming $\delta K_i/K_i = \sigma_K/K$ and  $\delta V_i/V_i = \sigma_V/V$ the error on P can be approximated by
\begin{equation}
\label{eq:error}
\frac{\sigma P}{P} \approx \sqrt{ \left( \frac{ s_K}{K} \right)^2 +  \frac{1}{n}  \left(  \frac{\sigma_K}{K} \right)^2 + \frac{K_n^2 + K_0^2}{P^2}  \left( \frac{\sigma_V}{V} \right)^2 } ,
\end{equation}
where $s_K/K$ an the systematic error in our measurements due to the approximations in the determination of $K_T$, see Ref. \cite{decker_proposed_1970} for derivation. Systematic error on $V$ cancel.
Assuming $s_K/K = 10^{-2}, \sigma_K/K = 10^{-2}$ and $\sigma_V/V = 10^{-3}$ at $n = 50$ equally spaced measurements we obtain $\sigma P/P = 10^{-2}$ at P = 100\,GPa for MgO, where  $s_K/K$ is the dominating contribution at high pressure. The assumption $\sigma_K/K = s_K/K = 10^{-2}$ is smaller than our result but a realistic estimate for future measurements employing measurements at two or more slightly different temperatures per pressure point \cite{wehinger_full_2017}. The accuracy of the primary pressure scale might further be improved using very high quality crystals of lower symmetry, where the extraction of elastic moduli from TDS works remarkably well \cite{wehinger_full_2017}. 
Our method can be extend not only to systems of low symmetry but also to opaque systems where measurements using Brillouin scattering get to their limits. The rather universal application of our method to small single crystals of arbitrary shape, symmetry and optical properties opens attractive possibilities for elasticity measurements at geophysical relevant pressures. Accurate measurements will help to improve our understanding of condition and composition of planets interior by comparison with seismic data. The quantitative treatment of TDS furthermore constrains the atomic displacement patterns \cite{wahlberg_implications_2017} which is important for the crystallographic refinement of new high-pressure phases.

%\section{Summary}
In summary, we have shown that the elastic moduli of a single crystal can be obtained by analyzing diffuse scattering in a high-pressure x-ray diffraction experiment. Elastic properties can thus be extracted together with the crystal structure at exactly the same conditions. This is of outstanding relevance for high-pressure measurements in material science, geophysics and for the investigation of fundamental interactions of other quasi-particles with phonons in condensed matter physics. 
The simultaneous measurements allow to establish a primary pressure scale using one single experimental technique. This becomes relevant at high static pressures, where excellent hydrostatic conditions can only be assumed on a very small volume. 


%\section{Acknowledgment}
We thank Volodymyr Svitlyk, Alexei Bosak and Jeroen Jakobs for excellent support with the experiment and help in preparation of sample and pressure cells. Mohamed Mezouar, Alexei Bosak and Michael Krisch are greatly acknowledged for fruitful discussions. Data and analysis software are available at \url{http://ftp.esrf.fr/scisoft/TDS2EL/}.


\bibliographystyle{apsrev4-1_BW}
\bibliography{high-pressure-elasticity}

\end{document}
