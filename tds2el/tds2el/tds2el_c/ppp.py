from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys

entrata = sys.argv[1]


script = ""


start = "#__PIPPO_MACRO_BEGIN"
end   = "#__PIPPO_MACRO_END"

accumul=0
for l in open(entrata,"r").read().split("\n"):
    if l[:len(start)] == start:
        accumul=1
    elif l[:len(end)] == end:
        exec(script)
        accumul=0
        script = ""
    elif accumul:
        script = script+l+"\n"
    else:
        print (l)
