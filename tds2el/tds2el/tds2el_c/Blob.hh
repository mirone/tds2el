#ifndef BLOB_HH

#define BLOB_HH

#include"Geo.hh"
#include"Tensor.hh"
class Blob{
public:
  int hkl[3];
  int Gi ;
  int posy;
  int posx ;
  int dims[3] ;
  float * harvest;
  float * my_harvest;
  Tensor * Tens;
  int centering;

  float corr;
  float DcorrDy;
  float DcorrDx;

  float Wcorr;
  float DWcorrDn;
  float DWcorrDy;
  float DWcorrDx;
  float DWcorrDnn;
  float DWcorrDyy;
  float DWcorrDxx;
  float DWcorrDny;
  float DWcorrDyx;
  float DWcorrDnx;
  float *eXp ;
  int NeXp; 

  float braggOffset_pixel[3];

  Blob * otherBlob ; 
  
  Blob(int * hkl ,  int Gi , int poy , int pox , int dz, int dy , int dx , float* harvest, Geo * geo, int centering, int NeXp, float *exclusions) ;

  ~Blob();
  
  void setUp(int centering) ;

  float *braggOffset ;  // inizialmente nullo, settato se si trovano punti oltre threshold ( negativi )

  int npointsTot ; // numero di tutti i buoni punti
  int *indexes  ;  // da dove partono  i punti per la proiezione iesima ( attorno a Gi )
  int *pos      ;  // dove si trova in harvest
  float *sim_intensity;
  Geo* geo ;

  float * getQ0s();
  void setTensor( Tensor * tens);

  double simulate(float temp,  float second_Temp, int rare);

  void  getDataForOld(int &myNpt, float *& myhkl, float * &mydata, float *& myqs, float *& myfacts);

  
  void clear_simulation() ;

  void stickIntensity( float *, float * ) ;

  void  getQfromCenter(int n,int iy,int ix, float *QQ) ;
  float interpolateAtQ( int  i0,int i1,int i2, float *  myq    );
  void merge(Blob * otherBlob);
};

#endif
