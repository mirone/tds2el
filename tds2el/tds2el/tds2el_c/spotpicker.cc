#include<map>
#include<vector>
#include"spotpicker.h"
#include<stdio.h>
#include<string.h>
#include<mpi.h>

#include<emmintrin.h>
#define FLOAT_TO_INT(in,out)  \
     out=_mm_cvtss_si32(_mm_load_ss(&(in)));


#include"coo.h"
#include"csr.h"
#include"csc.h"

#include<math.h>
#include<assert.h>


#define max(a,b) (((a)>(b))? (a):(b))
#define min(a,b) (((a)<(b))? (a):(b))


void get_Q0C(int dim1, int dim2,
	     double * Q0,
	     double dist,
	     double det_origin_X,
	     double det_origin_Y,
	     double pixel_size,
             double*  dm,
             double * p0,
	     double lmbda,
	     double*  MD
	     ) {
  
  // double q;
  // double p_total_tmp=0;
  
  double PX,PY,PZ;
  double pX,pY,pZ;
  
  double PaY,PaZ;
  double norm;
  
  for(int iy=0; iy<dim1; iy++) {
    for(int ix=0; ix<dim2; ix++) {
      PX = -dist;
      
      PaY = ix -det_origin_X;
      PaZ = iy -det_origin_Y;
      
      PY  = MD[0*2+0] * PaY + MD[0*2+1] * PaZ ; 
      PZ  = MD[1*2+0] * PaY + MD[1*2+1] * PaZ ; 
      PY *= pixel_size;
      PZ *= pixel_size;
      
      pX = PX*dm[0*3+0] +  PY*dm[0*3+1] + PZ*dm[0*3+2] ; 
      pY = PX*dm[1*3+0] +  PY*dm[1*3+1] + PZ*dm[1*3+2] ; 
      pZ = PX*dm[2*3+0] +  PY*dm[2*3+1] + PZ*dm[2*3+2] ; 
      
      norm= sqrt( pX*pX + pY*pY + pZ*pZ   );
      
      pX =   (  pX/norm + p0[0]/dist  )/lmbda ; 
      pY =   (  pY/norm + p0[1]/dist  )/lmbda ; 
      pZ =   (  pZ/norm + p0[2]/dist  )/lmbda ; 
      
      Q0[( iy*dim2 + ix ) *3 +0  ] = pX;
      Q0[( iy*dim2 + ix ) *3 +1  ] = pY;
      Q0[( iy*dim2 + ix ) *3 +2  ] = pZ;
    }
  }  
}
    



void window_maximumC(float *target,float * source,
		     int dire, int mindist, int dz, int dy, int dx) {

  // int dim = dz*dy*dx;
  int iz, iy, ix;
  int izt, iyt, ixt;
  // int it;
  float mass;

  if(dire==0) {
    for(iz=0; iz<dz; iz++) {
      for(iy=0; iy<dy; iy++) {
	for(ix=0; ix<dx; ix++) {
	  mass = -1.0e38;
	  for(izt=max(0,iz-mindist); izt<min( dz, iz+mindist ) ; izt++) {
	    mass=max(mass, source[  (izt*dy+iy )*dx+ix ]);
	  }
	  target[  (iz*dy+iy )*dx+ix ] = mass ; 
	}
      }
    }
  }
  if(dire==1) {
    for(iz=0; iz<dz; iz++) {
      for(iy=0; iy<dy; iy++) {
	for(ix=0; ix<dx; ix++) {
	  mass = -1.0e38;
	  for(iyt=max(0,iy-mindist); iyt<min( dy, iy+mindist ) ; iyt++) {
	    mass=max(mass, source[  (iz*dy+iyt )*dx+ix ]);
	  }
	  target[  (iz*dy+iy )*dx+ix ] = mass ; 
	}
      }
    }
  }
  if(dire==2) {
    for(iz=0; iz<dz; iz++) {
      for(iy=0; iy<dy; iy++) {
	for(ix=0; ix<dx; ix++) {
	  mass = -1.0e38;
	  for(ixt=max(0,ix-mindist); ixt<min( dx, ix+mindist ) ; ixt++) {
	    mass=max(mass, source[  (iz*dy+iy )*dx+ixt ]);
	  }
	  target[  (iz*dy+iy )*dx+ix ] = mass ; 
	}
      }
    }
  }
}


// static int massimo=0;
void clonaC(   int * hkls_start ,
	       float *target,
	       int iim ,
	       int * interp_prostart,
	       float *interp_compacted_data,
	       float *data ) {
  // static int massimo=0;
  printf(" iim %d   %d punti \n",  iim, interp_prostart[iim+1] - interp_prostart[iim]  );
  for(int k=interp_prostart[iim]; k<interp_prostart[iim+1]; k++) {
    int ipix = (int) interp_compacted_data[4*k+0];
    int ihkl = (int) interp_compacted_data[4*k+1];
    int nin  = (int) interp_compacted_data[4*k+2];
    float fact =  interp_compacted_data[4*k+3];
    // if(hkls_start[ihkl]+nin>massimo) {
    //   massimo = hkls_start[ihkl]+nin;
    // }
    target[ hkls_start[ihkl]+nin   ] += fact* data[ipix];
    // target[ nin   ] += fact* data[ipix];
  }
  // FILE *f=fopen("massimo","a");
  // fprintf(f,"MASSIMO %d \n", massimo);
  // fclose(f);
}


void AcTypeOp(AC_Struct *in,AC_Struct  *out, int *len, MPI_Datatype *typeptr)
{
  int i;
  // int j;
  for (i=0; i < *len; i++) {
    if( in[i].merit <  out[i].merit) {
      out[i].phi=in[i].phi;
      out[i].merit=in[i].merit;
      out[i].ij=in[i].ij;
    }
  }
}


void calculate_laplacian( float *laplacian, float *Volume, int dim) {
  int n_rows=dim*dim*dim; 
  
  for(int i=0; i<n_rows; i++) {
    laplacian[i]=   0.0f   ;
  }
  int stride=dim*dim;
  for(int ax=0; ax<3; ax++) {
    int i;
    
    for(int iz=(ax==0); iz<dim -(ax==0)  ; iz++) {
      for(int iy=(ax==1); iy<dim-(ax==1); iy++) {
	for(int ix=(ax==2); ix<dim-(ax==2); ix++) {
	  i = (iz*dim+ iy  )*dim  +ix;
	  laplacian[i] -=  2.0f*Volume[i];
	  laplacian[  i  ] +=   Volume[ i-stride]; 
	  laplacian[  i  ] +=   Volume[ i+stride]; 
	}
      }
    }
    for(int iz=0; iz<(dim*(ax!=0)+ (ax==0)) ; iz++) {
      for(int iy=0; iy<(dim*(ax!=1)+ (ax==1)); iy++) {
	for(int ix=0; ix<(dim*(ax!=2)+ (ax==2)); ix++) {
	  i = (iz*dim+ iy  )*dim  +ix;
	  laplacian[  i  ] -=  Volume[i];
	  laplacian[  i  ] +=   Volume[ i+stride]; 
	}
      }
    }
    for(int iz=0+(dim-1)*(ax==0); iz< dim; iz++) {
      for(int iy=0+(dim-1)*(ax==1); iy< dim; iy++) {
	for(int ix=0+(dim-1)*(ax==2); ix< dim; ix++) {
	  i = (iz*dim+ iy  )*dim  +ix;
	  laplacian[  i  ] -=   Volume[i];
	  laplacian[  i  ] +=   Volume[ i-stride]; 
	}
      }
    }
    stride/=dim;
  }
}


float  calculate_grad(float *grad,float * Volume,float *data, int  dim,int  n_row,
		      int n_col, int  nnz, int *Bp, int *Bj, float *Bx, float beta,float**auxs )  {

  float *projection = auxs[0];
  for(int i=0; i<n_col; i++) {
    projection[i]=0.0f;
  }
  double error=0.0;

  printf(" data %p \n", data);

  csc_matvec<int,float>(n_col,             // la trasposta. L' orig e' dai dati al volume
			     n_row, 
			     Bp, 
			     Bj, 
			     Bx,
			     Volume,
			     projection);

  if(data !=NULL) {
    for(int i=0; i< n_col; i++) {
      projection[i] -= data[i];
      error += projection[i]*projection[i];
    }
  }
  for(int i=0; i<n_row; i++) {
    grad[i]=0.0f;
  }

  printf(" qui error %e\n", error);

  csr_matvec<int,float>(n_row,             // L' orig che va dai dati al volume
			n_col, 
			Bp, 
			Bj, 
			Bx,
			projection,
			grad);

  float *laplacian = auxs[0];

  {
    double sum=0;
    for(int i=0; i< n_row; i++) {
      sum +=grad[i]*grad[i] ;
    }
    printf(" qua %e\n", sum );
    

  }

  calculate_laplacian( laplacian, Volume, dim);

  printf(" quo \n");


  // float *laplacian2 = auxs[1];
  // calculate_laplacian( laplacian2, laplacian, dim);
  // printf("return0 \n");
  
  for(int i=0; i<n_row; i++) {
    grad[i] += -beta* laplacian[i] ;
    error -= beta*Volume[i]*laplacian[i];
  }
  printf("return \n");
  return error/2;
}


#define NINTERP 8

SpotPicker::SpotPicker(int Hmax, int do_elastic, float qcube, int navoid, int * pavoid, float DeltaQ_max) {

  this->MT=0;
  
  this->avoid.assign(100000,0);
  this->interp_np4image.assign(100000,0);

  has_centering=0;

  
  for(int i=0; i<navoid; i++) {
    for(int k=max(0,pavoid[i]); k<pavoid[i]+1; k++)    this->avoid[k] = 1;
  }
  
  this->do_elastic = do_elastic;
  this->qcube  = qcube;


  // this->Qmax=0.0;
  
  this->Qmax = DeltaQ_max  ;

  
  CreateAcTypeMPI( &AcTypeMPI);
  if(do_elastic) {
    H=Hmax;
    H2p1 = 2*H+1;
    np4hkl.assign(H2p1*H2p1*H2p1  , 0);
    alone4hkl.assign(H2p1*H2p1*H2p1  , 0.0);
    Nalone4hkl.assign(H2p1*H2p1*H2p1  , 0);
    takeit.assign(H2p1*H2p1*H2p1  , 0);
    hkl_start.assign(H2p1*H2p1*H2p1  +1, 0);

    NCENTER_centering = 10 ; // stride = 2+ NCENTER_centering*(3+1+1)    nspots attivo dq1 dq2 dq3 mass iphi
    centering4hkl.assign(    H2p1*H2p1*H2p1*(2+ NCENTER_centering*(3+1+1) )   , 0  );

    
    AC_Struct dummy_ac;
    dummy_ac.merit = 1.0e6;
    dummy_ac.phi   = 0.0;
    dummy_ac.ij    =   -1; 
    
    AC_data.assign(H2p1*H2p1*H2p1, dummy_ac )     ;
    MPI_Op_create((MPI_User_function *) AcTypeOp,1,&AcOp);

  } else {
    H=Hmax; 
    np4hkl.assign(Hmax*Hmax*Hmax  , 0);
    alone4hkl.assign(Hmax*Hmax*Hmax  , 0);
    data.assign( Hmax*Hmax*Hmax*(3+1+1)*NINTERP,0.0);
    hkl_start.assign(2, 0);
  }

  this->avoid.assign(100000,0);
  this->interp_np4image.assign(100000,0);

}



void SpotPicker::ac_export(  void * target) {
  memcpy( target, &(AC_data[0].phi),  AC_data.size()*sizeof(AC_Struct) )  ;
}

void SpotPicker::ac_import(  void *source  , int N)  {
  AC_data.resize(N);
  if (source) {
    memcpy(  &(AC_data[0].phi), source,   N*sizeof(AC_Struct) )  ;
  } else {
    memset(  &(AC_data[0].phi), 0,   N*sizeof(AC_Struct) )  ;
  }
}


void SpotPicker::CreateAcTypeMPI( MPI_Datatype *AcTypeMPI)
{
    int block_lengths[3];                        // # of elt. in each block
    MPI_Aint displacements[3];                   // displac.
    MPI_Datatype typelist[3];                    // list of types
    MPI_Aint start_address, address;            // use for calculating displac.

    block_lengths[0] = 1;
    block_lengths[1] = 1;
    block_lengths[2] = 1;

    typelist[0] = MPI_FLOAT;
    typelist[1] = MPI_FLOAT;
    typelist[2] = MPI_INT;

    displacements[0] = 0;

    AC_Struct mt;

    // MPI_Address(&mt.phi, &start_address);
    // MPI_Address(&mt.merit, &address);

    MPI_Get_address(&mt.phi, &start_address);
    MPI_Get_address(&mt.merit, &address);

    displacements[1] = address - start_address;

    // MPI_Address(&mt.ij,&address);
    MPI_Get_address(&mt.ij,&address);
    
    displacements[2] = address-start_address;

    // MPI_Type_struct(3,block_lengths, displacements,typelist,AcTypeMPI);
    MPI_Type_create_struct(3,block_lengths, displacements,typelist,AcTypeMPI);
    MPI_Type_commit(AcTypeMPI);


    for(int i=0; i<3; i++) AC_displ[i]=displacements[i];

}







inline float    getdq_cube(float *q, int H ,	int *hkl, float Dqcube , float Ccube ) {
  float dentro=0.0, tmp;
  for(int i=0; i<3;i++) {
    tmp = (q[i]-Ccube) /Dqcube;
    FLOAT_TO_INT( tmp , hkl [i]  );  
    if( hkl[i]<H) {
      float d = q[i]-Ccube -  hkl [i]*Dqcube ; 
      dentro += d*d ;
    } else {
      dentro += 2.0e20; 
    }
  }
  return sqrt(dentro);
}


inline int SpotPicker::getdq(float *q,float *cvs , float *brls, float *dQhkl ,float *Dhkl ,
		 int * hkl  , float DeltaQ_min , float DeltaQ_max , float &dist2,
			     std::vector<float> & centering4hkl, int ishift, int hascentering, float *exclusion_plane, int NeXp) {
  float *eXp = exclusion_plane;
  int i, ieXp, OKeXp;
  float scal[3];
  float sum=0.0f;
  for(i=0; i<3;i++) {
    scal[i] = q[0]*cvs[i*3+0] + q[1]*cvs[i*3+1] + q[2]*cvs[i*3+2] ;
    FLOAT_TO_INT(scal[i], hkl [i]  );  
    Dhkl[i] = scal[i]-hkl [i];
  }
  int pos4hkl =  poshkl( hkl[0],hkl[1],hkl[2] ) ;

  float Dxyz[]={0,0,0};

  if( hascentering) {
    int pos =  pos4hkl  *   (2+NCENTER_centering*(3+1+1) )  ;
    int ncent =  centering4hkl[ ishift + pos +1 ];
    if (ncent ==-1 ) {
    } else {
      for(int k=0; k<3; k++) Dxyz[k]  = centering4hkl[  ishift + pos +2 +ncent*(3+1+1) +k ]  ; 
    }
  }

  for(i=0; i<3;i++) {
    dQhkl[i] =  Dhkl[0]* brls[0*3+i] + Dhkl[1]* brls[1*3+i] + Dhkl[2]* brls[2*3+i]  - Dxyz[i] ; 
    sum += dQhkl[i]*dQhkl[i];
  }
  dist2=sum;

  OKeXp=1;
  for(ieXp=0; ieXp<NeXp; ieXp++) {
    if(eXp[3+4*ieXp] >0 ) {
      float XX=0;
      for(i=0; i<3;i++) {
	XX +=   dQhkl[i]*eXp[i +4*ieXp]  ; 
      }
      if(!( fabs(XX)>eXp[3+4*ieXp]* fabs(DeltaQ_min) || DeltaQ_min == 0  )  ) {
	OKeXp = 0;
      }
    } else {
      float XX=0;
      float scalprod=0;
      float d=0;
      for(i=0; i<3;i++) {
	scalprod +=   dQhkl[i]*eXp[i +4*ieXp]  ; 
      }
      for(i=0; i<3;i++) {
	d = dQhkl[i] - scalprod*eXp[i +4*ieXp];
	XX +=   d*d  ; 
      }
      if(!( sqrt(XX)>fabs(eXp[3+4*ieXp])* fabs(DeltaQ_min) || DeltaQ_min == 0  )  ) {
	OKeXp = 0;
      }
    }
  }
  
  
  return (sum>DeltaQ_min*DeltaQ_min  && sum< DeltaQ_max*DeltaQ_max   &&  OKeXp );
}


void inverti_33( float* AA, float *dd, float *xx ) {

#define A(i,j) ((double)(AA[(i)*3+j]))
#define d(i)   ((double)(dd[i]      )) 
#define x(i)   ((double)(xx[i]      ))  
  
  double det =   (A(0,2)*A(1,1)*A(2,0) - A(0,1)*A(1,2)*A(2,0) - 
		  A(0,2)*A(1,0)*A(2,1) + A(0,0)*A(1,2)*A(2,1) + 
		  A(0,1)*A(1,0)*A(2,2) - A(0,0)*A(1,1)*A(2,2));
  
  xx[0]=-((-(A(1,2)*A(2,1)*d(0)) + 
	   A(1,1)*A(2,2)*d(0) + A(0,2)*A(2,1)*d(1) - 
	   A(0,1)*A(2,2)*d(1) - A(0,2)*A(1,1)*d(2) + 
	   A(0,1)*A(1,2)*d(2)))/det ;
  
  xx[1]= -((A(1,2)*A(2,0)*d(0) - 
	    A(1,0)*A(2,2)*d(0) - A(0,2)*A(2,0)*d(1) + 
	    A(0,0)*A(2,2)*d(1) + A(0,2)*A(1,0)*d(2) - 
	    A(0,0)*A(1,2)*d(2)))/det ;
  
  xx[2]= -((-(A(1,1)*A(2,0)*d(0)) + 
	    A(1,0)*A(2,1)*d(0) + A(0,1)*A(2,0)*d(1) - 
	    A(0,0)*A(2,1)*d(1) - A(0,1)*A(1,0)*d(2) + 
	    A(0,0)*A(1,1)*d(2)))/det;

#undef A
#undef d
#undef x
}

void SpotPicker::harvest(int npoints, int dim1, 
			 float *QQ, float *Kin,
			 float *cellvectors,
			 float *brillvectors,
			 float *data3d,
			 float *Filter ,
			 float DeltaQ_min ,
			 float DeltaQ_max,
			 int nharv,
			 int * toharvest,
			 float phi,
			 int double_exclusion, float powder_width, 
			 float *Qfin_delta,
			 float *Qfin_phi,
			 float * cellvectors_delta,
			 int nimage, int ntot_images, float threshold,
			 float * centering4hkl_ptr, float *eXp, int NeXp) {



  float eXp_default[] = {0,0,0,-100.0};
  if (eXp==NULL) {
    eXp = eXp_default;
    NeXp = 0; 
  } else {
    if ( eXp[0]==0 && eXp[1]==0 && eXp[2]==0 ) {
      eXp = eXp_default;
      NeXp = 0; 
    }
  }

  
  assert(this->do_elastic);

  if(toharvest) {
    takeit.assign(H2p1*H2p1*H2p1  , 0);
    for(int j=0; j< nharv; j++) {
      int pos4hkl =  poshkl(toharvest+3*j ) ;
      if ( pos4hkl < H2p1*H2p1*H2p1) {
	takeit[pos4hkl]=1;
      }
    }
  } else {
    takeit.assign(H2p1*H2p1*H2p1  , 1);
  }

  int incamerati = 0;
  int i;
  float *q;
  float dQhkl[3];
  float Dhkl_dum[3];
  float Dhkl[3];

  float DQhkl_delta[3];
  float DQhkl_phi[3];
  float AA[3*3] ; 
  float shift[3];
  
  int hkl[3];
  int hkl_dum[3];
  int dentro;
  int prendi;
  float dist2;

  int DIMCENT =    H2p1*H2p1*H2p1  * (2+NCENTER_centering*(3+1+1) ) ;

  
  
  float man_dist =0;
  for(i=0; i<9 ; i++) {
    man_dist += fabs( brillvectors[i])  ; 
  }


  has_centering=0;
  if( centering4hkl_ptr   ) {

    
    memcpy( &(centering4hkl[0])  ,centering4hkl_ptr  ,  H2p1*H2p1*H2p1  * (2+NCENTER_centering*(3+1+1) ) * sizeof(float)   );
    int nitems = 1;
    
    if(Qfin_delta) {
      centering4hkl.resize(2*DIMCENT,0);
      
      memcpy( &(centering4hkl[  DIMCENT ])  ,centering4hkl_ptr +DIMCENT ,  H2p1*H2p1*H2p1  * (2+NCENTER_centering*(3+1+1) ) * sizeof(float)   );
      nitems = 2 ; 
    }

    
    has_centering = 1 ;

    for(int i_item=0; i_item<nitems; i_item++) {
      
      for(int ipos=0; ipos < H2p1*H2p1*H2p1  * (2+NCENTER_centering*(3+1+1) ) ; ipos+= (2+NCENTER_centering*(3+1+1) )  ) {
	int N = centering4hkl[i_item*DIMCENT+ipos];
	if(N==0)  {
	  centering4hkl[i_item*DIMCENT+ ipos + 1 ]  =  -1  ;
	}
	else {
	  int imin = 0 ; 
	  float amin = fabs(  nimage - centering4hkl[i_item*DIMCENT+ipos + 2   +0*(3+1+1)   +3 ] ) ; 
	  for(int ib =1 ; ib< N ; ib++) {
	    float bmin = fabs(  nimage - centering4hkl[i_item*DIMCENT+ipos + 2   +ib*(3+1+1)   +3 ] ) ; 
	    if(    bmin < amin           ) {
	      amin = bmin; 
	      imin = ib ; 
	    }
	  }
	  if( amin<100.0) {
	    centering4hkl[i_item*DIMCENT+ipos + 1 ]  =  imin  ;
	  } else {
	    centering4hkl[i_item*DIMCENT+ipos + 1 ]  =  -1 ;	    
	  }
	}
      }
    }
  }

  // printf(" HAS CENTERING %d\n", has_centering);
  for(i=0, q = QQ ; i< npoints; i++, q+=3) {
    
    dentro = getdq(q,cellvectors, brillvectors, 
		   dQhkl , Dhkl,  hkl  , DeltaQ_min , DeltaQ_max , dist2, centering4hkl , 0, has_centering, eXp, NeXp);

    

    
   // if(i%12) continue;


    
    int pos4hkl =  poshkl( hkl[0],hkl[1],hkl[2] ) ;

    
    {
      int pos =  pos4hkl  *   (2+NCENTER_centering*(3+1+1) )  ;
      if (centering4hkl[ pos +1 ] ==-1 ) {
	dentro=0 ; 
      }
      if(Qfin_delta) {    
	if (centering4hkl[ DIMCENT*has_centering +  pos +1 ] ==-1 ) {
	  dentro=0 ; 
	}
      }
    }


    
    if(!has_centering) {
      // if( !dentro && data3d[i]<-threshold) {
      if(  data3d[i]<-threshold) {
	int pos =  pos4hkl*(2+NCENTER_centering*(3+1+1) ) ;
	int where=0;
	if (  centering4hkl[pos]==0   ) {
	  centering4hkl[pos]=1;
	  where=0;
	} else {
	  int Nwhere = centering4hkl[pos] ;
	  where = Nwhere ; // se non si trova 
	  for(int k=0; k<Nwhere; k++) {
	    if (  fabs(centering4hkl[pos+2+k*(3+1+1)+3] -  nimage)  <  10  ) {
	      where=k  ; 
	    }
	  }
	}
	if(where>=NCENTER_centering ) {
	  printf("too many replica for the same spot : increase NCENTER_centering\n" );
	  exit(1);
	}
	if ( where >= centering4hkl[pos])  centering4hkl[pos]+=1 ;
	float mass = centering4hkl[pos + 2+where*(3+1+1 )+4] ;
	float Dmass = -data3d[i] ; 
	centering4hkl[pos  + 2+where*(3+1+1 ) +4   ]  +=  Dmass;
	float addendum[] = { dQhkl[0], dQhkl[1], dQhkl[2], (float) nimage  };
	for(int k=0; k<4; k++) {
	  centering4hkl[pos  + 2+where*(3+1+1 ) +k   ]  *= mass    ;
	  centering4hkl[pos  + 2+where*(3+1+1 ) +k   ]  += addendum[k] *Dmass; 
	  centering4hkl[pos  + 2+where*(3+1+1 ) +k   ]  /= (mass+Dmass) ; 
	}
	if(where)
	printf(" MUltiple Sweep per %d %d %d   %e %e  \n", hkl[0], hkl[1], hkl[2], centering4hkl[pos  + 2+where*(3+1+1 ) +3   ], centering4hkl[pos]  );

	
      }
    }
    
    
    int braggout=0;
    // DOUBLE EXCLUSION
    if(double_exclusion%2 ==1) {
      int k;
      float KQ=0.0;
      float QQ =0.0;
      float Kout;
      float Qhkl;
      for(k=0; k<3; k++) {
	Kout = q[k] + Kin[k];
	Qhkl =
	  hkl[0]* brillvectors[0*3+k] +
	  hkl[1]* brillvectors[1*3+k] +
	  hkl[2]* brillvectors[2*3+k] ;
	KQ+= Kout*Qhkl; 
	QQ+= Qhkl*Qhkl; 
      }
      float criterium = fabs( KQ*2+QQ)/sqrt(QQ); 
      if( criterium < DeltaQ_min*(man_dist  )) {
	braggout=1;
      }
      criterium = fabs( KQ*2-QQ)/sqrt(QQ); 
      if( criterium < DeltaQ_min*(man_dist)) {
	braggout=1;
      }
    }
    int poudre=0;
    if((double_exclusion/2)%2 ==1 ) {
      int k;
      float qq=0.0;
      float QQ =0.0;
      float Qhkl;
      for(k=0; k<3; k++) {
	Qhkl =
	  hkl[0]* brillvectors[0*3+k] +
	  hkl[1]* brillvectors[1*3+k] +
	  hkl[2]* brillvectors[2*3+k] ;
	qq+= q[k]*q[k]; 
	QQ+= Qhkl*Qhkl; 
      }
      QQ = sqrt(QQ);
      qq = sqrt(qq);
      if( fabs(qq-QQ) < DeltaQ_min*powder_width) {
	poudre=1;
      }
    }
    prendi=0;
    if( Filter==NULL && dentro)  {
      prendi=1;
    } else  if( dentro &&  Filter[i] ) {
      prendi=1;
    }
    if(  ntot_images==0 &&   data3d[i]!= data3d[i]    ) prendi = 0 ;
    if( !MT) {
      if (  data3d[i]<0   )  {
	data3d[i] = NAN;
      }
    }
    
    {
      float sum=0.0f;
      for(int k=0; k<3;k++) {
	sum += dQhkl[k]*dQhkl[k];  
      }
      if( sum >  (DeltaQ_max*DeltaQ_max*4) && sum <  (DeltaQ_max*DeltaQ_max*4.8 )  && (Filter==NULL ||Filter[i]) ) {
	/// && sum <  (DeltaQ_max*DeltaQ_max*2.4)
	alone4hkl [pos4hkl] += data3d[i] ; 
	Nalone4hkl[pos4hkl] += 1 ; 
      }
    }
    
    
    if(takeit[pos4hkl]) {
      
      if( prendi and ! (braggout||poudre)) {
	{
	  
	  // riposizionamento per i delta
	  int stilltakeit = 1;
	  if(Qfin_delta) {
	    int dim0 =  npoints/dim1;
	    
	    getdq(Qfin_delta+i*3,cellvectors_delta, brillvectors,
		  DQhkl_delta , Dhkl_dum,  hkl_dum  , DeltaQ_min , DeltaQ_max , dist2 ,
		  centering4hkl, DIMCENT*has_centering , has_centering,eXp, NeXp); //  qua ci devo metter l' altro

	    getdq(Qfin_phi+i*3,cellvectors, brillvectors, 
		  DQhkl_phi , Dhkl_dum,  hkl_dum  , DeltaQ_min , DeltaQ_max , dist2 , centering4hkl,0, has_centering, eXp, NeXp);
	    
	    int ix, iy; 
	    ix = i%dim1; 
	    iy = i/dim1;

	    for(int k=0; k<3; k++) {
	      DQhkl_delta[k] =  - (DQhkl_delta[k] -dQhkl[k])  ;// il segno meno perche devo levarlo

	      AA[k*3+0]       =  DQhkl_phi[k]  -  dQhkl[k]  ;
	      if(iy<dim0-1) {
		AA[k*3+1]       =  QQ[((iy+1)*dim1+ix)*3+k]  -  QQ[ i*3+k ] ;
	      } else {
		AA[k*3+1]       =  -QQ[((iy-1)*dim1+ix)*3+k]  +  QQ[ i*3+k ] ;
	      }
	      if(ix<dim1-1) {
		AA[k*3+2]       =  QQ[(iy*dim1+ix+1)*3+k]  -  QQ[ i*3+k ]  ;
	      } else {
		AA[k*3+2]       =  -QQ[(iy*dim1+ix-1)*3+k]  +  QQ[ i*3+k ]  ;		
	      }
	    }
	    inverti_33( AA, DQhkl_delta, shift );


	    // printf("   shift %e %e %e \n" , shift[0], shift[1], shift[2]);
	   

	    
	    // shift[0]=0; /////// <<<<<<<< =============================
	    // shift[1]=0; /////// <<<<<<<< =============================
	    // shift[2]=0; /////// <<<<<<<< =============================
	     
	    shift[0] = nimage+ shift[0];
	    int ip0 = ((int)( shift[0] +  100000)) -100000;
	    int ip1 = ip0 +1 ;
	    shift[0] -= ip0;
	    
	    shift[1] = iy+ shift[1];
	    int iy0 = ((int)(  shift[1] +  100000)) - 100000;
	    int iy1 = iy0 + 1 ;
	    shift[1] -= iy0;

	    
	    shift[2] = ix+ shift[2];
	    int ix0 = ((int)(  shift[2] +  100000)) - 100000;
	    int ix1 = ix0 + 1 ;
	    shift[2] -= ix0;
	    
	    if(ip0<0 || ip1>= ntot_images ) stilltakeit=0 ;
	    if(iy0<0 || iy1>= dim0 ) stilltakeit=0 ; 
	    if(ix0<0 || ix1>= dim1 ) stilltakeit=0 ;
	    if(stilltakeit) {
	      if(  Filter[iy0*dim1+ix0]==0  )  stilltakeit=0 ;
	      if(  Filter[iy0*dim1+ix1]==0  )  stilltakeit=0 ;
	      if(  Filter[iy1*dim1+ix0]==0  )  stilltakeit=0 ;
	      if(  Filter[iy1*dim1+ix1]==0  )  stilltakeit=0 ;
	      if(  this->avoid[ip0] ) stilltakeit=0 ;
	      if(  this->avoid[ip1] ) stilltakeit=0 ;
	    }
	    // stilltakeit=1;
	    if(stilltakeit) {

	      for(int dp=0; dp<2; dp++) {
		for(int dy=0; dy<2; dy++) {
		  for(int dx=0; dx<2; dx++) {
		    interp_data.push_back(  ip0 +dp  );
		    interp_data.push_back(  (iy0+dy)*dim1 + (ix0+dx)   );
		    interp_data.push_back(pos4hkl);
		    interp_data.push_back(np4hkl[pos4hkl] );
		    interp_data.push_back(  fabs(     ((1-dp)-shift[0])
						     *((1-dy)-shift[1])
						     *((1-dx)-shift[2])    )   );
		    interp_np4image[ip0+dp] += 1;
		  }
		}
	      }
	    }
	  }
	  if( stilltakeit) {
	    // printf(" accumulo \n");
	    idatahkl.push_back(  pos4hkl   );
	    data.insert(data.end(), dQhkl, dQhkl+3    );
	    data.push_back(data3d[i]);
	    data.push_back(phi);
	    data.push_back(i);
	    data.push_back(dist2);
	    np4hkl[pos4hkl] += 1;
	    incamerati++;
	  }

	}
      }
      if(dist2<AC_data[pos4hkl].merit){
	AC_data[pos4hkl].merit=dist2;
	AC_data[pos4hkl].ij = i ; 
	AC_data[ pos4hkl ].phi = phi ; 
      }
    }
  }
  printf(" INCAMERATI %d  %d \n", incamerati , nimage );
}

void SpotPicker::harvestCube(int npoints,
			     float *QQ,
			     float *data3d,
			     float *Filter ) {
  assert(this->do_elastic==0);
  
  float *q;
  int hkl[3];
  float dentro;
  int prendi;

  float Dq = this->qcube /  H;
  float Cq = -this->qcube /2;
  int idata;
  for( idata=0, q = QQ ; idata< npoints; idata++, q+=3) {

    dentro = getdq_cube(q, H ,
			hkl, Dq, Cq );
    prendi=0;
    if( Filter==NULL && dentro<1.0e10)  {
      prendi=1;
    } else  if( dentro<1.0e10 &&  Filter[idata] ) {
      prendi=1;
    }
    if( prendi ) {
      int poscubo  = (  hkl[0]*H + hkl[1] )*H + hkl[2]   ;
      int n = np4hkl[poscubo];
      int i=n;
      if(n==NINTERP) {
	i=n-1;
	while(i>=0 &&    dentro>data[poscubo*5*NINTERP +i*5 + 4 ]   ) i--;
      } else {
	np4hkl[poscubo]++;
      }
      if(i>=0) {
	data[poscubo*5*NINTERP +i*5 + 4 ] = dentro;
	data[poscubo*5*NINTERP +i*5 + 0 ] = q[0];
	data[poscubo*5*NINTERP +i*5 + 1 ] = q[1];
	data[poscubo*5*NINTERP +i*5 + 2 ] = q[2];
	data[poscubo*5*NINTERP +i*5 + 3 ] =  data3d[idata]     ;
      }
    }
  }
}
 

void SpotPicker::compatta(int all_alone) {

  int myrank, nprocs;

  if (all_alone) {
    myrank=0;
    nprocs=1;
  } else {
    MPI_Comm_rank(MPI_COMM_WORLD,&(myrank));
    MPI_Comm_size(MPI_COMM_WORLD,&(nprocs));
  }
  
  if(this->do_elastic) {

    if(myrank==0) {
      MPI_Reduce(MPI_IN_PLACE , &(alone4hkl[0]), alone4hkl.size() , MPI_FLOAT, MPI_SUM, 0,  MPI_COMM_WORLD   );
      MPI_Reduce(MPI_IN_PLACE , &(Nalone4hkl[0]), Nalone4hkl.size() , MPI_INT, MPI_SUM, 0,  MPI_COMM_WORLD   );
      for(int i=0; i< (int)Nalone4hkl.size(); i++) {
	if(Nalone4hkl[i]) alone4hkl[i] =  alone4hkl[i]/ Nalone4hkl[i] ;
	else  alone4hkl[i] =1.0;
      }
      std::vector<float> tmp;
      
      centering4hkl.resize(  H2p1*H2p1*H2p1*(2+ NCENTER_centering*(3+1+1))      )   ;
      
      if(!has_centering) {
	tmp.assign(    H2p1*H2p1*H2p1*(2+ NCENTER_centering*(3+1+1) )   , 0  );
	for(int iproc=1; iproc<nprocs; iproc++) {
	  printf(" CONTROLLO I CENTERING DA %d \n",   iproc  );
	  MPI_Status status;
	  MPI_Recv(  &( tmp[0]  ),H2p1*H2p1*H2p1*(2+ NCENTER_centering*(3+1+1)),MPI_FLOAT,iproc,1829+iproc, MPI_COMM_WORLD, &status);
	  for(int pos4hkl=0; pos4hkl<H2p1*H2p1*H2p1; pos4hkl++) {
	    int pos =  pos4hkl*(2+NCENTER_centering*(3+1+1) ) ;
	    for(int from = 0; from <  tmp[pos ]; from ++) {
	      float iav = tmp[pos+2+from*(3+1+1)+3] ;
	      int Nwhere = centering4hkl[pos] ;
	      int where = Nwhere ; // se non si trova 
	      for(int k=0; k<Nwhere; k++) {
		int ht,kt,lt;
		hkl( ht,kt,lt, pos4hkl);
		printf(" comparo per %d %d %d    %e %e \n", ht,kt,lt  ,  iav  , centering4hkl[pos+2+k*(3+1+1)+3]);
		if (  fabs(centering4hkl[pos+2+k*(3+1+1)+3] -  iav)  <  10  ) {
		  where=k  ; 
		}
	      }
	      if(where>=NCENTER_centering ) {
		printf("too many replica for the same spot in compatta: increase NCENTER_centering\n" );
		exit(1);
	      }
	      if ( where >= centering4hkl[pos])  centering4hkl[pos]+=1 ;
	      float mass_from = tmp[pos + 2+from*(3+1+1 )+4] ;
	      float mass_to = centering4hkl[pos + 2+where*(3+1+1 )+4] ;
	      float mass_total = mass_to + mass_from;
	      centering4hkl[pos  + 2+where*(3+1+1 ) +4   ]  =  mass_total;
	      
	      for(int k=0; k<4; k++) {
		centering4hkl[pos  + 2+where*(3+1+1 ) +k   ]  = ( mass_to *  centering4hkl[pos  + 2+where*(3+1+1 ) +k   ]
								  +mass_from * tmp[pos  + 2+from*(3+1+1 ) +k   ])  / mass_total ;
	      }
	    }
	    {
	      int pos =  pos4hkl*(2+NCENTER_centering*(3+1+1) ) ;
	      int ht,kt,lt;
	      if( centering4hkl[pos] ) {
		hkl( ht,kt,lt,pos4hkl );
		printf("  %d %d %d     %e \n", ht,kt,lt ,  centering4hkl[pos]);
	      }
	    }
	  }
	}
      }
    } else {
      MPI_Reduce(&(alone4hkl[0]), MPI_IN_PLACE , alone4hkl.size()   , MPI_FLOAT, MPI_SUM, 0,  MPI_COMM_WORLD   );
      MPI_Reduce(&(Nalone4hkl[0]), MPI_IN_PLACE , Nalone4hkl.size() , MPI_INT, MPI_SUM, 0,  MPI_COMM_WORLD   );
      if(!has_centering) {
	printf(" MANDO I CENTERING  %d \n",    myrank );
	MPI_Send(  &( centering4hkl[0]) ,   H2p1*H2p1*H2p1*(2+ NCENTER_centering*(3+1+1) )      ,MPI_FLOAT,0,1829+myrank, MPI_COMM_WORLD);
      }
    }
    
    std::vector<int> hkl_start_cumsum[nprocs];
    
    MPI_Allreduce(MPI_IN_PLACE,&(AC_data[0]),  AC_data.size() ,AcTypeMPI,AcOp,MPI_COMM_WORLD);

    for(int  i=0; i< (int) np4hkl.size(); i++) {
      hkl_start[i+1]=hkl_start[i]+np4hkl[i];
    }

    if(myrank==0) {
      MPI_Reduce(MPI_IN_PLACE,&(hkl_start[0]),(int)(np4hkl.size()+1),MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
    } else {
      MPI_Reduce(&(hkl_start[0]),NULL,(int)(np4hkl.size()+1),MPI_INT,MPI_SUM,0,MPI_COMM_WORLD );
    }

    if(myrank==0) {
      // hkl_start_cumsum[0] = hkl_start ;
      hkl_start_cumsum[0].resize(np4hkl.size(),0) ;
      for(int iproc=1; iproc<nprocs; iproc++) {
	MPI_Status status;
	int n = np4hkl.size();
	std::vector<int> dum(n,0);


	hkl_start_cumsum[iproc].resize(n);

	if(iproc==1) {
	  for(int k=0; k< n; k++) {
	    hkl_start_cumsum[iproc][k] =  np4hkl[k] ; ////  +   hkl_start[k] ; 
	  }
	} else {

	  MPI_Recv(  &( dum[0]  ), n,MPI_INT,iproc-1,2636, MPI_COMM_WORLD, &status);
	  for(int k=0; k< n; k++) {
	    hkl_start_cumsum[iproc][k] =  hkl_start_cumsum[iproc-1][k] +   dum[k] ; 
	  }
	}
      }
    } else { 
      int n = np4hkl.size();

      if(myrank  < nprocs-1  ) {
	MPI_Send(  &( np4hkl[0]  ), n,MPI_INT,0,2636, MPI_COMM_WORLD);
      }
    }


    
    interp_prostart.assign( this->interp_np4image.size()+1,0 )    ;

    std::vector<int>    prostart  ;


    std::vector<float>  interp_exchange;

    int ntot_interpD = interp_data.size();

    MPI_Allreduce(MPI_IN_PLACE,  &ntot_interpD, 1,MPI_INT, MPI_SUM,MPI_COMM_WORLD );
    if(ntot_interpD) {
      for(int i=0; i< (int) this->interp_np4image.size(); i++) {
	interp_prostart[i+1]=interp_prostart[i]+this->interp_np4image[i];
      }      
      if(myrank==0) {
	MPI_Reduce(MPI_IN_PLACE,&(interp_prostart[0]),
		   (int)(interp_prostart.size()),
		   MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);
      } else {
	MPI_Reduce(&(interp_prostart[0]),
		   NULL,(int)( interp_prostart.size()),
		   MPI_INT,MPI_SUM,0,MPI_COMM_WORLD );
      }
      if(myrank==0) {
	interp_compacted_data.resize( interp_prostart.back()*4 );
	prostart =  interp_prostart ; 
      }


      for(int iproc=0; iproc<nprocs; iproc++) {


	if(myrank==0) {
	  if(iproc==0) {
	    interp_exchange = this->interp_data;   ; 
	  } else {
	    int n;
	    MPI_Status status;
	    MPI_Recv(  &(n), 1,MPI_INT,iproc,1636, MPI_COMM_WORLD, &status);
	    interp_exchange.resize(n);
	    if(n) MPI_Recv(  &(interp_exchange[0]), n ,MPI_FLOAT,iproc,1637, MPI_COMM_WORLD, &status);
	  }
	} else {
	  if(iproc==myrank) {
	    int n = this->interp_data.size();
	    MPI_Send(  &(n), 1,MPI_INT,0,1636, MPI_COMM_WORLD);
	    if(n) MPI_Send(  &(this->interp_data[0]), this->interp_data.size(),MPI_FLOAT,0,1637, MPI_COMM_WORLD);
	  }
	}
	if(myrank==0) {
	  for(int i=0; i<(int)interp_exchange.size(); i+=5) {
	    int pos = prostart[(int) interp_exchange[i+0]]*4 ; 
	    interp_compacted_data[ pos +0 ]= interp_exchange[i+1]; // numero pixel
	    interp_compacted_data[ pos +1 ]= interp_exchange[i+2]; // su quale spot pos4hkl
	    interp_compacted_data[ pos +2 ]= interp_exchange[i+3]  +hkl_start_cumsum[iproc][(int)interp_exchange[i+2]]    ;
	    // in quale punto della nuvola
	    interp_compacted_data[ pos +3 ]= interp_exchange[i+4]; // il fattore
	    prostart[(int) interp_exchange[i+0]]+=1;
	  }
	}
      }
    }

 
    if(myrank==0) {
      compacted_data.resize(7* hkl_start.back() );
    }

    np4hkl.assign(  np4hkl.size()  ,0) ;
    
    {
      int ndatas[nprocs];
      int lendat = (int) idatahkl.size();
      MPI_Gather( &(  lendat), 1, MPI_INT, ndatas , 1 , MPI_INT, 0, MPI_COMM_WORLD);
      int ntot=0;
      // int nsends[nprocs];
      int ndispls[nprocs+1];
      ndispls[0]=0;
      if(myrank==0) {
	for(int i=0; i<nprocs; i++) {
	  ntot+= ndatas[i];
	  ndispls[i+1] = ndispls[i]+ndatas[i];
	}
      }
      ndatas[0]=0;
      int Nsend=0;
      if(myrank==0) idatahkl.resize(ntot);
      if(myrank) Nsend = lendat; 
      
      MPI_Gatherv(&(idatahkl[0]), Nsend, MPI_INT, &(idatahkl[0]), ndatas, ndispls, MPI_INT, 0, MPI_COMM_WORLD);

      if(myrank==0) data.resize(7*ntot);
      for(int i=0; i<nprocs; i++) {
	ndatas[i]  *=7;
	ndispls[i] *=7;
      }

      MPI_Gatherv(&(data[0]), Nsend*7, MPI_FLOAT,  &(data[0]), ndatas, ndispls, MPI_FLOAT, 0, MPI_COMM_WORLD);
    }

    if(myrank==0) {
      for(int i=0; i< (int) idatahkl.size(); i++) {    
	int ipos =   idatahkl[i]; 
	int start = hkl_start[ipos] + np4hkl[ipos];
	for(int k = 0; k<7; k++) {
	  compacted_data[7*start+k] = data[7*i+k];
	}
	np4hkl[ipos]++;
      }

          // {
	  //   int start, end, hklpos;
	  //   hklpos = poshkl(-1,1,10);
	  //   start = hkl_start[hklpos]; 
	  //   end   = start+np4hkl[hklpos];
	  //   printf("DOPO  PER PROCESSO %d  start  %d end %d \n",myrank,  start, end );
	  // }
    }

    idatahkl.clear();
    data.clear(); 
  } else {

    int i;
    int ntot=0;
    for(i=0; i< (int) np4hkl.size(); i++) {
      ntot+=np4hkl[i];
    }
    hkl_start[ 1] = ntot;
    hkl_start[ 0] = 0;


    if(myrank==0) {
      MPI_Reduce(MPI_IN_PLACE , &(hkl_start[0]), 2, MPI_INT, MPI_SUM, 0,  MPI_COMM_WORLD   );
    } else {
      MPI_Reduce(  &(hkl_start[0]),  NULL , 2, MPI_INT, MPI_SUM, 0,  MPI_COMM_WORLD );
    }


    compacted_data.resize(7* hkl_start.back() );

    int ipos=0;
    // * 5* NINTERP
    for(i=0; i< H*H*H ; i++) {    
      int n = np4hkl[i]  ; 
      for(int k = 0; k<n; k++) {
	for(int j=0; j<4; j++) {
	  compacted_data[7*ipos+j] = data[   i*5*NINTERP  +  k*5 +j  ];
	}
	for(int j=4; j<7; j++) compacted_data[7*ipos+j] = 0 ; 
	ipos++;
      }
    }

    {
      int ndatas[nprocs];
      int lendat = ntot;
      MPI_Gather( &(  lendat), 1, MPI_INT, ndatas , 1 , MPI_INT, 0, MPI_COMM_WORLD);


      int ntot=0;
      int nsends[nprocs];
      int ndispls[nprocs+1];
      ndispls[0]=0;
      if(myrank==0) {
	for(int i=0; i<nprocs; i++) {
	  ntot+= ndatas[i];
	  nsends[i] = ndatas[i]*7;
	  ndispls[i+1] = ndispls[i]+nsends[i];
	}
	nsends[0]=0;
      }
      int Nsend=0;
      if(myrank) Nsend = lendat; 
      MPI_Gatherv(&(compacted_data[0]), Nsend*7, MPI_FLOAT,  &(compacted_data[0]),nsends , ndispls, MPI_FLOAT, 0, MPI_COMM_WORLD);


    }

    np4hkl.assign(  np4hkl.size()  ,0) ;
    if(myrank==0) np4hkl[0]=compacted_data.size()/7;

    idatahkl.clear();
    data.clear();
    if(myrank) compacted_data.clear();
  }
}



void steepest_descent( float *Volume,  float *donnees, int dim, int  n_row,
		       int n_col, int  nnz, int *Bp,  int *Bj,  float *Bx, float beta   ) {
  
  float *auxs[4];
  for(int i=0; i<4; i++) auxs[i] = new float [max(n_col, n_row)] ; 
  float *grad=auxs[0];

  float err;

  for (int iter=0; iter<100; iter++) {
    err=calculate_grad(grad, Volume, donnees, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+1 ) ; 
    
    double mod_grad=0.0;
    for(int i=0; i<n_row; i++) {
      mod_grad += grad[i]*grad[i];
    }
    mod_grad=sqrt(mod_grad);
       
    for(int i=0; i<n_row; i++) {
      grad[i]/=mod_grad; 
    }
    float *grad2=auxs[1];
    calculate_grad(grad2, grad, NULL, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+2 ) ;
    double alpha = 0.0;
    for(int i=0; i<n_row; i++) {
      alpha += grad[i]*grad2[i];
    }
    
    for(int i=0; i<n_row; i++) {
      Volume[i] -=  grad[i]*mod_grad/alpha   ;
    }
    printf( " errore est %e  mod_grad est  %e\n",  err,mod_grad ) ;
   
    // err=calculate_grad(grad2, Volume, donnees, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+2 ) ; 
    // alpha = 0.0;
    // for(int i=0; i<n_row; i++) {
    //   alpha += grad[i]*grad2[i];
    // }
    
    // printf( " Scal  %e errore est %e  mod_grad est  %e\n", alpha, err,mod_grad ) ;
  }
  for(int i=0; i<4; i++) delete auxs[i] ; 
}


void cg( float *Volume,  float *donnees, int dim, int  n_row,
		       int n_col, int  nnz, int *Bp,  int *Bj,  float *Bx, float beta   ) {
  
  float *auxs[5];
  for(int i=0; i<5; i++) auxs[i] = new float [max(n_col, n_row)] ; 
  float *grad=auxs[0];
  float *grad_old=auxs[1];
  float *p=auxs[2];

  float err;
  err=calculate_grad(grad, Volume, donnees, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+3 ) ; 
  for(int i=0; i<n_row; i++) p[i]=grad[i];
  double rrold=0.0,rr;
  for(int i=0; i<n_row; i++) {
    rrold += grad[i]*grad[i];
  }
  rr=rrold;
 
  for (int iter=0; iter<50; iter++) {

    for(int i=0; i<n_row; i++) {
      grad_old[i] = grad[i];
    }
    
    calculate_grad(grad, p, NULL, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+3 ) ;
    double pap = 0.0;
    for(int i=0; i<n_row; i++) {
      pap += p[i]*grad[i];
    }
    
    for(int i=0; i<n_row; i++) {
      Volume[i] -=  p[i]*rr/pap   ;
    }

    err=calculate_grad(grad, Volume, donnees, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+3 ) ; 
    rrold=rr;
    rr=0;
    for(int i=0; i<n_row; i++) {
      rr+=  grad[i]*(grad[i]-grad_old[i]);
    }
    float beta = rr/rrold  ; 
    for(int i=0; i<n_row; i++) {
      p[i]=  grad[i]+  p[i]*beta ;
    }
    printf( " errore est %e  mod_grad est  %e\n",  err,rr ) ;
  }
  for(int i=0; i<5; i++) delete auxs[i] ; 
}


void Fista( float *Volume,  float *donnees, int dim, int  n_row,
	    int n_col, int  nnz, int *Bp,  int *Bj,  float *Bx, float beta , float lip, int Niter  ) {
  
  float *auxs[4];
  for(int i=0; i<4; i++) auxs[i] = new float [max(n_col, n_row)] ; 
  float *grad=auxs[0];
  float *Vold = auxs[1];

  float err;

  float t=1;
  for (int iter=0; iter<Niter; iter++) {
    err=calculate_grad(grad, Volume, donnees, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+2 ) ; 
    
    float tnew = (1.0+sqrt(1+4*t*t))/2;
    float tmp;
    
    for(int i=0; i<n_row; i++) {
      tmp = Volume[i] -  grad[i]/lip;
      Volume[i]= tmp + ((t-1)/tnew)*( tmp-Vold[i]  )   ;
      Vold[i]=tmp;
    }
    t=tnew;
    
    printf( " errore est %e  \n",  err ) ;   
  }
  for(int i=0; i<4; i++) delete auxs[i] ; 
}


float Lipschitz(  int dim, int  n_row,
		       int n_col, int  nnz, int *Bp,  int *Bj,  float *Bx, float beta   ) {
  
  float *auxs[4];
  for(int i=0; i<4; i++) auxs[i] = new float [max(n_col, n_row)] ; 
  float *grad=auxs[0];
  float *vect=auxs[1];
  

  for(int i=0; i<n_row; i++) {
    vect[i]=1.0f/sqrt(float(n_row));
  }
  double mod_grad;
  for (int iter=0; iter<20; iter++) {
    calculate_grad(grad, vect, NULL, dim, n_row,n_col,  nnz, Bp, Bj, Bx, beta,auxs+2 ) ; 
    
    mod_grad=0.0;
    for(int i=0; i<n_row; i++) {
      mod_grad += grad[i]*grad[i];
    }
    mod_grad=sqrt(mod_grad);
    
    for(int i=0; i<n_row; i++) {
     vect[i]= grad[i]/mod_grad; 
    }
    printf("Lipschitz %e\n", mod_grad );
  }
  for(int i=0; i<4; i++) delete auxs[i] ; 

  return mod_grad;
}


void SpotPicker::getDataToFit(int *nspots, int *tofits_spots, int **hkls , int **Npts,
			      float **DQs,  float **Datas, float **ac_phis  , int ** ac_ijs, int nred,
			      float **tuttiPhi, int **tuttiIJ, float **tuttiDist)  {
  int N=0;
  int nspots_input=*nspots;
  *nspots=0;
  for(int i=0; i< (int) np4hkl.size(); i++) {

    int takespot=0;
    if(np4hkl[i]) {
      if( tofits_spots==NULL) {
	takespot=1;
      } else {
	
	int h,k,l;
	hkl( h,k,l,i) ;
	for(int j=0; j<nspots_input; j++) {
	  if( h == tofits_spots[3*j] && k == tofits_spots[3*j+1] && l == tofits_spots[3*j+2] ) {
	    takespot=1;
	  }
	} 
      }
      if ( takespot) {
	*nspots += 1;
	N       += np4hkl[i]/nred;
      }
    }
  }
  *hkls    = (int *) malloc( *nspots *3*sizeof(int) ) ;
  *Npts   = (int *) malloc( *nspots *sizeof(int)    ) ;
  *DQs    = (float*)  malloc( N*3*sizeof(float)    ) ;
  *Datas  = (float*) malloc( N*sizeof(float)    ) ;

  *tuttiPhi  = (float*)  malloc( N*sizeof(float)    )   ;
  *tuttiIJ   =  (int *) malloc( N *sizeof(int)    )   ;
  *tuttiDist = (float*)  malloc( N*sizeof(float)    )   ;

  *ac_phis  = (float*) malloc( *nspots *sizeof(float)    ) ;
  *ac_ijs   = (int *) malloc( *nspots  *sizeof(int) ) ;
  

  N=0;
  *nspots=0;
  for(unsigned int i=0; i<np4hkl.size(); i++) {
    int takespot=0;
    if(np4hkl[i]) {
      int h,k,l;
      hkl( h,k,l,i) ;
      
      if( tofits_spots==NULL) {
	takespot=1;
      } else {
	
	for(int j=0; j<nspots_input; j++) {
	  if( h == tofits_spots[3*j] && k == tofits_spots[3*j+1] && l == tofits_spots[3*j+2] ) {
	    takespot=1;
	  }
	} 
      }
      
      if ( takespot) {
	
	(*hkls)[3* *nspots  ] = h;
	(*hkls)[3* *nspots+1] = k;
	(*hkls)[3* *nspots+2] = l;
	(*Npts)[*nspots] =  np4hkl[i]/nred ; 

	(*ac_phis) [*nspots] =  AC_data[i].phi ; 
	(*ac_ijs ) [*nspots] =  AC_data[i].ij     ; 

	
	*nspots += 1;
	int start = hkl_start[i]; 
	
	for(int n=0; n<np4hkl[i]/nred; n++) {
	  for(int k=0; k<3; k++) {
	    (*DQs)[3*(N+n)+k] =  compacted_data[7*(start+n*nred)+k] ; 
	  }
	  (*Datas)[N+n] =  compacted_data[7*(start+n*nred)+3] ;
	  (*tuttiPhi)[N+n] =  compacted_data[7*(start+n*nred)+4] ;
	  (*tuttiIJ)[N+n] =  compacted_data[7*(start+n*nred)+5] ;
	  (*tuttiDist)[N+n] =  compacted_data[7*(start+n*nred)+6] ;
	}
	N       += np4hkl[i]/nred;
      }
    }
  }
}

int   SpotPicker::interpolatedSpot(int h, int k, int l, int dim, float *axis, float *corner, float * Volume, float beta, int Niter){
  int hklpos ;


  if(this->do_elastic) {
    hklpos = poshkl(h,k,l )    ;
  } else {
    hklpos = 0;
  }

  int start, end;
  start = hkl_start[hklpos]; 
  end   = start+np4hkl[hklpos];

  printf(" start end %d %d \n", start, end );


  if (end==start) return 0;

  std::vector<int> froms;
  std::vector<int> tos  ;
  std::vector<float> coeffs ; 
  std::vector<float> donnees ; 

  float scals[3];
  int   iscal, iscals[3];
  float dscal;

  float coeffs_tmp[8];
  int dim2=dim*dim;
  int offsets[] = { 0,1,dim, dim+1,  dim2+0,  dim2+1, dim2+dim, dim2+dim+1  };


  for(int is=start*7; is<end*7; is+=7) {
    int interno=1;

    coeffs_tmp[1] =  1.0f;
    coeffs_tmp[0] =  1.0f;
    
    int dimblock=1; // il primo axis e lungo x, quindi rapida di VOlume


    if(Niter>0) {
      for(int i=0; i<3; i++) {
	scals[i]=0;
	for(int k=0; k<3; k++) {
	  scals[i] +=  (compacted_data[is+k] -corner[k] ) *axis[i*3+k] ; 
	}
	if( scals[i]<0  || scals[i]> dim-1) interno=0;
	float tmp = scals[i]-0.5f;
	FLOAT_TO_INT( tmp ,   iscal) ;  
	iscals[i]=iscal;
	dscal  = scals[i]-iscal;
	
	for(int l=0; l<dimblock; l++) {
	  coeffs_tmp[dimblock+l] =  dscal * coeffs_tmp[l] ;
	}
	for(int l=0; l<dimblock; l++) {
	  coeffs_tmp[l]	 =  (1.0f-dscal)* coeffs_tmp[l];
	}
	dimblock*=2;
      }
    } else {
      for(int i=0; i<3; i++) {
	scals[i]=0;
	for(int k=0; k<3; k++) {
	  scals[i] +=  (compacted_data[is+k] -corner[k] ) *axis[i*3+k] ; 
	}
	if( scals[i]<0  || scals[i]> dim-1) interno=0;
	float tmp = scals[i]-0.5f;
	FLOAT_TO_INT( tmp ,   iscal) ;  
	iscals[i]=iscal;
	dscal  = scals[i]-iscal;
	if(dscal>0.5) {
	  for(int l=0; l<dimblock; l++) {
	    coeffs_tmp[dimblock+l] =   coeffs_tmp[l] ;
	  }
	  for(int l=0; l<dimblock; l++) {
	    coeffs_tmp[l]	 =  0* coeffs_tmp[l];
	  }
	} else {
	  for(int l=0; l<dimblock; l++) {
	    coeffs_tmp[dimblock+l] =  0* coeffs_tmp[l] ;
	  }
	  for(int l=0; l<dimblock; l++) {
	    coeffs_tmp[l]	 =  coeffs_tmp[l];
	  }
	}
	dimblock*=2;
      }
    }


    if(!interno) continue;

    int J  = donnees.size();
    int I0 = ( iscals[2]*dim+iscals[1])*dim + iscals[0]   ; // il primo axis e lungo x, quindi rapida di Volume

    for(int k=0; k<8; k++) {
      froms  .push_back(J) ;           // dai dati
      tos.push_back(I0+offsets[k]);    // al volume
      coeffs.push_back(  coeffs_tmp[k]  ) ; 
    }
    donnees.push_back(compacted_data[is+3])   ;
  }

  int n_col = donnees.size();  // dai dati 
  int n_row =  dim*dim*dim  ;    // al volume
  int nnz = coeffs.size()   ; 
  
  std::vector<int> Bp, Bj;
  std::vector<float> Bx;
  Bp.reserve(   n_row+1    ) ;
  Bj.reserve(nnz );
  Bx.reserve(nnz);

  coo_tocsr<int,float>( n_row,n_col,
			nnz,
			&tos[0],
			&froms[0],

			&coeffs[0],
			&Bp[0],
			&Bj[0],
			&Bx[0]);
  froms.clear();
  tos.clear();
  coeffs.clear();


  if(0) {
    steepest_descent( Volume, &donnees[0], dim, n_row,n_col,  nnz, &Bp[0], &Bj[0], &Bx[0], beta   ) ;
  } else  if(1) {




    float lip = Lipschitz( dim, n_row,n_col,  nnz, &Bp[0], &Bj[0], &Bx[0], beta   ) ;
    printf("LIPSHICTZ %e\n", lip);
    printf(" chiamo fista \n");
    Fista( Volume, &donnees[0] , dim,  n_row,
	   n_col,   nnz, &Bp[0], &Bj[0], &Bx[0]  ,  beta ,  lip*1.5 , abs(Niter) ) ;
  } else {
    cg( Volume, &donnees[0], dim, n_row,n_col,  nnz, &Bp[0], &Bj[0], &Bx[0], beta   ) ;
  }
  return  end - start; 
};


