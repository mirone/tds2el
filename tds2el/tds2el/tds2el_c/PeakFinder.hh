class PeakFinder {
public:
  int dim1;
  int dim2;
  float threshold;
  int peak_size ; 


  PeakFinder(int dim1, int dim2, float threshold, int peak_size);
  ~PeakFinder();
  
  void find_maxima(float *newim, int  iim , int procs ,int  myrank , int attheend , int &np, float *&p);
  
  
  float *  imsum ; 
  float *  imN ; 
  float *  imY ; 
  float *  imX ; 
  
};

