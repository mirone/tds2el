# -*- coding: utf-8 -*-
###################################################################################
# Fits, Elastic constants fits : Alessandro Mirone
# European Synchrotron Radiation Facility
###################################################################################
#distutils: extra_compile_args = -fopenmp
# distutils: language = c++


import cython
from cython.parallel cimport prange
from cpython cimport bool
cimport numpy
from numpy cimport ndarray
import math
from libc.stdlib cimport free
from libc.stdio cimport  printf
from libc.string cimport memcpy
from libcpp.vector cimport vector
import h5py

from libc.stdio cimport printf


cdef struct AC_Struct:
  float phi
  float merit 
  int ij


cdef extern from "math.h":
    double  fabs(float)nogil
import numpy


cdef extern from "PeakFinder.hh":
    cdef cppclass  PeakFinder :
      PeakFinder(int , int , float , int ) 
      void find_maxima(float *, int   , int  ,int   , int  , int &, float *&) nogil
      int dim1
      int dim2	

cdef class PeakFinder_cy:
    cdef PeakFinder  *thisptr

    def __cinit__(self ,
                  int dim1,
                  int dim2,
                  float threshold,
                  int peak_size
    ):
        self.thisptr = new PeakFinder(dim1,  dim2, threshold, peak_size)
        
    def __dealloc__(self):
        print(" DEALLOCAZIONE PeakFinder  ")
        del self.thisptr

    def find_maxima(self,
                    ndarray[numpy.float32_t, ndim = 2] newim,
                    int iim,
                    int procs,
                    int myrank,
                    int attheend) :
        
        assert   newim.flags["C_CONTIGUOUS"]
        
        assert newim.shape[0] == self.thisptr.dim1
        assert newim.shape[1] == self.thisptr.dim2

        
        cdef int np = 0
        cdef float *p = NULL

        self.thisptr.find_maxima( &(newim[0,0])  , iim, procs, myrank, attheend , np,p)

        cdef  float[:,:]  pos_py

        if np:
            pos_py  = numpy.zeros(  [np,3] , dtype=numpy.float32)	
            memcpy( &(pos_py[0,0]), p, np*3*sizeof(float)   ) 
            free( p )
            return pos_py
        else:
            pos_py   = numpy.zeros(  [0,3] , dtype=numpy.float32)	
            return pos_py
            
        
cdef extern from "fillPlanVolume.h":
   void fillPlanNearestC(
       int npointsX ,
       int npointsY  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float*  Xaxis ,
       float* Yaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )
   void fillPlanSincC(
       int npointsX ,
       int npointsY  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float*  Xaxis ,
       float* Yaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )
   void fillPlanSinc_xynC(
       int npointsX ,
       int npointsY  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float*  Xaxis ,
       float* Yaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )
   
   void fillVolumeNearestC(
       int npointsX ,
       int npointsY  ,
       int npointsZ  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float* Xaxis ,
       float* Yaxis  ,
       float* Zaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* prim_x ,
       float* prim_y ,
       float* prim_n,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )
   void fillVolumeSincC(
       int npointsX ,
       int npointsY  ,
       int npointsZ  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float* Xaxis ,
       float* Yaxis  ,
       float* Zaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* prim_x ,
       float* prim_y ,
       float* prim_n,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )
   void fillVolumeSinc_xynC(
       int npointsX ,
       int npointsY  ,
       int npointsZ  ,
       float* result_signal,
       float* result_sum   ,
       float *Origin, 
       float* Xaxis ,
       float* Yaxis  ,
       float* Zaxis  ,
       float dQ      ,
       int dim1      ,
       int dim2      ,
       float*  data  ,
       float* Filter ,
       float* thisQ  ,
       float* prim_x ,
       float* prim_y ,
       float* prim_n,
       float* dual_x ,
       float* dual_y ,
       float* dual_n
    )

cdef extern from "Tensor.hh" :
    cdef cppclass Tensor:
       Tensor() except +
       void setComponents( int , float *)
       void setVars      ( int , float *) except +
       int nvars
       

cdef class Tensor_cy:
    cdef Tensor *thisptr

    def __cinit__(self ):
        self.thisptr = new Tensor()
        
    def __dealloc__(self):
        print(" DEALLOCAZIONE TENSOR ")
        del self.thisptr

        
    def setTensorComponents( self,
                        ndarray[numpy.float32_t, ndim = 2]  vettori_componenti   ):
        assert   vettori_componenti.flags["C_CONTIGUOUS"]
        assert  vettori_componenti.shape[1] == 36
        cdef int nvars = vettori_componenti.shape[0]
        self.thisptr.setComponents(nvars , &(vettori_componenti[0,0]) ) 
        
    def setTensorVars( self,
                        ndarray[numpy.float32_t, ndim = 1]  tvars   )  :
        assert   tvars.flags["C_CONTIGUOUS"]
        assert   tvars.shape[0] == self.thisptr.nvars
        cdef int nvars = tvars.shape[0]
        self.thisptr.setVars(nvars , &(tvars[0]) ) 
        

        

cdef extern from "Geo.hh" :
    cdef cppclass Geo:

       int orientation_codes
       float lmbda
       float dist
       float pixel_size
       float det_origin_X
       float det_origin_Y
       float angular_step
       float beam_tilt_angle
       float d1
       float d2
       float alpha
       float beta
       float kappa
       float omega
       float omega_offset
       float start_phi
       float theta
       float theta_offset
       float pol_degree
       float pol_x
       float pol_y
       float pol_z
       
       float AA
       float BB
       float CC
       float aAA
       float aBB
       float aCC
       int Quat_or_angles
       float r1
       float r2
       float r3
       
       float qmin
       float qmax	     

       Geo() except +
       void precalculate()
       

cdef class Geo_cy:
    cdef Geo *thisptr
    
    def __dealloc__(self):
        del self.thisptr    


    def precalculate(self):
        self.thisptr.precalculate()
    
    def __cinit__(self ):
        self.thisptr = new Geo()

        
#__PIPPO_MACRO_BEGIN
for tipo, prop in [["int"  , "orientation_codes"],["float", "lmbda"],["float", "dist"],["float", "pixel_size"],["float", "det_origin_X"],["float", "det_origin_Y"],["float", "angular_step"],["float", "beam_tilt_angle"],["float", "d1"],["float", "d2"],["float", "alpha"],["float", "beta"],["float", "kappa"],["float", "omega"],["float", "omega_offset"],["float", "start_phi"],["float", "theta"],["float", "theta_offset"],["float", "pol_degree"],["float", "pol_x"],["float", "pol_y"],["float", "pol_z"]]:
    print ("""
    property {prop}:
        def __get__(self):
           return self.thisptr. {prop}
        def  __set__(self,   {tipo}  {prop} ):
           self.thisptr. {prop} = <{tipo}> {prop}
    """.format(tipo=tipo, prop=prop))
#__PIPPO_MACRO_END


#__PIPPO_MACRO_BEGIN
for tipo, prop in [["float",'qmin'], ["float",'qmax']]:
    print ("""
    property {prop}:
        def __get__(self):
           return self.thisptr. {prop}
        def  __set__(self,   {tipo}  {prop} ):
           self.thisptr. {prop} = <{tipo}> {prop}
    """.format(tipo=tipo, prop=prop))


#__PIPPO_MACRO_END



#__PIPPO_MACRO_BEGIN
for tipo, prop in [["float",'AA'],['float', 'BB'],['float', 'CC'],['float', 'aAA'],['float', 'aBB'],['float', 'aCC'],['str', 'Quat_or_angles'],['float', 'r1'],['float', 'r2'],['float','r3']]:
    if prop != 'Quat_or_angles' : 
        print ("""
    property {prop}:
        def __get__(self):
            return self.thisptr. {prop}
        def  __set__(self, {tipo}  {prop} ):
            self.thisptr. {prop} = <{tipo}> {prop}
        """.format(tipo=tipo, prop=prop))
    else:
        print ("""
    property {prop}:
        def __get__(self):
            if self.thisptr. {prop} == 0:
                 return "quats"
            else:
                 return "angles"
        def  __set__(self,  {prop} ):
            if {prop} == "angles":
               self.thisptr. {prop} = 1
            else:
               self.thisptr. {prop} = 0
        """.format( prop=prop))
        
#__PIPPO_MACRO_END
       
cdef extern from "Blob.hh" :
    cdef cppclass Blob:
        Blob(int *,  int, int, int , int, int, int, float*, Geo*, int, int, float * ) except +
        void setTensor( Tensor * )
        double simulate(float, float, int  )   except +
        void  getDataForOld(int &x, float *& , float * &, float *& , float *& ) 

        void clear_simulation()
        void stickIntensity( float *, float * ) 
        void  getQfromCenter(int ,int ,int , float *) 
        void merge(Blob * other) nogil
        
        #    hkl     Gi   poy  pox   dz   dy   dx    harvest
        float *braggOffset
        float *sim_intensity
        int    npointsTot
        
        int Gi ;
        int posy;
        int posx ;

        int dims[3]

	

        float corr;
        float DcorrDy;
        float DcorrDx;
        
        float Wcorr;
        float DWcorrDn;
        float DWcorrDy;
        float DWcorrDx;

        float DWcorrDnn;
        float DWcorrDyy;
        float DWcorrDxx;
        float DWcorrDny;
        float DWcorrDyx;
        float DWcorrDnx;




        Blob * otherBlob ; 

        
cdef extern from "spotpicker.h" :
    void clonaC(   int * hkls_start ,
                   float *target,
                   int iim ,
                   int * interp_prostart,
                   float *interp_compacted_data,
                   float *data )


    void window_maximumC(float *target,float * source,
                         int dire, int mindist, int dz, int dy, int dx)
    
        


    void get_Q0C(int dim1, int dim2,
                 double * Q0,
                 double dist,
                 double det_origin_X,
                 double det_origin_Y,
                 double pixel_size,
                 double*  det_mat,
                 double * p0,
                 double lmbda,
                 double*  MD
	     ) 

    cdef cppclass SpotPicker:
        SpotPicker(int, int, float, int, int*, float) except +
        void harvest(int , int,  float *, float *,   float *,
                     float *, float *,  float * ,
                     float  , float ,
                     int ,  int *, float, int,float,
                     float*, float*, float*, int, int, float,
                     float *, float *, int 
        ) 



        void harvestCube(int ,
                         float *,
                         float *,
                         float * ) 

        void  compatta(int all_alone)
        int interpolatedSpot(int , int , int , int , float *, float *, float * , float, int )
        void getDataToFit(int *, int *, int ** , int **, float **,  float ** , float**, int** , int,float **, int **, float ** )
 
        int H
        int H2p1
        int MT
        
        vector[int]   np4hkl
        vector[float]   alone4hkl
        vector[float]   centering4hkl
        vector[float] compacted_data
        vector[int]   hkl_start


        vector[int]    interp_prostart 
        vector[float]  interp_compacted_data



        float Qmax
        float qcube
        int do_elastic 

        int * AC_displ 


        void ac_export(  void * ) 
        void ac_import(  void * , int) 


        # void interpolatedSpot( h,  k,  l,  dimVol,  *axis,  *corner, * Volume)




def clona(   ndarray[numpy.int32_t, ndim = 1]  hkls_start ,
             ndarray[numpy.float32_t, ndim = 1] target,
             int iim ,
             ndarray[numpy.int32_t, ndim = 1] interp_prostart,
             ndarray[numpy.float32_t, ndim = 1] interp_compacted_data,
             ndarray[numpy.float32_t, ndim = 2] data ):

    assert   hkls_start.flags["C_CONTIGUOUS"]
    assert   target.flags["C_CONTIGUOUS"]
    assert   interp_prostart.flags["C_CONTIGUOUS"]
    assert   interp_compacted_data.flags["C_CONTIGUOUS"]
    assert   data.flags["C_CONTIGUOUS"]
    
    clonaC(<int*> & hkls_start[0],<float*> & target[0], iim, <int*> & interp_prostart[0],
           <float*> & interp_compacted_data[0],   <float*> & data[0,0])
    


cdef class Blob_cy:
    cdef Blob *thisptr
        
    def __dealloc__(self):
        del self.thisptr
    def merge2T( self, Blob_cy other_blob_cy):
        
        assert isinstance( other_blob_cy,  Blob_cy   )
        
        self.thisptr.merge(  other_blob_cy.thisptr  )


    def showDW(self):
        print " w000 " , self.thisptr.Wcorr, "     " , self.thisptr.otherBlob.Wcorr
    def __cinit__(self,
                  ndarray[numpy.int32_t, ndim = 1] hkl,
                  int Gi ,
                  int py,
                  int px,
                  ndarray[numpy.float32_t, ndim = 3] harvest,
                  Geo_cy   mygeo,
                  int centering,
                  ndarray[numpy.float32_t, ndim = 1] exclusions
    ):
        cdef int dimz,dimy, dimx
        dimz = harvest.shape[0]
        dimy = harvest.shape[1] 
        dimx = harvest.shape[2]

        assert   harvest.flags["C_CONTIGUOUS"]
        assert   hkl.flags["C_CONTIGUOUS"]
        assert   hkl.shape[0] == 3


        cdef int *Phkl = <int*>  &(hkl[0])


        cdef int NeXp = exclusions.shape[0]
        assert( NeXp%4) ==0

        self.thisptr = new Blob( Phkl ,
                                 Gi,
                                 py,
                                 px,
                                 dimz,
                                 dimy,
                                 dimx,
                                 &(harvest[0,0,0]),
                                 mygeo.thisptr,
                                 centering,
                                 NeXp,
                                 &(exclusions[0])
        )

        


        
#__PIPPO_MACRO_BEGIN
for tipo , prop in      [[ "int", "Gi"],["int", "posy"],["int", "posx"]]:
    print ("""
    property {prop}:
        def __get__(self):
           return self.thisptr. {prop}
    """.format(tipo=tipo, prop=prop))
#__PIPPO_MACRO_END

        
    def  setTensor(self, Tensor_cy tens ) :
        self.thisptr.setTensor( tens.thisptr)

    def clearSimulation(self ) :
        self.thisptr.clear_simulation( )
        
    def simulate(self, float temp, int rare=1):
        cdef double res
        res = 	   self.thisptr.simulate(temp, 0.0 , rare)
        return res
    
    def getDataForOld(self):

        cdef int Npt
        cdef float *hkl
        cdef float *data
        cdef float *qs
        cdef float *facts
        

        self.thisptr.getDataForOld(Npt, hkl, data, qs,  facts)

        cdef  float[:] hkls_py   = numpy.zeros(  [3] , dtype=numpy.float32)
        cdef  float[:] data_py   = numpy.zeros(  [self.thisptr.npointsTot  ] , dtype=numpy.float32)
        cdef  float[:] qs_py     = numpy.zeros(  [self.thisptr.npointsTot*3] , dtype=numpy.float32)
        cdef  float[:] facts_py  = numpy.zeros(  [self.thisptr.npointsTot*2] , dtype=numpy.float32)

        memcpy( &(hkls_py [0]), hkl , 3*sizeof(float)   )
        memcpy( &(data_py [0]), data , self.thisptr.npointsTot*sizeof(float)   )
        memcpy( &(qs_py   [0]), qs   , self.thisptr.npointsTot*3*sizeof(float)   )
        memcpy( &(facts_py[0]), facts, self.thisptr.npointsTot*2*sizeof(float)   )

        return Npt,hkls_py, data_py, qs_py, facts_py


    def simulate2T(self, float temp, float second_temp, int rare=1):
        cdef double res
        res = 	   self.thisptr.simulate(temp, second_temp, rare)
        return res


    def getQfromCenter(self, int n ,int iy ,int ix) :
        cdef float QQ[3]
        self.thisptr.getQfromCenter(n ,iy ,ix , QQ) 
        return numpy.array( [ QQ[0],QQ[1],QQ[2]    ],"f")
        
    
    property braggOffset:
        def __get__(self):
           cdef float * p 
           if self.thisptr.braggOffset == NULL:
               return None
           else:
               p = self.thisptr.braggOffset
               return numpy.array( [  p[0], p[1], p[2]    ],"f")
           
    property simIntensity:
        def __get__(self):
           cdef float * p 

           cdef int *Dim = self.thisptr.dims              
           cdef  float[:,:,:] simInt_py = numpy.zeros(  [Dim[0], Dim[1], Dim[2]] , dtype=numpy.float32)
           cdef  float[:,:,:] simRef_py = numpy.zeros(  [Dim[0], Dim[1], Dim[2]] , dtype=numpy.float32)


           if self.thisptr.sim_intensity == NULL:
               return None
           else:
               p = self.thisptr.sim_intensity

               self.thisptr.stickIntensity( &(simInt_py[0,0,0]), &(simRef_py[0,0,0]) ) 
               
               
               return simInt_py, simRef_py


    property  dim0:
        def __get__(self):
            return self.thisptr. dims[0]
        def  __set__(self,   int   val ):
            self.thisptr. dims[0] = <int> val
            
    property  dim1:
        def __get__(self):
            return self.thisptr. dims[1]
        def  __set__(self,   int   val ):
            self.thisptr. dims[1] = <int> val
            
    property  dim2:
        def __get__(self):
            return self.thisptr. dims[2]
        def  __set__(self,   int   val ):
            self.thisptr. dims[2] = <int> val
            

#__PIPPO_MACRO_BEGIN
for tipo , prop in      [["float","corr"],["float","DcorrDy"],["float","DcorrDx"],["float","Wcorr"],["float","DWcorrDn"],["float","DWcorrDy"],["float","DWcorrDx"],
                         ["float","DWcorrDnn"],["float","DWcorrDyy"],["float","DWcorrDxx"],
                         ["float","DWcorrDny"],["float","DWcorrDyx"],["float","DWcorrDnx"]
]:
    print ("""
    property {prop}:
        def __get__(self):
           return self.thisptr. {prop}
        def  __set__(self,   {tipo}  {prop} ):
           self.thisptr. {prop} = <{tipo}> {prop}
    """.format(tipo=tipo, prop=prop))
#__PIPPO_MACRO_END
        

cdef class PySpotPicker:
    cdef SpotPicker *thisptr    
  
    def __cinit__(self, int Hmax, float  qmax, int do_elastic=1, float qcube=0,
                  ndarray[numpy.int32_t, ndim = 1] avoid=numpy.array([],"i")  ):
        cdef int * pavoid
        cdef int navoid  = avoid.shape[0]
        if navoid:
            pavoid        = <int*> &avoid[0]
        else:
            pavoid        = <int*> NULL

        self.thisptr = new SpotPicker(Hmax,do_elastic, qcube , navoid, pavoid, qmax)


        
    def __dealloc__(self):
        del self.thisptr

#	            spotpicker.harvest(Qfin, cellvectors, data, Filter  ) 


    def setMT(self):
        self.thisptr.MT=1

    def harvest(self,
                ndarray[numpy.float32_t, ndim = 3] Qfin,
                ndarray[numpy.float32_t, ndim = 1] Kin_fin,
                ndarray[numpy.float32_t, ndim = 2] cellvectors ,
                ndarray[numpy.float32_t, ndim = 2] brills ,
                ndarray[numpy.float32_t, ndim = 2] data,
                ndarray[numpy.float32_t, ndim = 2] Filter,
                float qmin,
                float qmax,
                ndarray[numpy.int32_t, ndim = 2] toharvest,
                float phi, int double_exclusion, float powder_width,
                ndarray[numpy.float32_t, ndim = 3] Qfin_delta_O,
                ndarray[numpy.float32_t, ndim = 3] Qfin_phi_O,
                ndarray[numpy.float32_t, ndim = 2] cellvectors_delta_O,
                int nimage, int ntot_images, float threshold,
                ndarray[numpy.float32_t, ndim = 1] polgeo_correction_O =  numpy.array([],"f") ,  ## passed as a 1D array for simplicity but it has the Qfin shape
                ndarray[numpy.float32_t, ndim = 1] centering4hkl_O =  numpy.array([],"f") ,
                ndarray[numpy.float32_t, ndim = 1] eXp_O =  numpy.array([1,1,1,-10],"f") 
                ):

        cdef  int dim0 =  Qfin.shape[0]
        cdef  int dim1 =  Qfin.shape[1]
        cdef  int nharv  =  toharvest.shape[0]
        cdef  int *tohar
        cdef  float *  Qfin_delta
        cdef  float *  Qfin_phi
        cdef  float *  cellvectors_delta
        cdef  float *  centering4hkl_ptr
        cdef  float *  eXp_ptr
        cdef  int       NeXp;
        cdef  float *  polgeo_correction_ptr
        if toharvest.shape[1]>0:
            tohar = <int*> &toharvest[0,0]
        else:
            tohar = NULL
            nharv=0

        if polgeo_correction_O.shape[0]>0:
            polgeo_correction_ptr = <float*> &polgeo_correction_O[0]
        else:
            polgeo_correction_ptr = NULL
            nharv=0

        if centering4hkl_O.shape[0]>0:
            centering4hkl_ptr  = <float*> &centering4hkl_O[0]
        else:
            centering4hkl_ptr = NULL

        eXp_ptr  = <float*> &(eXp_O[0])
        NeXp     = eXp_O.shape[0]
        NeXp     = NeXp/4
        
        if Qfin_delta_O.shape[2]>0:
             Qfin_delta        = <float*> &Qfin_delta_O[0,0,0]
             Qfin_phi          = <float*> &Qfin_phi_O[0,0,0]
             cellvectors_delta = <float*> &cellvectors_delta_O[0,0]
        else:
             Qfin_delta        = NULL
             Qfin_phi          = NULL
             cellvectors_delta = NULL

        assert   Qfin.flags["C_CONTIGUOUS"]
        assert   Kin_fin.flags["C_CONTIGUOUS"]
        assert   data.flags["C_CONTIGUOUS"]
        assert   Filter.flags["C_CONTIGUOUS"]
        assert   cellvectors.flags["C_CONTIGUOUS"]
        # print " E qui con " , eXp_O		 
        self.thisptr.harvest( dim0*dim1, dim1,   &Qfin[0,0,0] ,&Kin_fin[0],
                              &cellvectors[0,0], 
                              &brills[0,0], 
                              &data[0,0], &Filter[0,0],     qmin, qmax ,
                              nharv, tohar , phi , double_exclusion, powder_width,
                              Qfin_delta, Qfin_phi, cellvectors_delta,
                              nimage, ntot_images, threshold,
                              centering4hkl_ptr,
                              eXp_ptr,
                              NeXp
                              # AGGIUNGI QUA POLGEO_CORRECTION_PTR
        )


    def harvestCube(self,
                    ndarray[numpy.float32_t, ndim = 3] Qfin,
                    ndarray[numpy.float32_t, ndim = 2] data,
                    ndarray[numpy.float32_t, ndim = 2] Filter,
                    ):

        cdef  int dim0 =  Qfin.shape[0]
        cdef  int dim1 =  Qfin.shape[1]
        
        assert   Qfin.flags["C_CONTIGUOUS"]
        assert   data.flags["C_CONTIGUOUS"]
        assert   Filter.flags["C_CONTIGUOUS"]
        self.thisptr.harvestCube( dim0*dim1,    &Qfin[0,0,0] ,
                                  &data[0,0], &Filter[0,0] )
        
    def harvestFromCollection(self,
                ndarray[numpy.float32_t, ndim = 2] Qfin,
                ndarray[numpy.float32_t, ndim = 1] Kin_fin,
                ndarray[numpy.float32_t, ndim = 2] cellvectors ,
                ndarray[numpy.float32_t, ndim = 2] brills ,
                ndarray[numpy.float32_t, ndim = 1] data,
                float qmin,
                float qmax,
                ):


        cdef  int dim0 =  Qfin.shape[0]
        cdef  int dim1 =  Qfin.shape[1]
        
        assert(dim1==3) 

        assert   Qfin.flags["C_CONTIGUOUS"]
        assert   Kin_fin.flags["C_CONTIGUOUS"]
        assert   data.flags["C_CONTIGUOUS"]
        assert   cellvectors.flags["C_CONTIGUOUS"]

        self.thisptr.harvest( dim0,0,     &Qfin[0,0] ,  &Kin_fin[0] ,
                              &cellvectors[0,0], 
                              &brills[0,0], 
                              &data[0], NULL ,     qmin, qmax, 1, <int*> NULL,0 , 0,0.0,
                              NULL, NULL, NULL, 0, 0,
			      1.0e30, NULL, NULL,0
                        )

    def compatta(self, int all_alone):
        self.thisptr.compatta(all_alone)


    #  hkls,Npts,DQs, Datas  = spotpicker.getDataToFit(tofit ) 
    def getDataToFit(self,
                     ndarray[numpy.int32_t, ndim = 2] tofit,
		     int nred
                     ):

        assert   tofit.flags["C_CONTIGUOUS"]
        cdef int nspots
        cdef int *Npts
        cdef int *hkls
        cdef float *Datas
        cdef float  *DQs
        cdef int *tofits_spots

        cdef float *ac_phis
        cdef int   *ac_ijs

        cdef float * tuttiPhi
        cdef int *tuttiIJ
        cdef float *tuttiDist
        
        nspots =  tofit.shape[0]


        if  tofit.shape[1]:
            tofits_spots = <int*> &(tofit[0,0])
        else:
            tofits_spots = NULL

        print ( " NSPOTS ", nspots)

        self.thisptr.getDataToFit(&nspots, tofits_spots , &hkls , &Npts, &DQs, &Datas, &ac_phis, &ac_ijs , nred, &tuttiPhi,  &tuttiIJ, &tuttiDist)

        print ( " NSPOTS ", nspots)
        
        cdef int N,i
        N=0
        for i in range(nspots):
            print Npts[i]
            N=N+Npts[i]


        print " N " , N
        cdef  int[:,:] hkls_py = numpy.zeros(  [nspots,3] , dtype=numpy.int32)
        cdef  int[:] Npts_py = numpy.zeros(  [nspots] , dtype=numpy.int32)	
        cdef  float[:,:]  DQs_py   = numpy.zeros(  [N,3] , dtype=numpy.float32)	
        cdef  float[:]  Datas_py = numpy.zeros(  [N] , dtype=numpy.float32)	

        cdef  int[:]    ac_ijs_py  = numpy.zeros(  [nspots] , dtype=numpy.int32)	
        cdef  float[:]  ac_phis_py = numpy.zeros(  [nspots] , dtype=numpy.float32)	

        cdef  float[:]  tuttiPhi_py = numpy.zeros(  [N] , dtype=numpy.float32)	
        cdef  int[:]  tuttiIJ_py = numpy.zeros(  [N] , dtype=numpy.int32)	
        cdef  float[:]  tuttiDist_py = numpy.zeros(  [N] , dtype=numpy.float32)	

        memcpy( &(hkls_py[0,0]), hkls, nspots*3*sizeof(int)   ) 
        memcpy( &(Npts_py[0]), Npts , nspots*sizeof(int)   ) 
 
        memcpy( &(ac_phis_py[0]), ac_phis , nspots*sizeof(float)   ) 
        memcpy( &(ac_ijs_py[0]),  ac_ijs, nspots*sizeof(int)   ) 

        memcpy( &(DQs_py[0,0]), DQs, N*3*sizeof(float)   ) 
        memcpy( &(Datas_py[0]), Datas, N*sizeof(float)   ) 

        memcpy( &(tuttiPhi_py[0])   ,tuttiPhi   , N*sizeof(float)   ) 
        memcpy( &(tuttiIJ_py[0])    ,tuttiIJ    , N*sizeof(int)   ) 
        memcpy( &(tuttiDist_py[0])  ,tuttiDist  , N*sizeof(float)   ) 


        
        free (hkls ) ;
        free (Npts ) ;
        free (DQs ) ;
        free (Datas ) ;
        free(ac_phis );
        free(ac_ijs);

        free( tuttiPhi   )
        free( tuttiIJ    )
        free( tuttiDist  )

      
        
        return hkls_py, Npts_py, DQs_py, Datas_py, ac_phis_py, ac_ijs_py, tuttiPhi_py, tuttiIJ_py, tuttiDist_py


    def saveHarvestToFile(self, filename, cellvectors,Q0, correction):


        
        
# cdef  numpy.ndarray[numpy.int32_t, ndim=1] 
        cdef  int[:] hkls_start = numpy.zeros(  [ self.thisptr.hkl_start.size()    ] , dtype=numpy.int32)
        cdef  int[:] np4hkl = numpy.zeros(  [self.thisptr.np4hkl.size()] , dtype=numpy.int32)	
        cdef  float[:] alone4hkl = numpy.zeros(  [self.thisptr.np4hkl.size()] , dtype=numpy.float32)	
        cdef  float[:]  compacted_data   = numpy.zeros(  [self.thisptr.compacted_data.size()] , dtype=numpy.float32)	
        cdef  float[:]  interp_compacted_data   = numpy.zeros(  [self.thisptr.interp_compacted_data.size()] , dtype=numpy.float32)	
        cdef  float[:]  centering4hkl  = numpy.zeros(  [self.thisptr.centering4hkl.size()] , dtype=numpy.float32)	
        cdef  int[:] interp_prostart = numpy.zeros(  [self.thisptr.interp_prostart.size()] , dtype=numpy.int32)	

        memcpy( &(np4hkl[0]), &(self.thisptr.np4hkl[0]) , self.thisptr.np4hkl.size() *sizeof(int)   )
        memcpy( &(alone4hkl[0]), &(self.thisptr.alone4hkl[0]) , self.thisptr.np4hkl.size() *sizeof(float)   )



        # print " COMPACTED DATA ",  self.thisptr.compacted_data.size()

        memcpy( &(compacted_data[0]), &(self.thisptr.compacted_data[0]) , self.thisptr.compacted_data.size() *sizeof(float)   )
 
        memcpy( &(centering4hkl[0]), &(self.thisptr.centering4hkl[0]) , self.thisptr.centering4hkl.size() *sizeof(float)   )
        
        if self.thisptr.interp_compacted_data.size():
            memcpy( &(interp_compacted_data[0]),&(self.thisptr.interp_compacted_data[0]),self.thisptr.interp_compacted_data.size() *sizeof(float) ) 
            memcpy( &(interp_prostart[0]),&(self.thisptr.interp_prostart[0]),self.thisptr.interp_prostart.size() *sizeof(int))

        memcpy( &(hkls_start[0]), &(self.thisptr.hkl_start[0]) , self.thisptr.hkl_start.size() *sizeof(int)   ) 

        dtypepyob = numpy.dtype({
                'phi'  :( numpy.float32 , 0                        ),
                'merit':( numpy.float32 , self.thisptr.AC_displ[1] ),
                'ij'   :( numpy.int32   ,  self.thisptr.AC_displ[2])
                })

        pyob = numpy.zeros([self.thisptr.np4hkl.size(),],dtype= dtypepyob )
        cdef AC_Struct [:] ac_view  =     pyob
        if self.thisptr.do_elastic:
            self.thisptr.ac_export(  &(ac_view[0].phi) ) 
            
            
        data = numpy.array(np4hkl )
        print " MAX " , data.max()
        print data.sum()

        h5=h5py.File( filename , "w")
        if self.thisptr.interp_compacted_data.size():
            if("interp" not in h5):
                h5 .create_group("interp")
            h5["interp"].create_dataset("interp_prostart",shape=(self.thisptr.interp_prostart.size(),), dtype=numpy.int32,data=interp_prostart)
            h5["interp"].create_dataset("interp_compacted_data",shape=(self.thisptr.interp_compacted_data.size(),),dtype=numpy.float32,data=interp_compacted_data)
            


        if("harvest" not in h5):
            h5 .create_group("harvest")

        h5["harvest"].create_dataset("hkls_start" , shape=(self.thisptr.hkl_start.size(),),  dtype=numpy.int32, data=hkls_start)
        h5["harvest"].create_dataset("np4hkl" , shape=(self.thisptr.np4hkl.size(),),  dtype=numpy.int32, data=np4hkl)
        h5["harvest"].create_dataset("alone4hkl" , shape=(self.thisptr.np4hkl.size(),),  dtype=numpy.float32, data=alone4hkl)

        h5["harvest"].create_dataset("centering4hkl" , shape=(self.thisptr.centering4hkl.size(),),  dtype=numpy.float32, data=centering4hkl)
        
        h5["harvest"].create_dataset("compacted_data" , shape=(self.thisptr.compacted_data.size(),),  dtype=numpy.float32, data=compacted_data)

        printf("  QMAX %e \n", self.thisptr.Qmax);

        h5["harvest/H" ] =  self.thisptr.H
        h5["harvest/H2p1" ] =  self.thisptr.H2p1
        h5["harvest/Qmax" ] =  self.thisptr.Qmax
        h5["harvest/qcube" ] =  self.thisptr.qcube
        h5["harvest/cellvectors" ] =  cellvectors
        h5["harvest/Q0" ] =  Q0
        h5["harvest/correction" ] =  correction
        if self.thisptr.do_elastic:
            h5["harvest"].create_dataset("AC_data" , shape=pyob.shape ,  dtype=dtypepyob, data=pyob)
            # h5["harvest/AC_data" ] =  pyob # ac_view
        h5.close()
        h5 = None

    def getQmax(self):
        return self.thisptr.Qmax

    def loadHarvestFromFile(self, filename):
        
        cdef AC_Struct [:] ac_view

        h5=h5py.File( filename , "r")

        cdef  int[:] hkls_start = h5["harvest/hkls_start"][:]
        cdef  int[:] np4hkl = h5["harvest/np4hkl" ][:]
        cdef  float[:] compacted_data = h5["harvest/compacted_data" ][:]
        cdef int npqs , i, k;
        self.thisptr.H    = <int> h5["harvest/H" ]  .value
        self.thisptr.H2p1 = <int> h5["harvest/H2p1" ].value
        self.thisptr.Qmax = h5["harvest/Qmax" ].value
        self.thisptr.qcube= h5["harvest/qcube" ].value
        if self.thisptr.do_elastic:
            if "AC_data" in h5["harvest"].keys():
                ac_view = h5["harvest/AC_data" ][:]
                self.thisptr.ac_import(&(ac_view[0].phi), np4hkl.size )
            else:
               self.thisptr.ac_import(NULL, np4hkl.size ) 

 
        self.thisptr.hkl_start.resize( hkls_start.size)
        self.thisptr.np4hkl.resize( np4hkl.size)
        npqs = hkls_start[ (<int> (hkls_start.size))-1  ]
        
        # self.thisptr.compacted_data.resize( compacted_data.size)
        self.thisptr.compacted_data.resize( 7*npqs )

        memcpy( &(self.thisptr.hkl_start[0]) ,   &(hkls_start[0]), self.thisptr.hkl_start.size() *sizeof(int)   ) 
        memcpy( &(self.thisptr.np4hkl[0])    ,   &(np4hkl[0]), self.thisptr.np4hkl.size() *sizeof(int)   )


        if npqs*7 != compacted_data.size:
            for i in range(npqs):
                for k in range(4):
                    self.thisptr.compacted_data[ i*7+k] = compacted_data[ i*4+k ]
                for k in range(4,7):
                    self.thisptr.compacted_data[ i*7+k] = 0
        else:
            memcpy( &(self.thisptr.compacted_data[0]) ,   &(compacted_data[0]), self.thisptr.compacted_data.size() *sizeof(float)   )
        

        Q0  =  numpy.array(h5["harvest/Q0" ],"f")

        h5.close()
        h5 = None

        # print hkls_start.size
        # print np4hkl.size
        # print compacted_data.size
        # print numpy.array(hkls_start)[-1]
        # print numpy.array(np4hkl).sum()
        # print numpy.array(compacted_data)[:10]
        # np4hkl = numpy.array(np4hkl)
        # l = [ (  np4hkl[i],i  )  for i in range(len(np4hkl))   ]
        # l.sort()
        # H = self.thisptr.H
        # for tok,i in l[-10:]:
        #     print tok,i
        #     print i% (2*H+1)-H,  (i/ (2*H+1))%(2*H+1)-H, ((i/ (2*H+1))/(2*H+1)  )-H
        # print self.thisptr.H
        
        return Q0



    def  interpolatedSpot(self,
                          int h,int k,int l, int dim, 
#                          ndarray[numpy.float32_t, ndim = 1] Qop, 
                          ndarray[numpy.float32_t, ndim = 2] rot ,
                          float beta, int Niter):


        cdef float qmax 
        if self.thisptr.do_elastic:
            qmax = self.thisptr.Qmax/math.sqrt(3.0)
        else:
            qmax = self.thisptr.qcube/2

        cdef  numpy.ndarray[numpy.float32_t, ndim=3] Volume = numpy.zeros(  [dim,dim,dim] , dtype=numpy.float32)	
        cdef  numpy.ndarray[numpy.float32_t, ndim=2] axis   = numpy.zeros(  [3,3] , dtype=numpy.float32)	
        cdef  numpy.ndarray[numpy.float32_t, ndim=1] corner = numpy.zeros(  [3] , dtype=numpy.float32)	

        corner[0] =  -qmax
        corner[1] =  -qmax
        corner[2] =  -qmax

        axis  [2,2] =  1.0 /(2.0*qmax/dim)  
        axis  [1,1] =  1.0 /(2.0*qmax/dim)
        axis  [0,0] =  1.0 /(2.0*qmax/dim)
        
        corner[:] = numpy.dot( rot, corner ) ### + Qop
        axis  [:] = numpy.tensordot(axis  , rot, axes = [[1],[1]] ) 
        
        cdef int nps
        print " chiamo interpolated " 
        nps = self.thisptr.interpolatedSpot( h,  k,  l,  dim,  &axis[0,0],  &corner[0], &Volume[0,0,0], beta, Niter)

        if nps:
            return Volume, corner, numpy.linalg.inv(axis)
        else:
            return None,None,None
   
    
def fillPlan(    ndarray[numpy.float32_t, ndim = 2] result_signal,
                       ndarray[numpy.float32_t, ndim = 2] result_sum,
                       ndarray[numpy.float32_t, ndim = 1] Origin,
                       ndarray[numpy.float32_t, ndim = 1] Xaxis,
                       ndarray[numpy.float32_t, ndim = 1] Yaxis,
                       float  dQ,
                       ndarray[numpy.float32_t, ndim = 2] data,
                       ndarray[numpy.float32_t, ndim = 2] Filter,
                       ndarray[numpy.float32_t, ndim = 3]  thisQ,
                       ndarray[numpy.float32_t, ndim = 3] dual_x,
                       ndarray[numpy.float32_t, ndim = 3] dual_y,
                       ndarray[numpy.float32_t, ndim = 3] dual_n ,
                       method  ) :
    for tok in [result_signal, result_sum, Origin, Xaxis,Yaxis, data, Filter, thisQ, dual_x, dual_y, dual_n  ]:
        assert   tok.flags["C_CONTIGUOUS"]
    for tok in [dual_x, dual_y, dual_n]:
        for i in range(3):
            assert thisQ.shape[i] == tok.shape[i]

 
    assert data.shape[0] == Filter.shape[0]
    assert data.shape[1] == Filter.shape[1]
    
    assert result_signal.shape[0] == result_sum.shape[0]
    assert result_signal.shape[1] == result_sum.shape[1]
    cdef int dim1,dim2
    dim1 = data.shape[0]
    dim2 = data.shape[1]
    assert thisQ.shape[2]==3
    assert thisQ.shape[0]==dim1
    assert thisQ.shape[1]==dim2
    assert Xaxis.shape[0]==Yaxis.shape[0]
    assert Origin.shape[0]==Yaxis.shape[0]
    assert Xaxis.shape[0]==3
    cdef npointsX = result_signal.shape[1]
    cdef npointsY = result_signal.shape[0]

    if method=="nearest":

        fillPlanNearestC(
            npointsX,
            npointsY,
            <float*> & result_signal[0,0],
            <float*> & result_sum   [0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
        
    if method=="sinc":
        fillPlanSincC(
            npointsX,
            npointsY,
            <float*> & result_signal[0,0],
            <float*> & result_sum   [0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
    if method=="sinc_xyn":
        fillPlanSinc_xynC(
            npointsX,
            npointsY,
            <float*> & result_signal[0,0],
            <float*> & result_sum   [0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
    
def fillVolume(	   ndarray[numpy.float32_t, ndim = 3] result_signal,
                   ndarray[numpy.float32_t, ndim = 3] result_sum,
                   ndarray[numpy.float32_t, ndim = 1] Origin,
                   ndarray[numpy.float32_t, ndim = 1] Xaxis,
                   ndarray[numpy.float32_t, ndim = 1] Yaxis,
                   ndarray[numpy.float32_t, ndim = 1] Zaxis,
                   float  dQ,
                   ndarray[numpy.float32_t, ndim = 2] data,
                   ndarray[numpy.float32_t, ndim = 2] Filter,
                   ndarray[numpy.float32_t, ndim = 3]  thisQ,
                   ndarray[numpy.float32_t, ndim = 3] prim_x,
                   ndarray[numpy.float32_t, ndim = 3] prim_y,
                   ndarray[numpy.float32_t, ndim = 3] prim_n,
                   ndarray[numpy.float32_t, ndim = 3] dual_x,
                   ndarray[numpy.float32_t, ndim = 3] dual_y,
                   ndarray[numpy.float32_t, ndim = 3] dual_n,
                   method
                   ) :
    
    for tok in [result_signal, result_sum, Origin, Xaxis,Yaxis,Zaxis, data, Filter, thisQ, prim_x, prim_y, prim_n , dual_x, dual_y, dual_n  ]:
        assert   tok.flags["C_CONTIGUOUS"]
    for tok in [prim_x, prim_y, prim_n,dual_x, dual_y, dual_n]:
        for i in range(3):
             assert thisQ.shape[i] == tok.shape[i]
    assert data.shape[0] == Filter.shape[0]
    assert data.shape[1] == Filter.shape[1]
    
    assert result_signal.shape[0] == result_sum.shape[0]
    assert result_signal.shape[1] == result_sum.shape[1]
    assert result_signal.shape[2] == result_sum.shape[2]

    cdef int dim1,dim2
    dim1 = data.shape[0]
    dim2 = data.shape[1]
    assert thisQ.shape[2]==3
    assert thisQ.shape[0]==dim1
    assert thisQ.shape[1]==dim2
    
    assert Xaxis.shape[0]==Yaxis.shape[0]
    assert Origin.shape[0]==Yaxis.shape[0]
    assert Origin.shape[0]==Zaxis.shape[0]
    assert Xaxis.shape[0]==3
    
    cdef npointsX = result_signal.shape[2]
    cdef npointsY = result_signal.shape[1]
    cdef npointsZ = result_signal.shape[0]


    if method=="nearest":


        fillVolumeNearestC(
            npointsX,
            npointsY,
            npointsZ,
            <float*> & result_signal[0,0,0],
            <float*> & result_sum   [0,0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            <float*> & Zaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & prim_x [0,0,0],
            <float*> & prim_y [0,0,0],
            <float*> & prim_n [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
    
    if method=="sinc":

        fillVolumeSincC(
            npointsX,
            npointsY,
            npointsZ,
            <float*> & result_signal[0,0,0],
            <float*> & result_sum   [0,0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            <float*> & Zaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & prim_x [0,0,0],
            <float*> & prim_y [0,0,0],
            <float*> & prim_n [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
    if method=="sinc_xyn":

        fillVolumeSinc_xynC(
            npointsX,
            npointsY,
            npointsZ,
            <float*> & result_signal[0,0,0],
            <float*> & result_sum   [0,0,0],
            <float*> & Origin [0],
            <float*> & Xaxis [0],
            <float*> & Yaxis [0],
            <float*> & Zaxis [0],
            dQ,
            dim1,
            dim2,
            <float*> & data   [0,0],
            <float*> & Filter [0,0],
            <float*> & thisQ [0,0,0],
            <float*> & prim_x [0,0,0],
            <float*> & prim_y [0,0,0],
            <float*> & prim_n [0,0,0],
            <float*> & dual_x [0,0,0],
            <float*> & dual_y [0,0,0],
            <float*> & dual_n [0,0,0]
        )
    
    
    

def get_Q0( ndarray[numpy.float64_t, ndim = 3]  Q0 ,
            double dist,
            double det_origin_X  ,
            double det_origin_Y,
            double pixel_size  ,
            ndarray[numpy.float64_t, ndim = 2] det_mat,
            ndarray[numpy.float64_t, ndim = 1]  p0,
            double lmbda ,
            ndarray[numpy.float64_t, ndim = 2] MD ):
    
    assert   Q0.flags["C_CONTIGUOUS"]
    assert   det_mat.flags["C_CONTIGUOUS"]
    assert   p0.flags["C_CONTIGUOUS"]
    assert   MD.flags["C_CONTIGUOUS"]

    cdef int dim1,dim2
    dim1 = Q0.shape[0]
    dim2 = Q0.shape[1]
    assert(  Q0.shape[2]==3 )

    assert(  det_mat.shape[0]==3 )
    assert(  det_mat.shape[1]==3 )
    
    assert(  p0.shape[0]==3 )
    assert(  MD.shape[0]==2 )
    assert(  MD.shape[1]==2 )
    
    get_Q0C(dim1,dim2,
            <double*> & (Q0[0,0,0]),
            dist, det_origin_X, det_origin_Y, pixel_size,
            <double*> & (det_mat[0,0]),
            <double*> & p0[0],
            lmbda,
            <double*> & MD[0,0],
        )
    

        
def window_maximum(   ndarray[numpy.float32_t, ndim = 3]  target ,
                      ndarray[numpy.float32_t, ndim = 3]  source ,
                      int dire,
                      int mindist):

    assert   source.flags["C_CONTIGUOUS"]
    assert   target.flags["C_CONTIGUOUS"]
    cdef int dz,dy,dx
    dz = target.shape[0]
    dy = target.shape[1]
    dx = target.shape[2]
    window_maximumC(<float*> & target[0,0,0],<float*> & source[0,0,0],
                    dire, mindist, dz,dy,dx
    )
        
        
