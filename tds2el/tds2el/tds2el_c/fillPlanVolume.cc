#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

#define OVERSAMPLING 200

static float sinc(float x) {
  if (x>M_PI) return 0.0;
  return (x == 0.0f) ? 1.0 : sin(M_PI * x)/(M_PI * x);
}

static float hamming_window(float i, float length) {
  return (0.54f - 0.46f * cos(M_PI+ 2*M_PI*((float)i/(float)(2*length) ) ) );
}

static float *get_sinc_sampling()
{
  int length = 5*OVERSAMPLING ; 
  float *sampling = (float *)malloc(length * sizeof(float));

  float step = 1.0/OVERSAMPLING ; 
  
  float value = 0;

  for (int i = 0; i < length; ++i, value += step) {
    sampling[i] = sinc(value) * hamming_window(i, (int) (    roundf(         M_PI*  OVERSAMPLING   )  ) );
  }

  return sampling;
}


int    checkIfCrosses( float * thisQ,int  dim1, int dim2   , float * Origin, float *Xaxis, float *Yaxis, int npointsX, int npointsY) {
  return 1;
  
  float Zaxis[3];
  for(int i=0; i<3; i++) {
    Zaxis[i] = Xaxis[(i+1)%3] *Yaxis[(i+2)%3] - Xaxis[(i+2)%3] *Yaxis[(i+1)%3];
  }
  float sides[4];
  {
    for(int j=0; j<4; j++) {
      sides[j] = 0 ;
      
      for(int k=0; k<3; k++) {
	sides[j] +=(   thisQ[ ((   (j/2)*(dim1-1)   )*dim2 + (  j%2   )*(dim2-1))*3 + k ]           - Origin[k]   ) *Zaxis[k];
      }
    }
  }
  {
    int count0=0; 
    int countP=0; 
    int countM=0; 
    for(int j=0; j<4; j++) {
      if(sides[j]==0) count0++;
      if(sides[j] >0) countP++;
      if(sides[j] <0) countM++;
    }
    printf(" %d %d %d \n", count0 , countP , countM); 
    if(count0 || ( countP && countM  ) || ( countM && countP) ) return 1;
  }
  for(int j=0; j<4; j++) {
    printf(" j %d \n", j );
    float dist0 =0;
    float dist =0;

    int pos[2];
    pos[0] = (  j%2   )*(dim1-1) ;
    pos[1] =   (j/2)*(dim2-1)   ; 
    int d0 = 1-2*(j%2)   ;
    int d1 = 1 - 2*(j/2);
    
    for(int k=0; k<3; k++) {
      dist0  +=   (thisQ[  (  pos[0]*dim2 + pos[1] )*3 + k ]           - Origin[k] )*Zaxis[k]      ;
    }
    int segno=1;
    if (dist0<0) {
      segno=-1;
      dist0 =  -dist0;
    }
    dist = dist0; 
    while(1)  {
      
      printf(" dist %e \n", dist);
      
      // avanza lungo d0 d1
      float V10 =0;
      float V01 =0;
      {
	int np  = pos[0] + d0;
	if(  np<0 || np>= dim1) {
	  V10 = INFINITY;
	} else {
	  for(int k=0; k<3; k++) {
	    V10  +=   (thisQ[  (  np*dim2 + pos[1] )*3 + k ]           - Origin[k] )     *Zaxis[k] *segno;
	  }
	}
      }
      {
	int np  = pos[1] + d1;
	if(  np<0 || np>= dim2) {
	  V01 = INFINITY;
	} else {
	  for(int k=0; k<3; k++) {
	    V01  +=   (thisQ[  (  pos[0]*dim2 + np )*3 + k ]  - Origin[k] )  * Zaxis[k] * segno ;
	  } 
	}
      }
      if ( V01<=0 || V10<=0) {
	return 1;
      }
      if( V01<V10) {
	if( V01< dist  )  {
	  dist = V01 ;
	  pos[1] += d1; 
	} else {
	   break;
	}
      } else  {
	if( V10< dist  )  {
	  dist = V10 ;
	  pos[0] += d0; 
	} else {
	  break;
	}
      }
    }
  }
  return 0;
}


void fillPlanC(
	       int npointsX ,
	       int npointsY  ,
	       float* result_signal,
	       float* result_sum   ,
	       float*  Origin ,
	       float*  Xaxis ,
	       float* Yaxis  ,
	       float dQ      ,
	       int dim1      ,
	       int dim2      ,
	       float*  data  ,
	       float* Filter ,
	       float* thisQ  ,
	       float* dual_x ,
	       float* dual_y ,
	       float* dual_n
	       ) {


  int itcrosses2D  =  checkIfCrosses(  thisQ, dim2, dim2   ,  Origin, Xaxis, Yaxis, npointsX, npointsY);
  if(!itcrosses2D) return;



  
  float Qpix[3] ;
  float deri[3][2] ;
  float * duals[3] = { dual_x,  dual_y,  dual_n     };
  float d_dist[3];

  static float *sinc_sampling = NULL;
  if(sinc_sampling==NULL) {
    sinc_sampling = get_sinc_sampling() ;
  }

#pragma omp parallel for  private(Qpix , deri,   d_dist ) 
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {

      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }


      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      for(int i=0; i<3; i++) {
	d_dist[i] = 0;
	for(int k=0; k<3; k++ ) {
	  d_dist[i] +=   Qpix[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	}
	deri[i][0] = 0.0;
	deri[i][1] = 0.0;
	for(int k=0; k<3; k++ ) {
	  deri[i][0]  +=   Yaxis[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri[i][1]  +=   Xaxis[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	}
      }
      float low[2], high[2];
      for(int idir=0; idir<2; idir++) {
	low[idir] = NAN ; 
	high[idir] = NAN ; 
      }
      for(int idir=0; idir<2; idir++) {
	
	for(int ieqA=0; ieqA<3; ieqA++) {
	  for(int ieqB=0; ieqB<3; ieqB++) {
	    if (ieqB==ieqA) continue;
	    if( deri[ieqB][(idir+1)%2] !=0.0 ) {
	      float fact  = deri[ieqA][ (idir+1)%2 ] / deri[ieqB][ (idir+1)%2 ] ;
	      float LOW ;
	      float HIGH;
	      if (fact>0) {
		LOW  = (-d_dist[ieqA]-1.0) - (-d_dist[ieqB]+1.0) *  fact ; 
		HIGH = (-d_dist[ieqA]+1.0) - (-d_dist[ieqB]-1.0) *  fact ;
	      } else {
		LOW  = (-d_dist[ieqA]-1.0) - (-d_dist[ieqB]-1.0) *  fact ; 
		HIGH = (-d_dist[ieqA]+1.0) - (-d_dist[ieqB]+1.0) *  fact ;
	      }
	      
	      float coeff =  deri[ieqA][ idir  ]  - deri[ieqB][ idir  ]*  fact ;
	      
	      if(isnan(low[idir]) ) {
		if(coeff>0) {
		  low [idir] = LOW /coeff;
		  high[idir] = HIGH/coeff;
		} else {
		  low [idir] = HIGH /coeff;
		  high[idir] = LOW  /coeff;
		}
	      } else {
		if(coeff>0) {
		  low [idir] = max(  LOW /coeff  , low [idir]  ) ;
		  high[idir] = min(  HIGH/coeff  , high[idir]  ) ;
		} else {
		  low [idir] =  max( HIGH /coeff,  low [idir]  );
		  high[idir] =  min( LOW  /coeff,   high[idir]);
		}
	      }
	    }
	  }
	}
      }

      
      int ly = max(  (int) ceil( low[0]/dQ  ) , 0) ;
      int lx = max(  (int) ceil( low[1]/dQ  ) , 0) ;
      int hy = min( (int) floor( high[0]/dQ  ),  npointsY-1)  ;
      int hx = min( (int) floor( high[1]/dQ  ),  npointsX-1)  ;
      
      float coo;
      float prod;

      // if( ly<=hy  &&   lx<=hx   )  {
      // 	printf( " spalmo %d %d ly , hy , lx , hx  %d %d %d %d    \n" ,ypix, xpix ,      ly , hy , lx , hx        );
      // }
      
      for (int iy=ly; iy<=hy; iy++) {
	for (int ix=lx; ix<=hx; ix++) {

	    prod=1.0;
	    for(int ico=0; ico<3; ico++) {
	      coo = d_dist[ico] + deri[ico][0]*dQ*iy        +   deri[ico][1]*dQ*ix ;
	      printf(" 1\n");
	      int i =  (int) roundf( fabs(coo)*OVERSAMPLING )  ;
	      printf(" 2 i %d   coo %e     OVERSAMPLING %d      \n", i, coo, OVERSAMPLING);
	      assert(      i  < 4*OVERSAMPLING ) ;
	      printf(" 3    %p   %d\n",    sinc_sampling , i    );
	      prod = prod * sinc_sampling[ i ] ;
	      printf(" 4\n");
	    }
	    // printf(" aggiungo \n");
	    result_signal[  iy*npointsX + ix ]  +=   prod * data[  ypix * dim2 +   xpix  ] ;
	    result_sum    [  iy*npointsX + ix ]  +=   prod   ;
	    // printf(" aggiungo OK \n");
	  
	}
      }
      
      if( ly<=hy  &&   lx<=hx   )  {
	printf( " spalmo %d %d   OK \n" ,ypix, xpix );
      }
      
    }
  }
};






void fillPlanNearestC(
		     int npointsX ,
		     int npointsY  ,
		     float* result_signal,
		     float* result_sum   ,
		     float*  Origin ,
		     float*  Xaxis ,
		     float* Yaxis  ,
		     float dQ      ,
		     int dim1      ,
		     int dim2      ,
		     float*  data  ,
		     float* Filter ,
		     float* thisQ  ,
		     float* dual_x ,
		     float* dual_y ,
		     float* dual_n
		     ) {



  int itcrosses2D  =  checkIfCrosses(  thisQ, dim2, dim2   ,  Origin, Xaxis, Yaxis, npointsX, npointsY);
  if(!itcrosses2D) return;

  
  float Qpix[3] ;

#pragma omp parallel for  private(Qpix ) 
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {

      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }


      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      
      float near_x = 0;
      float near_y = 0;
      
      for(int i=0; i<3; i++) {
	near_y +=   Yaxis[i]* Qpix[i]    ; 
	near_x +=   Xaxis[i]* Qpix[i]    ; 
      }


      near_y = max(near_y,0);
      near_y = min(near_y,(npointsY-1)*dQ );
      near_x = max(near_x,0);
      near_x = min(near_x,(npointsX-1)*dQ );

      
      
      float tmp, tmp2=0;
      
      for(int i=0; i<3; i++) {
	tmp  =  Qpix[i] - (  Yaxis[i]* near_y + Xaxis[i] * near_x )   ;
	
	tmp2 += tmp*tmp ;
	
      }

      if ( tmp2<dQ*dQ) {
	
	int iy = (int) roundf(  near_y/dQ     );
	int ix = (int) roundf(  near_x/dQ     );

	
	result_signal[  iy*npointsX + ix ]  +=    data[  ypix * dim2 +   xpix  ] ;
	result_sum    [  iy*npointsX + ix ]  +=   1.0   ;
	
      }
    }
  }
};

void fillPlanSincC(
		     int npointsX ,
		     int npointsY  ,
		     float* result_signal,
		     float* result_sum   ,
		     float*  Origin ,
		     float*  Xaxis ,
		     float* Yaxis  ,
		     float dQ      ,
		     int dim1      ,
		     int dim2      ,
		     float*  data  ,
		     float* Filter ,
		     float* thisQ  ,
		     float* dual_x ,
		     float* dual_y ,
		     float* dual_n
		     ) {



  int itcrosses2D  =  checkIfCrosses(  thisQ, dim2, dim2   ,  Origin, Xaxis, Yaxis, npointsX, npointsY);
  if(!itcrosses2D) return;


  
  float Qpix[3] ;

  // static float *sinc_sampling = NULL;
  // if(sinc_sampling==NULL) {
  //   sinc_sampling = get_sinc_sampling() ;
  // }

  int active = 0;
 
#pragma omp parallel for  private(Qpix ) 
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {

      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }


      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      
      float near_x = 0;
      float near_y = 0;
      
      for(int i=0; i<3; i++) {
	near_y +=   Yaxis[i]* Qpix[i]    ; 
	near_x +=   Xaxis[i]* Qpix[i]    ; 
      }


      near_y = max(near_y,0);
      near_y = min(near_y,(npointsY-1)*dQ );
      near_x = max(near_x,0);
      near_x = min(near_x,(npointsX-1)*dQ );

      
      
      int fuori = 0;
      float tmp;
      
      for(int i=0; i<3; i++) {
	tmp  =  Qpix[i] - (  Yaxis[i]* near_y + Xaxis[i] * near_x )   ;
	if( fabs(tmp) > dQ ) fuori = 1;
      }


  
      
      if( fuori) continue;

      
      int ly = max(  (int) ceil( near_y/dQ  - 1.0) , 0) ;
      int lx = max(  (int) ceil( near_x/dQ  - 1.0) , 0) ;
      int hy = min( (int) floor( near_y/dQ  + 1.0),  npointsY-1)  ;
      int hx = min( (int) floor( near_x/dQ  + 1.0),  npointsX-1)  ;

      // printf(  "  ly,hy,lx,hx  %d %d %d %d  \n",           ly,hy,lx,hx         ) ; 
      
      float prod;


      
      for (int iy=ly; iy<=hy; iy++) {
	for (int ix=lx; ix<=hx; ix++) {
	  
	  
	  
	  fuori = 0;
	  prod=1.0;
	  for(int i=0; i<3; i++) {
	    tmp  =  Qpix[i] - (  Yaxis[i]* iy + Xaxis[i] * ix ) *dQ  ;
	    float f = 1.0-fabs(tmp)/dQ ;
	    if (f<0) f=0;
	    prod = prod * f ;
	    }
	  if ( prod) {
	    result_signal[  iy*npointsX + ix ]  +=   prod * data[  ypix * dim2 +   xpix  ] ;
	    result_sum    [  iy*npointsX + ix ]  +=   prod   ;
	    active = 1 ; 
	  }
	}
      }
      
    }
  }
  if( active) {
    // printf(" active\n");
  }
};
void fillPlanSinc_xynC(
		     int npointsX ,
		     int npointsY  ,
		     float* result_signal,
		     float* result_sum   ,
		     float*  Origin ,
		     float*  Xaxis ,
		     float* Yaxis  ,
		     float dQ      ,
		     int dim1      ,
		     int dim2      ,
		     float*  data  ,
		     float* Filter ,
		     float* thisQ  ,
		     float* dual_x ,
		     float* dual_y ,
		     float* dual_n
		     ) {


  int itcrosses2D  =  checkIfCrosses(  thisQ, dim2, dim2   ,  Origin, Xaxis, Yaxis, npointsX, npointsY);
  if(!itcrosses2D) return;


  float Qpix[3] ;

  // static float *sinc_sampling = NULL;
  // if(sinc_sampling==NULL) {
  //   sinc_sampling = get_sinc_sampling() ;
  // }

 

  float deri[3][2] ;
  
  float * duals[3] = { dual_x,  dual_y,  dual_n     };
  
  float d_dist[3];

#pragma omp parallel for  private(Qpix ,deri, d_dist )   
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {


      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }


      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }

      for(int i=0; i<3; i++) {
	d_dist[i] = 0;
	for(int k=0; k<3; k++ ) {
	  d_dist[i] +=   Qpix[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	}
	deri[i][0] = 0.0;
	deri[i][1] = 0.0;
	for(int k=0; k<3; k++ ) {
	  deri[i][0]  +=   Yaxis[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri[i][1]  +=   Xaxis[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	}
      }

      float low[2], high[2];
      for(int idir=0; idir<2; idir++) {
	low[idir] = NAN ; 
	high[idir] = NAN ; 
      }     
            
      for(int idir=0; idir<2; idir++) {
	
	for(int ieqA=0; ieqA<3; ieqA++) {
	  for(int ieqB=0; ieqB<3; ieqB++) {
	    if (ieqB==ieqA) continue;
	    if( deri[ieqB][(idir+1)%2] !=0.0 ) {
	      float fact  = deri[ieqA][ (idir+1)%2 ] / deri[ieqB][ (idir+1)%2 ] ;
	      float LOW ;
	      float HIGH;
	      if (fact>0) {
		LOW  = (d_dist[ieqA]-1.0) - (d_dist[ieqB]+1.0) *  fact ; 
		HIGH = (d_dist[ieqA]+1.0) - (d_dist[ieqB]-1.0) *  fact ;
	      } else {
		LOW  = (d_dist[ieqA]-1.0) - (d_dist[ieqB]-1.0) *  fact ; 
		HIGH = (d_dist[ieqA]+1.0) - (d_dist[ieqB]+1.0) *  fact ;
	      }
	      
	      float coeff =  deri[ieqA][ idir  ]  - deri[ieqB][ idir  ]*  fact ;
	      
	      if(isnan(low[idir]) ) {
		if(coeff>0) {
		  low [idir] = LOW /coeff;
		  high[idir] = HIGH/coeff;
		} else {
		  low [idir] = HIGH /coeff;
		  high[idir] = LOW  /coeff;
		}
	      } else {
		if(coeff>0) {
		  low [idir] = max(  LOW /coeff  , low [idir]  ) ;
		  high[idir] = min(  HIGH/coeff  , high[idir]  ) ;
		} else {
		  low [idir] =  max( HIGH /coeff,  low [idir]  );
		  high[idir] =  min( LOW  /coeff,   high[idir]);
		}
	      }
	    }
	  }
	}
      }
      
      int ly = max(  (int) ceil( low[0]/dQ  ) , 0) ;
      int lx = max(  (int) ceil( low[1]/dQ  ) , 0) ;
      int hy = min( (int) floor( high[0]/dQ  ),  npointsY-1)  ;
      int hx = min( (int) floor( high[1]/dQ  ),  npointsX-1)  ;
      
      float coo;
      float prod;
      
      if( ly<=hy  &&   lx<=hx   )  {
	
	float near_x = 0;
	float near_y = 0;
	
	for(int i=0; i<3; i++) {
	  near_y +=   Yaxis[i]* Qpix[i]    ; 
	  near_x +=   Xaxis[i]* Qpix[i]    ; 
	}
       	// printf( " spalmo %d %d ly , hy , lx , hx  %d %d %d %d    \n" ,ypix, xpix ,      ly , hy , lx , hx        );
	// printf(" Centro sarebbe %e %e \n" ,  near_y/dQ,   near_x /dQ ) ; 
      }
      
      for (int iy=ly; iy<=hy; iy++) {
	for (int ix=lx; ix<=hx; ix++) {
	  prod=1.0;
	  for(int ico=0; ico<3; ico++) {
	    coo = fabs(-d_dist[ico] + deri[ico][0]*dQ*iy        +   deri[ico][1]*dQ*ix );
	    float f;
	    f = ( 1.0-coo);
	    if(f<0) f=0;
	    prod = prod * f ; 
	    // printf(" 1\n");
	    // int i =  (int) roundf( fabs(coo)*OVERSAMPLING )  ;
	    // printf(" 2 i %d   coo %e     OVERSAMPLING %d      \n", i, coo, OVERSAMPLING);
	    /// assert(      i  < 4*OVERSAMPLING ) ;
	    /// printf(" 3    %p   %d\n",    sinc_sampling , i    );
	    /// prod = prod * sinc_sampling[ i ] ;
	    /// printf(" 4\n");
	  }
	  // printf(" aggiungo \n");
	  result_signal[  iy*npointsX + ix ]  +=   prod * data[  ypix * dim2 +   xpix  ] ;
	  result_sum    [  iy*npointsX + ix ]  +=   prod   ;
	  // printf(" aggiungo OK \n");
	}
      }
      
      // if( ly<=hy  &&   lx<=hx   )  {
      // 	printf( " spalmo %d %d   OK \n" ,ypix, xpix );
      // }
      
    }
  }
}



void fillVolumeNearestC(
		 int npointsX ,
		 int npointsY  ,
		 int npointsZ  ,
		 float* result_signal,
		 float* result_sum   ,
		 float*  Origin ,
		 float*  Xaxis ,
		 float* Yaxis  ,
		 float* Zaxis  ,
		 float dQ      ,
		 int dim1      ,
		 int dim2      ,
		 float*  data  ,
		 float* Filter ,
		 float* thisQ  ,
		 float* prim_x ,
		 float* prim_y ,
		 float* prim_n,
		 float* dual_x ,
		 float* dual_y ,
		 float* dual_n
		 ) {
  
  
  float Qpix[3] ;

  
  float d_dist[3];
  int dims[] = {  npointsZ,   npointsY,     npointsX    } ;
  
  //    #pragma omp parallel for schedule(dynamic) reduction(+:aa,ab,ad,bb,bd,dd) private(M,myq, myqO)
  
#pragma omp parallel for  private(Qpix , d_dist) 
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {



      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }

      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      for(int i=0; i<3; i++) {
	d_dist[i] = 0;
      }
      
      for(int k=0; k<3; k++ ) {
	d_dist[0] +=   Qpix[k]*Zaxis[k]  ; 
	d_dist[1] +=   Qpix[k]*Yaxis[k]  ; 
	d_dist[2] +=   Qpix[k]*Xaxis[k]  ; 
      }
      for(int i=0; i<3; i++) {
	d_dist[i] = max( d_dist[i] , 0 ) ;
	d_dist[i] = min( d_dist[i] , (dims[i]-1)*dQ ) ;
      }
      float tmp2=0.0;
      for(int k=0; k<3; k++) {
	float tmp =  Qpix[k] - d_dist[0]*Zaxis[k]  - d_dist[1]*Yaxis[k]  - d_dist[2]*Xaxis[k] ;
	tmp2 += tmp*tmp ; 
      }
      if(tmp2<dQ*dQ) {
	
	int iz = (int) roundf(  d_dist[0]/dQ     );
	int iy = (int) roundf(  d_dist[1]/dQ     );
	int ix = (int) roundf(  d_dist[2]/dQ     );

	
	result_signal[ (iz*dims[1]+ iy)*dims[2] + ix ]  +=    data[  ypix * dim2 +   xpix  ] ;
	result_sum    [ (iz*dims[1]+ iy)*dims[2] + ix ]  +=   1.0   ;
	
      }
    }
  }
  
};


void fillVolumeSinc_xynC(
		 int npointsX ,
		 int npointsY  ,
		 int npointsZ  ,
		 float* result_signal,
		 float* result_sum   ,
		 float*  Origin ,
		 float*  Xaxis ,
		 float* Yaxis  ,
		 float* Zaxis  ,
		 float dQ      ,
		 int dim1      ,
		 int dim2      ,
		 float*  data  ,
		 float* Filter ,
		 float* thisQ  ,
		 float* prim_x ,
		 float* prim_y ,
		 float* prim_n,
		 float* dual_x ,
		 float* dual_y ,
		 float* dual_n
		 ) {
  float Qpix[3] ;
  float deri[3][3] ;
  float deri_dual[3][3] ;
  
  float * prims[3] = { prim_x,  prim_y,  prim_n     };
  float * duals[3] = { prim_x,  prim_y,  prim_n     };
  
  float d_dist[3];
  float d_dist_dual[3];
  int dims[] = {  npointsZ,   npointsY,     npointsX    } ;
  
#pragma omp parallel for  private(Qpix , deri, deri_dual,  d_dist, d_dist_dual  ) 
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {


      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }

      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      
      for(int i=0; i<3; i++) {
	d_dist[i] = 0;
	d_dist_dual[i] = 0;
	for(int k=0; k<3; k++ ) {
	  d_dist_dual[i] +=   Qpix[k]*duals[i][k+ 3*( ypix * dim2  + xpix )]  ; 
	}
      }
      
      for(int k=0; k<3; k++ ) {
	d_dist[0] +=   Qpix[k]*Zaxis[k]  ; 
	d_dist[1] +=   Qpix[k]*Yaxis[k]  ; 
	d_dist[2] +=   Qpix[k]*Xaxis[k]  ; 
      }
      
      for(int i=0; i<3; i++) {
	deri[i][0] = 0.0;
	deri[i][1] = 0.0;
	deri[i][2] = 0.0;
	deri_dual[i][0] = 0.0;
	deri_dual[i][1] = 0.0;
	deri_dual[i][2] = 0.0;
	for(int k=0; k<3; k++ ) {
	  
	  deri[i][0]  +=   Zaxis[k]*prims[i ][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri[i][1]  +=   Yaxis[k]*prims[i ][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri[i][2]  +=   Xaxis[k]*prims[i ][k+ 3*( ypix * dim2  + xpix )]  ;
	  
	  deri_dual[i][0]  +=   Zaxis[k]*duals[i ][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri_dual[i][1]  +=   Yaxis[k]*duals[i ][k+ 3*( ypix * dim2  + xpix )]  ; 
	  deri_dual[i][2]  +=   Xaxis[k]*duals[i ][k+ 3*( ypix * dim2  + xpix )]  ;


	  
	}
      }
      
      float low[3], high[3];
      for(int idir=0; idir<3; idir++) {
	low[idir] = d_dist[idir]; 
	high[idir] = d_dist[idir] ;
	for(int k=0; k<3; k++) {
	  low[idir]  -=  2*fabs(   deri[k][idir]     )   ; 
	  high[idir] +=  2*fabs(   deri[k][idir]     )   ; 
	}
      }
      
      int lz = max(  (int) ceil( low[0]/dQ  ) , 0) ;
      int ly = max(  (int) ceil( low[1]/dQ  ) , 0) ;
      int lx = max(  (int) ceil( low[2]/dQ  ) , 0) ;
      
      int hz = min( (int) floor( high[0]/dQ  ),  npointsZ-1)  ;
      int hy = min( (int) floor( high[1]/dQ  ),  npointsY-1)  ;
      int hx = min( (int) floor( high[2]/dQ  ),  npointsX-1)  ;
      
      float prod;
      
      for (int iz=lz; iz<=hz; iz++) {
	for (int iy=ly; iy<=hy; iy++) {
	  for (int ix=lx; ix<=hx; ix++) {
	    
	    
	    prod=1.0;
	    
	    float coo;
	    for(int ico=0; ico<3; ico++) {
	      
	      coo = fabs(-d_dist_dual[ico] + deri_dual[ico][0]*dQ*iz + deri_dual[ico][1]*dQ*iy        +   deri_dual[ico][2]*dQ*ix );
	      float f;
	      f = ( 1.0-coo);
	      if(f<0) f=0;
	      prod = prod * f ; 
	      
	    }
	    
	    
	    
	    if ( prod) {
	      result_signal[ (iz*dims[1]+ iy)*dims[2] + ix ]   +=   prod * data[  ypix * dim2 +   xpix  ] ;
	      result_sum    [ (iz*dims[1]+ iy)*dims[2] + ix ]  +=   prod   ;
	    }
	    
	  }
	}
      }
    }
  }




  
};


void fillVolumeSincC(
		 int npointsX ,
		 int npointsY  ,
		 int npointsZ  ,
		 float* result_signal,
		 float* result_sum   ,
		 float*  Origin ,
		 float*  Xaxis ,
		 float* Yaxis  ,
		 float* Zaxis  ,
		 float dQ      ,
		 int dim1      ,
		 int dim2      ,
		 float*  data  ,
		 float* Filter ,
		 float* thisQ  ,
		 float* prim_x ,
		 float* prim_y ,
		 float* prim_n,
		 float* dual_x ,
		 float* dual_y ,
		 float* dual_n
		 ) {
    
  float Qpix[3] ;

  float d_dist[3];
  int dims[] = {  npointsZ,   npointsY,     npointsX    } ; 

#pragma omp parallel for  private(Qpix , d_dist )   
  for(int ypix=0; ypix<dim1; ypix++) {
    for(int xpix=0; xpix<dim2; xpix++) {


      float f = data[  ypix * dim2 +   xpix  ]  ; 
      if( isnan(f) || f<=0 ) {
	continue;
      }

      
      if( Filter[  ypix * dim2 +   xpix  ]==0) continue;
      
      for(int i=0; i<3; i++ ) {
	Qpix[i]  =  thisQ[ i + 3*( ypix * dim2  + xpix ) ] - Origin[i]     ;
      }
      
      for(int i=0; i<3; i++) {
	d_dist[i] = 0;
      }
      
      for(int k=0; k<3; k++ ) {
	d_dist[0] +=   Qpix[k]*Zaxis[k]  ; 
	d_dist[1] +=   Qpix[k]*Yaxis[k]  ; 
	d_dist[2] +=   Qpix[k]*Xaxis[k]  ; 
      }

      for(int i=0; i<3; i++) {
	d_dist[i] = max( d_dist[i] , 0 ) ;
	d_dist[i] = min( d_dist[i] , (dims[i]-1)*dQ ) ;
      }
      
      int fuori = 0 ; 
      for(int k=0; k<3; k++) {
	float tmp =  Qpix[k] - d_dist[0]*Zaxis[k]  - d_dist[1]*Yaxis[k]  - d_dist[2]*Xaxis[k] ;
	if( fabs(tmp) > dQ ) fuori = 1;
      }
      
      if( fuori) continue;
      
      int lz = max(  (int) ceil( d_dist[0]/dQ  - 1.0) , 0) ;
      int ly = max(  (int) ceil( d_dist[1]/dQ  - 1.0) , 0) ;
      int lx = max(  (int) ceil( d_dist[2]/dQ  - 1.0) , 0) ;
      
      int hz = min( (int) floor( d_dist[0]/dQ  + 1.0),  dims[0]-1)  ;
      int hy = min( (int) floor( d_dist[1]/dQ  + 1.0),  dims[1]-1)  ;
      int hx = min( (int) floor( d_dist[2]/dQ  + 1.0),  dims[2]-1)  ;
      
      float prod;
      
      for (int iz=lz; iz<=hz; iz++) {
	for (int iy=ly; iy<=hy; iy++) {
	  for (int ix=lx; ix<=hx; ix++) {

	    prod=1.0;
	    for(int i=0; i<3; i++) {
	      float tmp  =  Qpix[i] - ( Zaxis[i]* iz +   Yaxis[i]* iy + Xaxis[i] * ix ) *dQ  ;
	      float f = 1.0-fabs(tmp)/dQ ;
	      if (f<0) f=0;
	      prod = prod * f ;
	    }
	    if ( prod) {
	      result_signal[ (iz*dims[1]+ iy)*dims[2] + ix ]   +=   prod * data[  ypix * dim2 +   xpix  ] ;
	      result_sum    [ (iz*dims[1]+ iy)*dims[2] + ix ]  +=   prod   ;
	    }
	    
	  }
	}
      }
    }
  }
};


