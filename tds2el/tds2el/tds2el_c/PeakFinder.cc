#include"PeakFinder.hh"
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#define max(a,b) (((a)>(b)) ? (a):(b))
#define min(a,b) (((a)<(b)) ? (a):(b))

void PeakFinder::find_maxima(float *newim, int  iim , int procs ,int  myrank , int attheend, int &np, float *&p) {
  
  
  for(int iy0=0; iy0<dim1; iy0++) {
    for(int ix0=0; ix0<dim2; ix0++) {
      int P0 = iy0*dim2+ix0;
      
      if(newim[ P0 ]> threshold ) {
	
	float mySum= newim[P0] ;
	float myN  = iim*mySum ;
	float myY  = iy0*mySum ;
	float myX  = ix0*mySum ;

	for(int iy1 =   max(0, iy0-peak_size) ;     iy1 <=   min(dim1-1, iy0+peak_size)    ; iy1++) {
	  
	  for(int ix1 =   max(0, ix0-peak_size) ;      ix1 <=  min(dim2-1, ix0+peak_size)  ; ix1++) {

	    // if( ix1> min(dim2-1, ix0+peak_size) +20)  {
	    //   np=0;
	    //   return;
	    // }
	    

	    
	    int P1 = iy1*dim2+ix1;
	    
	    if (imsum[ P1   ]>0) {
	      mySum += imsum[P1] ;
	      myN   += imN[P1]   ;
	      myY   += imY[P1]   ;
	      myX   += imX[P1]   ;
	      
	      imsum[P1]  = 0 ;
	      imN[P1]  = 0 ; 
	      imY[P1]  = 0 ; 		
	      imX[P1]  = 0 ; 		
	    }
	  }
	}
	imsum[P0]    =  mySum;
	imN  [P0]    = myN ; 
	imY  [P0]    = myY ; 		
	imX  [P0]    = myX ; 		
      }
    }
  }
  
  np = 0   ;
  p  = NULL;
  for(int iy0=0; iy0<dim1; iy0++) {
    for(int ix0=0; ix0<dim2; ix0++) {
      int P0 = iy0*dim2+ix0;
      if (imsum[P0]>0) {
	if(  (iim - imN[P0]/imsum[P0] )  > peak_size    ||   attheend   ) {
	  np++ ;
	}
      }
    }
  }
  // p = new float [np*3];
  if ( np) {
    p = (float*) malloc(np*3*sizeof(float)); 
    
    np = 0   ;
    
    for(int iy0=0; iy0<dim1; iy0++) {
      for(int ix0=0; ix0<dim2; ix0++) {
	int P0 = iy0*dim2+ix0;
	if (imsum[P0]>0) {
	  if(  (iim - imN[P0]/imsum[P0] )  > peak_size    ||   attheend   ) {
	    p[np*3+0] =  imN[P0]/imsum[P0]  ; 
	    p[np*3+1] =  imY[P0]/imsum[P0]  ; 
	    p[np*3+2] =  imX[P0]/imsum[P0]  ; 
	    np++ ;
	    
	    imsum[P0]    =  0;
	    imN  [P0]    =  0 ; 
	    imY  [P0]    =  0 ; 		
	    imX  [P0]    =  0 ; 		
	  }
	}
      }
    }
  }
};


PeakFinder::PeakFinder(int dim1, int dim2, float threshold, int peak_size) {
  this->dim1 = dim1;
  this->dim2 = dim2;

  this->threshold = threshold ;
  this->peak_size = peak_size ; 

#define CI(a)  a = new float[ dim1*dim2 ] ; \
  for(int i=0; i<dim1*dim2; i++) a[i]= 0 ;

  CI(imsum);
  CI(imN);
  CI(imY);
  CI(imX);
#undef  CI

}

PeakFinder::~PeakFinder() {
  delete []  imsum ; 
  delete []  imN; 
  delete []  imY; 
  delete []  imX; 
}



