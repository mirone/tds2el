# from __future__ import absolute_import
# from __future__ import print_function


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
import sys
if(sys.argv[0][-12:]!="sphinx-build"):
    import spotpicker_cy






import glob
# import fftw3f
import math
import string
from PyMca5.PyMcaIO import EdfFile
import h5py
import pylab
import random 
from . import FitT
from . import minimiser
import numpy
import os
import traceback
import pickle
import numpy as np

import ab2tds.dabax as dabax
from .prettytable import PrettyTable

RAGGRUPPA=1

FLAT=-1
import scipy.optimize
__doc__=(


r"""  .. toctree::
        :maxdepth: 3


running the code            
----------------
     
   tds2el is always runned with two arguments ::

     tds2el  inputfile

   * the inputfile contains driving options. Let's  call this file input.par. here an example ::

            conf_file             =  "3DRSR.conf"    
            filter_file           =  "mask6M"
            images_prefix         =  "images/cco1_1_"
            ascii_generators      =  "3(z),2(110),1~,m(100),m(110),m(010)"     


     * conf_file:  is the name of the file containings all the geometry parameters, and the
       parameters concerning your calculation options. it is your  freshly generated 3DRSR.conf
       to which you will add additional options as explained below for the different cases.

     * filter_file : is the mask : it is one for good pixel and zero for pixel to be discarded.

     * images_prefix : the list of files will be built by the following lines of code (tds2el.py) ::

             flist = glob.glob(params.images_prefix+"*.cbf" )
             flist.sort()
           
       it means that the file ordering is given by the name ordering. This is an important point
       because on this depends the rotation angle given at each file thourgh the incremental step ::

             params.phi += angular_step

parallel harvesting
^^^^^^^^^^^^^^^^^^^
     
    * when an harvesting is done, a huge amount of files needs to be readed. tds2el 
      can be runned in parallel in this case ::

         mpirun -n8 tds2el input.par

      in the case you want to use 8 processors.

    * when the harvesting is already done, and you run fits and interpolations,
      the program does not use mpi.

      It uses instead opencl for certain parts of the calculations but you always run the code 
      simply by ::

          tds2el inputfile



Global Visualisation of Volume
------------------------------

   this is in general the first step to do. it is used to write on a hdf5 file
   a cubic interpolation of the whole volume. Once the volume is written you can explore it 
   with PyMca. An auxiliary script will then allow you to transform the 3D coordinate of a spot
   in the volume, to hkl. This information can be used to select a collection of spots to fit,
   of the hkl of a given spot that you want to zoom.

preparation of the input file for Volume harvesting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

       * you can start with a freshly generated 3DRSR.conf. 
         At the bottom of 3DRSR.conf ( you can also rename it ) you add ::

            DO_ELASTIC=0
            cube_dim=256
            load_from_file= None
            save_to_file  =  "harvestvolume256.h5"

         What do these options stand for ? :

           * DO_ELASTIC=0  disactivate the harvesting of data points around Bragg peaks.
             The harvest will instead be done around the points of the cubic grid.
             Up to    NINTERP=8 data points per cubic point can be recolted.
             The closest ones will be selected. NINTERP is hard coded in spotpicker.cc

           * cube_dim=256  This determines the number of points in each dimension of the cube

           * load_from_file= None  this option always activate harvesting. 
             When this variable is set to a file name, the harvest will be loaded from that file
             for subsequent treatement.

           *  save_to_file  =  "harvestvolume256.h5"  The file the harvest will be written to.

preparation of the input file for Volume interpolation (Visualisation)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 
   * In this case the last section of 3DRSR.conf, will be instead  ::

             DO_ELASTIC=0
             cube_dim=256
             load_from_file= "harvestvolume256.h5"
             save_to_file  =  None
             VINTERP = True
             testSpot_beta = 0.03
             testSpot_Niter = 100  



   .. figure:: images/volinterp.png
     :width:  300
     :height: 300
     :align: right


   *  load_from_file= "harvestvolume256.h5" :
      we use the previously done harvest. Set also  save_to_file to None to avoid overwriting.

   *  VINTERP = True :
      we activate interpolation  for the points of the cubic grid :


   * How interpolation is done :

       we use a formula which works both for the oversampled and under-sampled cases.
       We find the minimum of an object function which minimised a fidelity term, given by 
       the squared norm of the difference between the data  :math:`{bf y}` , and the interpolated data,
       obtained interpolation the solution :math:`{bf x}` given on the cubic grid, plus
       a regularisation term, multiplied by  :math:`\beta`, which is given
       by the squared norm of the gradient.
       The solution :math:`{bf x}` is expressed mathematically as :
            
        .. math:: {\bf x^{*}} = argmin_{\bf x} \left( \frac{1}{2} \left\| Interp({\bf x}) - {\bf y} \right\|^2 + \beta \left\| \nabla {\bf x} \right\|^2 \right)  
       where :math:`\beta` is given by the parameter *testSpot_beta* and the optimisation is done through *testSpot_Niter*
       iteration of FISTA algorithm.  Full convergence is not needed, we are just doing visualisation :
             choosing a low number of iteration allows highlight the data granularity when data are more sparse then interpolation points  

   * How you can look at the obtained result :

       the result can be looked at by opening the hdf5 file. You can use PyMca ::

          pymca -f volinterp_256.h5

       the volume is dataset : */tests/spot_000_0/volume*
       There is an auxiliary script, that you can run ::

             tds2el_spotlocatorXXX volinterp_256.h5

       where XXX is the post-pended tds2version.
       This script will repeatdly ask you NX,NY,NZ, that you can read clicking on a image point with PyMca,
       and will transform them to HKL.




Working in the neighboroods of Bragg peaks
-------------------------------------------

   You can do :

     * fine interpolations around selected bragg peaks

     * optimise elastic constants to fit the neighbourood os selected Bragg peak
        The fit is done on the data. Not on the interpolation

   The preliminary step for both the above operations consists in harvesting from
   the raw data, the vicinity of selected spots

preparation of the input file for harvesting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

       * you can start with a freshly generated 3DRSR.conf. 
         At the bottom of 3DRSR.conf ( you can also rename it ) you add ::


           DeltaQ_max = 0.07
           DeltaQ_min = 0.01
           load_from_file= None
           save_to_file  =  "harvest_spots.h5"
           DO_ELASTIC=1
           collectSpots = [[0,0,-12],[0,1,-8],[2,0,-8],[-4 , 4, 8 ],[-1,0,4],[-1,1,8],[-2,-1,-2],[2,0,-2]  ]

         What do these options stand for ? :

           * DO_ELASTIC=1  activates enquiries concerning   data points around Bragg peaks.
        
           * DeltaQ_max, DeltaQ_min
               these numbers determine which area, around each (selected) peak, is harvested.
               These values are multiplied by the lenght of the longest reciprocal
               lattice vector ( :math:`{\bf a^* ,b^*, c^* }` and thus converted to maximal radius
               and minimal radius. All the data points that fall within these limits are harvested.

           * load_from_file= None  This option activates harvesting

           *  save_to_file  =  "harvest_spots.h5"  The file the harvest will be written to.
   
           * collectSpots =[ points ]  Every point is replicated using simmetry operations.
             harvesting will be done at all the replicated points.
             If  collectSpots =[ [] ]  then ALL the spots will be harvested.


           * REMEMBER that harvesting can be parallel ( see above introduction)

preparation of the input file for around-the-spots interpolation (Visualisation)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 

   .. figure:: images/testspots.png
     :width:  300
     :height: 300
     :align: right

   * In this case the last section of 3DRSR.conf, will be instead  ::


        DO_ELASTIC=1
        load_from_file= "harvest_spots.h5"
        save_to_file  =  None
        testSpot_beta = 0.03
        testSpot_Niter = 100 
        testSpot_N = 128
        testSpot = [-2,-1,-2]
        MaxNtest = 1000


   *  testSpot_beta and testSpot_Niter control the interpolation inversion iterative procedure as 
      explained above.

   * testSpot_N  determines how many points in each dimension the cubic grid will have.
     The cubic grid is made large enough to contain, spot by spot, all the harvested points.

   * testSpot = [-2,-1,-2]  indicates which spot will be analysed : ALL the SIMMETRY REPLICA of this 
     spot will be interpolated : if this is too much you can reduce MaxNtest to go faster

   * How you can look at the obtained result :

       the result can be looked at by opening the hdf5 file. You can use PyMca ::

          pymca -f tests.h5

       There is a data-group per interpolated spots.
  
   * Performance issue :
      there is some part of the iterative routines which can use OMP.
      You can optionally set OMP_NUM_THREADS to bigger than one.


Simulating the Thermal Diffused Scattering from Elastic Constants
-----------------------------------------------------------------

preparation of the input file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


   .. figure:: images/simspot.png
     :width:  300
     :height: 300
     :align: right


   * The preliminary step consists in doing a dry run to discover
     the structure of the elastic tensor.
     At this stage you just specify some spots to be fitted so that
     tds2el  knows that you are going to fit something  ::


          DO_ELASTIC=1
          load_from_file= "harvest_spots.h5"
          save_to_file  =  None
          fitSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]


     tds2el will stop, in this dry run, because we have not yet provided 
     the elastic constants, but before finishing will print on the screen
     the structure of the elastic tensor that it finds by simmetry operations ::
    

                           GET ELASTIC SYMMETRY 
                          1.000000 A11  | 1.000000 A11  | 1.000000 A13  |               | 1.000000 A15  |              
                                        | -2.000000 A66 |               |               |               |              
                          ---------------------------------------------------------------------------------------------
                          1.000000 A11  | 1.000000 A11  | 1.000000 A13  |               | -1.000000 A15 |              
                          -2.000000 A66 |               |               |               |               |              
                          ---------------------------------------------------------------------------------------------
                          1.000000 A13  | 1.000000 A13  | 1.000000 A33  |               |               |              
                                        |               |               |               |               |              
                          ---------------------------------------------------------------------------------------------
                                        |               |               | 1.000000 A44  |               | -1.000000 A15
                                        |               |               |               |               |              
                          ---------------------------------------------------------------------------------------------
                          1.000000 A15  | -1.000000 A15 |               |               | 1.000000 A44  |              
                                        |               |               |               |               |              
                          ---------------------------------------------------------------------------------------------
                                        |               |               | -1.000000 A15 |               | 1.000000 A66 
                                        |               |               |               |               |              
                          
     
     Mind that the obtained structure could be considerably more complicated if the alpha, beta, gamma angle
     wrote to 3DRSR.conf from the XCALIBUR file differ sligthly from the nominal values.
     Correct them in this case, or alternatively change the tolerance which is so far hard-coded in tds2el.py ::

                 if abs(trace_matrici[ i*6+j ])>1.0e-6:

   *  IMPORTANT NOTICE : the alignement convention is the one of GIACOVAZZO where one align :math:`{\bf a^*}`
      instead of :math:`{\bf a}` along the X axis. For this reason some elastic constant may have different idexes than usual :
         A14 may become -A15 and so on.
       

   *  To calculate the simulated diffused scattering you have to enter the values of elastic constants.
      You do this by setting the dictionary CGUESS. Here an example to get a simulation.  ::

             DO_ELASTIC=1
             load_from_file= "harvest_spots.h5"
             save_to_file  =  None
             fitSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]
             shiftW=0.0
             CGUESS={"A11":  [  254.307231832 , -1000  , 10000]  ,
                     "A33":  [ 144.467519525  , -1000  , 10000]         ,
                     "A44":  [ 120.580683913  , -1000  , 10000]         ,
                     "A66":  [ 169.691768154  , -1000  , 10000]         ,
                     "A13":  [ -7.53771283211 , -1000  , 10000]         ,
                     "A15":  [ 46.561752961   , -1000  , 10000]
                     }
             doMinimisation=0
             visSim=1
             testSpot_beta = 0.03
             testSpot_Niter = 100 
             testSpot_N = 128

      * CGUESS is the dictionary with initial values, minimum, maximum

    
 
      * doMinimisation=0  desactivate the optimisation ( the Fit to the data )

      * fitSpots=[.....]   all the reported spots ( plus the simmetry replica if REPLICA==1)
           will be simulated.

      *  shiftW ( if it is not present in the input file, its default value is 0.0)
         is the maximum shifts, in the three directions x,y,z that can be indipendently given to each spot

      * REPLICA=0   if 1 fitSpots will be replicated by simmetry.

      * visSim=1 activates the visualisation ( if you do a simulation it is for looking at it )
            which activates the interpolation around all the fitted spots.
               
      * How you can look at the obtained result ::

               pymca -f simtests.h5

      * PERFORMANCE ISSUE :
          tds2el calculates simulated data at all the data harvested data points(which are replicated by simmetry).
        Then for each one of them, an iterative interpolation is done for the cubic grid. It is this last operation 
        that takes time. Therefore you might :
 
            *  select a not too long list for fitSpots

            * set the OMP_NUM_THREADS


DOING THE FIT
-----------------------------------------------------------------

preparation of the input file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    * The input file is basically the same as for simulating the spots
      but with doMinimisation set to True and, if you dont want to wait for subsequent interpolations,
      visSim=0. The ontained parameters will be printed at the screen once the fit has converged

             DO_ELASTIC=1
             load_from_file= "harvest_spots.h5"
             save_to_file  =  None
             fitSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]
             CGUESS={"A11":  [  254.307231832 , -1000  , 10000]  ,
                     "A33":  [ 144.467519525  , -1000  , 10000]         ,
                     "A44":  [ 120.580683913  , -1000  , 10000]         ,
                     "A66":  [ 169.691768154  , -1000  , 10000]         ,
                     "A13":  [ -7.53771283211 , -1000  , 10000]         ,
                     "A15":  [ 46.561752961   , -1000  , 10000]
                     }

             doMinimisation=1
             visSim=0

    * PERFORMANCE ISSUE : very important

        you are going to do a lot of tests before producing an useful result.
        On the other hand the data amount is huge. TO go really fast
        use, in your 3DRSR file ::

           nred=10

        or higher. One point over nred only will be used. This might eventually accelerate
        also the interpolation because you have less data points, but in this case you 
        will have more coarse granularity and you might need a higher beta.
        If you dont set nred it is nred=1 by default.

    * ANOTHER PERFORMANCE ISSUE :

        The simulation calculation are done with openCL, by default on GPU.
        On a non GPU machine use ::

             devicetype="CPU"

        to use all available cores.


    * Convergenge control :

             ftols is by default=0.00001.
             If you set ftol=0.001, in 3DRSR.conf, convergence condition will be satisfied  sooner.



Fine retuning of angles r1 r2 r3 and cell parameters a,b,c
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    The fit is done not only on the elastic constants ( and a pro-spot factor
    plus baseline ) but also on Alignement parameters  r1 r2 r3 and cell parameters a,b,c.
    ( For more informations about alignement geometry 
    See thesis of Mathias MEYER *Construction of a multi-purpose X-ray CCD detector and its implementation on a 4-circle kappa goniometer*)
    Universit de Lausanne

   As default these Alignement corrections are constrained between 0- and 0+

   Their value is given by the internal A_variables symbol whose default is ::
    
     A_variables = [ minimiser.Variable(0.0,0.0,0.0) for i in range(6) ]  # r1,r2,r3, AA, BB, CC

   You can free the optimisation for these variables by opening their range ::

     A_variables = ( 
   	       [ minimiser.Variable(0.0,-1.0e-1,1.0e-1) for i in range(3) ]
	       +
	       [ minimiser.Variable(0.0,-3.0e-2,3.0e-2) for i in range(3) ]
	      )

   You can add the above lines to the 3DRSR.conf file 
   The optimised value is the correction ( 0 means no correction)
   and the final value is printed at the end of the fit   



"""
)
try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print("MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print("MPI NOT LOADED ")


# import sift
from math import pi, sin, cos
try :
    import fabio
except :
    print('Warning : fabio module could not be initialised')

import numpy as np
import sys, time
__version__ = 0.7


if(sys.argv[0][-12:]!="sphinx-build"):
    from . import spotpicker_cy

def  Raggruppa(  params,  hkls, Npts, DQs, Datas, ac_phis, ac_ijs,
                 tuttiPhi_py, tuttiIJ_py, tuttiDist_py ) :
    res_hkls  =[]
    res_Npts  =[]
    res_DQs   =   DQs
    res_Datas =   Datas
    res_ac_phis = []
    res_ac_ijs  = []
    references = []
    
    start = 0
    for N, (h,k,l)  in zip(Npts, hkls):
        end = start+ N

        view_phis    =    tuttiPhi_py[start:end]
        view_tuttiIJ =    tuttiIJ_py [start:end]
        view_Dist    =    tuttiDist_py [start:end]
        view_DQs     =    DQs    [start:end,:3]
        view_Datas    =    Datas   [start:end]



        ordine = numpy.argsort(view_phis )
        
        view_phis[:]    = view_phis[ordine]
        view_tuttiIJ[:] = view_tuttiIJ[ordine]
        view_Dist[:]    = view_Dist[ordine]
        view_DQs[:]     = view_DQs[ordine]
        view_Datas[:]    = view_Datas[ordine]

        salti = (view_phis[1:]- view_phis[:-1])/params.angular_step
        stacchi = numpy.where(salti>20)[0].tolist()
        stacchi.append( len( view_DQs  )) 

        pos_start = 0
        count     = 1

        for stacco in stacchi:
            res_hkls.append([ h,k,l, count ])
            res_Npts.append( stacco - pos_start    )
            imin = pos_start +  numpy.argmin( view_Dist[ pos_start : stacco ])

            res_ac_phis.append( view_phis [imin]   ) 
            res_ac_ijs .append( view_tuttiIJ[imin]    ) 

            Subview_DQs = view_DQs     [pos_start:  stacco]
            Subview_Datas = view_Datas [pos_start:  stacco]
            
            qdists = numpy.sqrt(  ( Subview_DQs*Subview_DQs).sum(axis = -1)  )
            qmin = qdists.min()
            qmax = qdists.max()

            # qmask = numpy.less(  qdists ,  (9*qmin+qmax)/10.0     ) * numpy.less(0, Subview_Datas) * ( -numpy.isnan(Subview_Datas ))
            # references.append( Subview_Datas[qmask]  )
            
            qmask = numpy.less(  qdists ,  (9*qmin+qmax)/10.0     )   # * numpy.less(0, Subview_Datas) * ( -numpy.isnan(Subview_Datas ))
            tok = Subview_Datas[qmask]
            tok[numpy.isnan(tok)] = numpy.nan
            tok[numpy.less(tok,0)] = numpy.nan
            
            references.append( tok )

            
            pos_start = stacco
            count +=1
            
        start = end

    
    return numpy.array(res_hkls), numpy.array(res_Npts), numpy.array(res_DQs), numpy.array(res_Datas), numpy.array(res_ac_phis), numpy.array(res_ac_ijs),numpy.array(references)
         
#--------------------------------------------------------------------------------------------------------
# Rotation, Projection and Orientation Matrix
#--------------------------------------------------------------------------------------------------------

# e' una rotazione degli assi non delle coordinate
#  va bene cosi perche si ruota il campione quindi il raggio controruota nello spazio K
def Rotation(angle, rotation_axis=0):
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val

#--------------------------------------------------------------------------------------------------------

# questa rispetta la convenzione Ma per le cordinate   Rotation(45.0,"z") != Arb_Rot(45.0,[0,0,1])
def Arb_Rot(angle, rot_axis):
    angle = np.radians(angle)
    assert(len(rot_axis)==3)
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat

#--------------------------------------------------------------------------------------------------------

def Mirror(mirror_axis):
    x,y,z = {"x":[-1,1,1], "y":[1,-1,1], "z":[1,1,-1],0:[-1,1,1], 1:[1,-1,1], 2:[1,1,-1]}[mirror_axis]
    mat = np.array([[x,0,0],[0,y,0],[0,0,z]])
    return mat 

#--------------------------------------------------------------------------------------------------------

def ascii_gen2operator(generator, cell_vects):
    print(generator)
    I = np.identity(3)
    gendict = {  '1':I,
                 '1~':-I,
                 '2(x)':Arb_Rot(180,[1,0,0]),
                 '2(y)':Arb_Rot(180,[0,1,0]),
                 '2(z)':Arb_Rot(180,[0,0,1]),
                 '4(x)':Arb_Rot(90,[1,0,0]),
                 '4(y)':Arb_Rot(90,[0,1,0]),
                 '2(110)':np.array([[0,1,0],[1,0,0],[0,0,-1]]),
                 '3(z)':Arb_Rot(120,[0,0,1]),
                 '3(111)':np.array([[0,1,0],[0,0,1],[1,0,0]]),
                 '4(z)':Arb_Rot(90,[0,0,1]),
                 '4(z)~':-I*Arb_Rot(90,[0,0,1]),
                 'm(x)':Mirror(0),
                 'm(y)':Mirror(1),
                 'm(z)':Mirror(2),
                 'm(100)':  np.array([[-1,1,0],[0,1,0],[0,0,1]])   ,
                 'm(010)':  np.array([[1,0,0],[1,-1,0],[0,0,1]])   ,
                 'm(110)':  np.array([[0,-1,0],[-1,0,0],[0,0,1]])  
          }
    mat = gendict[generator]
    pos = generator.find("(")
    if pos!=-1:
        if "1" in generator[pos:]:
            basis = np.array( [  tok/np.sqrt( (tok*tok).sum())    for tok in cell_vects ]  )
            mat = np.dot( basis.T  ,   np.dot(mat, np.linalg.inv(basis.T)   )  ) 
    np.savetxt(sys.stdout,mat,fmt="%e")
    print(" ") 
    return mat

#--------------------------------------------------------------------------------------------------------

def existing_combination(candidate_op,op_list,sigma):
    for op in op_list:
        absdiff = abs(candidate_op - op)
        if absdiff.sum()<sigma:
            return True
    return False






def Theta(Q, Lambda  ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0
    return numpy.arcsin( dum  )




#--------------------------------------------------------------------------------------------------------

def genlist2oplist(genlist, cellvects, dtype=np.float32):
    base_op_list = [ascii_gen2operator(gen, cellvects ) for gen in genlist]
    I = np.identity(3)
    combined_op_list = [I]
    while 1:
        new_add_in_cycle = False
        for op in combined_op_list:
            for base_op in base_op_list:
                newop = np.dot(op,base_op)
                if not existing_combination(newop,combined_op_list,0.01):
                    combined_op_list.append(newop)
                    new_add_in_cycle = True
        if not new_add_in_cycle:
            break
    return np.array(combined_op_list).astype(dtype)

#--------------------------------------------------------------------------------------------------------

def Prim_Rot_of_RS(omega, phi, kappa, alpha, beta, omega_offset):
    # """ 
    # Primary rotation of reciprocal space. 
    # Omega, kappa, phi are the nominal values of the angle
    # """
    tmp = Rotation(omega + omega_offset, 2)
    tmp = np.dot(tmp, Rotation(alpha, 1))
    tmp = np.dot(tmp, Rotation(kappa, 2))
    tmp = np.dot(tmp, Rotation(-alpha, 1))
    tmp = np.dot(tmp, Rotation(beta, 1))
    tmp = np.dot(tmp, Rotation(phi, 2))
    tmp = np.dot(tmp, Rotation(-beta, 1))
    return tmp

#--------------------------------------------------------------------------------------------------------

def DET(theta, theta_offset, d2, d1):
    # """ 
    # Rotation matrix for the detector 
    # theta is the nominal theta value and d1,D2 are the tilts of detector
    # """
    tmp = Rotation(theta + theta_offset, 2)
    tmp = np.dot(tmp, Rotation(d2, 1))
    tmp = np.dot(tmp, Rotation(d1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------    

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------

def P0(dist, b2):
    # """ Primary projection of pixel coordinates (X,Y) to the reciprocal space. """
    B = Rotation(b2, 1) # Beam tilt matrix
    p0 = np.dot(B, [ dist, 0, 0 ])
    return p0


#--------------------------------------------------------------------------------------------------------
# Parameter Class
#--------------------------------------------------------------------------------------------------------


def read_configuration_file(cfgfn):
	# """
	# cfgfn is the filename of the configuration file.
	# the function return an object containing information from configuration file (cf inside cfg file).
	# """
	
	try:
            s=open(cfgfn,"r")
	except:
		print(" Error reading configuration file " ,  cfgfn)			
		exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
		print("*** print_exception:")
		traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                              limit=None, file=sys.stdout)
		raise Exception
	class Parameters():
            devicetype="GPU"
            VINTERP=False
            testSpot=None
            REPLICA=0
            nred=1
            ftol=0.00001
            threshold=1.0e200
            badradius=0.0
            airscattering=None
            airscattering_debug = 0
            shiftW=0.0
            files_halos =[]
            nred=1
            DO_ELASTIC=1
            collectSpots = None
            fitSpots = True
            exec(s)
	cfg = Parameters()
	s.close()
	return cfg

def mag_max(l):
    # """
    # l is a list of coordinates
    # """
    return max(np.sqrt(np.sum(l * l, axis= -1)))
    
#--------------------------------------------------------------------------------------------------------


def   get_Qfin( Q0,  
               params):

    R = Prim_Rot_of_RS(params.omega, params.phi, params.kappa, params.alpha, params.beta, params.omega_offset)
    U = Snd_Rot_of_RS(params.r1, params.r2, params.r3)
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    return Qfin

############################################################################################
## ASSI CRISTALLOGRAFICI

def modulus(a):
    return np.sqrt((a*a).sum())

def arcCos(   a,b   ):
    c = (a*b).sum()
    c=c/modulus(a)
    c=c/modulus(b)
    return np.arccos(c)
    


# If the number is odd returns -1
# If two numbers are equal retorns zero
def ciclicity(a,b,c):
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1   

def getCellVectors(param, iT ):
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC[iT],np.array([0.0,0.0,  1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-param.aBB[iT],np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 
    print(" ora " , xc) 

    sa = ( math.cos( param.aBB[iT]*math.pi/180 )*math.cos( param.aCC[iT]*math.pi/180 )-math.cos( param.aAA[iT]*math.pi/180 ))
    sa = sa/(math.sin( param.aBB[iT]*math.pi/180 )*math.sin( param.aCC[iT]*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    xa  = xa*param.AA[iT]
    xb  = xb*param.BB[iT]
    xc  = xc*param.CC[iT]
    cellvectors = np.array([ xa,xb,xc ] )

    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  list(map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   ))
 
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( -betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/(math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar

    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume


    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")


def getCellVectors(param ):
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0,  1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 
    print(" ora " , xc) 

    sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
    sa = sa/(math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    xa  = xa*param.AA
    xb  = xb*param.BB
    xc  = xc*param.CC
    cellvectors = np.array([ xa,xb,xc ] )

    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  list(map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   ))
 
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( -betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/(math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar
    # Brillvectors[2,:] *=-1

    
    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume


    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")



def get_elastic_simmetry(all_ops):
    components = [(0,0),(1,1),(2,2),(1,2),(2,0),(0,1)]
    comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
                  ,(2,1):3,(0,2):4,(1,0):5}  
    res={}
    transf = []
    for op in all_ops:
        # np.savetxt(sys.stdout,op,fmt="%e")
        # print ""

        # if   op[2,2]!=1.0     or  abs(np.linalg.det(op)-1)>1.0e-5   :
        #     continue

        # if  abs( op[0,0]-1.0) >1.0e-3  and  abs( op[0,0]+0.5) >1.0e-3:
        #     continue

        mat = np.zeros([6,6],"d")
        for i, (k,l) in enumerate(components):
            dispatch_k = op[:,k]
            dispatch_l = op[:,l]
            for K,cK in enumerate(dispatch_k ) :
                for L,cL in enumerate(dispatch_l ) :
                    fact=1.0
                    if (K!=L):
                        fact=fact/2.0
                    if (k!=l):
                        fact=fact*2.0
                    mat[comppos[(K,L)], i ] += cK*cL * fact
        transf.append(mat)

    ricerca = [(i,i) for i in range(6)]
    for i in range(5):
        for j in range(i+1,6):
            ricerca.append((i,j))

    # NMC=100000
    for (i,j) in ricerca[:]:
        matrici = np.zeros([6,6])
        matrici[i,j]=1
        matrici[j,i]=1
        trace_matrici =  np.zeros([6,6],"d")
        for rot in transf:
            add =  np.tensordot(  matrici , rot,([-2],[-1]))
            add =  np.tensordot(  add , rot,([-2],[-1]))
            trace_matrici[:] +=  add
        trace_matrici = (np.reshape(trace_matrici, [36]))

        
        for vect,I in list(res.values()):
            trace_matrici -= vect*trace_matrici[I]

        if abs(trace_matrici[ i*6+j ])>1.0e-5:
            vect =  trace_matrici/trace_matrici[ i*6+j ]
            I =  i*6+j
            for vv,II in list(res.values()):
                n0 =  np.less(1.0e-5, abs(vv)).sum()
                nuovo  =  vv - vect * vv[I]
                n1 =  np.less(1.0e-5, abs(nuovo)).sum()
                if n1<n0:
                    vv[:]=nuovo
            res[(i,j)]=vect, I


    displays = [[[] for j in range(6)] for i in range(6) ]


    ndisplaylines=0
    maxlen=0
    for vect,II in list(res.values()):
        i,j = II/6 +1,  II%6 +1
        for K,v in enumerate(vect):
            if(abs(v)>1.0e-5):
                k,l = K/6 ,  K%6 
                add = "%f A%d%d"%(v,i,j)  
                displays[k][l].append(    add     )
                # displays[l][k] = displays[k][l]
                ndisplaylines = max(  ndisplaylines, len(  displays[k][l]))
                maxlen = max( maxlen, len(add)   ) 
           

    for i in range(6):
        for il in range(ndisplaylines):
            linea=""
            for j in range(6):
                add=""
                if il <  len(displays[i][j]):
                    add= displays[i][j] [il]
                add = add+" "*( maxlen-len(add))
                if  j<5:
                    add = add +" | "
                linea = linea + add
            print(linea)
        if i < 5:
            print("-"*(len(linea)))

    nomi_variabili=[]
    vettori_variabili=[]
    print(ricerca)
    for i,j in ricerca:
        if (i,j) in res:
            nome = "A%d%d"%(i+1,j+1)
            vettore,I =  res[(i,j)]

            nomi_variabili.append(nome)
            vettori_variabili.append(vettore)
        

    print(" THE ELASTIC TENSOR VARIABLES ARE ")
    print(nomi_variabili)
    return nomi_variabili,vettori_variabili


def writeVolumeOnFile(volume, filename,h,k,l,i,II, cellvectors,corner, axis, isexp=1):
    
    if II==0:
        h5=h5py.File( filename , "w")
    else:
        h5=h5py.File( filename , "r+")


    if isexp:
        if("exp" not in h5):
            h5 .create_group("exp")
        subdir = "exp/"+("spot_%d%d%d_%d/"%(h,k,l, i) ) 
    else:
        if("sim" not in h5):
            h5 .create_group("sim/")
        subdir = "sim/"+("spot_%d%d%d_%d/"%(h,k,l, i) ) 
        


    if( subdir not in h5):
        h5 .create_group(subdir)

    h5[subdir+"/volume"]  = volume
    h5[subdir+"/cellvectors"]  = cellvectors
    h5[subdir+"/corner"]       = corner
    h5[subdir+"/axis"]         = axis
    h5.flush()
    h5.close()
    
    h5 = None
        


def get_DWs(DW_file,   cellVects  ):

    result = {}

    h5 = h5py.File(DW_file,"r")
    o_cellVects  = h5["cellVects"][:]

    vects_el2tds = cellVects  .T
    vects_ab2tds = o_cellVects.T

    A = numpy.dot(     vects_ab2tds         ,   numpy.linalg.inv(vects_el2tds) )

    group = h5["/Codes_1/DWs/"]
    chiavi = list(group.keys())
    
    for chiave in chiavi :
        result[string.atof(chiave)] = group[chiave+"/DWf_33"  ][:]
                       
    result["atomNames"] = h5["/atomNames"]
    result["atomReducedPositions"] = h5["/atomReducedPositions"][:]
    redpos = result["atomReducedPositions"]
    result["atomPositions"]  = np.dot(redpos, cellVects)
    
    return result


def Get_dwscatt_facts(qtots, DW_infos, TT , params , hkls_list, Npts_list):
    # GUARDARE IL 2 PI
    qtots[:]=qtots*2*math.pi
  
    QQs = numpy.array_split(qtots,nprocs)[myrank]
    NNs = numpy.array_split(  numpy.arange(len(qtots)) , nprocs   )[myrank]

    
    #  RIMETTI QUA
    WW3x3s = DW_infos[TT]  # una matrice 3X3 per ogni atomo                    
    a = numpy.tensordot(  QQs  ,  WW3x3s ,  ([-1], [-1])  )  # a adesso est [ nqs, Natoms , 3  ] 
    a = ( QQs[:,None,:]  *  a    ).sum(axis=-1)    # a adesso est [ nqs , Natoms]
    atomNames = DW_infos["atomNames"]
    tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
    scatterers = [  tf0.Element(aname  ) for aname in  atomNames ]
    
    # print "  SCATTS "
    # print atomNames
    # print params.lmbda*numpy.ones( qtots.shape[0] )
    # print Theta( qtots , params.lmbda  )
    # for  scatterer in scatterers :
    #     print  scatterer.f0Lambda(params.lmbda*numpy.ones( qtots.shape[0] ), Theta( qtots , params.lmbda  )   )
    
    sctfacts   = numpy.array( [ scatterer.f0Lambda(params.lmbda*numpy.ones( QQs.shape[0] ), Theta( QQs , params.lmbda  )   )
                                for scatterer in scatterers ]  ).astype(numpy.complex64)
    
    sctfacts = sctfacts.T # adesso scattFactors_site_q est [nspots ,Natoms   ]



    if 1 :
        redpos =    DW_infos ["atomReducedPositions"] 

        for hkl, N, Nsum in zip( hkls_list, Npts_list, numpy.cumsum(Npts_list)-numpy.array(Npts_list)):

            hkl = hkl[:3]
            inizio = max(0, Nsum-NNs[0])
            fine   = min(   Nsum+N-NNs[0] , NNs[-1]-NNs[0]+1)
            # print( inizio, fine, len(sctfacts))

            factors = numpy.exp( -2.0j*math.pi * (  (redpos*hkl).sum(axis=-1) )  )
            if(inizio<fine):
                # print(" per temperature ", TT , " hkl    " ,  numpy.array(hkl), "....  ", sctfacts[  inizio], sctfacts[  fine-1])

                sctfacts[   inizio:fine   ] =  sctfacts[   inizio:fine   ]*factors

    else:

        positions  =    DW_infos ["atomPositions"]
        
        factors = numpy.exp( -   1.0j *    np.tensordot( qtots,  positions, axes=([-1],[-1]) )           )   # nqs x Natoms
        
        sctfacts = factors *  sctfacts 
        
    
    a=(numpy.exp(-a) *  sctfacts ).sum(axis=-1)  # a adesso est [ nqs]
    
    # return sctfacts.sum(axis=-1)

    # a[:]=1
    
    return a



#--------------------------------------------------------------------------------------------------------
# 3D Main
#--------------------------------------------------------------------------------------------------------

generators = ['1','1~','2(x)','2(y)','2(z)','2(110)','3(z)','3(111)','4(z)','4(z)~','m(x)','m(y)','m(z)']
class Error:
    def __init__( self, aa, ad, dd,      ab, bd, bb ):
        self.aa = aa
        self.ad = ad
        self.dd = dd
        self.ab = ab
        self.bd = bd
        self.bb = bb
 
    def error(self,  view=0):

        aa = self.aa 
        
        ad = self.ad 
        dd = self.dd
        
        ab = self.ab 
        bd = self.bd



        mask=np.equal(0,aa)+np.equal(0,ad)+np.equal(0,dd)

        aa[mask]=1
        ad[mask]=1
        dd[mask]=1
        ab[mask]=1
        bd[mask]=1
        
        dets = aa*dd-ad*ad
        Ca   = ( dd *ab -ad*bd   ) /dets 
        Cd   = (-ad*ab  +aa *bd    ) /dets                
        gtzero = numpy.less(0,  Cd).astype("f")
        gtzero[:]=1
        ad=ad*gtzero
        bd=bd*gtzero
        dets = aa*dd-ad*ad
        dets[mask]=1
        
        print ("aa", aa)
        print ("dd", dd)
        print ("ad", ad)
        
        Ca   = ( dd *ab -ad*bd   ) /dets 
        Cd   = (-ad*ab  +aa *bd    ) /dets 

        print("Ca ", Ca)
        print("Cd ", Cd)
        
        errors  = Ca*Ca*aa + Cd*Cd*dd +2*Ca*Cd*ad  + self.bb  - 2*Ca*ab -2*Cd*bd

        errors[ mask]=0

        
        if ( numpy.less( errors,0 )  ).sum():
            raise        
        res = errors.sum()


        if view==0:
            return res
        else:
            return res,  Ca, Cd


    

def main3d(params  ,  all_ops_dp, DW_infos ):
     
    assert(type(params.load_from_file)==type([]))
    assert(type(params.T_list)==type([]))
    assert( len(params.T_list) ==  len( params.load_from_file   )   )

    # cellvectors_list, brillvectors_list = [], []
    # for iT in range(len(params.load_from_file)):
    #     cellvectors, brillvectors = getCellVectors(params, iT)
    #     cellvectors_list.append(cellvectors)
    #     brillvectors_list.append(brillvectors)

    
    cellvectors, brillvectors = getCellVectors(params)

    ## if  params.load_from_file is not None and nprocs>1:
        # comm.Finalize()
        # if myrank>0:
        #     print(myrank, "  RITORNA ") 
        #     return

    doesnotmatter=1
    
    spotpickers = [ spotpicker_cy.PySpotPicker(doesnotmatter, 0.0, do_elastic=params.DO_ELASTIC ) for fname in params.load_from_file ] # qmax sara messo da loadharvestfromfile
    
    h5=h5py.File(params.load_from_file[0],"r")
    H2p1 = int(h5["harvest/H2p1"].value)
    HHHH = int(h5["harvest/H"].value)
 

    
    
    aloni = []
    for fname in params.files_halos:
        h5=h5py.File(fname,"r")
        data = numpy.array(h5["harvest/alone4hkl"][:])
        H2p1 = int(h5["harvest/H2p1"].value)
        HHHH = int(h5["harvest/H"].value)
        aloni.append(data)
    # print( "ALONI")
    # print( aloni[1]/aloni[0])

    
    print(params.load_from_file)
    
    for spotpicker,fname in zip(spotpickers, params.load_from_file):
        print ("CARICO ",  fname)
        spotpicker.loadHarvestFromFile(fname )  
    QMAX_S      = [ S.getQmax() for S in spotpickers]

    # if myrank>0:
    #     print(myrank, "  RITORNA ") 
    #     return

    Q4W = []

    if params.fitSpots:
        print(" GET ELASTIC SYMMETRY ")
        Elastic_Simmetry = get_elastic_simmetry(all_ops_dp) 

        tofit=[]
        Rot4fittedhkl={}



        for h,k,l in  params.collectSpots:
            Q_tmp =   h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
            if params.REPLICA:
                replicas=all_ops_dp[:]
            else:
                replicas=I = [np.identity(3) ]
            for op in replicas:
                op.astype(np.float32)
                Qop = np.dot(op,Q_tmp)
                h,k,l = np.round(np.dot(cellvectors, Qop)).astype("i")
                if (h,k,l) not in tofit:
                    tofit.append( (h,k,l))
                    Rot4fittedhkl[(h,k,l)] = op


        # tmps = spotpickers[0].getDataToFit(numpy.array(tofit[:]), params.nred )

        tofit=numpy.array(tofit)
        hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py  = spotpickers[0].getDataToFit(tofit[:], params.nred )
        hkls = numpy.array(hkls)
        Npts = numpy.array(Npts)
        DQs = numpy.array(DQs)
        Datas = numpy.array(Datas)
        tuttiPhi_py = numpy.array(tuttiPhi_py)
        tuttiIJ_py = numpy.array(tuttiIJ_py)
        tuttiDist_py = numpy.array(tuttiDist_py)
        ac_phis= numpy.array(ac_phis)
        ac_ijs= numpy.array(ac_ijs)
        if(RAGGRUPPA):
            tmps = Raggruppa(  params,  hkls, Npts, DQs, Datas, ac_phis,
                               ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py )
            
        # tofit = tmps[0]

        
        Q4W = []

        if RAGGRUPPA:
            for h,k,l, ispot in tmps[0] :
                q4w_tmp =   h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
                Q4W.append(q4w_tmp*2*math.pi)
        else:
            for h,k,l in hkls :
                q4w_tmp =   h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
                Q4W.append(q4w_tmp*2*math.pi)

                    
        Q4W = numpy.array(Q4W ) 
                    
        tofit=np.array(tofit).astype("i")[:]
        if len(tofit)==0:
            tofit.shape=0,0

        print(" OTTENGO DATA ")
        
        hkls_list, Npts_list, DQs_list, Datas_list, ac_phis_list, ac_ijs_list , references=  [ [],[], [], [], [], [],  [] ]
        tmp_list = hkls_list, Npts_list, DQs_list, Datas_list, ac_phis_list, ac_ijs_list , references
        
        # hkls_tmp, Npts_tmp, DQs_tmp, Datas_tmp, ac_phis_tmp, ac_ijs_tmp  = spotpicker.getDataToFit(tofit[:], params.nred )
        Facts_list = []



        


        if 0:


            hkls_0, Npts_0, DQs_0, Datas_0, ac_phis_0, ac_ijs_0, tuttiPhi_py_0, tuttiIJ_py_0, tuttiDist_py_0  = spotpickers[1].getDataToFit(tofit[:], params.nred )
            hkls_1, Npts_1, DQs_1, Datas_1, ac_phis_1, ac_ijs_1, tuttiPhi_py_1, tuttiIJ_py_1, tuttiDist_py_1  = spotpickers[2].getDataToFit(tofit[:], params.nred )
            Datas_0= np.array(Datas_0) 
            Datas_1= np.array(Datas_1) 

            
            print( " NAN 1 ", np.isnan(Datas_0).sum())
            print( " NAN 2 ", np.isnan(Datas_1).sum())

            maschera = np.isnan(Datas_1)
            
            Datas_0[np.isnan(Datas_1)] = np.nan
            Datas_1[np.isnan(Datas_0)] =0
            Datas_0[np.isnan(Datas_0)] =0
            
            
            print ((np.array(hkls_0)-np.array(hkls_1) ).sum())
            print (np.abs(np.array(Datas_0)-np.array(Datas_1)).sum())
            print ((np.array(DQs_0)-np.array(DQs_1)).sum())

            spotpickers=spotpickers[:2]
            params.load_from_file = params.load_from_file[:2]
            params.T_list = params.T_list[:2]
            # raise
        
        assert(len(spotpickers)==2)

        
        
        for TT, spotpicker_obj in zip(params.T_list, spotpickers):
            if(RAGGRUPPA):
                tmps = spotpicker_obj.getDataToFit(tofit[:], params.nred )
                tmps = [numpy.array(t ) for t in tmps ]
                tmps = Raggruppa(  params, *tmps )
                for a,b in zip( tmp_list, tmps ):
                    a.append(b)

 
                hkls, Npts, DQs_tmp  =  tmps[:3]
                hkls = numpy.array(hkls)
                Npts = numpy.array(Npts)


                Ntot = np.array(  Npts).sum()
                qtots = np.zeros([Ntot,3],"f")
                qtots[:] =  DQs_tmp
                pos = 0


                
            else:
                hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py  = spotpicker_obj.getDataToFit(tofit[:], params.nred )

                
                hkls_list.append(hkls)
                Npts_list.append(Npts)
                DQs_list.append(DQs)
                Datas_list.append(Datas)
                ac_phis_list.append(ac_phis)
                ac_ijs_list.append(ac_ijs)

                
                Ntot = np.array(  Npts).sum()
                qtots = np.zeros([Ntot,3],"f")
                qtots[:] =  DQs
                pos = 0

                

            
            if(RAGGRUPPA):
                for (h,k,l, ispot), npt in zip( hkls,  Npts ):
                    print( h,k,l)
                    qtots[pos:pos+npt] = qtots[pos:pos+npt] + h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
                    pos=pos+npt
            else:
                for (h,k,l ), npt in zip( hkls,  Npts ):
                    print( h,k,l)
                    qtots[pos:pos+npt] = qtots[pos:pos+npt] + h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
                    pos=pos+npt
                
                
            tmp = Get_dwscatt_facts(qtots, DW_infos, TT , params , hkls[:,:3], Npts)

#            print " RATIO " 
#            ratio = tmp/Datas_list[-1]
#            print ratio
#            print ratio.min()
#            print ratio.max()
            
            # tmp[:]=1

            print (" GATHER ")
            tmpG = comm.gather(tmp, root=0)
            print( " OK " )
            if myrank==0:
                tmpG = numpy.concatenate(tmpG)
                print( tmpG.shape)
            
            if myrank==0:
                Facts_list.append(numpy.array((tmpG*tmpG.conjugate()).real))


                
        if myrank:
            return
            
        print( " Facts_list[0].sum()  " , Facts_list[0].sum()   )
        print( " Facts_list[1].sum()  " , Facts_list[1].sum()   )

        start=0
        
        Datas_list[0] = numpy.array(Datas_list[0][:])
        Datas_list[1] = numpy.array(Datas_list[1][:])

        for icheck in range(2):
            print(" DATASET ",   icheck)
            d =  Datas_list[icheck]
            start=0
            for npt,hk in zip(Npts, hkls):
                print(" spot ", hk," ha ",  npt-  np.isnan( d[start:start+npt]).sum()    , "  su ", npt)
                start=start+npt


        
        fcts=[]
        if(0):
            for (ispot,hkl_npts), hkl in zip(enumerate(Npts), hkls_list[0]):
                h,k,l, icount = hkl
                pos4hkl =  (   (h+HHHH)  *H2p1 +  (k+HHHH)  )*H2p1 +(l+HHHH)
                
                factor_goal = params.T_list[1]*1.0/params.T_list[0]
                factor      =  (references[1][ispot]  /  references[0][ispot])
                factor = factor[  -numpy.isnan(factor)    ]
                factor=factor.mean()
                
                print (" FACTOR " , (factor))
                print (" FACTOR " ,ispot,  (references[1][ispot]  /  references[0][ispot]))
                refactor    =  factor_goal/factor*1.00000
                print ( " REFACTOR BY ", refactor, factor_goal , factor   )
                
                Datas_list[1][start:start+hkl_npts] = Datas_list[1][start:start+hkl_npts]* refactor
                fcts.append(refactor*1.0)
                ## print (" FACTOR : "  ,  ( aloni[1][pos4hkl]/aloni[0][pos4hkl] ), pos4hkl)
                start = start + hkl_npts
        print( fcts)
        if len(aloni):
            for (ispot,hkl_npts), hkl in zip(enumerate(Npts), hkls_list[0]):
                h,k,l, icount = hkl
                pos4hkl =  (   (h+HHHH)  *H2p1 +  (k+HHHH)  )*H2p1 +(l+HHHH)
                # Datas_list[0][start:start+hkl_npts] = Datas_list[0][start:start+hkl_npts]*( aloni[1][pos4hkl]/aloni[0][pos4hkl] )
                print (" FACTOR : "  ,  ( aloni[1][pos4hkl]/aloni[0][pos4hkl] ), pos4hkl)
                start = start + hkl_npts
            tmp = ( aloni[1]/aloni[0] )
        
            numpy.savetxt(sys.stdout,tmp) 


        # zeros_1 = ((Datas_list[0]<0)).astype("d").sum()
        # zeros_2 = ((Datas_list[1]<0)).astype("d").sum()
        # nan_1 = (numpy.isnan(Datas_list[0])).astype("d").sum()
        # nan_2 = ((numpy.isnan(Datas_list[1]))).astype("d").sum() 
        # print (" CE NE SONO " , zeros_1, zeros_2, nan_1, nan_2)

        Datas_list[0] = numpy.array(Datas_list[1])*1.-numpy.array(Datas_list[0])*(1.0+0.0)+0.0
        # Datas_list[0][maschera] = np.nan

        delta = numpy.array(DQs_list[1])-numpy.array(DQs_list[0])
        print(" SCARTO IN Q ", (delta*delta).sum(), " " , (numpy.array(DQs_list[0])*numpy.array(DQs_list[0])).sum())
            
        comp_names, comp_vects = Elastic_Simmetry

        # A e una matrice 6x6
        comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
                      ,(2,1):3,(0,2):4,(1,0):5}  


        KELVIN2ERG = 1.380648780669e-16
        HNOBAR_CGS   = 6.626069e-27  ## /(2.0*math.pi)
        DENSITY    = params.density
        # 1 Pa = J/m3   ==>    erg/cm3  = 0.1 J/m3   ==>  1GPa = 10**10 erg/cm3
        GPA2CGS  = 1.0e+10
        A2CM  = 1.0e-8      # dans le calcul de la frequence on utilise des Qs qui sont en A**-1
        FACTOR4T =  A2CM*  math.sqrt(DENSITY/GPA2CGS)*  KELVIN2ERG / (    HNOBAR_CGS)


        
        # fit_object_list =  [ 
        #     FitT.Fit( hkls,brillvectors,  Npts, DQs, Datas, FACTOR4T * (params.T_list[0]),
        #               FACTOR4T * params.T_list[1] , Facts_list[0]   ,  Facts_list[1] ,
        #               comp_vects, 
        #               devicetype=params.devicetype  ,  max_workgroup_size=128  )
        #     for  hkls,  Npts, DQs, Datas, T ,      in zip(hkls_list,  Npts_list, DQs_list,
        #                                                              Datas_list, params.T_list)[:1]
        # ]
        
        fit_object_list =  [ 
            FitT.Fit( hkls_list[0],brillvectors, Npts_list[0] , DQs_list[0], Datas_list[0], FACTOR4T * (params.T_list[0]),
                      FACTOR4T * params.T_list[1] ,   Facts_list[0]   ,  Facts_list[1] ,
                      comp_vects, 
                      devicetype=params.devicetype  ,  max_workgroup_size=128  )
        ]

        # print " SCALDO I MOTORI " 
        # err = fit_object.error(  np.concatenate( [ np.random.random( len(comp_names)  ), np.zeros([6],"f")] ) )


        class fitProxy:
            def __init__(self,fit_object_list, variables, vvnames ):
                self.fit_object_list=fit_object_list
                self.variables=variables
                self.vvnames= vvnames
                self.iter=1
                                   
            def error(self):
                x = [minimiser.par(tok) for tok in self.variables ]
                mins = []
                maxs = []
                for tok in variables:
                    if hasattr(tok,"min"):
                        mins.append(tok.min  )
                        maxs.append(tok.max  )
                    else:
                        mins.append(0  )
                        maxs.append(0  )
                        
                aa, ad, dd,      ab, bd, bb, ll, la, lb, ld = 0,0,0,0,0,0,0,0,0,0
                for fit_o in self.fit_object_list :
                    aa_t, ad_t, dd_t,  ab_t, bd_t, bb_t = fit_o.error(x)

                    aa = aa + aa_t
                    ad = ad + ad_t
                    dd = dd + dd_t
                    ab = ab + ab_t
                    bd = bd + bd_t
                    bb = bb + bb_t

                ewl_o = Error(aa, ad, dd,  ab, bd, bb   )
                
 
                res = ewl_o.error()
                print(" ERROR " , res)
    

                entries =  self.vvnames
                file = open("parametri.txt","w")
                file.write("iter=%d\n"%(self.iter))
                self.iter+=1
                for n,v in zip(entries,x):
                    file.write("%s=%s\n"%(n,v))
                file.close()
                N=len(entries)
                for n in range(0,N,8):
                    pt = PrettyTable(entries[n:n+8] )
                    pt.add_row(   x    [n:n+len(entries[n:n+8]) ]) 
                    pt.add_row(   mins [n:n+len(entries[n:n+8]) ]) 
                    pt.add_row(   maxs  [n:n+len(entries[n:n+8])]) 
                    print( pt)

                # print " ERROR ", res
                    
                return res

            def setitercounter(self, n):
                self.itc=n
            def getitercounter(self):
                return self.itc

        dependent_case=0
        for tok in comp_names:
            if len(params.CGUESS[tok])==1:
                dependent_case = 1
            else:
                independent = tok

        if not dependent_case:
            variables = [ minimiser.Variable( ( params.CGUESS[tok][0]   +0.0*random.random()) , params.CGUESS[tok][1] ,params.CGUESS[tok][2]) for tok in comp_names ]
            independentVariables = variables
        else:
            tok = independent
            independent_variable = minimiser.Variable( ( params.CGUESS[tok][0]   +0.0*random.random()) , params.CGUESS[tok][1] ,params.CGUESS[tok][2])
            variables = []
            independentVariables = []
            for tok in comp_names:
                if tok == independent :
                    variables.append(independent_variable)
                    independentVariables.append(independent_variable)
                else:
                    ratio = params.CGUESS[tok][0] / params.CGUESS[independent][0]
                    newvar = minimiser.DependentVariable( "par(%s) * %e  " %(independent, ratio)  ,{ independent:independent_variable  } )
                    variables.append(newvar)
                    
            
        qmaxx = numpy.array( QMAX_S  ).max()
        
        vvnames = comp_names



        if(params.fitSpots):
            tominimise=fitProxy(fit_object_list,variables, vvnames )

            # print "===================="
            # print tominimise.error()
            # variables[0].value *=1.001
            # print tominimise.error()
            # variables[0].value /=1.001
            # variables[1].value *=1.001
            # print tominimise.error()
            # variables[1].value /=1.001
            # variables[2].value *=1.001
            # print tominimise.error()
            # variables[2].value /=1.001

            # variables[0].value /=1.001
            # print tominimise.error()
            # variables[0].value *=1.001
            # variables[1].value /=1.001
            # print tominimise.error()
            # variables[1].value *=1.001
            # variables[2].value /=1.001
            # print tominimise.error()
            # variables[2].value *=1.001
            
            # for k in range(2000):
            #      for v in variables:
            #          v.value*=(1.+0.05*(k+1))/(1.+0.05*k)
            #          print v
            #      print "ERROR ------"
            #      for i in range( 1):
            #          r = tominimise.error()
            #          print "ERROR ", variables[0].value, r
            #          open("errori.txt","a").write("%e %e\n"%(   variables[0].value, r  )     )
            # raise
            
            miniob=minimiser.minimiser(tominimise,independentVariables)

            
            try:
                miniob.amoeba(params.ftol)  
            except   KeyboardInterrupt:
                print("MINIMISATION INTERRUPTED BY KEYBOARD, NOW GOING ON")
            
        if(params.visSim):
            x = [minimiser.par(tok) for tok in variables ] 
                
            aa, ad, dd,      ab, bd, bb, ll, la, lb, ld = 0,0,0,0,0,0,0,0,0,0
            simdata_s=[]
            expdata_s=[]
            for fit_o in  tominimise.fit_object_list  :
                aa_t, ad_t, dd_t,  ab_t, bd_t , bb_t,    simdata, expdata = fit_o.error(x, view=1)

                simdata[numpy.isnan(expdata)]= numpy.nan

                print (" SUM " , (numpy.isnan(expdata)*1).sum())
                
                simdata_s.append(simdata)
                expdata_s.append(expdata)
                aa = aa + aa_t
                ad = ad + ad_t
                dd = dd + dd_t
                ab = ab + ab_t
                bd = bd + bd_t
                bb = bb + bb_t

            ad[2]=0    
            bd[2]=0    
            dd[2]=0
            
            ewl_o = Error(aa, ad, dd,  ab, bd, bb  )
            
            res,  Ca, Cd = ewl_o.error( view=1)

      
            print("Cd--   ", Cd)
            
            for idataset,(Temperature,fobj) in enumerate(zip(params.T_list,fit_object_list )):
                
                simdata, expdata =  simdata_s[idataset], expdata_s[idataset]
                simdata[numpy.isnan(expdata)]= numpy.nan


                
                N=0
                II=0
                IIexp=0
                start=0

                DQs = DQs_list[idataset]

                assert(numpy.abs(Npts_list[idataset]-fobj.Npts).sum()==0)
                try:
                    x=enumerate(fobj.Npts)
                    for ispot,hkl_npts in enumerate(fobj.Npts):

                        simdata[start:start+hkl_npts] = Ca[ispot]*(simdata[start:start+hkl_npts])+Cd[ispot]
                        # simdata[start:start+hkl_npts] =(simdata[start:start+hkl_npts])


                        HKL = hkls_list[idataset][ispot]
                        _qmin=0.0
                        _qmax= QMAX_S[idataset]

                        spotpickerSim = spotpicker_cy.PySpotPicker(1, _qmax)

                        dum=numpy.array([1.0e30,0,0],"f")
                        
                        spotpickerSim.setMT()
                        
                        spotpickerSim.harvestFromCollection(numpy.array(DQs)[start:start+hkl_npts],dum, 
                                                            cellvectors,brillvectors,
                                                            simdata[start:start+hkl_npts],
                                                            _qmin ,
                                                            _qmax)
                        all_alone=1
                        spotpickerSim.compatta(all_alone)
                        rot = np.eye(3,dtype="f")
                        print(list(Rot4fittedhkl.keys()))
                        rot =  numpy.array(Rot4fittedhkl[tuple(HKL[:3])],"f")
                        print(" spot picker ")
                        
 
                        volume, corner, axis = spotpickerSim.interpolatedSpot( 0,0,0,
                                                              params.testSpot_N , rot ,
                                                              params.testSpot_beta,
                                                              params.testSpot_Niter )
                        spotpickerSim = spotpicker_cy.PySpotPicker(1, _qmax)

                        # print("  NAN ", numpy.isnan(expdata[start:start+hkl_npts]).sum())
                        # print(" nont  NAN ", expdata[start:start+hkl_npts].size -numpy.isnan(expdata[start:start+hkl_npts]).sum())
                        # expdata[start:start+hkl_npts][numpy.isnan(expdata[start:start+hkl_npts])]=0
                        # print("  <0 ", numpy.less(0,expdata[start:start+hkl_npts]).sum())
                        # raise 
                        
                        spotpickerSim.setMT()
                        spotpickerSim.harvestFromCollection(numpy.array(DQs)[start:start+hkl_npts],dum,
                                                            cellvectors,brillvectors,
                                                            expdata[start:start+hkl_npts],
                                                            _qmin ,
                                                            _qmax)        
                        spotpickerSim.compatta(all_alone)
                        volumeE, corner, axis = spotpickerSim.interpolatedSpot( 0,0,0,
                                                              params.testSpot_N , rot ,
                                                              params.testSpot_beta,
                                                              params.testSpot_Niter )
                        h,k,l, icount=HKL
                        if volume is not None:
                            writeVolumeOnFile(volume, "simtests_%s.h5"%Temperature,h,k,l,icount,II, cellvectors, corner, axis, isexp=0)
                            print("SCRIVO ", h,k,l)
                            II+=1
                        else:
                            print(" for ", h,k,l,"  non si sono trovati punti ")

                        if volumeE is not None:
                            writeVolumeOnFile(volumeE, "simtests_%s.h5"%Temperature,h,k,l,icount,II, cellvectors, corner, axis, isexp=1)
                            II +=1
                        else:
                            print(" for ", h,k,l,"  non si sono trovati punti ")
                        start=start+hkl_npts
                except KeyboardInterrupt:
                    pass
   
        if(1 or params.fitSpots):
            for name, v in zip(comp_names, variables ):
                print(name , " = " , v.value)


def main(params):
    Config_fname = params.conf_file

    print('---------------------------------------------------------------')
    ascii_gen_list = params.ascii_generators .replace(',',' ').split()    

        
   
    # print 'Setting Parameters ...'
    paramsSystem = read_configuration_file(params.conf_file)

    cellvectors, brillvectors = getCellVectors(paramsSystem)
    # print cellvectors

    # ops_list = genlist2oplist(ascii_gen_list)
    ops_list_dp = genlist2oplist( ascii_gen_list ,cellvectors, dtype=np.float64)

    DW_infos = get_DWs(params.DW_file,  cellvectors   )


    if paramsSystem.collectSpots is None and paramsSystem.file_maxima is not None:
        maxima_data = pickle.load( open(paramsSystem.file_maxima, 'r'))
        picchi = set()
        for  fname , infos in maxima_data.items():
            for pos, infopeak in infos.items():
                if infopeak["accepted"]:
                    h,k,l = infopeak["hkl_rounded"]
                    picchi = set.union( picchi ,set( ((int(h), int(k), int(l) ),)  ))
        for  fname , infos in maxima_data.items():
            for pos, infopeak in infos.items():
                if not infopeak["accepted"]:
                    h,k,l = infopeak["hkl_rounded"]
                    picchi = picchi - set(((int(h), int(k), int(l) ),))
        paramsSystem.collectSpots =list(picchi)

    print (" SPOTS " , paramsSystem.collectSpots )
    
    main3d(paramsSystem,
           ops_list_dp, DW_infos)
    print(" processo ", myrank, " aspetta ")
    comm.Barrier()

USAGE = """ USAGE :
    tds2el inputfile

input file containing

conf_file             =  "astring"    
filter_file           =  "astring"
images_prefix         =  "astring"
ascii_generators      =  "1,1~,2(x),2(y),2(z),2(110),3(z),3(111),4(z),4(z)~,m(x),m(y),m(z)"  # not necessarily all of them



"""
print(__name__) 

# if __name__ == "__main__":

if(sys.argv[0][-12:]!="sphinx-build"):

    if len(sys.argv) < 2:
        print(USAGE)
    else:

        class Parameters:
            conf_file=None
            volume_reconstruction = True
            exec(open(sys.argv[1]).read())
        main( Parameters())

    # sys.exit()


