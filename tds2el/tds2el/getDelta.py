import sys
import minimiser
import numpy as np
class par1:
    exec(open(sys.argv[1],"r").read())


class par2:
    exec(open(sys.argv[2],"r").read())
    

keys = dir(par1)

RES="""DELTA={
"""   
for k in keys:
    a1  = getattr(par1,k)
    if hasattr(par2,k):
        a2  = getattr(par2,k)
    
        if isinstance(a1,float) and isinstance(a2,float):
            diff = a2-a1
            if(abs(diff)>0):
                RES=RES+"\"%s\":%s,\n"  %(  k, str(diff)  )

RES=RES+"}\n"

print RES
        
