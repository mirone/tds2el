import numpy
import sys
from silx.gui import qt
import MyStackView

import hdf5dialog
import h5py

def main():

    StackViewMainWindow  = MyStackView.StackViewMainWindow

    app = qt.QApplication(sys.argv[1:])

    a, b, c = numpy.meshgrid(numpy.linspace(-10, 10, 200),
                             numpy.linspace(-10, 5, 150),
                             numpy.linspace(-5, 10, 120),
                             indexing="ij")
    mystack = numpy.asarray(numpy.sin(a * b * c) / (a * b * c),
                            dtype='float32')

    # linear calibrations (a, b), x -> a + bx
    dim0_calib = (-10., 20. / 200.)
    dim1_calib = (-10., 15. / 150.)
    dim2_calib = (-5., 15. / 120.)

    # sv = StackView()

    class Parameters:
        exec(open(sys.argv[2],"r").read())

    # N = (len(sys.argv)-1)/2


    N = len(Parameters.items)

    sv = StackViewMainWindow(N)

    sv.setColormap("jet", autoscale=True)

    # sv.setManyStack([mystack, mystack],          calibrations=[dim0_calib, dim1_calib, dim2_calib])

    # sv.setStack(mystack,
    #             calibrations=[dim0_calib, dim1_calib, dim2_calib])

    # sv.setLabels(["dim0: -10 to 10 (200 samples)",
    #               "dim1: -10 to 5 (150 samples)",
    #               "dim2: -5 to 10 (120 samples)"])


    class mydialog(hdf5dialog.hdf5dialog):
        def consoleAction(self,h5group, vn ):

            dati = []
            name = h5group.name

            pos=name.rfind("/")
            post = name[pos+1:]
            print post
            for item  in Parameters.items:
                dato = 0
                for fact,name, tipo in item:
                    print " LEGGO ", name
                    h5 = h5py.File(name,"r")
                    print " ECCO ", "/"+tipo+"/"+post+"/volume"
                    h5group = h5["/"+tipo+"/"+post+"/volume"]
                    dato = dato + fact*(h5group[:] )
                dati.append(dato)


            sv.setStack(dati)

    sv.show()


    filename = sys.argv[1]
    window = mydialog (filenames = [filename], storage = [0], groupname = "/", RO=True)
    # window.setModal(True)
    window.show()
    # window.exec_()



    app.exec_()
    

if len(sys.argv)!=3:
    print (""" USAGE :  tds2el_stackview filename.h5 items.inp

where items.inp has this example format 
  
items = [
    [
	[1.0, "simtest.h5", "exp"]
    ]     ,
    [
	[1.0, "simtest.h5", "sim"]
    ] 
]

""")

else:
    main()
