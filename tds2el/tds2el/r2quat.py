import numpy as np
import math
import sys

#--------------------------------------------------------------------------------------------------------
# Rotation, Projection and Orientation Matrix
#--------------------------------------------------------------------------------------------------------

# e' una rotazione degli assi non delle coordinate
#  va bene cosi perche si ruota il campione quindi il raggio controruota nello spazio K
def Rotation(angle, rotation_axis=0):
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    # r1=-r1
    # r2=-r2
    # r3=-r3
    
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))
    
    return tmp


def M2r(M):

    if math.fabs(M[2,0])!=1.0:
        r1 = math.atan2(M[2,1],M[2,2])
        if  (math.fabs(M[2,1]/math.sin(r1))>math.fabs(M[2,2]/math.cos(r1))):
            r2 = math.atan2( -M[2,0], M[2,1]/math.sin(r1)  )
        else:
            r2 = math.atan2( -M[2,0], M[2,2]/math.cos(r1)  )

        r3 = math.atan2(M[1,0],M[0,0])
        return -np.degrees(r1),-np.degrees(r2),-np.degrees(r3)
    else:
        raise Exception( " ce cas reste a programmer, http://www.chrobotics.com/library/understanding-euler-angles ")



def quat2M(quat):
    x,y,z = quat
    d = x*x+y*y+z*z
    if d>1:
        d=math.sqrt(d)
        x/=d
        y/=d
        z/=d
    # print x,y,z
    d = x*x+y*y+z*z
    if d>1.0:
        d=1.0
    a=math.sqrt(1.0-d)
    
    res= np.array(
        [
            [  a*a +x*x -y*y-z*z  ,             2*x*y +2*a*z          ,       2*x*z -2*a*y                  ] ,
            [        2*x*y -2*a*z ,      a*a -x*x + y*y-z*z           ,      2*y*z +2*a*x               ] ,
            [      2*x*z +2*a*y   ,             2*y*z -2*a*x          ,     a*a -x*x -y*y  + z*z             ] 

        ]
    )
    return res.astype("f")

def M2quat(M):
    Q=np.zeros([4,4],"d")

    t = M[0,0]+M[1,1]+M[2,2]
    
    for i in range(3):
        Q[i,i]= 2*M[i,i] - t
        for j in range(3):
            if j!=i:
                Q[i,j] = M[i,j]+M[j,i]

        Q[i,3] = M[(i+1)%3,  (i+2)%3 ] - M[ (i+2)%3,  (i+1)%3 ] 
        Q[3,i] = M[(i+1)%3,  (i+2)%3 ] - M[ (i+2)%3,  (i+1)%3 ] 
    Q[3,3]= t

    Q=Q/3

    evals, evects = np.linalg.eigh(Q)


    res =  evects.T[np.argmax(evals) ]
    res = res[:3]*np.sign( res[3])
    return res

def r2quat(q1,q2,q3):
    M = Snd_Rot_of_RS( q1,q2,q3 )
    quat = M2quat(M)
    return quat


     
if __name__ == "__main__":
    r1 = 45.0
    r2 = 39
    r3 = 60


    M =     Snd_Rot_of_RS(r1, r2, r3)

    print( "M")
    print( M)

    R1,R2,R3 = M2r(M)
    print( "R")
    print( R1,R2,R3)


    M =     Snd_Rot_of_RS(R1, R2, R3)
    print( M)

    quat = M2quat(M)

    print( "quat")
    print( quat)


    M2 = quat2M( quat )

    print( "M2")
    print( M2)

    print( "----------- " )
    print( np.dot(M2.T,M2))
    print( " +++++++++++++++++++ " )

    R1,R2,R3 = M2r(M2)
    print( "R")
    print( R1,R2,R3)

    quat = M2quat(M2)

    print( "quat")
    print( quat)


