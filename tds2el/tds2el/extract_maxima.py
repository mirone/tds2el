#!/usr/bin/env python
# -*- coding: utf-8 -*-
#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/




import glob
# import fftw3f
import math
from PyMca5.PyMcaIO import EdfFile
from PyMca5.PyMcaIO.TiffIO import TiffIO

import h5py
import pylab
import random 
import Fit
import minimiser
import numpy
import os
import scipy
from skimage.feature import peak_local_max
import pickle

__doc__=(


r"""  .. toctree::
        :maxdepth: 3


running the code            
----------------
     
   extract_maxima  is always runned with one argument ::

     tds2el  inputfile

   * the inputfile contains driving options. Let's  call this file input.inp. here an example ::

            filter_file           =  "mask2M.edf"
            images_prefix         =  "Diffuse1/bronze7_0001p_"
            safety_radius         =  4
            threshold_abs=10
            threshold_rel=0.05


     * filter_file : is the mask : it is one for good pixel and zero for pixel to be discarded.

     * images_prefix : the list of files will be built by the following lines of code  ::

             flist = glob.glob(params.images_prefix+"*.cbf" )
             flist.sort()
           
       it means that the file ordering is given by the name ordering. This is an important point,
       in this case for extracting maxima you need to compare with neighbours in 2D but also
       with previous and following images.  Therefore it is important that images have a numerical
       postfix with fixed lenght.

     * safety_radius :  this indicates how far you must stay away from bad pixels and from the borders.
       This is to avoid to take false maxima, that are such because there is a limitations

   * Output : a file  "massimi.p" which is a pickled list of maxima. it will later be used by analyse_maxima.py


parallel harvesting
^^^^^^^^^^^^^^^^^^^
     
    * when an harvesting is done, a huge amount of files needs to be readed. tds2el 
      can be runned in parallel in this case ::

         mpirun -n8 extract_maxima  input.par

      in the case you want to use 8 processors.


"""
)

try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print "MPI LOADED , nprocs = ", nprocs
except:
    nprocs=1
    myrank = 0
    print "MPI NOT LOADED "


# import sift
from math import pi, sin, cos
try :
    import fabio
except :
    print 'Warning : fabio module could not be initialised'

import numpy as np
import sys, time
__version__ = 0.7


#--------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------------------------
# Parameter Class
#--------------------------------------------------------------------------------------------------------





#--------------------------------------------------------------------------------------------------------
# 3D Main
#--------------------------------------------------------------------------------------------------------

generators = ['1','1~','2(x)','2(y)','2(z)','2(110)','3(z)','3(111)','4(z)','4(z)~','m(x)','m(y)','m(z)']

def main3d(Filter_fname,flist,safety_radius , t_abs, t_rel, binning ):


    img = fabio.open(flist[0])
    DIM1orig, DIM2orig = img.data.shape

    data = img.data[::binning, ::binning]
    dim1, dim2 = data.shape
    
    print 'Image Dimension :', dim1, dim2


    if safety_radius>0.0 :
        DIM1=1
        while DIM1<dim1*2:
            DIM1*=2
        DIM2=1
        while DIM2<dim2*2:
            DIM2*=2

        import fftw3f
        ry = numpy.fft.fftfreq(DIM1,1.0/DIM1) .astype("f")
        rx = numpy.fft.fftfreq(DIM2,1.0/DIM2) .astype("f")

        fftw_direct=  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")
        fftw_rec   =  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")

        fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['measure'])
        fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['measure'])
        

    if safety_radius>0.0:

        disco = (ry*ry)[:,None] + (rx*rx)
        disco = (numpy.less( disco, safety_radius* safety_radius   )).astype("f")

        fftw_direct[:] = disco
        fft_plan_F()
        disco_reci = numpy.array(fftw_rec)

    total = len(flist)

    print 'Loading images filter ...'
    if(Filter_fname[-4:]==".edf"):
        Filter=EdfFile.EdfFile(Filter_fname,"r").GetData(0).astype(np.float32)[::binning, ::binning]
        assert( Filter.shape ==  (dim1,dim2) ) 
    else:
        f = open(Filter_fname,'rb')
        raw_data = f.read()
        Filter = np.fromstring(raw_data, np.uint8).reshape((dim1,dim2)).astype(np.float32)


    massimo=0.0
    i=0


    print " safety_radius " , safety_radius

    fftw_direct[:]=0.0
    fftw_direct[  : Filter.shape[0]  ,  : Filter.shape[1]] =  1-Filter
    fft_plan_F()
    fftw_rec[:] = fftw_rec * disco_reci
    fft_plan_B()
    size =  data.size
    FilterBig = numpy.less( abs(fftw_direct [  : data.shape[0]  ,  : data.shape[1]] )/size , 0.1  )

    Nring = 32
    middle = 15
    sigma=5.0
    ring = np.zeros([Nring, data.shape[0], data.shape[1]],"f")
    ring_images = [None]*Nring

    nblockfl = len(flist)/nprocs+1
    res={}
    count = 0 
    if myrank==2:
        for iim, fname in enumerate(flist[:]):
            print iim, fname

    comm.Barrier()

    marge = Nring-middle
    for iim, fname in enumerate(flist[:]):
        if ((iim/nblockfl)%nprocs != myrank)  and   ( ((iim-marge)/nblockfl )  %nprocs != myrank    )  and   ( ((iim+marge) /nblockfl )   %nprocs != myrank    )   :
            continue

        print 'Working on image %s' % fname   

        ring[count%Nring] = fabio.open(fname).data.astype(np.float32)[::binning, ::binning]*Filter

        # if (iim/nblockfl)%nprocs==myrank:
        #     nameout=params.airscattering_debug_dir+"/"+os.path.basename(fname)+".tif"
        #     TiffIO(nameout,"w+").writeImage(  ring[count%Nring]  )


        ring_images[count%Nring] = fname
        if count==Nring-1:
            iplan=middle
        elif count>=Nring:
            iplan+=1
        else:
            count+=1
            continue
        count+=1

        blurred_ring = np.roll(scipy.ndimage.filters.gaussian_filter(ring,sigma, mode='wrap'), middle-(iplan%Nring),axis=0)

        picchi = peak_local_max( blurred_ring[middle-1:middle+2]  , min_distance=1, threshold_abs=t_abs,threshold_rel=t_rel)

        myimage = ring_images[ (iplan%Nring)  ]

        Giplan = iim -((Nring-1) -middle)
        if (myrank==1) :print "==== ",  Giplan,    ring_images[iplan%Nring]

        # if (Giplan/nblockfl)%nprocs==myrank:
        #     nameout=params.airscattering_debug_dir+"/"+os.path.basename(myimage)+".blur.tif"
        #     TiffIO(nameout,"w+").writeImage(  blurred_ring[middle]     )

        print "IMAGE ", myimage,iim, Giplan, picchi

        if len(picchi):
            picchi=picchi[:,1:]
            picchiL=picchi.T.tolist()
            good = np.where( FilterBig[picchiL]>0  )
            print FilterBig.sum()
            print " good " , good
            if len(good[0]):
                res[  myimage]=((picchi[good].T*binning).tolist() ,Giplan  )
                
    comm.Barrier()
    for i in range(1,nprocs):
        if myrank==0:
            edict = comm.recv( source=i, tag=777+i)
            res.update(edict)
        else:
            if i==myrank:
                comm.send( res ,dest=0, tag=777+i)  
    if  myrank==0:
        maxima_file = open('massimi.p', 'w')
        pickle.dump(res, maxima_file)
        pickle.dump((DIM1orig ,DIM2orig), maxima_file)
        maxima_file.close()


def main(params):
    Filter_fname = params.filter_file

    flist = glob.glob(params.images_prefix+"*.cbf" )
    flist.sort()
   
    # print 'Setting Parameters ...'


    main3d( params.filter_file,flist, params.safety_radius, params.threshold_abs, params.threshold_rel, params.bin)
    print " processo ", myrank, " aspetta "
    comm.Barrier()

    
USAGE = """ USAGE :
    extract_maxima inputfile

example input file containing 

filter_file           =  "newMask.edf"
images_prefix         =  "Diffuse1/bronze7_0001p_"
safety_radius         =  4
threshold_abs=10
threshold_rel=0.05


"""
print __name__ 

# if __name__ == "__main__":

if(sys.argv[0][-12:]!="sphinx-build"):

    if len(sys.argv) < 2:
        print USAGE
    else:

        class Parameters:
            filter_file = None
            images_prefix = None
            volume_reconstruction = True
            bin = 1
            exec(open(sys.argv[1]).read())
        main( Parameters())

    # sys.exit()


