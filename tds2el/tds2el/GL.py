import numpy
import sys
from silx.gui import qt
from silx.gui.plot3d.SceneWindow import SceneWindow, items

# Data
points = numpy.loadtxt(sys.argv[1])
x, y, z = points.T
values = 0


class MouseInteraction(qt.QObject):
    icount=0
    def __init__(self, parent=None):
        super(MouseInteraction, self).__init__(parent)
        self._clicked = False

    def eventFilter(self, watched, event):
        if (event.type() == qt.QEvent.MouseButtonPress and
                event.button() == qt.Qt.LeftButton):
            self._clicked = True
        elif event.type() == qt.QEvent.MouseMove:
            self._clicked = False
        elif (event.type() == qt.QEvent.MouseButtonRelease and
                event.button() == qt.Qt.LeftButton and self._clicked):
            self._clicked = False
            self.clicked(watched, event.x(), event.y())
        elif  (event.type() == qt.QEvent.KeyPress) :
            print(" key pressed ", qt.QKeyEvent(event).key()    )
            print(" key pressed ", qt.QKeyEvent(event).text()=="a"    )
            # print(" key pressed ", qt.QKeyEvent(event).text()[0][1]    )
        return False



    
    @staticmethod
    def _windowToScene(viewport, x, y, ndcZ=0.):
        ndc = viewport.windowToNdc(x, y)
        if ndc is None:
            return None
        else:
            ndc = numpy.array((ndc[0], ndc[1], ndcZ, 1.), dtype=numpy.float32)
            camPos = viewport.camera.intrinsic.transformPoint(
                ndc, direct=False, perspectiveDivide=True)
            scenePos = viewport.camera.extrinsic.transformPoint(
                camPos, direct=False)
            return scenePos

    def clicked(self, widget, x, y):
        scatter = widget.getSceneGroup().getItems()[0]
        assert isinstance(scatter, items.Scatter3D)

        xData = scatter.getXData(copy=False).ravel()
        yData = scatter.getYData(copy=False).ravel()
        zData = scatter.getZData(copy=False).ravel()
        values = numpy.zeros(len(xData), dtype=numpy.float32)

        viewport = widget.viewport
        ndcZ = viewport._pickNdcZGL(x, y)
        if ndcZ != 1:
            pickedPos = self._windowToScene(viewport, x, y, ndcZ)[:3]
            points = numpy.array((xData, yData, zData)).T
            sqDist = numpy.sum((points - pickedPos)**2, axis=1)
            index = numpy.argmin(sqDist)
            print('Picked point', index, points[index])

            values[index] = self.icount
            self.icount += 1

        scatter.setData(xData, yData, zData, values)


qapp = qt.QApplication([])
window = SceneWindow()

sceneWidget = window.getSceneWidget()
sceneWidget.setBackgroundColor((0.8, 0.8, 0.8, 1.))
sceneWidget.setForegroundColor((0.1, 0.1, 0.1, 1.))
sceneWidget.setProjection('orthographic')

mouseInteraction = MouseInteraction()
sceneWidget.installEventFilter(mouseInteraction)

scatter3d = sceneWidget.add3DScatter(x, y, z, values)
#scatter3d.getColormap().setName('viridis')
scatter3d.getColormap().setColormapLUT((
    (0., 0., 0.), (1., 0., 0.), (0., 1., 0.)  , (0., 0., 1.)))
scatter3d.getColormap().setVMax(3)

scatter3d.setSymbol('o')
scatter3d.setSymbolSize(11)

window.show()

qapp.exec_()
