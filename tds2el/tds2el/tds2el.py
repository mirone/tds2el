#!/usr/bin/env python
# -*- coding: utf-8 -*-
#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
from __future__ import print_function

import sys
if(sys.argv[0][-12:]!="sphinx-build"):
    import spotpicker_cy

import glob
import collections
# import fftw3f
import math
from PyMca5.PyMcaIO import EdfFile
from PyMca5.PyMcaIO.TiffIO import TiffIO

if(sys.argv[0][-12:]!="sphinx-build"):

    import ab2tds.dabax as dabax

try:
    from prettytable import PrettyTable
except:
    from tds2el.prettytable import PrettyTable

import h5py
import pylab
import random 
import Fit
import minimiser
import numpy
import os
import traceback
import string
import copy

import datetime
import time
from scipy.interpolate import interp1d
import scipy.signal
import scipy
import sys

import os
import pickle
import copy

import r2quat

__doc__=(


r"""  .. toctree::
        :maxdepth: 3


running the code            
----------------
      
   tds2el is always runned with one arguments ::

     tds2el  inputfile

   * the inputfile contains driving options. Let's  call this file input.par. here an example ::


        conf_file             =  "tds2el.par"  
        filter_file           = "filter.edf"  
        images_prefix         =  "G19K45/set1_0001p_0"
        images_prefix         =  ".cbf"
        ascii_generators      =  "1~,2(z),2(y)"
        normalisation_file    = "normalisation.dat"



     * conf_file:  is the name of the file containings all the experimental  parameters, and the
       parameters concerning your calculation options. You can compose it starting from the input file
       that you have used for the alignement, with updated refined parameters, and then adding some more options
       that we document here below.

     * filter_file : is the mask : it is one for good pixel and zero for pixel to be discarded.
       There is also another mechanism which discards data from the fit : when the data pixel has a negative
       value in the experimental image that's a signal for the pixdel to be discarded.

     * images_prefix : the list of files will be built by the following lines of code (tds2el.py) ::

             flist = glob.glob(params.images_prefix+"*"+  params.images_postfix)
             flist.sort()
           
       it means that the file ordering is given by the name ordering. This is an important point
       because on this depends the rotation angle given at each file thourgh the incremental step ::

             paramsExp.phi += angular_step

     * ascii_generators is used to determine the structure of elastic constants tensor.

     * normalisation_file contains two columns : filename ( with path) and denominator value.
       Look at the dedicated section of the documentation to see how you can generate one.


parallel or not parallel
------------------------
     
    * when an harvesting is done, a huge amount of files needs to be readed. tds2el 
      can be runned in parallel in this case ::

         mpirun -n 8 tds2el input.par

      in the case you want to use 8 processors.

    * when the harvesting is already done, and you run fits and interpolations,
      the program does not use mpi.

      It uses instead opencl for certain parts of the calculations but you always run the code 
      simply by ::

          tds2el inputfile




Working in the neighboroods of Bragg peaks
-------------------------------------------

   You can do :

     * fine interpolations around selected bragg peaks

     * optimise elastic constants to fit the neighbourood os selected Bragg peak
        The fit is done on the data. Not on the interpolation

   The preliminary step for both the above operations consists in harvesting from
   the raw data, the vicinity of selected spots

preparation of the input file for harvesting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

       *  For the conf_file you can compose it starting from the input file
          that you have used for the alignement, with updated refined parameters, and then adding following  options :: 

            DeltaQ_max = 0.15
            DeltaQ_min = 0.04
            load_from_file= None
            save_to_file  =  "harvest_spots.h5"
            DO_ELASTIC=1

              

            threshold = 500000.0

            if False:
                collectSpots = [[0,0,-12],[0,1,-8],[2,0,-8],[-4 , 4, 8 ],[-1,0,4],[-1,1,8],[-2,-1,-2],[2,0,-2]  ]
            else:
                file_maxima = "tagged_selectedpeaks.p"

            REPLICA_COLLECTION = 0  # default to 0



       What do these options stand for ? :

           * DO_ELASTIC=1  activates enquiries concerning   data points around Bragg peaks. ( the other less documented  options 
             DO_ELASTIC=0 is used to do rough  harvesting of the volume for whole volume visualisation, but is less documented here and at the bottom) 
        
           * DeltaQ_max, DeltaQ_min
	     these numbers determine which area, around each (selected) peak, is harvested.
	     These values are multiplied by the lenght of the longest reciprocal
	     lattice vector ( :math:`{\bf a^* ,b^*, c^* }` and thus converted to maximal radius
             and minimal radius. All the data points that fall within these limits are harvested.

           * load_from_file= None  This option activates harvesting. It indicates that there is no previous knowledge 
             from previous harvesting.

           * save_to_file  =  "harvest_spots.h5"  The file the harvest will be written to.
   

           * collectSpots =[ points ] the spots around which data are collected. If alternatively a tagged maxima *file_maxima*  is given
             ( tagged aftern alignement refinement)
             then the list will be formed with the hkl tags of the accepted spots.  Note that a same hkl can give multiple peaks
             due to sweeping. At this version of tds2el  hkl will be accepted only if all the spots tagged hkl are accepted.
             When a scan produces separated maxima for the same hkl the two blobs are fitted separately, this means that in future development 
             of the code one could select just the selected ones in case of multiple hkl  

           * REPLICA_COLLECTION :  where it is set to 1 ( or True) then the collected sport are, besides those specified by
             collectSpots or maxima file, all those that can be obtained by symmetry operations.


	   * threshold  : when a pixel value exceed threshold no pixel values from that image are fitted ,
             however the points of a blob exceeding threshold are used to calculate the position
             of the bragg peak in reciprocal space.  This is done doing a weighted average.
             The averaging is not an exact procedure because of the pixel saturation but we hope 
             that the saturated points are closely packed around the exact position. For centering  be effective you
             have to harvest twice. The second you you use the option *centering_file*.

           * REMEMBER that harvesting can be parallel ( see above introduction)

           * TO enquire which hkl have been collected in a given and how many points each add the following ad rerun ::

                 load_from_file  =  "harvest_spots.h5"
                 show_hkls = True

		 
Recentering
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

           * If you want to obtain correct harvested Q, which are centered over bragg spot, calculated 
             on the basis of the most intese pixels, you have to run the harvesting procedure twice.
             the second time you set *centering_file* to the previous harvest file ::

                   centering_file ="harvest_spots.h5"

             and you get a new harvest where Q are not calculated from geometry only ,
             but also recorrection based on bragg spot position.

Simulating the Thermal Diffused Scattering from Elastic Constants
-----------------------------------------------------------------

preparation of the input file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


   .. figure:: images/simspot.png
     :width:  300
     :height: 300
     :align: right


   * The preliminary step consists in doing a dry run to discover
     the structure of the elastic tensor.
     At this stage you just specify some spots to be fitted so that
     tds2el  knows that you are going to fit something  ::


          DO_ELASTIC=1
          load_from_file= "harvest_spots.h5"
          save_to_file  =  None
          fitSpots = True

     tds2el will stop, in this dry run, because we have not yet provided 
     the elastic constants, but before finishing will print on the screen
     the structure of the elastic tensor that it finds by simmetry operations ::
    

           GET ELASTIC SYMMETRY 
            1.000000 A11 | 1.000000 A12 | 1.000000 A13 |              |              |             
            ---------------------------------------------------------------------------------------
            1.000000 A12 | 1.000000 A22 | 1.000000 A23 |              |              |             
            ---------------------------------------------------------------------------------------
            1.000000 A13 | 1.000000 A23 | 1.000000 A33 |              |              |             
            ---------------------------------------------------------------------------------------
                         |              |              | 1.000000 A44 |              |             
            ---------------------------------------------------------------------------------------
                         |              |              |              | 1.000000 A55 |             
            ---------------------------------------------------------------------------------------
                         |              |              |              |              | 1.000000 A66


                          
     And the program stops if CGUESS is not provided. CGUEES can now be created with initial values, minima, maxima
     for A11,A12   and all the elastic constants that have been just printed in the dry run.
     Mind that the obtained structure could be considerably more complicated if the aAA,aBB.... differ more than a given tolerance
     from the nominal values.
     Correct them in this case, or alternatively change the tolerance which is so far hard-coded in tds2el.py ::

                 if abs(trace_matrici[ i*6+j ])>1.0e-6:

   *  IMPORTANT NOTICE : the alignement convention is the one of GIACOVAZZO where one align :math:`{\bf a^*}`
      instead of :math:`{\bf a}` along the X axis. For this reason some elastic constant may have different indexes than usual :
      A14 may become -A15 and so on.
       

   *  To calculate the simulated diffused scattering you have to enter the values of elastic constants.
      You do this by setting the dictionary CGUESS. Here an example to get a simulation.  ::


             ###########################
             #
             #  Temperature and density
             #  just in case
             #
             #   At room temperature you can simultaneously scale
             #   frequencies and intensity because of the bose factor getting linear in T.
             #   Our fit  is in this case insensitive to absolute scale
             #   because the intensity factors are absorbed by
             #   the fitting procedure. This because we dont have an absolute
             #   scale of the absolute intensity : we dont know the exact shape
             #   of the sample, beam profile,  and many other factors
             Temperature = 300.0  # Kelvin  330 is the default value
             density = 2.71     # g/cc    2.71 is the default value 
             ### 

             DO_ELASTIC=1
             load_from_file= "harvest_spots.h5"
             save_to_file  =  None
             fitSpots = True
             CGUESS={"A11":  [  254.307231832 , -1000  , 10000]  ,
                "A22":  [  254.307231832 , -1000  , 10000]  ,
                "A33":  [ 144.467519525  , -1000  , 10000]         ,
                "A44":  [ 120.580683913  , -1000  , 10000]         ,
                "A55":  [ 169.691768154  , -1000  , 10000]         ,
                "A66":  [ 169.691768154  , -1000  , 10000]         ,
                "A12":  [ -7.53771283211 , -1000  , 10000]         ,
                "A13":  [ 46.561752961   , -1000  , 10000],
                "A23":  [ 46.561752961   , -1000  , 10000]
             } 
             doMinimisation=0
             visSim=1
             testSpot_beta = 0.03
             testSpot_Niter = 100 
             testSpot_N = 128
             if True:   # to fit just one spot
                 collectSpots = [[-2,-2,1]  ]
             else:
                 file_maxima = "tagged_newpeaks.p" # or all the spots, 
                 # The visualisation is not so optimised as the fit of elastic constants
                 # and runs on CPUs



      * CGUESS is the dictionary with initial values, minimum, maximum

    
 
      * doMinimisation=0  desactivate the optimisation ( the Fit to the data ). 

      * REPLICA=0   if 1 collectSpots will be replicated by simmetry.

      * visSim=1 activates the visualisation ( if you do a simulation it is for looking at it )
        which activates the interpolation around all the fitted spots.
        Visualisation is not so optimised, while fitting of the TDS is very oiptimised and runs on GPU.
	You can therefore when you set do_Minimisation=0, desactivate visSim, for the fit,
        outherwise you may have to wait some minutes more after the fit
        waiting for the visualisation to be calculated and written, and then rerun without minimisation,
        with visSim=1 and on a reduced set of spots.
            
      * How interpolation is done when activated by visSim=1:
	we use a formula which works both for the oversampled and under-sampled cases.
	We find the minimum of an object function which minimised a fidelity term, given by 
	the squared norm of the difference between the data  :math:`{bf y}` , and the interpolated data,
	obtained interpolation the solution :math:`{bf x}` given on the cubic grid, plus
	a regularisation term, multiplied by  :math:`\beta`, which is given
	by the squared norm of the gradient.
	The solution :math:`{bf x}` is expressed mathematically as :
            
        .. math:: {\bf x^{*}} = argmin_{\bf x} \left( \frac{1}{2} \left\| Interp({\bf x}) - {\bf y} \right\|^2 + \beta \left\| \nabla {\bf x} \right\|^2 \right)  

        where :math:`\beta` is given by the parameter *testSpot_beta* and the optimisation is done through *testSpot_Niter*
        iteration of FISTA algorithm.  Full convergence is not needed, we are just doing visualisation: choosing a low number of iteration 
        allows highlight the data granularity when data are more sparse then interpolation points. The relevant parameters are   testSpot_beta testSpot_Niter.
 
      * How you can look at the obtained result ::

               pymca -f simtests.h5

      * PERFORMANCE ISSUE for VISUALISATION:
        tds2el calculates simulated data at all the data harvested data points(which are replicated by simmetry).
        Then for each one of them, an iterative interpolation is done for the cubic grid. It is this last operation 
        that takes time. Therefore you might :
 
            *  select a not too long list of spots



DOING THE FIT
-----------------------------------------------------------------

preparation of the input file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    * The input file is basically the same as for simulating the spots
      but with doMinimisation set to True and, if you dont want to wait for subsequent interpolations,
      visSim=0. The obtained parameters will be printed at the screen once the fit has converged.
      They are also written on file *parametri.txt*  ::

             DO_ELASTIC=1
             load_from_file= "harvest_spots.h5"
             save_to_file  =  None
             collectSpots = [[-2,-1,-2],[-1,0,4],[0,1,-8] ,[2,0,-2], [-2,-1,-2],  ]
             CGUESS={"A11":  [  254.307231832 , -1000  , 10000]  ,
                     "A33":  [ 144.467519525  , -1000  , 10000]         ,
                     "A44":  [ 120.580683913  , -1000  , 10000]         ,
                     "A66":  [ 169.691768154  , -1000  , 10000]         ,
                     "A13":  [ -7.53771283211 , -1000  , 10000]         ,
                     "A15":  [ 46.561752961   , -1000  , 10000]
                     }

             doMinimisation=1
             visSim=0

    * PERFORMANCE ISSUE : very important

        you are going to do a lot of tests before producing an useful result.
        On the other hand the data amount is huge. TO go really fast
        use, in your conf_file ::

           nred=10

        or higher. One point over nred only will be used. This might eventually accelerate
        also the interpolation because you have less data points, but in this case you 
        will have more coarse granularity and you might need a higher beta.
        If you dont set nred it is nred=1 by default.

    * ANOTHER PERFORMANCE ISSUE :

        The simulation calculation are done with openCL, by default on GPU.
        On a non GPU machine use ::

             devicetype="CPU"

        to use all available cores.


    * Convergenge control :

             ftols is by default=0.00001.
             If you set ftol=0.001, in conf_file, convergence condition will be satisfied  sooner.



Fine retuning of angles r1 r2 r3 and cell parameters a,b,c
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    THIS PART IS NOT SO USEFUL IF YOU ARE RECENTERING (Option centering_file ="harvest_spots.h5").
    The fit is done not only on the elastic constants ( and a pro-spot factor
    plus baseline ) but also on Alignement parameters  r1 r2 r3 and cell parameters a,b,c.
    ( For more informations about alignement geometry 
    See thesis of Mathias MEYER *Construction of a multi-purpose X-ray CCD detector and its implementation on a 4-circle kappa goniometer*)
    Université de Lausanne

   As default these Alignement corrections are constrained between 0- and 0+

   Their value is given by the internal A_variables symbol whose default is ::

     A_variables = [ minimiser.Variable(0.0,0.0,0.0) for i in range(6+4+2+2) ]  # (6+4+2+2) parameters corresponding to 
                                                                                #          r1,r2,r3, AA, BB, CC ,
                                                                                #          omega, alpha, beta, kappa
                                                                                #          d1,d2,
                                                                                #          det_origin_X, det_origin_Y
                   

   These are variation variables which are added to their corresponding parameter.
   You can free (or not) the optimisation for these variables by opening (or closing) their min-max interval ::

     A_variables = ( 
   	       [ minimiser.Variable(0.0,-1.0e-1,1.0e-1) for i in range(3) ]
	       +
	       [ minimiser.Variable(0.0,-3.0e-2,3.0e-2) for i in range(3) ]
               + [      minimiser.Variable(0.0,0.0,0.0) for i in range(8)                                              ] 
	      )

   You can add the above lines to the conf_file file 
   The optimised value is the correction ( 0 means no correction)
   and the final value is printed at the end of the fit, on screen and also in file *parametri.txt*
   The following lines allow to open or close selected group of intervals with less hand editing   ::

    activ =1*0
    activ1=1*0
    activ2=1*0
    activ3=1*0
    A_variables = (
    		[ minimiser.Variable(0.0,-1.5*activ1  ,1.5*activ1 ) for i in range(1) ]+  # r1,
    		[ minimiser.Variable(0.0,-1.5*activ1 ,1.5*activ1 ) for i in range(1) ]+  # r2
    		[ minimiser.Variable(0.0,-1.5*activ1 ,1.5*activ1 ) for i in range(1) ]+  # r3,
    		[ minimiser.Variable(0.0,-0.1*activ2 ,0.1*activ2 ) ,-1,   minimiser.Variable(0.0,-0.1*activ2 ,0.1*activ2 )    ]+  #  AA, BB, CC
    		[ minimiser.Variable(0.0,-2.0*activ3 ,2.0*activ ) ,
		  minimiser.Variable(0.0,-2.0*activ3,2.0*activ3) ,
		  minimiser.Variable(0.0,-2.0*activ3,2.0*activ3) ,
		  minimiser.Variable(0.0,-2.0*activ3,2.0*activ3) ,
		  ]+        # omega, alpha, beta, kappa
                      [         minimiser.Variable(0.0,-0.2*activ,+0.2*activ) for i in range(2)  ] +   #                      d1,d2,
                      [         minimiser.Variable(0.0,-5*activ,+5*activ) for i in range(2)  ]       #       det_origin_X, det_origin_Y
		      )		



"""
)



old = """
Global Visualisation of Volume
------------------------------

   this is in general the first step to do. it is used to write on a hdf5 file
   a cubic interpolation of the whole volume. Once the volume is written you can explore it 
   with PyMca. An auxiliary script will then allow you to transform the 3D coordinate of a spot
   in the volume, to hkl. This information can be used to select a collection of spots to fit,
   of the hkl of a given spot that you want to zoom.

preparation of the input file for around-the-spots interpolation (Visualisation)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 

   .. figure:: images/testspots.png
     :width:  300
     :height: 300
     :align: right

   * In this case the last section of conf_file, will be instead  ::


        DO_ELASTIC=1
        load_from_file= "harvest_spots.h5"
        save_to_file  =  None
        testSpot_beta = 0.03
        testSpot_Niter = 100 
        testSpot_N = 128
        testSpot = [-2,-1,-2]
        MaxNtest = 1000


   *  testSpot_beta and testSpot_Niter control the interpolation inversion iterative procedure as 
      explained above.

   * testSpot_N  determines how many points in each dimension the cubic grid will have.
     The cubic grid is made large enough to contain, spot by spot, all the harvested points.

   * testSpot = [-2,-1,-2]  indicates which spot will be analysed : ALL the SIMMETRY REPLICA of this 
     spot will be interpolated : if this is too much you can reduce MaxNtest to go faster

   * How you can look at the obtained result :

       the result can be looked at by opening the hdf5 file. You can use PyMca ::

          pymca -f tests.h5

       There is a data-group per interpolated spots.
  

Some details about the inners of the Fit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  * Geometrical and polarisation corrections are now switched off, but can be easily 
    re-activated as an option

  * More importantly each spot calculation has a prefactor which is automatically adjusted during each step of the fitting procedure
    to minimise the quadratic distance between calculation and experiment.

  * A single hkl may be represented by more than one spot if the scan sweep a Bragg peak more than one time. They are treated each one 
    with its own factor.

  * Moreover Each spot calculation is added with its own constant background , which is adjusted automatically together with the factor,
    and this background is constrained to be definite positive.

  * Study file Fit.py for more details.

"""

try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")


# import sift
from math import pi, sin, cos
try :
    import fabio
except :
    print( 'Warning : fabio module could not be initialised')

import numpy as np
import sys, time
__version__ = 0.7

def get_DWs(DW_file,   cellVects  ):

    result = {}

    h5 = h5py.File(DW_file,"r")

    result["atomNames"] = h5["/atomNames"]
    result["atomReducedPositions"] = h5["/atomReducedPositions"][:]

    redpos = result["atomReducedPositions"]

    result["atomPositions"]  = np.dot(redpos, cellVects)
    
    return result

def Theta(Q, Lambda  ):
    dum = numpy.sqrt( numpy.sum(Q*Q, axis=1)) * Lambda/2.0/math.pi/2.0
    return numpy.arcsin( dum  )

def Get_dwscatt_facts(qtots,  DW_infos,   params , hkls_list, Npts_list):
    # GUARDARE IL 2 PI
    qtots[:]=qtots*2*math.pi
  
 
    positions  =    DW_infos ["atomPositions"]
    

    atomNames = DW_infos["atomNames"]
    tf0 = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
     
    factors = numpy.exp( -   1.0j *    np.tensordot( qtots,  positions, axes=([-1],[-1]) )           )   # nqs x Natoms
    
    scatterers = [  tf0.Element(aname  ) for aname in  atomNames ]
    sctfacts   = numpy.array( [ scatterer.f0Lambda(params.lmbda*numpy.ones( qtots.shape[0] ), Theta( qtots  , params.lmbda  )   )
                                for scatterer in scatterers ]  ).astype(numpy.complex64)
    sctfacts = sctfacts.T # adesso scattFactors_site_q est [nqs ,Natoms   ]

    result =(factors *  sctfacts ).sum(axis=-1)  # a adesso est [ nqs]
    
    return result


#--------------------------------------------------------------------------------------------------------
# Rotation, Projection and Orientation Matrix
#--------------------------------------------------------------------------------------------------------

# e' una rotazione degli assi non delle coordinate
#  va bene cosi perche si ruota il campione quindi il raggio controruota nello spazio K
def Rotation(angle, rotation_axis=0):
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val

#--------------------------------------------------------------------------------------------------------

# questa rispetta la convenzione Ma per le cordinate   Rotation(45.0,"z") != Arb_Rot(45.0,[0,0,1])
def Arb_Rot(angle, rot_axis):
    angle = np.radians(angle)
    assert(len(rot_axis)==3)
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat

#--------------------------------------------------------------------------------------------------------

def Mirror(mirror_axis):
    x,y,z = {"x":[-1,1,1], "y":[1,-1,1], "z":[1,1,-1],0:[-1,1,1], 1:[1,-1,1], 2:[1,1,-1]}[mirror_axis]
    mat = np.array([[x,0,0],[0,y,0],[0,0,z]])
    return mat 

#--------------------------------------------------------------------------------------------------------

def ascii_gen2operator(generator, cell_vects):
    print( generator)
    I = np.identity(3)
    gendict = {  '1':I,
                 '1~':-I,
                 '2(x)':Arb_Rot(180,[1,0,0]),
                 '2(y)':Arb_Rot(180,[0,1,0]),
                 '2(z)':Arb_Rot(180,[0,0,1]),
                 '4(x)':Arb_Rot(90,[1,0,0]),
                 '4(y)':Arb_Rot(90,[0,1,0]),
                 '2(110)':np.array([[0,1,0],[1,0,0],[0,0,-1]]),
                 '3(z)':Arb_Rot(120,[0,0,1]),
                 '3(111)':np.array([[0,1,0],[0,0,1],[1,0,0]]),
                 '4(z)':Arb_Rot(90,[0,0,1]),
                 '4(z)~':-I*Arb_Rot(90,[0,0,1]),
                 'm(x)':Mirror(0),
                 'm(y)':Mirror(1),
                 'm(z)':Mirror(2),
                 'm(100)':  np.array([[-1,1,0],[0,1,0],[0,0,1]])   ,
                 'm(010)':  np.array([[1,0,0],[1,-1,0],[0,0,1]])   ,
                 'm(110)':  np.array([[0,-1,0],[-1,0,0],[0,0,1]])  
          }
    mat = gendict[generator]
    pos = generator.find("(")
    if pos!=-1:
        if "1" in generator[pos:]:

            toRec = np.array( [  tok  for tok in cell_vects ]  )
            fromRec = np.linalg.inv(toRec)
            mat = np.dot( fromRec  ,   np.dot(mat, toRec  )  )
            
            # basis = np.array( [  tok/np.sqrt( (tok*tok).sum())    for tok in cell_vects ]  )
            # mat = np.dot( basis.T  ,   np.dot(mat, np.linalg.inv(basis.T)   )  )
            
    np.savetxt(sys.stdout,mat,fmt="%e")
    print( " " )
    return mat

#--------------------------------------------------------------------------------------------------------

def existing_combination(candidate_op,op_list,sigma):
    for op in op_list:
        absdiff = abs(candidate_op - op)
        if absdiff.sum()<sigma:
            return True
    return False
    
#--------------------------------------------------------------------------------------------------------

def genlist2oplist(genlist, cellvects, dtype=np.float32):
    base_op_list = [ascii_gen2operator(gen, cellvects ) for gen in genlist]
    I = np.identity(3)
    combined_op_list = [I]
    while 1:
        new_add_in_cycle = False
        for op in combined_op_list:
            for base_op in base_op_list:
                newop = np.dot(op,base_op)
                if not existing_combination(newop,combined_op_list,0.01):
                    combined_op_list.append(newop)
                    new_add_in_cycle = True
        if not new_add_in_cycle:
            break
    return np.array(combined_op_list).astype(dtype)

#--------------------------------------------------------------------------------------------------------

def Prim_Rot_of_RS(omega, phi, kappa, alpha, beta, omega_offset):
    # """ 
    # Primary rotation of reciprocal space. 
    # Omega, kappa, phi are the nominal values of the angle
    # """
    # omega=-omega
    # phi  = -phi
    # kappa = -kappa
    # alpha=-alpha
    # beta=-beta

    tmp = Rotation(omega + omega_offset, 2)
    tmp = np.dot(tmp, Rotation(alpha, 1))
    tmp = np.dot(tmp, Rotation(kappa, 2))
    tmp = np.dot(tmp, Rotation(-alpha, 1))
    tmp = np.dot(tmp, Rotation(beta, 1))
    tmp = np.dot(tmp, Rotation(phi, 2))
    tmp = np.dot(tmp, Rotation(-beta, 1))
    return tmp

#--------------------------------------------------------------------------------------------------------

def DET(theta, theta_offset, d2, d1):
    # """ 
    # Rotation matrix for the detector 
    # theta is the nominal theta value and d1,D2 are the tilts of detector
    # """
    tmp = Rotation(theta + theta_offset, 2)
    tmp = np.dot(tmp, Rotation(d2, 1))
    tmp = np.dot(tmp, Rotation(d1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------    

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    # r1=-r1
    # r2=-r2
    # r3=-r3
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------

def P0(dist, b2):
    # """ Primary projection of pixel coordinates (X,Y) to the reciprocal space. """
    B = Rotation(b2, 1) # Beam tilt matrix
    p0 = np.dot(B, [ dist, 0, 0 ])
    return p0

#--------------------------------------------------------------------------------------------------------
# Corrections
#--------------------------------------------------------------------------------------------------------

MD0 = np.array([[1, 0], [0, 1]  ], dtype=np.int32)
MD1 = np.array([[-1, 0], [0, 1] ], dtype=np.int32)
MD2 = np.array([[1, 0], [0, -1] ], dtype=np.int32)
MD3 = np.array([[-1, 0], [0, -1]], dtype=np.int32)
MD4 = np.array([[0, 1], [1, 0]  ], dtype=np.int32)
MD5 = np.array([[0, -1], [1, 0] ], dtype=np.int32)
MD6 = np.array([[0, 1], [-1, 0] ], dtype=np.int32)
MD7 = np.array([[0, -1], [-1, 0]], dtype=np.int32)

#--------------------------------------------------------------------------------------------------------
# Parameter Class
#--------------------------------------------------------------------------------------------------------


def read_configuration_file(cfgfn):
	# """
	# cfgfn is the filename of the configuration file.
	# the function return an object containing information from configuration file (cf inside cfg file).
	# """
	
	try:
            s=open(cfgfn,"r")
	except:
		print( " Error reading configuration file " ,  cfgfn			)
		exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
		print( "*** print_exception:")
		traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                              limit=None, file=sys.stdout)
		raise Exception
	class Parameters():
            A_variables = [ minimiser.Variable(0.0,0.0,0.0) for i in range(6+8) ]  # r1,r2,r3, AA, BB, CC ,
            # omega, alpha, beta, kappa,
            # d1,d2, det_origin_X, det_origin_Y
            A_names     = [  "r1","r2","r3", "AA", "BB", "CC",
                             "omega", "alpha", "beta", "kappa",
                             "d1","d2", "det_origin_X", "det_origin_Y"
                ]
            devicetype="GPU"
            VINTERP=False
            testSpot=None
            fitSpots=False
            REPLICA=0
            nred=1
            ftol=0.00001
            threshold=1.0e200
            badradius=0.0
            airscattering=None
            airscattering_debug = 0
            airscattering_factor=1.0
            airscattering_debug_dir = "./"
            shiftW=0.0
            double_exclusion = 0
            powder_width     = 0.0
            avoid = None
            DELTA = None
            
            show_hkls=0
            startfrom=None
            normal_to_pol = None
            cpt_corr = None
            REPLICA_COLLECTION = 0
            centering_file=None
            start_phi = 0
            increment = None
            collectSpots=None
            file_maxima = None
            MaxNtest = 1000
            doMinimisation = 0
            visSim=0
            Temperature = 300.0
            density     = 2.71
            USEQUAT = False
            eXp = None
            file4sim = "simtest.h5"
            personal_postfix=""
            exec(s)
	cfg = Parameters()
	s.close()
	return cfg

def mag_max(l):
    # """
    # l is a list of coordinates
    # """
    return max(np.sqrt(np.sum(l * l, axis= -1)))
    
#--------------------------------------------------------------------------------------------------------


def   get_Qfin( Q0,  
               paramsExp):

    R = Prim_Rot_of_RS(paramsExp.omega, paramsExp.phi, paramsExp.kappa, paramsExp.alpha, paramsExp.beta, paramsExp.omega_offset)
    if paramsExp.USEQUAT == False:
        U = Snd_Rot_of_RS(paramsExp.r1, paramsExp.r2, paramsExp.r3)
    else:
         U = r2quat.quat2M( (paramsExp.r1, paramsExp.r2, paramsExp.r3) )
         
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    return Qfin

############################################################################################
## ASSI CRISTALLOGRAFICI

def modulus(a):
    return np.sqrt((a*a).sum())

def arcCos(   a,b   ):
    c = (a*b).sum()
    c=c/modulus(a)
    c=c/modulus(b)
    return np.arccos(c)
    

# def getCellVectors(param):

#     xa = np.array([0.0,1.0,0.0 ])
#     xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0,-1.0 ]))         ,     xa      ) 

#     xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 1.0,0.0,0.0 ]))         ,      np.array([0.0,1.0,0.0 ])     ) 
#     print " ora " , xc 

#     sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
#     sa = sa/math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 )
#     omegaa = math.asin( sa  ) 
#     xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([-1.0,0.0,0.0 ]))      ,    xc   ) 

#     xa  = xa*param.AA
#     xb  = xb*param.BB
#     xc  = xc*param.CC
#     cellvectors = np.array([ xa,xb,xc ] )
#     return cellvectors


# If the number is odd returns -1
# If two numbers are equal retorns zero
def ciclicity(a,b,c):
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1   

def getCellVectors(param):

    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0,  1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 
    print( " ora " , xc )

    sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
    sa = sa/(math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    
    xa  = xa*param.AA
    xb  = xb*param.BB
    xc  = xc*param.CC
    cellvectors = np.array([ xa,xb,xc ] )


    
    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   )
 
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( -betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/(math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar

    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume

    # Brillvectors[:] *=-1
    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")


def get_elastic_simmetry(all_ops):
    components = [(0,0),(1,1),(2,2),(1,2),(2,0),(0,1)]
    comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
                  ,(2,1):3,(0,2):4,(1,0):5}  
    res={}
    transf = []
    for op in all_ops:
        # np.savetxt(sys.stdout,op,fmt="%e")
        # print( "")

        # if   op[2,2]!=1.0     or  abs(np.linalg.det(op)-1)>1.0e-5   :
        #     continue

        # if  abs( op[0,0]-1.0) >1.0e-3  and  abs( op[0,0]+0.5) >1.0e-3:
        #     continue

        mat = np.zeros([6,6],"d")
        for i, (k,l) in enumerate(components):
            dispatch_k = op[:,k]
            dispatch_l = op[:,l]
            for K,cK in enumerate(dispatch_k ) :
                for L,cL in enumerate(dispatch_l ) :
                    fact=1.0
                    if (K!=L):
                        fact=fact/2.0
                    if (k!=l):
                        fact=fact*2.0
                    mat[comppos[(K,L)], i ] += cK*cL * fact
        transf.append(mat)

    ricerca = [(i,i) for i in range(6)]
    for i in range(5):
        for j in range(i+1,6):
            ricerca.append((i,j))

    # NMC=100000
    for (i,j) in ricerca[:]:
        matrici = np.zeros([6,6])
        matrici[i,j]=1
        matrici[j,i]=1
        trace_matrici =  np.zeros([6,6],"d")
        for rot in transf:
            add =  np.tensordot(  matrici , rot,([-2],[-1]))
            add =  np.tensordot(  add , rot,([-2],[-1]))
            trace_matrici[:] +=  add
        trace_matrici = (np.reshape(trace_matrici, [36]))

        
        for vect,I in res.values():
            trace_matrici -= vect*trace_matrici[I]

        if abs(trace_matrici[ i*6+j ])>1.0e-5:
            vect =  trace_matrici/trace_matrici[ i*6+j ]
            I =  i*6+j
            for vv,II in res.values():
                n0 =  np.less(1.0e-5, abs(vv)).sum()
                nuovo  =  vv - vect * vv[I]
                n1 =  np.less(1.0e-5, abs(nuovo)).sum()
                if n1<n0:
                    vv[:]=nuovo
            res[(i,j)]=vect, I


    displays = [[[] for j in range(6)] for i in range(6) ]


    ndisplaylines=0
    maxlen=0
    for vect,II in res.values():
        i,j = II/6 +1,  II%6 +1
        for K,v in enumerate(vect):
            if(abs(v)>1.0e-5):
                k,l = K/6 ,  K%6 
                add = "%f A%d%d"%(v,i,j)  
                displays[k][l].append(    add     )
                # displays[l][k] = displays[k][l]
                ndisplaylines = max(  ndisplaylines, len(  displays[k][l]))
                maxlen = max( maxlen, len(add)   ) 
           

    for i in range(6):
        for il in range(ndisplaylines):
            linea=""
            for j in range(6):
                add=""
                if il <  len(displays[i][j]):
                    add= displays[i][j] [il]
                add = add+" "*( maxlen-len(add))
                if  j<5:
                    add = add +" | "
                linea = linea + add
            print( linea)
        if i < 5:
            print( "-"*(len(linea)))

    nomi_variabili=[]
    vettori_variabili=[]
    print( ricerca)
    for i,j in ricerca:
        if res.has_key((i,j)):
            nome = "A%d%d"%(i+1,j+1)
            vettore,I =  res[(i,j)]

            nomi_variabili.append(nome)
            vettori_variabili.append(vettore)
        

    print( " THE ELASTIC TENSOR VARIABLES ARE ")
    print( nomi_variabili)
    return nomi_variabili,vettori_variabili


def writeVolumeOnFile(volume, filename,h,k,l,i,II, cellvectors,corner, axis, isexp=1):
    
    if II==0:
        h5=h5py.File( filename , "w")
    else:
        h5=h5py.File( filename , "r+")


    if isexp:
        if("exp" not in h5):
            h5 .create_group("exp")
        subdir = "exp/"+("spot_%d%d%d_%d/"%(h,k,l, i) ) 
    else:
        if("sim" not in h5):
            h5 .create_group("sim/")
        subdir = "sim/"+("spot_%d%d%d_%d/"%(h,k,l, i) ) 
        


    if( subdir not in h5):
        h5 .create_group(subdir)

    h5[subdir+"/volume"]  = volume
    h5[subdir+"/cellvectors"]  = cellvectors
    h5[subdir+"/corner"]       = corner
    h5[subdir+"/axis"]         = axis
    h5.flush()
    h5.close()
    
    h5 = None
        


def get_Q0(dim1,dim2, paramsExp, ij=None) :
    p0 = P0(paramsExp.dist, paramsExp.beam_tilt_angle)
    #        if paramsExp.orientation_codes ==1111:
    if paramsExp.orientation_codes>=0 :
        MD = [MD0,MD1, MD2, MD3, MD4 , MD5, MD6 , MD7 ][paramsExp.orientation_codes]
    else :
        MD =  MD4
    # else:
    #     raise Exception," orientazione sconosciuta"
    Q0 = np.zeros((dim1, dim2, 3), dtype=np.float32)
    if ij is None:
        P_total_tmp = np.zeros((dim1, dim2 , 3), dtype=np.float32)
    else:
         P_total_tmp = np.zeros((1, 1 , 3), dtype=np.float32)
    P_total_tmp[:, :, 0 ] = -paramsExp.dist
    # P_total_tmp[:, :, 1 ] = (np.arange(dim1) - ( dim1 )/2.0)[:,None]
    # P_total_tmp[:, :, 2 ] = (np.arange(dim2) - ( dim2 )/2.0)[None,:]
    # A,B = dim1 - 733.40078, dim2 -   11.71199\
    A,B =  paramsExp.det_origin_X  ,     paramsExp.det_origin_Y
    if ij is None:
        P_total_tmp[:, :, 1 ] = (np.arange(dim2) -   A   )[None,:]
        P_total_tmp[:, :, 2 ] = (np.arange(dim1) -   B    )[:,None]
    else:
        # P_total_tmp[:, :, 2 ] = (np.arange((ij%dim2), (ij%dim2)+1,1) -   B    )[None,:]
        # P_total_tmp[:, :, 1 ] = (np.arange((ij/dim2), (ij/dim2)+1,1  ) -   A    )[:,None]
        P_total_tmp[:, :, 1 ] = (np.arange((ij%dim2), (ij%dim2)+1,1) -   A    )[None,:]
        P_total_tmp[:, :, 2 ] = (np.arange((ij/dim2), (ij/dim2)+1,1  ) -   B    )[:,None]

        
    P_total_tmp[:, :, 1:3]=  (np.tensordot(P_total_tmp[:, :, 1:3]  * np.array(paramsExp.pixel_size)    ,MD , axes=([2], [1]))) 
    P_total_tmp = np.tensordot(P_total_tmp, DET(paramsExp.theta, paramsExp.theta_offset, paramsExp.d2, paramsExp.d1), axes=([2], [1]))
    P_total_tmp_modulus = np.sqrt(np.sum(P_total_tmp * P_total_tmp, axis= -1))
    Q0_tmp = P_total_tmp.T / P_total_tmp_modulus.T
    Q0 = ((Q0_tmp.T + p0 / paramsExp.dist) / paramsExp.lmbda).astype(np.float32)
    v_in  = p0 / paramsExp.dist
    v_out =  Q0_tmp.T

    ## todo : rotate also nomal_to_pol as in P0()
    if paramsExp.normal_to_pol is not None :
        p0xn_out  = (np.cross(p0, paramsExp.normal_to_pol, axis=0)*v_out).sum(axis=-1)
        n_out     =    (v_out*paramsExp.normal_to_pol ).sum(axis=-1)
        correction =  paramsExp.pol_degree*(1- p0xn_out*p0xn_out) +(1-paramsExp.pol_degree)*(1-n_out*n_out)
    else:
        correction = numpy.ones(   Q0.shape[:-1]  ,  "f")

    if paramsExp.cpt_corr is not None:
        print( 'Computation of Flux Density and Parallax Correction ...')
        C3 = (paramsExp.dist ** 3 / (  (P_total_tmp.T* P_total_tmp.T).sum(axis=-1)   ) ** (3/2.)).astype(np.float32)
        correction = correction *C3
        
    # P0xn = np.cross(p0, paramsExp.normal_to_pol, axis=0)
    # P0xn_modulus = np.sqrt(np.sum(P0xn * P0xn, axis= -1))
    # POL_tmp = paramsExp.pol_degree * (1 - ((P0xn * P_total_tmp).sum(axis= -1) / (P0xn_modulus * P_total_tmp_modulus)) ** 2)
    # POL_tmp +=(1 - paramsExp.pol_degree) * (1 - ((paramsExp.normal_to_pol * P_total_tmp).sum(axis=-1) / P_total_tmp_modulus) ** 2)
    
    return Q0, -p0 / paramsExp.dist / paramsExp.lmbda   , correction



# hkls, Npts, DQs, Datas, ac_phis, ac_ijs = Raggruppa(    hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py ) 

def  Raggruppa(  paramsExp,  hkls, Npts, DQs, Datas, ac_phis, ac_ijs,
                 tuttiPhi_py, tuttiIJ_py, tuttiDist_py ) :
    res_hkls  =[]
    res_Npts  =[]
    res_DQs   =   DQs
    res_Datas =   Datas
    res_ac_phis = []
    res_ac_ijs  = []

    start = 0
    for N, (h,k,l)  in zip(Npts, hkls):
        end = start+ N

        view_phis    =    tuttiPhi_py[start:end]
        view_tuttiIJ =    tuttiIJ_py [start:end]
        view_Dist    =    tuttiDist_py [start:end]
        view_DQs     =    DQs    [start:end,:3]
        view_Datas    =    Datas   [start:end]



        ordine = numpy.argsort(view_phis )
        
        view_phis[:]    = view_phis[ordine]
        view_tuttiIJ[:] = view_tuttiIJ[ordine]
        view_Dist[:]    = view_Dist[ordine]
        view_DQs[:]     = view_DQs[ordine]
        view_Datas[:]    = view_Datas[ordine]

        salti = (view_phis[1:]- view_phis[:-1])/paramsExp.angular_step
        stacchi = numpy.where(salti>20)[0].tolist()
        stacchi.append( len( view_DQs  )) 

        pos_start = 0
        count     = 1

        for stacco in stacchi:
            res_hkls.append([ h,k,l, count ])
            res_Npts.append( stacco - pos_start    )
            imin = pos_start +  numpy.argmin( view_Dist[ pos_start : stacco ])

            res_ac_phis.append( view_phis [imin]   ) 
            res_ac_ijs .append( view_tuttiIJ[imin]    ) 

            
            pos_start = stacco
            count +=1
            
        start = end


        
    return numpy.array(res_hkls), numpy.array(res_Npts), numpy.array(res_DQs), numpy.array(res_Datas), numpy.array(res_ac_phis), numpy.array(res_ac_ijs)
            
        
#--------------------------------------------------------------------------------------------------------
# 3D Main
#--------------------------------------------------------------------------------------------------------

generators = ['1','1~','2(x)','2(y)','2(z)','2(110)','3(z)','3(111)','4(z)','4(z)~','m(x)','m(y)','m(z)']

def main3d(paramsExp  ,Filter_fname,flist,  all_ops_dp, paramsInput , DW_infos ):
     

    cellvectors, brillvectors = getCellVectors(paramsExp)

    np.savetxt(sys.stdout,brillvectors,fmt="%e")
    print( "")
    np.savetxt(sys.stdout,cellvectors,fmt="%e")
    print( "")

    maxBrill = np.max(np.sqrt(    (brillvectors*brillvectors).sum(axis=-1) ))


    if  paramsExp.load_from_file is not None and nprocs>1:
        # comm.Finalize()
        if myrank>0:
            print(  myrank, "  RITORNA " )
            return
        

    if paramsExp.load_from_file is None:

        img = fabio.open(flist[0])
       
        # time1 = time.time()
        # print( '-> t = %.2f s' % (time1 - time0))
        data = img.data
        dim1, dim2 = data.shape

        print( 'Image Dimension :', dim1, dim2)


        if paramsExp.badradius>0.0  or paramsExp.airscattering is not None:
            DIM1=1
            while DIM1<dim1*2:
                DIM1*=2
            DIM2=1
            while DIM2<dim2*2:
                DIM2*=2

            import fftw3f
            ry = numpy.fft.fftfreq(DIM1,1.0/DIM1) .astype("f")
            rx = numpy.fft.fftfreq(DIM2,1.0/DIM2) .astype("f")

            fftw_direct=  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")
            fftw_rec   =  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")
            
            fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['measure'])
            fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['measure'])
            # fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['estimate'])
            # fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['estimate'])
            
        if paramsExp.badradius>0.0:

            disco = (ry*ry)[:,None] + (rx*rx)
            disco = (numpy.less( disco, paramsExp.badradius* paramsExp.badradius   )).astype("f")

            fftw_direct[:] = disco
            fft_plan_F()
            disco_reci = numpy.array(fftw_rec)

        if paramsExp.airscattering is not None:

            dist = (ry*ry)[:,None] + (rx*rx)
            dist = numpy.sqrt( dist ).astype("f") * paramsExp.pixel_size

            if not os.path.exists(paramsExp.airscattering ):
                #  (550.0*numpy.exp(-1.26*(dist)**0.348+1.4/(dist**1)))          
                diffu =eval(paramsExp.airscattering)
            else:
                diffusione = numpy.loadtxt( paramsExp.airscattering  )
                diff_xp = diffusione[:,0]
                diff_fp = diffusione[:,1]

                diffu = numpy.interp(numpy.ravel(dist), diff_xp, diff_fp )
                diffu.shape = dist.shape

            if myrank==0:
                
                if not os.path.exists(paramsExp.airscattering_debug_dir):
                    os.makedirs(paramsExp.airscattering_debug_dir)
                EdfFile.EdfFile(paramsExp.airscattering_debug_dir+"/"+"diffu.edf","w+").WriteImage({}, diffu)
            comm.Barrier()

            fftw_direct[:] = paramsExp.airscattering_factor*diffu
            fft_plan_F()
            print( " DIVIDO PER ==", fftw_rec.size)
            air_reci = numpy.array(fftw_rec)/fftw_rec.size

            if myrank==0:
                EdfFile.EdfFile(paramsExp.airscattering_debug_dir+"/"+"distR0.edf","w+").WriteImage({}, air_reci.real )
                EdfFile.EdfFile(paramsExp.airscattering_debug_dir+"/"+"distR1.edf","w+").WriteImage({}, air_reci.imag )

        if paramsExp.normal_to_pol is not None :
            paramsExp.normal_to_pol = np.array(paramsExp.normal_to_pol).astype(np.float32)# be carefull of the list type alla should be nparray

        Q0, Kin , correction= get_Q0(dim1,dim2,paramsExp) 

        corners = np.array([Q0[0, 0, :], Q0[0, dim2 - 1, :], Q0[dim1 - 1, 0, :], Q0[dim1 - 1, dim2 - 1, :]])
        Qmax = mag_max(corners) # maximal magnitude for reciprocal vector corresponding to the corners pixels
        print( 'Qmax = ', Qmax)


        # if paramsExp.cpt_corr :
        #     print( 'Computation of Polarisation Correction ...')

        #     P0xn = np.cross(p0, paramsExp.normal_to_pol, axis=0)
        #     P0xn_modulus = np.sqrt(np.sum(P0xn * P0xn, axis= -1))
        #     POL_tmp = paramsExp.pol_degree * (1 - ((P0xn * P_total_tmp).sum(axis= -1) / (P0xn_modulus * P_total_tmp_modulus)) ** 2)
        #     POL_tmp +=(1 - paramsExp.pol_degree) * (1 - ((paramsExp.normal_to_pol * P_total_tmp).sum(axis=-1) / P_total_tmp_modulus) ** 2)

        # else :
        #     print( 'Computation of Polarisation Correction : Canceled')

        # POL_tmp = POL_tmp.astype(np.float32)

        # if paramsExp.cpt_corr:
        #     print( 'Computation of Flux Density and Parallax Correction ...')
        #     C3 = (paramsExp.dist ** 3 / (paramsExp.dist ** 2 + np.sum (XY_array_tmp * XY_array_tmp, axis= -1)) ** (3/2.)).astype(np.float32)
        # else:
        #     print( 'Computation of Flux Density and Parallax Correction : Canceled')

        total = len(flist)

        print( 'Loading images filter ...')
        if(Filter_fname[-4:]==".edf"):


            fim = fabio.open(Filter_fname)
            Filter = fim.data
            # Filter=EdfFile.EdfFile(Filter_fname,"r").GetData(0).astype(np.float32)
            assert( Filter.shape ==  (dim1,dim2) ) 
        else:
            f = open(Filter_fname,'rb')
            raw_data = f.read()
            Filter = np.fromstring(raw_data, np.uint8).reshape((dim1,dim2)).astype(np.float32)

        angular_step = paramsExp.angular_step

        paramsExp.phi =0.0
        paramsExp.phi =paramsExp.start_phi
        massimo=0.0
        i=0

        hkl_max = int( Qmax*mag_max(cellvectors)+0.4999999999)

        if paramsExp.DO_ELASTIC:
            if paramsExp.avoid is not None:
                spotpicker = spotpicker_cy.PySpotPicker(hkl_max,  maxBrill* paramsExp.DeltaQ_max,   do_elastic=1, avoid=numpy.array(paramsExp.avoid,"i"))
            else:
                spotpicker = spotpicker_cy.PySpotPicker(hkl_max,  maxBrill* paramsExp.DeltaQ_max,    do_elastic=1)
        else:
            qcube = 2*Qmax
            spotpicker = spotpicker_cy.PySpotPicker( int(paramsExp.cube_dim),  Qmax    , do_elastic=0, qcube = qcube)


        nblockfl = len(flist)/nprocs+1
        nblockfl = 10

        if paramsExp.DELTA is not None:
            paramsExp_delta = copy.deepcopy(paramsExp)
            for key in paramsExp.DELTA:
                setattr(paramsExp_delta, key,  getattr(paramsExp,key)   +   paramsExp.DELTA[key]  )
            cellvectors_delta, brillvectors_delta = getCellVectors(paramsExp_delta)
        else:
            cellvectors_delta, brillvectors_delta = numpy.array([[]],"f") , numpy.array([[]],"f") 


        if paramsExp.centering_file is not None:
            if( type(paramsExp.centering_file)!= type([]) ):
                h5=h5py.File( paramsExp.centering_file  , "r")
                centering4hkl = ( h5["harvest/centering4hkl"][:]).astype("f")
                h5.close()
            else:
                centering4hkl=[]
                for icf in range(2):
                    h5=h5py.File( paramsExp.centering_file[icf]  , "r")
                    centering4hkl.append( ( h5["harvest/centering4hkl"][:]).astype("f"))
                    h5.close()
                centering4hkl  = numpy.concatenate(centering4hkl)
                

        Ntot_images = len(flist)
        if paramsExp.increment is not None:
            FFs = numpy.loadtxt(paramsExp.increment)[:,1]+1
        for iim, fname in enumerate(flist):

            if (iim/nblockfl)%nprocs != myrank:
                paramsExp.phi += angular_step
                continue


            if paramsExp.startfrom is not None and iim < paramsExp.startfrom:
                paramsExp.phi += angular_step
                continue
            
            print( 'Working on image %s' % fname)
            Newimg = fabio.open(fname)
            data = Newimg.data.astype(np.float32)*Filter
            if paramsExp.increment is not None:
                print( " INCREMENTO ", FFs[iim])
                data[:] = data*FFs[iim]
                
            # data[ numpy.equal(0, Filter) ] = numpy.nan
            # Flux = float([ t for t in string.split(Newimg.header["_array_data.header_contents"],"\n") if "Flux" in t ][0].split()[2])

            
            if paramsInput.normalisation_file is  None:
                data[:]=data / correction
            else:
                data[:]=data/ paramsInput.normalisation[ fname ]    / correction


            data[ numpy.equal(0, Filter) ] = -1



            # filtbig = numpy.less(50000.0, data)*1
            # filtlow = numpy.less( scipy.signal.medfilt(data),500 )*1
            # bads = filtbig*filtlow
            # wherebads = np.where(bads)
            # if len(bads[0]):
            #     for a,b in zip(   *( wherebads)):
            #         file = open("bads.txt","a")
            #         file.write(   "%d %d\n"%( a,b))
            #         file = None
                    


            

            # extramask = numpy.less( paramsExp.threshold  ,data)
            # data[extramask ] = numpy.nan
            # print( " SETTO ", extramask.sum() , "  to NAN ")
            
            isbragg=0
            strongs = numpy.less( paramsExp.threshold  ,data)
            if strongs.sum() :
                print( " HOT SPOTS DETECTED ,  ", fname)
                if paramsExp.badradius>0.0:
                    isbragg=1
                    fftw_direct[:]=0.0
                    fftw_direct[  : data.shape[0]  ,  : data.shape[1]] =  strongs
                    fft_plan_F()
                    fftw_rec[:] = fftw_rec * disco_reci
                    fft_plan_B()
                    size =  data.size
                    extramask = numpy.less(0.1, abs(fftw_direct [  : data.shape[0]  ,  : data.shape[1]] )/size   )
                    print( " SETTO ", extramask.sum() , "  to NAN " )
                    # data[extramask ] = numpy.nan
                    data[extramask ] = - numpy.abs(data[extramask ])
                else:
                    data[: ] = -numpy.abs(data)
                    
            Qfin    = get_Qfin(Q0, paramsExp)
            if  paramsExp.DELTA is not None:
                params_delta = copy.deepcopy(paramsExp)
                for key in paramsExp.DELTA:
                    setattr(params_delta, key,  getattr(paramsExp,key)   +   paramsExp.DELTA[key]  )
                Qfin_delta       = get_Qfin(Q0, params_delta)

                params_delta = copy.deepcopy(paramsExp)
                params_delta.phi += angular_step
                Qfin_phi          = get_Qfin(Q0, params_delta)
            else:
                Qfin_delta  = numpy.array([[[]]],"f")
                Qfin_phi    = numpy.array([[[]]],"f")
            
            Kin_fin = get_Qfin(Kin, paramsExp)

            dum_data=numpy.abs(data)
            
            massimo=max(massimo, dum_data.max())
            # print( " MASSIMO ", massimo, data.max())
            nmax = np.argmax(dum_data)
            qmax =  np.reshape(Qfin,[-1,3])[nmax]
            mmax = dum_data.max()
            print( " the maximum is ", mmax , qmax, " and hkl are " ,  (cellvectors*qmax).sum(axis=-1),"  fname ", fname," i,j ",  np.where(dum_data==mmax))

            if paramsExp.DO_ELASTIC:
                toharvest=[]
                for h,k,l in  paramsExp.collectSpots:
                    Q = h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]
                    if paramsExp.REPLICA_COLLECTION:
                        for op in all_ops_dp[:]:
                            op.astype(np.float32)
                            Qop = np.dot(op,Q)
                            h,k,l = np.round(np.dot(cellvectors, Qop)).astype("i")
                            toharvest.append( (h,k,l))
                    else:
                        toharvest.append( (h,k,l))
                if len (toharvest)==0:
                    toharvest=[[]]
                    toharvest=numpy.array(toharvest)


                if 0:  ## curiosity driven checks
                    # qqs = numpy.reshape(Q0, [-1,3])
                    mods = numpy.sqrt((Q0*Q0).sum(axis=-1))
                    modsmax = mods.max()

                    goods = numpy.less(modsmax*0., mods)*  numpy.less( 0 , Filter ) * numpy.less( data ,60 ) * numpy.less( 0,  data  ) 

                    hkls  = numpy.tensordot(  Qfin  , cellvectors , ( [ -1 ] , [-1] )   )
                    Dhkls = hkls - numpy.round( hkls )
                    Dhkls_mod = numpy.sqrt((Dhkls*Dhkls).sum(axis=-1))
                    Dhkls_mod_max = Dhkls_mod.max()

                    goodhkls =  numpy.less(Dhkls_mod_max*0.0 ,Dhkls_mod)  
                    dum2 = data[goods * goodhkls  ]
                    
                    # dum = data*Filter*(numpy.less(modsmax*0.8, mods) )
                    # qfin_ = numpy.reshape(Qfin[numpy.less(0,dum)[:,:,None]*numpy.array([True,True,True])  ], [-1,3])
                    # dum2 = dum[numpy.less(0,dum)  ]
                    # qfin_ = qfin_[numpy.less(dum2,80)[:,None]  *numpy.array([True,True,True])    ]
                    # qfin_ = numpy.reshape(qfin_, [-1,3])
                    # dum2 = dum2[numpy.less(dum2, 80)  ]
                    # hjks_  = numpy.tensordot(  qfin_  , cellvectors , ( [ -1 ] , [-1] ) )
                    # Dhkls_ = hjks_-numpy.round(hjks_)
                    # Dhkls_mod = numpy.sqrt((Dhkls_*Dhkls_).sum(axis=-1))
                    # Dhkls_mod_max = Dhkls_mod.max()
                    # dum2 = dum2[ numpy.less(Dhkls_mod_max*0.0 ,Dhkls_mod)  ]
                                        
                    if not isbragg:
                        open("moyennes.txt","a").write("%d %e\n"%(iim, dum2.mean()))
                    
                    
                    # hist, edges = numpy.histogram( dum2, 200,range=(1, 1001.0  ))
                    # name = "hist_"+fname[-10:]
                    # numpy.savetxt(name, hist)
                                                   

                if paramsExp.eXp is  None:
                    eXp = np.array([0,0,0,-100],"f")
                else:
                    eXp = np.array( paramsExp.eXp  ,"f")
                    assert(eXp.size%4==0)
                    nitems_exp = eXp.size/4
                    for i in range(nitems_exp):
                        v = eXp[i*4:i*4+3]
                        f = math.sqrt((v * v).sum())
                        eXp[i*4:i*4+3] = eXp[i*4:i*4+3]/f

                    
                if paramsExp.centering_file is not None:
                    # print( "RACCOLGO CON ",  paramsExp.centering_file, myrank)
                    spotpicker.harvest(Qfin, Kin_fin.astype("f"),  cellvectors,brillvectors, data, Filter.astype("f") ,
                                       maxBrill* paramsExp.DeltaQ_min ,
                                       maxBrill* paramsExp.DeltaQ_max, np.array(toharvest,"i"),
                                       paramsExp.phi, paramsExp.double_exclusion, paramsExp.powder_width, 
                                       Qfin_delta, Qfin_phi, cellvectors_delta, iim, Ntot_images, paramsExp.threshold,
                                       polgeo_correction_O = correction.ravel(),
                                       centering4hkl_O = centering4hkl,
                                       eXp_O = eXp
                                       )
                else:
                    # print( " RACCOLGO con eXp ", eXp)
                    spotpicker.harvest(Qfin,
                                       Kin_fin.astype("f"),
                                       cellvectors,brillvectors,
                                       data,
                                       Filter.astype("f") ,
                                       maxBrill* paramsExp.DeltaQ_min ,
                                       maxBrill* paramsExp.DeltaQ_max,
                                       np.array(toharvest,"i"),
                                       paramsExp.phi,
                                       paramsExp.double_exclusion, paramsExp.powder_width, 
                                       Qfin_delta,
                                       Qfin_phi,
                                       cellvectors_delta,
                                       iim,
                                       Ntot_images,
                                       paramsExp.threshold,
                                       polgeo_correction_O = correction.ravel().astype("f"),
                                       eXp_O = eXp
                                       )
                     


            else:
                # for op in all_ops_dp[:1]:
                #     Q=numpy.tensordot(Qfin, op, ([2],[1]) )
                print( " VOLUME " )
                spotpicker.harvestCube(Qfin,data, Filter.astype("f") )

            paramsExp.phi += angular_step

        comm.Barrier()

        print(  myrank, "  COMPATTA " )

        all_alone = 0
        spotpicker.compatta(all_alone)

                
        print(  myrank, "  COMPATTA OK" )

        comm.Barrier()

        if   nprocs>1:
            if myrank>0:
                print(  myrank, "  RITORNA " )
                return

        if paramsExp.save_to_file is not None:
            print( " process " , myrank, " passa di qua save... ")
            spotpicker.saveHarvestToFile(paramsExp.save_to_file, cellvectors,Q0, correction)

    else:
        doesnotmatter=1
        spotpicker = spotpicker_cy.PySpotPicker(doesnotmatter, 0.0, do_elastic=paramsExp.DO_ELASTIC )
        print( paramsExp.load_from_file)

        Q0 = spotpicker.loadHarvestFromFile(paramsExp.load_from_file) #   float pas assez de precision
        
        spotpicker.loadHarvestFromFile(paramsExp.load_from_file)


        #needs to know dim1,2 for get_Q0 below
        img = fabio.open(flist[0])
        datadum = img.data
        dim1, dim2 = datadum.shape
        Q0_original, Kin, correction= get_Q0(dim1,dim2, paramsExp)
        Q0_original_serial = numpy.reshape(  Q0_original,  [-1,3] )



    if myrank>0:
        print(  myrank, "  RITORNA " )
        return


    if paramsExp.VINTERP:
        cube_dim = int(paramsExp.cube_dim)
        rot = np.eye(3,dtype="f")
        volume, corner, axis = spotpicker.interpolatedSpot( 0,0,0,
                                                 cube_dim , rot ,
                                                 paramsExp.testSpot_beta,
                                                 paramsExp.testSpot_Niter )
        if volume is not None:
            writeVolumeOnFile(volume, "volinterp_%d.h5"%cube_dim,0,0,0,0,0, cellvectors,corner, axis)
        else:
            print( " for ", h,k,l,"  non si sono trovati punti ")

    if paramsExp.show_hkls:
        tmp = numpy.array([[]],"i")
        hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py  = spotpicker.getDataToFit(tmp, paramsExp.nred )        
        print( numpy.array(hkls).tolist())
        print( numpy.array(Npts).tolist())
        return


            
    if paramsExp.testSpot is not None:
        totest = paramsExp.collectSpots
        # h,k,l = paramsExp.testSpot
        # totest=[]
        # alreadyconsidered=[]
        # Q = h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]
        # for op in all_ops_dp[:]:
        #     op=op.astype(np.float32)
        #     Qop = np.dot(op,Q)
        #     h,k,l = np.round(np.dot(cellvectors, Qop)).astype("i")
        #     if (h,k,l) not in alreadyconsidered:
        #         totest.append([ (h,k,l),op])
        #         alreadyconsidered.append( (h,k,l) )

        print( "SPOTS TO BE INSPECTED ")
        for i in range(len(totest)):
            print( i, "\t",totest[i][0])

        II=0
        for i in range(min( len(totest), paramsExp.MaxNtest+10000 )):
            # h,k,l  = totest[i][0]
            h,k,l  = totest[i]
            # rot    = totest[i][1]


           # Qop = np.dot((all_ops_dp[i]).astype(np.float32),Q)
            print( " QUI ")
            rot = np.eye(3,dtype="f")

            volume, corner, axis = spotpicker.interpolatedSpot( h,k,l,
                                                  # paramsExp.testSpot_N , Qop, rot ,
                                                  paramsExp.testSpot_N , rot ,
                                                  paramsExp.testSpot_beta,
                                                  paramsExp.testSpot_Niter )
            if volume is not None:
                writeVolumeOnFile(volume, "tests.h5",h,k,l,i ,II , cellvectors, corner, axis )
                II+=1
            else:
                print( " for ", h,k,l,"  non si sono trovati punti ")

    if paramsExp.show_hkls:
        tmp = numpy.array([[]],"i")
        hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py  = spotpicker.getDataToFit(tmp, paramsExp.nred )

        
        
        print( numpy.array(hkls).tolist())
        print( numpy.array(Npts).tolist())
        return
 
    
    spotshift_variables = []           
    if paramsExp.fitSpots:
        print( " GET ELASTIC SYMMETRY ")
        Elastic_Simmetry = get_elastic_simmetry(all_ops_dp) 

        if not hasattr( paramsExp, "CGUESS") :
            print( " NOW STOPPING, TO DO TRHE FIT YOU NEED TO SET CGUESS ")
            return

        
        tofit=[]
        Rot4fittedhkl={}

        maxQspot = 0.0
        minQspot = 10000
        
        for h,k,l in  paramsExp.collectSpots:
         Q = h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]


         qmod =  math.sqrt( (Q*Q).sum())
         if qmod>0.3:
         # if qmod <=0.62321845296+0.000001 +100000:
             maxQspot = max( maxQspot,    qmod)
             minQspot = min( minQspot,    qmod)

             # print( h,k,l)
             # print( Q)

             if paramsExp.REPLICA:
                 replicas=all_ops_dp[:]
             else:
                 replicas=I = [np.identity(3) ]

             for op in replicas:
                 op.astype(np.float32)
                 Qop = np.dot(op,Q)
                 h,k,l = np.round(np.dot(cellvectors, Qop)).astype("i")
                 if (h,k,l) not in tofit:
                     tofit.append( (h,k,l))
                     Rot4fittedhkl[(h,k,l)] = op
        tofit=np.array(tofit).astype("i")[:]
        if len(tofit)==0:
            tofit.shape=0,0

                    
        print( " TOFIT :  nspots = ",len(tofit),"  maxQspot =  ", maxQspot, " minQspot   ", minQspot)

            
        print( " OTTENGO DATA ")
        hkls, Npts, DQs, Datas, ac_phis, ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py  = spotpicker.getDataToFit(tofit[:], paramsExp.nred )

        hkls = numpy.array(hkls)
        Npts = numpy.array(Npts)
        DQs = numpy.array(DQs)
        Datas = numpy.array(Datas)
        tuttiPhi_py = numpy.array(tuttiPhi_py)
        tuttiIJ_py = numpy.array(tuttiIJ_py)
        tuttiDist_py = numpy.array(tuttiDist_py)


        ac_phis= numpy.array(ac_phis)
        ac_ijs= numpy.array(ac_ijs)
        


        

        
       
        if(1):
            hkls, Npts, DQs, Datas, ac_phis, ac_ijs = Raggruppa(  paramsExp,  hkls, Npts, DQs, Datas, ac_phis,
                                                                  ac_ijs, tuttiPhi_py, tuttiIJ_py, tuttiDist_py )



        Q0_serial = np.reshape( Q0,  [-1,3] )
        print( " XXXXXXXXXXXX " )
        print( ac_ijs.shape)
        print( len(tofit))
        print( Q0_serial[ac_ijs])
        
        q=  Q0_serial[ac_ijs][0]
        paramsExp.phi=ac_phis[0]
        qfin = get_Qfin( q, paramsExp)

        print( (cellvectors*qfin).sum(axis=-1))


        #needs to know dim1,2 for get_Q0 below
        img = fabio.open(flist[0])
        datadum = img.data
        dim1, dim2 = datadum.shape


        list_derivates=[]
        para_names=[]



        spotshift_variables = [   ] 

        
        for phi,ij, spothkl in zip(ac_phis, ac_ijs, hkls):

            spotshift_variables.append(  minimiser.Variable(0.0,-paramsExp.shiftW, paramsExp.shiftW)  )
            spotshift_variables.append(  minimiser.Variable(0.0,-paramsExp.shiftW, paramsExp.shiftW)  )
            spotshift_variables.append(  minimiser.Variable(0.0,-paramsExp.shiftW, paramsExp.shiftW)  )
            
            para_names=[]

            q0 = Q0_serial[ij]



            paramsExp.phi=phi
            q  = get_Qfin( q0, paramsExp    ) 
            print( " ============================================== ")
            print( np.array(spothkl))
            print( np.dot(cellvectors, q))
            print( np.round(np.dot(cellvectors, q)))
            
            derivates = []
            pnames = [ "r1", "r2", "r3" ] 
            PD_dict={}
            para_names=para_names+pnames
            for pn in pnames :
                setattr(paramsExp, pn,  getattr(paramsExp, pn)+1.0e-4)
                derivates.append(  (get_Qfin( q0, paramsExp)-q)*1.0e4 )
                setattr(paramsExp, pn,  getattr(paramsExp, pn)-1.0e-4)

                
            pnames = [ "AA", "BB", "CC" ] 
            para_names=para_names+pnames
            for pn in pnames :
                setattr(paramsExp, pn,  getattr(paramsExp, pn)+1.0e-4)
                dum, brillvectors_bis = getCellVectors(paramsExp)
                derivates.append( -np.dot(  spothkl[:3] , ( brillvectors_bis-brillvectors)  )*1.0e4 )
                setattr(paramsExp, pn,  getattr(paramsExp, pn)-1.0e-4)


            pnames = [ "omega", "alpha", "beta", "kappa" ] 
            para_names=para_names+pnames
            for pn in pnames :
                setattr(paramsExp, pn,  getattr(paramsExp, pn)+1.0e-4)
                derivates.append(  (get_Qfin( q0, paramsExp)-q)*1.0e4 )
                setattr(paramsExp, pn,  getattr(paramsExp, pn)-1.0e-4)
 
            
            pnames = ["d1","d2", "det_origin_X", "det_origin_Y" ] 
            para_names=para_names+pnames
            for pn in pnames :
                setattr(paramsExp, pn,  getattr(paramsExp, pn)+1.0e-0)

                if(1):
                    print(" ----" ,  ij)
                    Q0varied, Kin, correction = get_Q0(dim1,dim2, paramsExp, ij)
                    setattr(paramsExp, pn,  getattr(paramsExp, pn)-1.0e-0)
                ## Qdiff = numpy.reshape(  (Q0varied-Q0_original)*1.0e0, [-1,3])
                    print( Q0varied)
                    Qdiff =  (Q0varied[0,0]-Q0_original_serial[ij])*1.0e0
                    q0  = Qdiff  #### [ij]
                else:
                    Q0varied, Kin, correction = get_Q0(dim1,dim2, paramsExp)
                    setattr(paramsExp, pn,  getattr(paramsExp, pn)-1.0e-0)
                    Qdiff = numpy.reshape(  (Q0varied-Q0_original)*1.0e0, [-1,3])
                    q0  = Qdiff[ij]


                derivates.append(  get_Qfin( q0, paramsExp) )
                print( " DERIVATA " , derivates[-1  ])
           
            list_derivates.append(derivates)

        print( numpy.array(Npts))
        print( numpy.array(hkls))

        DQs=np.array(DQs,"f")
        Datas=np.array(Datas,"f")
        hkls = np.array(hkls)
        Npts = np.array(Npts)

        comp_names, comp_vects = Elastic_Simmetry

        # A e una matrice 6x6
        comppos = {   (0,0):0,(1,1):1,(2,2):2,(1,2):3,(2,0):4,(0,1):5
                      ,(2,1):3,(0,2):4,(1,0):5}  



        KELVIN2ERG = 1.380648780669e-16
        HNOBAR_CGS   = 6.626069e-27  ## /(2.0*math.pi)
        DENSITY    = paramsExp.density
        # 1 Pa = J/m3   ==>    erg/cm3  = 0.1 J/m3   ==>  1GPa = 10**10 erg/cm3
        GPA2CGS  = 1.0e+10
        A2CM  = 1.0e-8      # dans le calcul de la frequence on utilise des Qs qui sont en A**-1
        FACTOR4T =  A2CM*  math.sqrt(DENSITY/GPA2CGS)*  KELVIN2ERG / (    HNOBAR_CGS)

        if DW_infos is not None:
            Ntot = np.array(  Npts).sum()
            qtots = np.zeros([Ntot,3],"f")
            qtots[:] =  DQs
            pos = 0
            for (h,k,l, ispot), npt in zip( hkls,  Npts ):
                print( h,k,l)
                qtots[pos:pos+npt] = qtots[pos:pos+npt] + h*brillvectors[0]+ k*brillvectors[1]+ l*brillvectors[2]     
                pos=pos+npt       

            Facts = Get_dwscatt_facts(qtots, DW_infos,   paramsExp , hkls[:,:3], Npts)
            Facts = numpy.array((Facts*Facts.conjugate()).real)
        else:
            Ntot = np.array(  Npts).sum()            
            Facts = np.array([Ntot],"f")
            
        fit_object = Fit.Fit( hkls, brillvectors, Npts, DQs, Datas,  FACTOR4T *paramsExp.Temperature, Facts,
                              comp_vects, list_derivates,
                              devicetype=paramsExp.devicetype  ,  max_workgroup_size=128   )

        # print( " SCALDO I MOTORI " )
        # err = fit_object.error(  np.concatenate( [ np.random.random( len(comp_names)  ), np.zeros([6],"f")] ) )


        class fitProxy:
            def __init__(self,fit_object, variables, vvnames):
                self.fit_object=fit_object
                self.variables=variables
                self.vvnames = vvnames
            def error(self, freeze=0):
                x = [minimiser.par(tok) for tok in self.variables ] 
                mins = [tok.min for tok in self.variables ] 
                maxs = [tok.max for tok in self.variables ]
                if freeze!=0:
                    print( "-------------FREEZING ---------------------- with ", freeze)
                    self.fit_object.error(x,freeze=freeze)
                    return None
                res = self.fit_object.error(x)

                entries =  self.vvnames
                file = open("parametri%s.txt"%paramsExp.personal_postfix,"w")
                for n,v in zip(entries,x):
                    file.write("%s=%s\n"%(n,v))
                file.close()
                N=len(entries)
                for n in range(0,N,8):
                    pt = PrettyTable(entries[n:n+8] )
                    pt.add_row(   x    [n:n+len(entries[n:n+8]) ]) 
                    pt.add_row(   mins [n:n+len(entries[n:n+8]) ]) 
                    pt.add_row(   maxs  [n:n+len(entries[n:n+8])]) 
                    print( pt)
                    
                print( " error " , res)
                return res
            def setitercounter(self, n):
                self.itc=n
            def getitercounter(self):
                return self.itc

        
        variables = [ minimiser.Variable( ( paramsExp.CGUESS[tok][0]   +0.0*random.random()) , paramsExp.CGUESS[tok][1] ,paramsExp.CGUESS[tok][2]) for tok in comp_names ]


        A_variables     =[]
        A_variables4fit =[]
        for v in paramsExp.A_variables:
            if type(v)==type(-1):
                assert(v<0)
                A_variables.append(  A_variables[v] )
            else:
                A_variables.append(v)
                A_variables4fit.append(v)


       
    
        
        variables4fit = variables + A_variables4fit +spotshift_variables
        variables = variables + A_variables +spotshift_variables
        vvnames = comp_names + paramsExp.A_names

 
        if(paramsExp.doMinimisation):
                  
            
            tominimise=fitProxy(fit_object,variables, vvnames)
            miniob=minimiser.minimiser(tominimise,variables4fit)

   

            try:
                miniob.amoeba(paramsExp.ftol)  
                # miniob.amoeba(0.00000000000001)  
                # miniob.amoeba(0.00000000000001)  
                # miniob.amoebaAnnealed( paramsExp.ftol, lambda x: 1000.0/(1.0+x*0.3), 100,
                #                        arret=1.0e-12, maxiters=100,centershiftFact=0.8)
            except   KeyboardInterrupt:
                print( "MINIMISATION INTERRUPTED BY KEYBOARD, NOW GOING ON")

            # tominimise.error(freeze=0.2)
            # try:
            #     miniob.amoeba(paramsExp.ftol)  
            #     # miniob.amoeba(0.00000000000001)  
            #     # miniob.amoeba(0.00000000000001)  
            #     # miniob.amoebaAnnealed( paramsExp.ftol, lambda x: 1000.0/(1.0+x*0.3), 100,
            #     #                        arret=1.0e-12, maxiters=100,centershiftFact=0.8)
            # except   KeyboardInterrupt:
            #     print( "MINIMISATION INTERRUPTED BY KEYBOARD, NOW GOING ON")
                
            # tominimise.error(freeze=0.2)

            # try:
            #     miniob.amoeba(paramsExp.ftol)  
            #     # miniob.amoeba(0.00000000000001)  
            #     # miniob.amoeba(0.00000000000001)  
            #     # miniob.amoebaAnnealed( paramsExp.ftol, lambda x: 1000.0/(1.0+x*0.3), 100,
            #     #                        arret=1.0e-12, maxiters=100,centershiftFact=0.8)
            # except   KeyboardInterrupt:
            #     print( "MINIMISATION INTERRUPTED BY KEYBOARD, NOW GOING ON")




                
            for name, v in zip(comp_names+para_names, variables ):
                print( name , " = " , v.value)


            
        if(paramsExp.visSim):
            print( " chiamo ancora errore ")
            simdata, expdata =  fit_object.error(  np.array( [minimiser.par(var) for var in variables] ), view=1 )

            simdata[numpy.isnan(expdata)]= numpy.nan
            # expdata[numpy.isnan(expdata)]=0

            start = 0
            II=0
            IIexp=0
            try:            
                for ispot,( HKL,hkl_npts) in enumerate(zip(hkls, Npts)):

                    print( ispot,  hkl_npts, HKL)
                    _qmin=0.0
                    _qmax=spotpicker.getQmax()
                    
                    spotpickerSim = spotpicker_cy.PySpotPicker(1, _qmax)
                    dum = numpy.zeros(3,"f")
                    dum[0]=1.0e30
                    spotpickerSim.harvestFromCollection(DQs[start:start+hkl_npts],dum,
                                                        cellvectors,brillvectors,
                                                        simdata[start:start+hkl_npts],
                                                        _qmin ,
                                                        _qmax)

                    all_alone = 1
                    spotpickerSim.compatta(all_alone)
                    
                    rot = np.eye(3,dtype="f")
                    # print( Rot4fittedhkl.keys())
                    # rot =  numpy.array(Rot4fittedhkl[tuple(HKL[:3])],"f")
                    print( " spot picker " )
                    volume, corner, axis = spotpickerSim.interpolatedSpot( 0,0,0,
                                                          paramsExp.testSpot_N , rot ,
                                                          paramsExp.testSpot_beta,
                                                          paramsExp.testSpot_Niter )

                    # volumeE, cornerE, axisE = spotpicker.interpolatedSpot( h,k,l,
                    #                                                        paramsExp.testSpot_N , rot ,
                    #                                                        paramsExp.testSpot_beta,
                    #                                                        paramsExp.testSpot_Niter )
                    spotpickerSim = spotpicker_cy.PySpotPicker(1,_qmax)
                    spotpickerSim.harvestFromCollection(DQs[start:start+hkl_npts],dum,
                                                        cellvectors,brillvectors,
                                                        expdata[start:start+hkl_npts],
                                                        _qmin ,
                                                        _qmax)
                    
                    all_alone = 1
                    spotpickerSim.compatta(all_alone)
                    volumeE, corner, axis = spotpickerSim.interpolatedSpot( 0,0,0,
                                                          paramsExp.testSpot_N , rot ,
                                                          paramsExp.testSpot_beta,
                                                          paramsExp.testSpot_Niter )

                    if len(HKL)==4:
                        h,k,l, count=HKL
                    else:
                        h,k,l=HKL
                        count=1
                        
                    if volume is not None:
                        writeVolumeOnFile(volume,  paramsExp.file4sim,h,k,l,count,II, cellvectors, corner, axis, isexp=0)
                        II+=1
                    else:
                        print( " for ", h,k,l,"  non si sono trovati punti ")

                    if volumeE is not None:
                        writeVolumeOnFile(volumeE,  paramsExp.file4sim ,h,k,l,count,II, cellvectors, corner, axis, isexp=1)
                        II +=1
                    else:
                        print( " for ", h,k,l,"  non si sono trovati punti ")

                    start=start + hkl_npts
            except  KeyboardInterrupt:
                print( " VISUALISATION INTERRUPTED BY KEYBOARD, GOING ON")
                

            for name, v in zip(comp_names+para_names, variables ):
                print( name , " = " , v.value)
        print( para_names)
        print( variables)

def main(paramsInput):

    
    Config_fname = paramsInput.conf_file
    Filter_fname = paramsInput.filter_file

    print( paramsInput.conf_file, paramsInput.filter_file)

    flist = glob.glob(paramsInput.images_prefix+"*"+paramsInput.images_postfix )
    flist.sort()


    print('---------------------------------------------------------------')
    ascii_gen_list = paramsInput.ascii_generators .replace(',',' ').split()    

        
   
    # print( 'Setting Parameters ...')
    paramsSystem = read_configuration_file(paramsInput.conf_file)

    cellvectors, brillvectors = getCellVectors(paramsSystem)
    # print cellvectors

    # ops_list = genlist2oplist(ascii_gen_list)
    ops_list_dp = genlist2oplist( ascii_gen_list ,cellvectors, dtype=np.float64)

    # print Config_fname

    if not hasattr(paramsSystem, "DW_file"):
        DW_infos = None
    else:
        DW_infos = get_DWs(paramsSystem.DW_file,  cellvectors   )

    if paramsSystem.collectSpots is None and paramsSystem.file_maxima is not None:
        maxima_data = pickle.load( open(paramsSystem.file_maxima, 'r'))
        
        picchi = set()
        for  fname , infos in maxima_data.items():
            for pos, infopeak in infos.items():
                if infopeak["accepted"]:
                    h,k,l = infopeak["hkl_rounded"]
                    picchi = set.union( picchi ,set( ((int(h), int(k), int(l) ),)  ))
                    
        for  fname , infos in maxima_data.items():
            for pos, infopeak in infos.items():
                if not infopeak["accepted"]:
                    h,k,l = infopeak["hkl_rounded"]
                    picchi = picchi - set(((int(h), int(k), int(l) ),))
                    
        paramsSystem.collectSpots =list(picchi)
    
    print( " COLLECT SPOTS " )
    print(  paramsSystem.collectSpots)

    main3d(paramsSystem,
           paramsInput.filter_file,
           flist,
           ops_list_dp, paramsInput, DW_infos)
    print( " processo ", myrank, " aspetta ")
    comm.Barrier()

USAGE = """ USAGE :
    tds2el inputfile

input file containing

conf_file             =  "astring"    
filter_file           =  "astring"
images_prefix         =  "astring"
ascii_generators      =  "1,1~,2(x),2(y),2(z),2(110),3(z),3(111),4(z),4(z)~,m(x),m(y),m(z)"  # not necessarily all of them



"""

print(" name " ,  __name__ )

if(sys.argv[0][-12:]!="sphinx-build"):

#    if __name__ == "__main__":

        if len(sys.argv) < 2:
            print( USAGE)
        else:

            class Parameters:
                conf_file=None
                filter_file = None
                images_prefix = None
                images_postfix = ".cbf"
                volume_reconstruction = True
                normalisation_file = None
                exec(open(sys.argv[1]).read())


            paramsInput = Parameters()
            
            if paramsInput.normalisation_file  is not None:
                paramsInput.normalisation={}
                sl = open(paramsInput.normalisation_file ,"r").read().split("\n")
                for  l in sl:
                    fnv = l.split()
                    if len(fnv):
                        fn,v = fnv
                    paramsInput.normalisation[fn]=float(v)

                
            main( paramsInput)

    # sys.exit()


