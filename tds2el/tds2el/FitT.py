#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

import numpy
import pyopencl, pyopencl.array
from opencl import ocl
import gc
import os 
import pyopencl.version
import subprocess as sub
import string
import math

import logging

logger = logging.getLogger("Fit")
class Object(object):
    pass
class  oclContext(object):
    def __init__(self, devicetype="CPU",memory=0, profile=False):
        
        self.memory=memory

        possible_devicesid=None
        if devicetype=="GPU" :
            try:
                args=["oarprint", "host",   "-P",  "gpu_num",  "-F",  " % " ]
                p = sub.Popen(args=args ,stdout=sub.PIPE,stderr=sub.PIPE)
                resources, errors = p.communicate()
            except:
                resources=""
                
            if len(resources):
                print "============ ", resources

                possible_devicesid=map(string.atoi,string.split(resources,","))

        print "POSSIBLES = ", possible_devicesid

        self.device = ocl.select_device(type=devicetype, memory=self.memory, best=True, possibles=possible_devicesid)

        if self.device is None:
            print " No device of devicetype= ", devicetype, "  has been found "
            raise Exception, (" No device of devicetype= " + devicetype + "  has been found " )

        self.ctx = pyopencl.Context(devices=[pyopencl.get_platforms()[self.device[0]].get_devices()[self.device[1]]])

        if profile:
            self.queue = pyopencl.CommandQueue(self.ctx, properties=pyopencl.command_queue_properties.PROFILING_ENABLE)
        else:
            self.queue = pyopencl.CommandQueue(self.ctx)

class Fit:
    kernels= {"diffused":1024,"matvectmult_accumulation":1024}
    def __init__( self, hkls, Brillvectors, Npts, DQs, Datas, Temp0, Temp1,DWS_factors_0, DWS_factors_1, 
                  comp_vects, 
                  profile=False, devicetype="CPU" ,  max_workgroup_size=128 ):

        
        oclctx = oclContext( devicetype=devicetype,memory=0, profile=profile)
        self.oclctx = oclctx
        self.device = oclctx.device 
        self.ctx    = oclctx.ctx    
        self.queue  = oclctx.queue  
        self.Npts   = Npts
        self.buffers = {}

        self.nspots = hkls.shape[0]


        self.Temp0 = Temp0
        self.Temp1 = Temp1
        hkls    =numpy.array(hkls    )
        DQs     =numpy.array(DQs     )
        Datas   =numpy.array(Datas   )
        comp_vects=numpy.array(comp_vects,"f")

        print " MAX .." , DQs.max()


        dq_moduli = numpy.sqrt((DQs*DQs).sum(axis=-1)) 
        
        self.dqmax =  dq_moduli.max()
        self.dqmin =  dq_moduli.min()

        
        self.npars_el  = comp_vects.shape[0]
        self.events=[]
        self.profile=profile

        NQs = Datas.shape[0]
        self.NQs=NQs
        self.max_workgroup_size = max_workgroup_size
        wg = 128
        NQs_padded = (NQs+(wg-1)) & ~(wg - 1)
        self.wgsize = (wg,) 
        self.procsize = (NQs_padded,)
        self.NQs_padded = NQs_padded
        
        self._allocate_buffers()
        
        self.buffers[ "dws_facts_0"][:len(DWS_factors_0)]= numpy.array(DWS_factors_0 ,"f")
        self.buffers[ "dws_facts_1"][:len(DWS_factors_1)]= numpy.array(DWS_factors_1 ,"f")

        if  pyopencl.version.VERSION >= (2013,1):
            self.buffers[ "Datas"][:]=0
            self.buffers[ "Datas"][:NQs] = Datas
            self.buffers[ "ones" ][:]     = 0.0
            self.buffers[ "ones" ][:NQs]     = 1.0
            self.buffers[ "DQs" ] [:]=0
            self.buffers[ "freeze" ][:]     = 0.0
            self.buffers[ "freeze" ][:NQs]     = (1 - numpy.isnan(Datas ) ).astype("f")

        else:
            tmp_Datas = self.buffers[ "Datas"].get()
            tmp_ones  = self.buffers[ "ones" ].get()
            tmp_DQs   = self.buffers[ "DQs"  ].get()
            tmp_shiftshkl   = self.buffers[ "shiftshkl"  ].get()
            tmp_tos         = self.buffers[ "tos"  ].get()
            tmp_freeze   = self.buffers[ "freeze"  ].get()

            tmp_Datas[:]     = 0
            tmp_Datas[:NQs]  = Datas
            tmp_ones [:]     = 0.0
            tmp_ones [:NQs]  = 1.0
            tmp_DQs  [:]     = 0
            tmp_freeze [:]     = 0.0
            tmp_freeze [:NQs]  =   (1 - numpy.isnan(Datas ) ).astype("f")
          
            self.buffers[ "Datas"].set(tmp_Datas)
            self.buffers[ "ones" ].set(tmp_ones )
            self.buffers[ "DQs"  ].set(tmp_DQs  )
            self.buffers[ "freeze"  ].set(tmp_freeze   )
   


        print " SHIFTS " 
        print " hkls " 
        print hkls
        print " BRILLS " 
        print Brillvectors

        for i in range(3):
            qs = numpy.array(DQs[:,i])
            N=0
            for k , n in enumerate(Npts):
                shift = hkls[k][0]*Brillvectors[0,i]+hkls[k][1]*Brillvectors[1,i]+hkls[k][2]*Brillvectors[2,i]
                print "   NP " ,   k , " i  ", i , "   " , shift
                if  pyopencl.version.VERSION >= (2013,1):
                    self.buffers[ "DQs" ] [i*NQs_padded+N:((i)*NQs_padded)+N+n] = qs[N:N+n]  # +shift
                    self.buffers[ "shiftshkl" ] [3*k+i] =   shift
                else:
                     tmp_DQs [i*NQs_padded+N:((i)*NQs_padded)+N+n] = qs[N:N+n]  # +shift
                     tmp_shiftshkl[3*k+i] =   shift
                     
                N=N+n

        self.DQs=DQs

        if  pyopencl.version.VERSION >= (2013,1):
            self.buffers[ "comp_vects" ] [:] = numpy.reshape( comp_vects,[-1]   )
        else:
            self.buffers[ "comp_vects" ]. set( numpy.reshape( comp_vects,[-1]   ))

        N=0
        for i , n in enumerate(Npts):
            if  pyopencl.version.VERSION >= (2013,1):
                self.buffers[ "tos" ][N:N+n] =   i
            else:
                tmp_tos[N:N+n] =   i
            N=N+n

        


        if  pyopencl.version.VERSION < (2013,1):
            self.buffers[ "Datas"].set(tmp_Datas)
            self.buffers[ "ones" ].set(tmp_ones )
            self.buffers[ "DQs"  ].set(tmp_DQs  )
            self.buffers[ "shiftshkl"  ].set(tmp_shiftshkl   )
            self.buffers[ "tos"  ].set(tmp_tos)              

        self.programs = {}
        self._compile_kernels()

        
    def _allocate_buffers(self):
        self.buffers[ "Datas" ]     = pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "DQs" ]       = pyopencl.array.empty(self.queue,(3*self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "dws_facts_0" ] =  pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "dws_facts_1" ] =  pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "debug" ]     =  self.buffers[ "dws_facts_0" ]    

        self.buffers[ "comp_vects" ]  = pyopencl.array.empty(self.queue,(self.npars_el*6*6,), dtype=numpy .float32 )

        self.buffers[ "tos" ]       = pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .int32 )

        self.buffers[ "intensity" ]     = pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )

        self.buffers[ "freeze" ]       = pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "pars" ]     =  pyopencl.array.empty(self.queue,(self.npars_el,), dtype=numpy .float32 ) 
        
        self.buffers[ "aa" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )
        self.buffers[ "ab" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )
        self.buffers[ "bb" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )
        self.buffers[ "dd" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )
        self.buffers[ "ad" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )
        self.buffers[ "bd" ]     = pyopencl.array.empty(self.queue,(self.nspots,), dtype=numpy .float64 )

        self.buffers[ "ones" ]     = pyopencl.array.empty(self.queue,(self.procsize[0],), dtype=numpy .float32 )
        self.buffers[ "shiftshkl" ]   = pyopencl.array.empty(self.queue,(self.nspots*3,), dtype=numpy .float32 )
        self.buffers[ "shiftsAC" ]   = pyopencl.array.zeros(self.queue,(self.nspots*3,), dtype=numpy .float32 )
        
      

    def _free_buffers(self):
        """
        free all memory allocated on the device
        """
        if hasattr(self,"buffer"):
            for buffer_name in self.buffers:
                if self.buffers[buffer_name] is not None and buffer_name != "debug" :
                    try:
                        del self.buffers[buffer_name]
                        self.buffers[buffer_name] = None
                    except pyopencl.LogicError:
                        logger.error("Error while freeing buffer %s" % buffer_name)


 
    def error(self, pars, view=0, freeze=0):

        if  pyopencl.version.VERSION >=(2013,1):
            self.buffers[ "pars" ][:]=numpy.array(pars,"f")[:self.npars_el]  
        else:
            self.buffers[ "pars" ].set(numpy.array(pars,"f")[:self.npars_el])

        if self.profile:
            self.events.append(("diffused" , evt))


        tocalc = [   ( "aa" , "intensity" ,  "intensity" ), 
                     ( "ab", "intensity" ,  "Datas"  ),
                     ( "bb", "Datas" , "Datas"    ),
                     ( "dd", "ones"  ,  "ones"   ),
                     ( "ad", "intensity" ,  "ones"   ),
                     ( "bd", "Datas" ,  "ones"   ),
                 ]

        evt = self.programs["diffused"].diffused_Temp(self.queue, self.procsize, self.wgsize,
                                                      numpy.int32( self.NQs),
                                                      numpy.int32( self.npars_el),
                                                      numpy.float32(self.Temp0),
                                                      numpy.float32(self.Temp1),
                                                      self.buffers[ "pars" ].data ,
                                                      self.buffers["DQs"].data     , 
                                                      self.buffers["comp_vects"].data, 
                                                      self.buffers["intensity"].data, 
                                                      self.buffers["tos"].data,
                                                      self.buffers["shiftshkl"].data,
                                                      self.buffers["shiftsAC"].data,
                                                      self.buffers["dws_facts_0"].data,
                                                      self.buffers["dws_facts_1"].data,
                                                      self.buffers["freeze"].data  ## uso il buffer freeze per i pesi Ma non serve piu
                                                      )
                     
        # if freeze!=0:
        #         self.buffers[ "freeze" ][:NQs]     = freeze

        for a,b,c in tocalc:
            if  pyopencl.version.VERSION >=(2013,1):
                self.buffers[ a  ][:] =0
            else:
                tmp=self.buffers[ a  ].get()
                tmp[:]=0
                self.buffers[ a  ].set(tmp)


                
            # if a=="aa": print " PRERESULT ", pyopencl.array.dot( self.buffers[ b  ], self.buffers[ c  ]   )
            evt = self.programs["matvectmult_accumulation"].matvectmult_accumulation_double( self.queue, self.procsize, self.wgsize,
                                                                                      numpy.int32( self.NQs),
                                                                                      self.buffers[ "tos"  ].data  ,
                                                                                      self.buffers[ b  ].data ,
                                                                                      self.buffers[ c  ].data,
                                                                                      self.buffers[ "freeze" ].data ,
                                                                                      self.buffers[ a  ].data
                                                                                      )
            # if a=="aa":  print " RESULT " , self.buffers[ a  ].get()

            if self.profile:
                self.events.append(("matvectmult_accumulation" , evt))
  
            exec("%s=self.buffers[ a  ].get()"%a)

            
        if view==0:
            print "ritorno "
            return aa, ad, dd,      ab, bd, bb 
        else:
            fitres = self.buffers["intensity"].get()[:self.NQs]
            datres = self.buffers["Datas"].get()[:self.NQs]

            return aa, ad, dd,      ab, bd, bb,   fitres,datres
                
    def _free_kernels(self):
        """
        free all kernels
        """
        if hasattr(self,"programs"):
            self.programs = {}
        
    def __del__(self):
        """
        Destructor: release all buffers
        """

        
        self._free_buffers()
        self._free_kernels()

        self.queue = None
        self.ctx = None
        gc.collect()



    def _compile_kernels(self):
        """
        Call the OpenCL compiler
        """
        for kernel in self.kernels:
            
            print " COMPILO " , kernel

            kernel_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), kernel + ".cl")
            kernel_src = open(kernel_file).read()
            kernel_src = ("#define BLOCKSIZE %d\n"%(self.wgsize[0], ))+kernel_src
            kernel_src = ("#define MAX_CONST_SIZE_pars   %d\n"%(self.npars_el*4, ))+kernel_src
            kernel_src = ("#define MAX_CONST_SIZE_comps  %d\n"%(self.npars_el*6*6*4, ))+kernel_src
            kernel_src = ("#define MAX_CONST_SIZE_tos  %d\n"%(self.nspots*4, ))+kernel_src
            kernel_src = ("#define MAX_CONST_SIZE_shifts  %d\n"%(self.nspots*3*4, ))+kernel_src

#  #define MAX_CONST_SIZE_pars 84
#  #define MAX_CONST_SIZE_comps (21*6*6*4)

            if "__len__" not in dir(self.kernels[kernel]):
                wg_size = min(self.max_workgroup_size, self.kernels[kernel])
            else:
                wg_size = self.max_workgroup_size
            try:
                program = pyopencl.Program(self.ctx, kernel_src).build('-D WORKGROUP_SIZE=%s' % wg_size)
            except pyopencl.MemoryError as error:
                raise MemoryError(error)
            except pyopencl.RuntimeError as error:
                logger.error("Failed compiling kernel '%s' with workgroup size %s: %s", kernel, wg_size, error)
                raise error

            self.programs[kernel] = program

    def log_profile(self):
        """
        If we are in debugging mode, prints out all timing for every single OpenCL call
        """
        t = 0.0
        orient = 0.0
        descr = 0.0
        if self.profile:
            for e in self.events:
                if "__len__" in dir(e) and len(e) >= 2:
                    et = 1e-6 * (e[1].profile.end - e[1].profile.start)
                    print("%50s:\t%.3fms" % (e[0], et))
                    t += et
                    # if "orient" in e[0]:
                    #     orient += et
                    # if "descriptors" in e[0]:
                    #     descr += et

        print("_"*80)
        print("%50s:\t%.3fms" % ("Total execution time", t))
        # print("%50s:\t%.3fms" % ("Total Orientation assignment", orient))
        # print("%50s:\t%.3fms" % ("Total Descriptors", descr))
