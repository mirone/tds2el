import PyMca5
from PyMca5.PyMcaGui.plotting import RGBCorrelatorGraph
qt = RGBCorrelatorGraph.qt
app = qt.QApplication([])
app.lastWindowClosed.connect(app.quit)
from PyMca5.PyMcaGui import MaskImageWidget
import fabio
data=fabio.open("air_wbmstop_0001p.cbf").data.astype("f")
container = MaskImageWidget.MaskImageWidget(selection=True,profileselection=True,aspect=True,imageicons=True,maxNRois=2)
container.setImageData(data)
container.show()


container.setSelectionMask(mask)
from PyMca5.PyMcaIO.EdfFile import EdfFile
EdfFile("newMask.edf","w").WriteImage({},Mask.astype("f"))
Mask=mask+mask2
mask2=data<0
