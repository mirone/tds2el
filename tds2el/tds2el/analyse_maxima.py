#!/usr/bin/env python
# -*- coding: utf-8 -*-
###################################################################################
# Fit of elastic constants from diffused thermal scattering 
# Author : 
#             Alessandro Mirone
#             
#             Routines for rotating the data to locate them in reciprocal 3D
#             are from the first stages of 3DRSR project of Gael Goret
###################################################################################
import glob
# import fftw3f
import math
from PyMca5.PyMcaIO import EdfFile
from PyMca5.PyMcaIO.TiffIO import TiffIO

import h5py
import pylab
import random 
import Fit
import minimiser
import numpy
import os
import pickle
__doc__=(


r"""  .. toctree::
        :maxdepth: 3


running the code            
----------------
     
   tds2el is always runned with two arguments ::

     tds2el  inputfile



     * inputfile:  is the name of the file containings all the geometry parameters, and the
       parameters concerning your calculation options. it is your  freshly generated 3DRSR.conf
       to which you will add additional options as explained below for the different cases.




preparation of the input file 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

       * Example ::

             maxima_file = "maxima.p"
             orientation_codes  =  1
             dist               =  365.998788452 
             beam_tilt_angle    = -0.43108
             det_origin_X = 1153.34
             det_origin_Y = 1773.31
             pixel_size = 0.172
             d1 = -0.34142
             d2 = -0.50502
             theta = 0.0 
             theta_offset = 0.06708
             lmbda  = 0.6895
             angular_step = 0.1

             r1 = 0.0
             r2 = 0.0
             r3 = 0.0

             d1 = -0.34142
             d2 = -0.50502

             AA = 5.29209
             BB = 6.56846
             CC = 23.57155
             aAA = 90.
             aBB = 90.
             aCC = 90.

             beta = 0.0
             alpha = 50.0
             omega =88.82030359201644
             omega_offset = 0.0

             kappa =  2.09345545375988

             ftol = 1.0e-7

             variations =  {
               "r1": minimiser.Variable(0.0,-2,2),
               "r2": minimiser.Variable(-4,-6,10),
               "r3": minimiser.Variable(0.0,-2,2) ,
               "AA": minimiser.Variable(0.0,-0.1,0.1),
               "BB": minimiser.Variable(0.0,-0.1,0.1),
               "CC": minimiser.Variable(0.0,-0.1,0.1),
               "omega": minimiser.Variable(0.0,-2.0,2.),
               "alpha": minimiser.Variable(0.0,-2.0*0,6.0*0),
               "beta": minimiser.Variable(0.0,-.5*0,.5*0),
               "kappa": minimiser.Variable(0.0,-2.0,10.0) ,
               "d1": minimiser.Variable(0.0,-0.0,0.0),
               "d2": minimiser.Variable(0.0,-0.0,0.0),
               "det_origin_X": minimiser.Variable(0.0,-20,20),
               "det_origin_Y": minimiser.Variable(0.0,-20,20)
            }
"""
)
try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print "MPI LOADED , nprocs = ", nprocs
except:
    nprocs=1
    myrank = 0
    print "MPI NOT LOADED "


# import sift
from math import pi, sin, cos

import numpy as np
import sys, time
__version__ = 0.7


if(sys.argv[0][-12:]!="sphinx-build"):
    import spotpicker_cy


#--------------------------------------------------------------------------------------------------------
# Rotation, Projection and Orientation Matrix
#--------------------------------------------------------------------------------------------------------

# e' una rotazione degli assi non delle coordinate
#  va bene cosi perche si ruota il campione quindi il raggio controruota nello spazio K
def Rotation(angle, rotation_axis=0):
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val

#--------------------------------------------------------------------------------------------------------

# questa rispetta la convenzione Ma per le cordinate   Rotation(45.0,"z") != Arb_Rot(45.0,[0,0,1])
def Arb_Rot(angle, rot_axis):
    angle = np.radians(angle)
    assert(len(rot_axis)==3)
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat

#--------------------------------------------------------------------------------------------------------

def Mirror(mirror_axis):
    x,y,z = {"x":[-1,1,1], "y":[1,-1,1], "z":[1,1,-1],0:[-1,1,1], 1:[1,-1,1], 2:[1,1,-1]}[mirror_axis]
    mat = np.array([[x,0,0],[0,y,0],[0,0,z]])
    return mat 

#--------------------------------------------------------------------------------------------------------

def Prim_Rot_of_RS(omega, phi, kappa, alpha, beta, omega_offset):
    # """ 
    # Primary rotation of reciprocal space. 
    # Omega, kappa, phi are the nominal values of the angle
    # """
    # omega=-omega
    # phi  = -phi
    # kappa = -kappa
    # alpha=-alpha
    # beta=-beta

    tmp = Rotation(omega + omega_offset, 2)
    tmp = np.dot(tmp, Rotation(alpha, 1))
    tmp = np.dot(tmp, Rotation(kappa, 2))
    tmp = np.dot(tmp, Rotation(-alpha, 1))
    tmp = np.dot(tmp, Rotation(beta, 1))
    tmp = np.dot(tmp, Rotation(phi, 2))
    tmp = np.dot(tmp, Rotation(-beta, 1))
    return tmp

#--------------------------------------------------------------------------------------------------------

def DET(theta, theta_offset, d2, d1):
    # """ 
    # Rotation matrix for the detector 
    # theta is the nominal theta value and d1,D2 are the tilts of detector
    # """
    tmp = Rotation(theta + theta_offset, 2)
    tmp = np.dot(tmp, Rotation(d2, 1))
    tmp = np.dot(tmp, Rotation(d1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------    

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    # r1=-r1
    # r2=-r2
    # r3=-r3
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------

def P0(dist, b2):
    # """ Primary projection of pixel coordinates (X,Y) to the reciprocal space. """
    B = Rotation(b2, 1) # Beam tilt matrix
    p0 = np.dot(B, [ dist, 0, 0 ])
    return p0

#--------------------------------------------------------------------------------------------------------
# Corrections
#--------------------------------------------------------------------------------------------------------

MD0 = np.array([[1, 0], [0, 1]  ], dtype=np.int32)
MD1 = np.array([[-1, 0], [0, 1] ], dtype=np.int32)
MD2 = np.array([[1, 0], [0, -1] ], dtype=np.int32)
MD3 = np.array([[-1, 0], [0, -1]], dtype=np.int32)
MD4 = np.array([[0, 1], [1, 0]  ], dtype=np.int32)
MD5 = np.array([[0, -1], [1, 0] ], dtype=np.int32)
MD6 = np.array([[0, 1], [-1, 0] ], dtype=np.int32)
MD7 = np.array([[0, -1], [-1, 0]], dtype=np.int32)

#--------------------------------------------------------------------------------------------------------
# Parameter Class
#--------------------------------------------------------------------------------------------------------


def read_configuration_file(cfgfn):
	# """
	# cfgfn is the filename of the configuration file.
	# the function return an object containing information from configuration file (cf inside cfg file).
	# """
	
	try:
            s=open(cfgfn,"r")
	except:
		print " Error reading configuration file " ,  cfgfn			
		exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
		print "*** print_exception:"
		traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                              limit=None, file=sys.stdout)
		raise Exception
	class Parameters():
            npoints=0
            transform_only  = 0

            exec(s)
	cfg = Parameters()
	s.close()
	return cfg

def mag_max(l):
    # """
    # l is a list of coordinates
    # """
    return max(np.sqrt(np.sum(l * l, axis= -1)))
    
#--------------------------------------------------------------------------------------------------------


def   get_Qfin( Q0,  
               params):

    R = Prim_Rot_of_RS(params.omega, params.phi, params.kappa, params.alpha, params.beta, params.omega_offset)
    U = Snd_Rot_of_RS(params.r1, params.r2, params.r3)
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    return Qfin

############################################################################################
## ASSI CRISTALLOGRAFICI

def modulus(a):
    return np.sqrt((a*a).sum())

def arcCos(   a,b   ):
    c = (a*b).sum()
    c=c/modulus(a)
    c=c/modulus(b)
    return np.arccos(c)
    
def getCellVectors(param):

    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0,1.0 ]))         ,     xa      ) 
    # print xa
    # print xb
    # raise " OK " 
    # print " giro  " ,np.array([1.0,0.0,0.0 ]) , " di " , -param.aBB

    xc = np.dot(  Arb_Rot(param.aBB,np.array([0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 
    # print " ora " , xc 

    sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
    sa = sa/math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 )
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    xa  = xa*param.AA
    xb  = xb*param.BB
    xc  = xc*param.CC
    cellvectors = np.array([ xa,xb,xc ] )
    return np.array(cellvectors,"f")

# def getCellVectors(param):

#     xa = np.array([0.0,1.0,0.0 ])
#     xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0,-1.0 ]))         ,     xa      ) 

#     xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 1.0,0.0,0.0 ]))         ,      np.array([0.0,1.0,0.0 ])     ) 
#     print " ora " , xc 

#     sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
#     sa = sa/math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 )
#     omegaa = math.asin( sa  ) 
#     xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([-1.0,0.0,0.0 ]))      ,    xc   ) 

#     xa  = xa*param.AA
#     xb  = xb*param.BB
#     xc  = xc*param.CC
#     cellvectors = np.array([ xa,xb,xc ] )
#     return cellvectors


# If the number is odd returns -1
# If two numbers are equal retorns zero
def ciclicity(a,b,c):
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1   

def getCellVectors(param):
    xa = np.array([0.0,1.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0, -1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 1.0,0.0,0.0 ]))         ,      np.array([0.0,1.0,0.0 ])     ) 
    print " ora " , xc 

    sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
    sa = sa/math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 )
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([-1.0,0.0,0.0 ]))      ,    xc   ) 

    xa  = xa*param.AA
    xb  = xb*param.BB
    xc  = xc*param.CC
    cellvectors = np.array([ xa,xb,xc ] )

    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   )
 


    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 )
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   ) 

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar

    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume


    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")


def analisi (D,hkl) :
    err = round(D)-D
    D = round(D)
    errc= numpy.round(hkl)-hkl
    poss =[]
    for h in range(6):
        for k in range(6):
            for l in range(6):
                if D == h*h+l*l+k*k:
                    poss.append([h,k,l])
    return poss, err*err, (errc*errc).sum()
            
    


#--------------------------------------------------------------------------------------------------------
# 3D Main
#--------------------------------------------------------------------------------------------------------

def main3d(params   ):
     

    cellvectors, brillvectors = getCellVectors(params)

    maxBrill = np.max(np.sqrt(    (brillvectors*brillvectors).sum(axis=-1) ))


    file_maxima = open(params.maxima_file,"r")
    maxima = pickle.load(file_maxima)
    dim1,dim2 = pickle.load(file_maxima)
    params.dim1 = dim1
    params.dim2 = dim2
    
    if 1:
        dim1, dim2 = params.dim1, params.dim2
        print 'Image Dimension :', dim1, dim2
        def get_Q0(dim1,dim2, params) :
            p0 = P0(params.dist, params.beam_tilt_angle)
            MD = [MD0,MD1, MD2, MD3, MD4 , MD5, MD6 , MD7 ][params.orientation_codes]
            Q0 = np.zeros((dim1, dim2, 3), dtype=np.float32)
            
            P_total_tmp = np.zeros((dim1, dim2 , 3), dtype=np.float32)
            P_total_tmp[:, :, 0 ] = -params.dist
            
            A,B = dim1 - params.det_origin_X  , dim2 -   params.det_origin_Y
            
            P_total_tmp[:, :, 1 ] = (np.arange(dim1) -   A    )[:,None]
            P_total_tmp[:, :, 2 ] = (np.arange(dim2) -   B    )[None,:]
            
            P_total_tmp[:, :, 1:3]=  (np.tensordot(P_total_tmp[:, :, 1:3],MD , axes=([2], [1]))) * params.pixel_size
            
            
            XY_array_tmp = np.array(P_total_tmp[:, :, 1:3] )
            
            P_total_tmp = np.tensordot(P_total_tmp, DET(params.theta, params.theta_offset, params.d2, params.d1), axes=([2], [1]))
            
            P_total_tmp_modulus = np.sqrt(np.sum(P_total_tmp * P_total_tmp, axis= -1))
            
            Q0_tmp = P_total_tmp.T / P_total_tmp_modulus.T
            Q0 = ((Q0_tmp.T + p0 / params.dist) / params.lmbda).astype(np.float32)
            return Q0
        
        
        Q0 = get_Q0( dim1, dim2, params)
        

        angular_step = params.angular_step

        params.phi =0.0
        massimo=0.0
        i=0


        variables_dict  = {
            "r1": minimiser.Variable(0.0,-2,2),
            "r2": minimiser.Variable(-4,-6,10),
            "r3": minimiser.Variable(0.0,-2,2) ,
            "AA": minimiser.Variable(0.0,-0.1,0.1),
            "BB": minimiser.Variable(0.0,-0.1,0.1),
            "CC": minimiser.Variable(0.0,-0.1,0.1),
            
            "omega": minimiser.Variable(0.0,-2.0,2.),
            "alpha": minimiser.Variable(0.0,-2.0*0,6.0*0),
            "beta": minimiser.Variable(0.0,-.5*0,.5*0),
            "kappa": minimiser.Variable(0.0,-2.0,10.0) ,
            
            "d1": minimiser.Variable(0.0,-0.0,0.0),
            "d2": minimiser.Variable(0.0,-0.0,0.0),
            
            "det_origin_X": minimiser.Variable(0.0,-20,20),
            "det_origin_Y": minimiser.Variable(0.0,-20,20)
            }

        variables_dict  = params.variations
        
        variables = []

        for key in variables_dict.keys():
            v=variables_dict[key]
            if( type(variables_dict[key]) !=type(1)   ):
                variables.append(variables_dict[key] )
                v.value += getattr(params,  key )
                v.min   += getattr(params,  key )
                v.max   += getattr(params,  key )
            else:
                # variables_dict[key] = variables_dict[[key]]
                variables_dict[key] = variables[variables_dict[key]]
                
        ##variables_dict["CC"] = variables_dict["BB"] = variables_dict["AA"]
            
        SHOW=[None,None,None]
        class fitProxy:
            def __init__(self,variables_dict, maxima):
                self.variables_dict=variables_dict
                

                self.maxima=maxima
                
            def error(self, write=0):
                vals = []
                vnam=[]
                for key in self.variables_dict.keys():
                    v=self.variables_dict[key]
                    setattr(params,  key,  v.value)
                    vals.append(v.value)
                    d = v.max-v.min
                    if(d>0) :
                        if (v.value-v.min)*(v.max-v.value)/d/d <0.05:
                            if( (v.value-v.min) > (v.max-v.value) ): 
                                print key , " verso il alto "
                            else:
                                print key , " verso l'basso "
                    vnam.append( key )
                Q0 = get_Q0 ( dim1, dim2, params)
                
                ERR=0
                ERRC=0
                MAX=0
                params.phi  = 0
                maxs=[]
                errs=[]
                hkls=[]
                hkls_signed=[]
                
                fnlist=[]                
                for fname, (tok, iim) in self.maxima.items():
                    fnlist.append((iim,fname))
                

                qtotal=[]
                dists = []
                for iim, fname in fnlist:
                    params.phi = iim*params.angular_step
                    
                    if not self.maxima.has_key(fname):
                        continue
                    
                    [posY, posX], n = self.maxima[fname]

                    Qfin = get_Qfin(Q0[posY, posX ] , params)
                    
                    for i in range(len(Qfin)):
                        qtotal.append(Qfin[i])
                        dists.append(  (Qfin[i]*Qfin[i]).sum() )
                        
                if params.npoints:
                    inds = numpy.argsort(dists)
                    qtotal=numpy.array(qtotal)[  inds[:params.npoints] ]


                
                for QQ in qtotal:
                    hkl  = (cellvectors*QQ).sum(axis=-1)
                    hkls.append( numpy.round(abs(hkl))  )
                    hkls_signed.append( numpy.round(hkl)  )
                    # print hkl
                    MAX=max(MAX, ( abs( abs(hkl)  -numpy.round(abs(hkl)))).max())
                    maxs.append( ( abs( abs(hkl)  -numpy.round(abs(hkl)))).max()) 
                    # print " hkl  = ", hkl
                    hkl2 = (hkl*hkl).sum()
                    a, err, errC = analisi(hkl2, hkl)
                    # print " hkl2 = " , hkl2 , a
                    ERR=ERR+err
                    ERRC+=errC
                    errs.append(errC)
                # print " ERR " , ERR, ERRC
                if write:
                    print " MAX-- ", MAX
                    print numpy.asarray(qtotal)
                    numpy.savetxt("q3d.txt",numpy.asarray(qtotal))

                ordine = numpy.argsort(maxs ) 

                
                hkls = numpy.array(hkls)[ ordine]
                maxs = numpy.array(maxs)[ ordine]
                SHOW[0]=hkls
                SHOW[2]=hkls_signed
                SHOW[1]=maxs
                errs.sort()
                print maxs
                ERRC = numpy.array(errs[:-20]).sum()
                print ERRC                
                for _v, _n in zip( vnam, vals):
                    print _v," = ", _n
                file = open("variables.txt","w")
                for name , v in zip(vnam, vals):
                    file.write("%s = %s\n"%(name,v))
                file.close()
                return ERRC

            
            def setitercounter(self, n):
                self.itc=n
            def getitercounter(self):
                return self.itc
       
        
        fitobject = fitProxy(variables_dict, maxima)
        
        tominimise=fitobject
        miniob=minimiser.minimiser(tominimise,variables)
            

        
        if not params.transform_only:
            print " LAUNCHING MINIMIZATION "
            miniob.amoeba(params.ftol)
            # miniob.amoeba(0.00000000000001)  
            # miniob.amoeba(0.00000000000001)  

            print fitobject.error()

        else:
            print " JUST TRANSFORMING ONCE TO CHECK "
            fitobject.error(write=1)
        
        hkls, maxs, hkls_signed = SHOW
        for h,m in zip ( hkls_signed, maxs) :
            print h,m
        
        for key in variables_dict.keys():
            v=variables_dict[key].value
            print key,"=", v
        

            
def main(params):
    
    paramsSystem = read_configuration_file(sys.argv[1])
    main3d(paramsSystem)
    print " processo ", myrank, " aspetta "
    comm.Barrier()

USAGE = """ USAGE :
    tds2el inputfile

input file containing


"""
print __name__ 

# if __name__ == "__main__":

if(sys.argv[0][-12:]!="sphinx-build"):

    if len(sys.argv) < 2:
        print USAGE
    else:

        class Parameters:
            conf_file=None
            filter_file = None
            images_prefix = None
            volume_reconstruction = True
            exec(open(sys.argv[1]).read())
        main( Parameters())

    # sys.exit()


