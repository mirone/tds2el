#!/usr/bin/env python
# -*- coding: utf-8 -*-
#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/
from __future__ import print_function


import glob
# import fftw3f
import math


import h5py
import pylab
import random 
import minimiser
import numpy
import os
import pickle
import collections
import copy
import r2quat
import traceback

__doc__=(
r"""
 The aim of tds2el_analyse_maxima_fourier is to do an alignement of the sample
  and a refinement of a selectable set of alignement and geometry
  parameters. In the first phase it can be runned to just convert
  maxima positions to reciprocal space points.
  This is done with option ::

     transform_only  = 1 # or True

  It will the generate a three columns file *q3d.txt* containing reciprocal space points.
  
  These points can be visualised in 3D graphics using *tds2el_rendering*.
  The idea is that when the initial parameters are not too bad and the conventions
  are not wrong, then the points should be regularly placed in 3D space.
  Then and only then   transform_only can be set to 0, and refinement can start,
  first by aligning the sample and finding approximate lattice parameters,
  Then by global optimisation.

       
   * tds2el_analyse_maxima_fourier  is always runned with two arguments ::
 
      tds2el_analyse_maxima_fourier  input_file

     An example of input file is given below. As a first step you can simply convert the maxima prositions,
     that you have extracted with tds2el_extract_maxima, to 3d points in reciprocal space. To do this you need
     to have a rough idea of the geometry :download:`pdf <geometry.pdf>` and to set the option  ``transform_only  = 1``.

   * Supposing that you have done a first run with ``transform_only=1`` , then a three-columns file ``q3d.txt`` has been produced with
     reciprocal space positions.  You can have a look at how regular it is. If points are regularly placed, then we can proceed to
     the further steps which consists in aligning the sample and fine refinement of the geometry. To visualise the maxima in reciprocal space  ::

      tds2el_rendering q3d.txt

     .. image:: pymol_alignement.png

   * Example of input file to verify preliminary alignement (``transform_only=1``) and then to align the sample axis (``transform_only=0``). Geometry is described :download:`here <geometry.pdf>`.
     Multiple solutions exists:as an example here you would get a good alignement  with ``orientation_codes=2``, instead of 0,   if you change sign to kappa and omega(for this particular geometry). ::
 
        #########################################
        ### Really important parameters
        ## to be set to discover what the geometry conventions
        ## really are

        maxima_file = "selectedpeaks2.p"

        orientation_codes=0  # 8 possibilities look at analyse_maxima_fourier.py
        theta =  19.0
        theta_offset = 0.0

        alpha = 50.0
        beta = 0.0
        angular_step = +0.1

        det_origin_Y = 548.0 
        det_origin_X = 513.0-20

        dist = 244.467
        pixel_size = 0.172
        kappa = 60.0
        omega = -20.3

        transform_only  = 1

        # lmbda is not so important in the desperate phase. It just expand the size of the lattice
        lmbda = 0.98

        #####################################################
        ### Other parameters that are generally small or that can
        ##  be refined later like r1,r2,r3
        beam_tilt_angle = 0.48067
        d1 = -0.19731
        d2 = -0.37025
        omega_offset =  0
        start_phi = 0
        r1 =  0
        r2 = 0
        r3 =  0
        ftol = 1.0e-7
        AA = 0
        BB = 0
        CC = 0
        aAA = 90
        aBB = 90
        aCC = 90

        variations =  {
        }

   *  Fourier alignement : To get the alignement of the sample set ``transform_only=0``. When  ``r1,r2,r3`` are all equal to zero, this triggers an automatic finding of the cristallography axis
      by a study of the Fourier transform of the peak distribution. In the above example ``aAA,aBB,aCC`` are given. This constraints the found axis to have cosinus which are 
      at no more than  a 0.05 distance from the cosinus of ``aAA,aBB,aCC``. If one of these is None, no particular constraint is applied on that cosinus. The axis are searched in the order a,b,c
      from the shortest to the longest. 
           
          * in some difficult cases you may use lim_aa variable which must contain lower and upper limit for AA ( Angstroems units) ::

                  lim_aa = [4.0,4.4]

          which helps finding the proper first axis.

      By running ``tds2el_analyse_maxima_fourier  input_file`` you get an estimation of the alignement ::

        scisoft12> tds2el_analyse_maxima_fourier_v1  ana2.par
        MPI LOADED , nprocs =  1
        tds2el_v1.analyse_maxima_fourier
        Image Dimension : 1043 981
         FMOD  15
         FMOD  19
         FMOD  23
        #############
        ESTIMATION by FOURIER 
        AA=5.342002e+00
        BB=6.766536e+00
        CC=8.191069e+00
        aAA=9.036321e+01
        aBB=9.014279e+01
        aCC=8.920714e+01
        r3=-1.630977e+02
        r2=3.897638e+01
        r1=-9.677916e+01 
         ############################ 
         processo  0  aspetta 


      In reality you get several possible choices. You may have several choice first because of the symmetry operation, second because fro some lattices, like Calcite,
      the same distribution of Bragg peaks can be fitted with almost as good precision with a wrong set of axis having very close AA,BB,CC and angles
      but the wrong orientation. When several choices are possible they are ranked according to the suggestion given by the inputted AA,BB,CC,
      the choice which is closest to the inputted combination of AA,BB,CC is printed last, so that it is the most easily  visible on the screen.
      Now you can insert these estimation in the input file, and see how well it is aligned now by resetting ``transform_only=1``, with subsequent regeneration of ``q3d.txt`` and rendering

     .. image:: pymol_fourieraligned.png

   *  You can update the input file with these parameters, then to do a further optimisation, you can express the 
      search range around some selected variables ::

        variations =  {
            "r1": minimiser.Variable(0.0,-2,2),
            "r2": minimiser.Variable(0,-6,10),
            "r3": minimiser.Variable(0.0,-2,2) ,
            "AA": minimiser.Variable(0.0,-0.1,0.1),
            "BB": minimiser.Variable(0.0,-0.1,0.1),
            "CC": minimiser.Variable(0.0,-0.1,0.2),
            "aAA": minimiser.Variable(0.0,-0.1,0.1),
            "aBB": minimiser.Variable(0.0,-0.1,0.1),
            "aCC": minimiser.Variable(0.0,-0.1,0.2),
            "omega": minimiser.Variable(0.0,-2.0,2.),
            "alpha": minimiser.Variable(0.0,-2.0*0,6.0*0),
            "beta": minimiser.Variable(0.0,-.5*0,.5*0),
            "kappa": minimiser.Variable(0.0,-2.0,10.0) ,
            "d1": minimiser.Variable(0.0,-0.0,0.0),
            "d2": minimiser.Variable(0.0,-0.0,0.0),
            "det_origin_X": minimiser.Variable(0.0,-20,20),
            "det_origin_Y": minimiser.Variable(0.0,-20,20),
            "dist": minimiser.Variable(0.0,-10,10)   		    
        }


      these specify the variations which are added to the nominal values. A variable that is not appearing here is fixed.
      The syntax is ``minimiser.Variable(initial_value,min,max)``. if min and max are equal. The variable is fixed.
      The used algorithm is simplex descent, which is slow but roboust.  The stop is determined by the *ftol* parameter.
      During the fit an output is produced at each step  with the following form ::

        ----------------------------------------------------------------------------------------------------
        THESE ARE THE MAXIMUM DEVIATION FROM INTEGER FOR EACH PEAK POSITION
        [ 0.00769985  0.00874746  0.0103178   0.01073825  0.01460457  0.01573443
          0.01678818  0.01696467  0.01738405  0.0190767   0.01931381  0.02025402
          0.02047992  0.02066052  0.02079439  0.02093673  0.02123821  0.0218755
          0.02206755  0.02210927  0.02229428  0.02239919  0.02248192  0.0225352
          0.02289104  0.02290201  0.02321625  0.02379918  0.02390862  0.0244956
          0.02491748  0.0251143   0.02519226  0.02532196  0.02573431  0.02581072
          0.02606213  0.02613378  0.02687216  0.02693677  0.02699292  0.02735329
          0.02740729  0.02866364  0.02887559  0.02986479  0.03001773  0.03003705
          0.03034055  0.03042722  0.03121853  0.03122759  0.03131694  0.0313735
          0.03142095  0.03175396  0.03199106  0.03258067  0.03260487  0.03271961
          0.0332849   0.03333455  0.03341007  0.03370857  0.0339517   0.03398085
          0.03427243  0.03504014  0.03567719  0.03595769  0.03609461  0.03629518
          0.03699565  0.03701109  0.037045    0.03711748  0.03732347  0.03821397
          0.03824425  0.03832698  0.03909755  0.03949523  0.04051018  0.04076242
          0.04114318  0.04152346  0.04158688  0.04177046  0.04212689  0.04253507
          0.04272914  0.0432117   0.04632831  0.04675317  0.04820037  0.05184102
          0.0528053   0.05769324  0.05784869  0.05879927  0.05955482  0.06063747
          0.06166363  0.06746936]
         ERROR  0.160689
         ----------------- obtained with variables here below ----------------------
        AA  =  5.29437816027
        aCC  =  90.0475179112
        aBB  =  90.0377539549
        aAA  =  89.9699951653
        r1  =  -97.0917210501
        r2  =  37.4926288928
        r3  =  -164.837866817
        CC  =  8.49977434955
        kappa  =  60.1505399461
        BB  =  6.74705222684
        d2  =  0.419158100662
        beta  =  0.0
        det_origin_X  =  506.027292059
        det_origin_Y  =  527.678671914
        alpha  =  50.0
        omega  =  -18.9479007388
        dist  =  241.047652156
        d1  =  -0.49890822079

     if a variable is hitting against one of the border a message is produced so that you can change the search span. The list of numbers is the maximum deviation, for each spot, from the nearest integer for hkl coordinates.

   * The refined alignement can be visualised again .. image:: pymol_refined_alignement.png

     .. image:: pymol_refined_alignement.png

    Which is not so bad. Still the deviations from integer hkl are not so small and for a good fit of elastic constants we need to do much better.
    The discrepancy may come from several effect.

     * Errors in the peak position detection. This might happen typically if the crystal is big so that the spot is large. or because there is not enough resolution.
       In this case the subsequent step  will consist in fitting the diffused scattering, optimising at the same time the elastic constants and few selected variable of the 
       geometry like r1,r2,r3, kappa, omega, AA,BB,CC. The advantage of this procedure is that the poistion information is distributed in the global envelop of each spot
       that we can fit globally, and we have many peaks but few geometry parameters.

     * The sample is very thin, so that the spot are well defined but there may be some mechanical instability, due to the thin sructure, and also 
       x-ray radiation could develop some stress with accompanying deformation.  In this case the Bragg spot is very sharp and we can take its position 
       as the center of the spot. 

"""

)
try:
    from mpi4py import MPI
    myrank = MPI.COMM_WORLD.Get_rank()
    nprocs = MPI.COMM_WORLD.Get_size()
    procnm = MPI.Get_processor_name()
    comm = MPI.COMM_WORLD
    print( "MPI LOADED , nprocs = ", nprocs)
except:
    nprocs=1
    myrank = 0
    print( "MPI NOT LOADED ")


# import sift
from math import pi, sin, cos

import numpy as np
import sys, time
__version__ = 1.0

if(sys.argv[0][-12:]!="sphinx-build"):
    import spotpicker_cy


if(sys.argv[0][-12:]!="sphinx-build"):
    pass


#--------------------------------------------------------------------------------------------------------
# Rotation, Projection and Orientation Matrix
#--------------------------------------------------------------------------------------------------------

# e' una rotazione degli assi non delle coordinate
#  va bene cosi perche si ruota il campione quindi il raggio controruota nello spazio K
def Rotation(angle, rotation_axis=0):
    if type(rotation_axis) == type("") :
        rotation_axis = {"x":0, "y":1, "z":2}[rotation_axis]
    assert((rotation_axis >= 0 and rotation_axis <= 3))
    angle = np.radians(angle)
    ret_val = np.zeros([3, 3], np.float32)
    i1 = rotation_axis
    i2 = (rotation_axis + 1) % 3
    i3 = (rotation_axis + 2) % 3
    ret_val[i1, i1  ] = 1
    ret_val[i2, i2  ] = np.cos(angle)
    ret_val[i3, i3  ] = np.cos(angle)
    ret_val[i2, i3  ] = np.sin(angle)
    ret_val[i3, i2  ] = -np.sin(angle)
    return ret_val

#--------------------------------------------------------------------------------------------------------

# questa rispetta la convenzione Ma per le cordinate   Rotation(45.0,"z") != Arb_Rot(45.0,[0,0,1])
def Arb_Rot(angle, rot_axis):
    angle = np.radians(angle)
    assert(len(rot_axis)==3)
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat

#--------------------------------------------------------------------------------------------------------

def Mirror(mirror_axis):
    x,y,z = {"x":[-1,1,1], "y":[1,-1,1], "z":[1,1,-1],0:[-1,1,1], 1:[1,-1,1], 2:[1,1,-1]}[mirror_axis]
    mat = np.array([[x,0,0],[0,y,0],[0,0,z]])
    return mat 

#--------------------------------------------------------------------------------------------------------

def Prim_Rot_of_RS(omega, phi, kappa, alpha, beta, omega_offset):
    # """ 
    # Primary rotation of reciprocal space. 
    # Omega, kappa, phi are the nominal values of the angle
    # """
    # omega=-omega
    # phi  = -phi
    # kappa = -kappa
    # alpha=-alpha
    # beta=-beta

    tmp = Rotation(omega + omega_offset, 2)
    tmp = np.dot(tmp, Rotation(alpha, 1))
    tmp = np.dot(tmp, Rotation(kappa, 2))
    tmp = np.dot(tmp, Rotation(-alpha, 1))
    tmp = np.dot(tmp, Rotation(beta, 1))
    tmp = np.dot(tmp, Rotation(phi, 2))
    tmp = np.dot(tmp, Rotation(-beta, 1))
    return tmp

#--------------------------------------------------------------------------------------------------------

def DET(theta, theta_offset, d2, d1):
    # """ 
    # Rotation matrix for the detector 
    # theta is the nominal theta value and d1,D2 are the tilts of detector
    # """
    tmp = Rotation(theta + theta_offset, 2)
    tmp = np.dot(tmp, Rotation(d2, 1))
    tmp = np.dot(tmp, Rotation(d1, 0))
    return tmp

#--------------------------------------------------------------------------------------------------------    

def Snd_Rot_of_RS(r1, r2, r3):
    # """ Secondary rotation of reciprocal space (to orient the crystallographic axis in a special way) """
    # r1=-r1
    # r2=-r2
    # r3=-r3
    
    tmp = Rotation(r3, 2)
    tmp = np.dot(tmp, Rotation(r2, 1))
    tmp = np.dot(tmp, Rotation(r1, 0))
    
    return tmp

#--------------------------------------------------------------------------------------------------------

def P0(dist, b2):
    # """ Primary projection of pixel coordinates (X,Y) to the reciprocal space. """
    B = Rotation(b2, 1) # Beam tilt matrix
    p0 = np.dot(B, [ dist, 0, 0 ])
    return p0

#--------------------------------------------------------------------------------------------------------
# Corrections
#--------------------------------------------------------------------------------------------------------

MD0 = np.array([[1, 0], [0, 1]  ], dtype=np.int32)

MD1 = np.array([[-1, 0], [0, 1] ], dtype=np.int32)

MD2 = np.array([[1, 0], [0, -1] ], dtype=np.int32)

MD3 = np.array([[-1, 0], [0, -1]], dtype=np.int32)
MD4 = np.array([[0, 1], [1, 0]  ], dtype=np.int32)
MD5 = np.array([[0, -1], [1, 0] ], dtype=np.int32)
MD6 = np.array([[0, 1], [-1, 0] ], dtype=np.int32)
MD7 = np.array([[0, -1], [-1, 0]], dtype=np.int32)

#--------------------------------------------------------------------------------------------------------
# Parameter Class
#--------------------------------------------------------------------------------------------------------


def read_configuration_file(cfgfn):
	# """
	# cfgfn is the filename of the configuration file.
	# the function return an object containing information from configuration file (cf inside cfg file).
	# """
	
	try:
            s=open(cfgfn,"r")
	except:
		print( " Error reading configuration file " ,  cfgfn			)
		exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
		print( "*** print_exception:")
		traceback.print_exception(exceptionType, exceptionValue, exceptionTraceback,
                              limit=None, file=sys.stdout)
		raise Exception()
	class Parameters():
            npoints=0
            transform_only  = 0
            variations = {}
            AA = 1.0
            BB = 1.0
            CC = 1.0
            aAA = 90.0
            aBB = 90.0
            aCC = 90.0
            r1=0
            r2=0
            r3=0
            USEQUAT=False
            lim_aa= None
            start_phi = 0 
            exec(s)
	cfg = Parameters()
	s.close()
	return cfg

def mag_max(l):
    # """
    # l is a list of coordinates
    # """
    return max(np.sqrt(np.sum(l * l, axis= -1)))
    
#--------------------------------------------------------------------------------------------------------


def   get_Qfin( Q0,  
               params):


    R = Prim_Rot_of_RS(params.omega, params.phi, params.kappa, params.alpha, params.beta, params.omega_offset)




    if params.USEQUAT == False:
        U = Snd_Rot_of_RS(params.r1, params.r2, params.r3)
    else:
        # print params.r1, params.r2, params.r3
        U = r2quat.quat2M((params.r1, params.r2, params.r3))
        
    
    Q = np.tensordot (Q0 , R.T , axes=([-1], [1]))
    
    Qfin = np.tensordot (Q , U.T , axes=([-1], [1]))
    
    return Qfin

############################################################################################
## ASSI CRISTALLOGRAFICI

def modulus(a):
    return np.sqrt((a*a).sum())

def arcCos(   a,b   ):
    # print a,b
    c = (a*b).sum()
    
    c=c/modulus(a)
    c=c/modulus(b)
    # print c
    if c>1: c=1
    if c<-1:c=-1
    return np.arccos(c)
    

# If the number is odd returns -1
# If two numbers are equal retorns zero
def ciclicity(a,b,c):
  if( a==b or  a==c or c==b):
    return 0
  elif(  (b-a)*(c-b)*(a-c) <0  ):
    return 1
  else:
    return -1   

def getCellVectors(param):
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( param.aCC,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot(-param.aBB,np.array([ 0.0,1.0,0.0 ]))         ,      np.array([1.0,0.0,0.0 ])     ) 
    # print " ora " , xc 
    sa = ( math.cos( param.aBB*math.pi/180 )*math.cos( param.aCC*math.pi/180 )-math.cos( param.aAA*math.pi/180 ))
    sa = sa/(math.sin( param.aBB*math.pi/180 )*math.sin( param.aCC*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   )

    
    xa  = xa*param.AA
    xb  = xb*param.BB
    xc  = xc*param.CC
    cellvectors = np.array([ xa,xb,xc ] )

    AntiSymm= np.array([ [ [ ciclicity(i,j,k) for k in range(0,3) ] for j in range (0,3) ] for i in range(0,3) ])
    cellVolume=np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )
    cellVolume=np.dot(cellvectors[0],cellVolume)
    Brillvectors=np.zeros([3,3], np.float32)
    Brillvectors[0]= np.dot( np.dot(AntiSymm, cellvectors[2]),cellvectors[1] )/ cellVolume
    Brillvectors[1]= np.dot( np.dot(AntiSymm, cellvectors[0]),cellvectors[2] )/ cellVolume
    Brillvectors[2]= np.dot( np.dot(AntiSymm, cellvectors[1]),cellvectors[0] )/ cellVolume

    alphastar   =   arcCos(  Brillvectors[1],  Brillvectors[2]   ) *180/math.pi
    betastar    =   arcCos(  Brillvectors[0],  Brillvectors[2]   ) *180/math.pi
    gammastar   =   arcCos(  Brillvectors[0],  Brillvectors[1]   ) *180/math.pi

    Astar,  Bstar,  Cstar  =  map( modulus,  [ Brillvectors[0],  Brillvectors[1],  Brillvectors[2] ]   )
 
    xa = np.array([1.0,0.0,0.0 ])
    xb = np.dot(  Arb_Rot( gammastar ,np.array([0.0,0.0, 1.0 ]))         ,     xa      ) 
    xc = np.dot(  Arb_Rot( -betastar , np.array([ 0.0,1.0,0.0 ])) ,      np.array([1.0,0.0,0.0 ])     ) 

    sa = ( math.cos( betastar*math.pi/180 )*math.cos( gammastar*math.pi/180 )-math.cos( alphastar*math.pi/180 ))
    sa = sa/(math.sin( betastar*math.pi/180 )*math.sin( gammastar*math.pi/180 ))
    omegaa = math.asin( sa  ) 
    xc = np.dot(  Arb_Rot( -omegaa*180.0/math.pi  ,np.array([1.0,0.0,0.0 ]))      ,    xc   )

    Brillvectors[0]=  xa    *Astar
    Brillvectors[1]=  xb    *Bstar
    Brillvectors[2]=  xc    *Cstar
    cellVolume=np.dot( np.dot(AntiSymm,Brillvectors [2]),Brillvectors[1] )
    cellVolume=np.dot(Brillvectors[0],cellVolume)
    cellvectors=np.zeros([3,3], np.float32)
    cellvectors[0]= np.dot( np.dot(AntiSymm, Brillvectors[2]),Brillvectors[1] )/ cellVolume
    cellvectors[1]= np.dot( np.dot(AntiSymm, Brillvectors[0]),Brillvectors[2] )/ cellVolume
    cellvectors[2]= np.dot( np.dot(AntiSymm, Brillvectors[1]),Brillvectors[0] )/ cellVolume


    return np.array(cellvectors,"f"), np.array(Brillvectors,"f")


def analisi (D,hkl) :
    err = round(D)-D
    D = round(D)
    errc= numpy.round(hkl)-hkl
    poss =[]
    for h in range(6):
        for k in range(6):
            for l in range(6):
                if D == h*h+l*l+k*k:
                    poss.append([h,k,l])
    return poss, err*err, (errc*errc).sum()
            
    


#--------------------------------------------------------------------------------------------------------
# 3D Main
#--------------------------------------------------------------------------------------------------------

def main3d(params   ):
     

    # cellvectors, brillvectors = getCellVectors(params)


    file_maxima = open(params.maxima_file,"r")
    maxima = pickle.load(file_maxima)
    dim1,dim2 = pickle.load(file_maxima)
    params.dim1 = dim1
    params.dim2 = dim2

    # print maxima
    # print dim1,dim2

    
    if 1:
        dim1, dim2 = params.dim1, params.dim2
        print( 'Image Dimension :', dim1, dim2)
        def get_Q0(dim1,dim2, params) :
            p0 = P0(params.dist, params.beam_tilt_angle)
            MD = [MD0,MD1, MD2, MD3, MD4 , MD5, MD6 , MD7 ][params.orientation_codes]
            

            det_mat = DET(params.theta, params.theta_offset, params.d2, params.d1)

            if True:
                Q0 = np.zeros((dim1, dim2, 3), dtype=np.float64)
                spotpicker_cy.get_Q0( Q0 , params.dist, params.det_origin_X  ,
                                      params.det_origin_Y,  params.pixel_size  , det_mat.astype("d"), p0,params.lmbda , MD.astype("d") )
            else:
                Q0 = np.zeros((dim1, dim2, 3), dtype=np.float64)
                P_total_tmp = np.zeros((dim1, dim2 , 3), dtype=np.float32)
                P_total_tmp[:, :, 0 ] = -params.dist
                referenza = np.array([  -params.dist  ,0,0  ])
                A,B = params.det_origin_X  , params.det_origin_Y
                
                P_total_tmp[:, :, 2 ] = (np.arange(dim1) -   B    )[:,None]
                P_total_tmp[:, :, 1 ] = (np.arange(dim2) -   A   )[None,:]

                
                P_total_tmp[:, :, 1:3]=  (np.tensordot(P_total_tmp[:, :, 1:3],MD , axes=([2], [1]))) * params.pixel_size
                P_total_tmp = np.tensordot(P_total_tmp,det_mat , axes=([2], [1]))
                P_total_tmp_modulus = np.sqrt(np.sum(P_total_tmp * P_total_tmp, axis= -1))
                Q0_tmp = P_total_tmp.T / P_total_tmp_modulus.T
                Q0 = ((Q0_tmp.T + p0 / params.dist) / params.lmbda).astype(np.float32)

            
            # print Q0.shape
            # print dim1,dim2
            # print B,A
            
            return Q0
        
        
        Q0 = get_Q0( dim1, dim2, params)
        

        angular_step = params.angular_step

        params.phi =0.0
        massimo=0.0
        i=0


        variables_dict  = {
            "r1": minimiser.Variable(0.0,-2,2),
            "r2": minimiser.Variable(0,-6,10),
            "r3": minimiser.Variable(0.0,-2,2) ,
            "AA": minimiser.Variable(0.0,-0.1,0.1),
            "BB": minimiser.Variable(0.0,-0.1,0.1),
            "CC": minimiser.Variable(0.0,-0.1,0.1),
            
            "omega": minimiser.Variable(0.0,-2.0,2.),
            "alpha": minimiser.Variable(0.0,-2.0*0,6.0*0),
            "beta": minimiser.Variable(0.0,-.5*0,.5*0),
            "kappa": minimiser.Variable(0.0,-2.0,10.0) ,
            
            "d1": minimiser.Variable(0.0,-0.0,0.0),
            "d2": minimiser.Variable(0.0,-0.0,0.0),
            
            "det_origin_X": minimiser.Variable(0.0,-20,20),
            "det_origin_Y": minimiser.Variable(0.0,-20,20)
            }

        variables_dict  = params.variations
        
        variables = []

        for key in variables_dict.keys():
            v=variables_dict[key]
            if( type(variables_dict[key]) !=type(1)   ):
                variables.append(variables_dict[key] )
                v.value += getattr(params,  key )
                v.min   += getattr(params,  key )
                v.max   += getattr(params,  key )
            else:
                # variables_dict[key] = variables_dict[[key]]
                variables_dict[key] = variables[variables_dict[key]]
                
        ##variables_dict["CC"] = variables_dict["BB"] = variables_dict["AA"]
            
        SHOW=[None,None,None]
        class fitProxy:
            def __init__(self,variables_dict, maxima):
                self.variables_dict=variables_dict
                

                self.maxima=maxima
                
            def error(self, write=0):

                
                vals = []
                vnam=[]
                for key in self.variables_dict.keys():
                    v=self.variables_dict[key]
                    setattr(params,  key,  v.value)
                    vals.append(v.value)
                    d = v.max-v.min
                    if(d>0) :
                        if (v.value-v.min)*(v.max-v.value)/d/d <0.05:
                            if( (v.value-v.min) > (v.max-v.value) ): 
                                print( key , " constraints too tight on upper side ")
                            else:
                                print( key , " constraints too tight on lower side ")
                    vnam.append( key )

                if params.aAA is not None:
                    cellvectors, brillvectors = getCellVectors(params)


                    
                Q0 = get_Q0 ( dim1, dim2, params)
                
                ERR=0
                ERRC=0
                MAX=0
                params.phi  = 0
                maxs=[]
                errs=[]
                hkls=[]
                hkls_signed=[]
                
                # fnlist=[]                
                # for fname, (tok, iim) in self.maxima.items():
                #     fnlist.append((iim,fname))
                

                qtotal=[]
                dists = []
                for  fname , infos in self.maxima.items():
                   for pos, infopeak in infos.items():

                    # print pos
                    # print infopeak
                    # print infopeak.keys()   
                    iim = infopeak["Gi"] # 'mask' , 'pos' (ripos),  'II', 'fn', 'pmap', 'accepted', 'Gi'
                    params.phi = params.start_phi + iim*params.angular_step
                    
                    if not self.maxima.has_key(fname):
                        continue
                    
                    # [posY, posX], n = self.maxima[fname]
                    posY, posX = pos

                    Qfin = get_Qfin(Q0[posY, posX ] , params)


                    if params.aAA is not None:
                        hkl  = (cellvectors*Qfin).sum(axis=-1)
                        hkl_rounded   = numpy.round(hkl)

                        infopeak["hkl"] = hkl
                        infopeak["hkl_rounded"] = hkl_rounded
                    
                    if not infopeak["accepted"]:
                        continue
                    
                    # print pos
                    # print Q0[posY, posX ]
                    # print Qfin

                    qtotal.append(Qfin)
                    dists.append(  (Qfin*Qfin).sum() )
                    
                    # for i in range(len(Qfin)):
                    #     qtotal.append(Qfin[i])
                    #     dists.append(  (Qfin[i]*Qfin[i]).sum() )
                        
                if params.npoints:
                    inds = numpy.argsort(dists)
                    qtotal=numpy.array(qtotal)[  inds[:params.npoints] ]
                else:
                    qtotal=numpy.array(qtotal)
                # print qtotal.shape

                if write:
                    # print " MAX-- ", MAX
                    # print numpy.asarray(qtotal)
                    numpy.savetxt("q3d.txt",numpy.asarray(qtotal))
                    return numpy.asarray(qtotal)
                
                for QQ in qtotal:
                    hkl  = (cellvectors*QQ).sum(axis=-1)
                    hkls.append( numpy.round(abs(hkl))  )
                    hkls_signed.append( numpy.round(hkl)  )
                    # print hkl
                    MAX=max(MAX, ( abs( abs(hkl)  -numpy.round(abs(hkl)))).max())
                    maxs.append( ( abs( abs(hkl)  -numpy.round(abs(hkl)))).max()) 
                    # print " hkl  = ", hkl
                    hkl2 = (hkl*hkl).sum()
                    a, err, errC = analisi(hkl2, hkl)
                    # print " hkl2 = " , hkl2 , a
                    ERR=ERR+err
                    ERRC+=errC
                    errs.append(errC)
                # print " ERR " , ERR, ERRC

                    
                ordine = numpy.argsort(maxs ) 

                
                hkls = numpy.array(hkls)[ ordine]
                maxs = numpy.array(maxs)[ ordine]
                SHOW[0]=hkls
                SHOW[2]=hkls_signed
                SHOW[1]=maxs
                errs.sort()
                print( "-"*100)
                print( "THESE ARE THE MAXIMUM DEVIATION FROM INTEGER FOR EACH PEAK POSITION" )
                print( maxs)
                # ERRC = numpy.array(errs[:-20]).sum()
                ERRC = numpy.array(errs[:]).sum()
                print( " ERROR " , ERRC)
                print( " ----------------- obtained with variables here below ----------------------")
                for _v, _n in zip( vnam, vals):
                    print( _v," = ", _n)
                file = open("variables.txt","w")
                for name , v in zip(vnam, vals):
                    file.write("%s = %s\n"%(name,v))
                file.close()
                return ERRC

            
            def setitercounter(self, n):
                self.itc=n
            def getitercounter(self):
                return self.itc
       
        
        fitobject = fitProxy(variables_dict, maxima)
        
        tominimise=fitobject



        
        miniob=minimiser.minimiser(tominimise,variables)
            

        
        if not params.transform_only:

            if params.r1==0 and params.r2==0 and params.r3==0:

                qpoints = fitobject.error(write=1)

                if params.aCC==0 and params.aBB ==0 and params.aAA ==0 :                    
                    ds_list,ss_list, abc_list  = Fourier(qpoints ,cc=None, cb = None, ca=None, lim_aa= params.lim_aa)

                else:
                    aCC_info,aBB_info, aAA_info  = [  np.cos( np.radians(tok)) if tok is not None else None   for tok in [ params.aCC,params.aBB,params.aAA ]     ]                     
                    ds_list, ss_list, abc_list  = Fourier(qpoints,cc=aCC_info,cb=aBB_info, ca=aAA_info, lim_aa= params.lim_aa  )


                ordval=np.array([  -np.linalg.norm(np.array(abc)-np.array([params.AA,params.BB,params.CC])) for abc in      abc_list  ] )
                order = np.argsort(ordval)
                for ds, ss, abc in zip( np.array(ds_list)[order], np.array(ss_list)[order], np.array(abc_list)[order]  ) :

                    X,Y,Z = ds
                    aAA = np.degrees(math.acos(  np.dot(Y,Z )   ))
                    aBB = np.degrees(math.acos(  np.dot(X,Z )   ))
                    aCC = np.degrees(math.acos(  np.dot(X,Y )   ))

                    A = ds[0]
                    B = ds[1] - np.dot(ds[1],ds[0])*ds[0]
                    B = B/np.linalg.norm(B)
                    C = np.cross(A,B)

                    # print " DET " 
                    # print np.linalg.det( [X,Y,Z]   )
                    if np.dot(C, ds[2])<0:
                        # print " CAMBIO " 
                        Z = -Z
                        aAA = np.degrees(math.acos(  np.dot(Y,Z )   ))
                        aBB = np.degrees(math.acos(  np.dot(X,Z )   ))
                        aCC = np.degrees(math.acos(  np.dot(X,Y )   ))

                    # print " IN USCITA AVREI "
                    # print ds

                    params.AA,params.BB,params.CC      = abc
                    params.aAA,params.aBB,params.aCC    = aAA, aBB, aCC
                    # print  params.AA,params.BB,params.CC
                    # print  aAA, aBB, aCC
                    # print " in ricostruzione avrei "
                    # print getCellVectors(params)

                    mat_v, mat_vstar =  getCellVectors(params)
                    bAA = np.degrees(math.acos(  np.dot(mat_v[1],mat_v[2] )/np.linalg.norm(mat_v[1]) /np.linalg.norm(mat_v[2])   ))
                    bBB = np.degrees(math.acos(  np.dot(mat_v[0],mat_v[2] )/np.linalg.norm(mat_v[0]) /np.linalg.norm(mat_v[2])   ))
                    bCC = np.degrees(math.acos(  np.dot(mat_v[0],mat_v[1] )/np.linalg.norm(mat_v[0]) /np.linalg.norm(mat_v[1])   ))
                    print( " ########## " )
                    # print bAA, bBB, bCC

                    # print mat_v
                    # print np.array([X*abc[0],  Y*abc[1] ,Z*abc[2] ])
                    rottodo = np.dot(   np.array([X*abc[0],  Y*abc[1] ,Z*abc[2] ]).T, np.linalg.inv(  mat_v.T    )          )

                    # print " SCALARI " 
                    # print np.dot(rottodo[0],rottodo[1] )
                    # print np.dot(rottodo[0],rottodo[2] )
                    # print np.dot(rottodo[2],rottodo[1] )
                    # print np.linalg.det(rottodo)

                    print( "#############\n## ESTIMATION by FOURIER ")
                    print( "AA=%e\nBB=%e\nCC=%e" %tuple( abc))
                    print( "aAA=%e\naBB=%e\naCC=%e" %(aAA,aBB,aCC ))
                    
                    ###### questa matrice deve essere uguale a U di SNDrot
                    ## rot_r3 rot_r2 rot_r1

                    M = np.array(  [A,B,C] ).T


                    M =  rottodo

                    if math.fabs(M[2,0])!=1.0:
                        r1 = math.atan2(M[2,1],M[2,2])
                        if  (math.fabs(M[2,1]/math.sin(r1))>math.fabs(M[2,2]/math.cos(r1))):
                            r2 = math.atan2( -M[2,0], M[2,1]/math.sin(r1)  )
                        else:
                            r2 = math.atan2( -M[2,0], M[2,2]/math.cos(r1)  )

                        r3 = math.atan2(M[1,0],M[0,0])
                    else:
                        raise Exception( " ce cas reste a programmer, http://www.chrobotics.com/library/understanding-euler-angles ")

                    r3,r2,r1 = np.degrees( [ -r3,-r2,-r1 ] )

                    print( "r3=%e\nr2=%e\nr1=%e "%( r3,r2,r1))
                    print( "#############\n## FOLLOWING PARAMETERS ARE QUATERNIONS COEFFICIENTS. ")
                    print( "#############\n## TOBEUSED WITH USEQUAT=True ")
                    print( "# USEQUAT=True ")
                    quat = r2quat.r2quat( r1,r2,r3 )
                    print( "# r1=%e\n# r2=%e\n# r3=%e "%tuple(quat))


                    
                    print( " ############################ ")



                    
                    # print "    Snd_Rot_of_RS"

                    # print  Snd_Rot_of_RS(r1, r2, r3)
                    # print
                    # print " ############################## "
                    # print
                    # print M

                sys.exit(0)
                for t,d in zip(["AA","BB","CC"],abc):
                    if t in variables_dict:
                        v = variables_dict[t]
                        v.value += d
                        v.min   += d
                        v.max   += d
                
                for t,d in zip(["r1","r2","r3"],[r1,r2,r3]):
                    if t in variables_dict:
                        v = variables_dict[t]
                        v.value += d
                        v.min   += d
                        v.max   += d

            
            if len(variables_dict):
                print( " LAUNCHING MINIMIZATION ")

                # for x in np.arange(4.8,5.0,0.01):
                #     params.AA = x
                #     print fitobject.error()
                
                miniob.amoeba(params.ftol)

                
                
                print( fitobject.error())


                print( " NOW REWRITING MAXIMA FILE WITH HKL TAGGED PEAKS")
                file = open("tagged_"+params.maxima_file,"w")
                pickle.dump(maxima,file)
                pickle.dump((dim1,dim2),file)
                file.close()
                
            else:
                return

        else:
            print( " JUST TRANSFORMING ONCE TO CHECK ")
            fitobject.error(write=1)
        
        hkls, maxs, hkls_signed = SHOW
        if hkls is not None:
            for h,m in zip ( hkls_signed, maxs) :
                print( h,m)
        
        for key in variables_dict.keys():
            v=variables_dict[key].value
            print( key,"=", v)
        

            
def main():
    
    paramsSystem = read_configuration_file(sys.argv[1])
    main3d(paramsSystem)
    print( " processo ", myrank, " aspetta ")
    comm.Barrier()






def sphere_sampling ( N ):
  #  Reference:
  #
  #    Richard Swinbank, James Purser,
  #    Fibonacci grids: A novel approach to global modelling,
  #    Quarterly Journal of the Royal Meteorological Society,
  #    Volume 132, Number 619, July 2006 Part B, pages 1769-1793.
  #
  
  numoro = 1.618033988749895

  ps =  np.linspace(0.0, 1.0, num=N, endpoint=False)

  zs = 1-2*ps
  r=np.sqrt(  1-zs*zs   )
  xs = np.cos( numoro * 2 * np.pi * ps*N ) *r
  ys = np.sin( numoro * 2 * np.pi * ps*N ) *r

  res = np.zeros([N,3],"f")
  res[:,2]=zs
  res[:,1]=ys
  res[:,0]=xs
  return res

def get_fft(PP, bins, irtipo ):
     irtipo=irtipo
     freqs = np.fft.fftfreq(len(bins)-1)+0.000001
     h = numpy.histogram(  PP  ,  bins=bins      )[0]
     ft = np.fft.fft(h)
     ft[:irtipo ] = 0
     ft[-irtipo: ] = 0
     ft  =  np.abs(ft/np.sqrt(np.abs(freqs)))

     return ft
     
def angle_compatible(assi,i, directions, cc, cb,ca):
     if len(assi)==0:
          return True
     if len(assi)==1:
          if cc is None:
               return True
          else:
               return abs( np.dot(directions[assi[0]], directions[i])-cc )<0.05
          
     if len(assi)==2:

          if abs(np.linalg.det(   [directions[assi[0]],directions[assi[1]],  directions[i]]    ) )<0.2:
             return False
         
          if cb is not None:
               if abs( np.dot(directions[assi[0]], directions[i])-cb )>0.05:
                    return False
               
          if ca is not None:
               if abs( np.dot(directions[assi[1]], directions[i])-ca )>0.05:
                    return False

          return True

def Fourier(points, cc=None, cb = None, ca=None, lim_aa=None ):
    dsum=[]
    for i1 in range(len(points)-1):
         dmin = 1.0e37
         for i2 in range( len(points)):
             if i1!=i2:
                 d = np.linalg.norm(points[i2]-points[i1])
                 if dmin>d:
                     dmin=d
         dsum.append(dmin)

    rtipo = np.median(dsum) 
              
    N = 10000
    directions = sphere_sampling( N  )

    nearest = []
    for i,d in enumerate(directions):
         diff = directions-d
         diff = np.linalg.norm(diff, axis=-1)
         order = np.argsort(diff)

         diff = directions+d
         diff = np.linalg.norm(diff, axis=-1)
         order2 = np.argsort(diff)
         # print order2
         nearest.append( np.concatenate([order[1:6],order2[:6]]) )
         # print "nearests " , i
    
    Rmax  = np.linalg.norm( points ,axis=-1).max()
    Nexp = 2

    PP =    np.tensordot(directions , points, [[-1],[-1]]) 

    bins = np.linspace(-Nexp*Rmax, Nexp*Rmax, num=2015 , endpoint = True )

    # print Nexp*Rmax, rtipo
    
    irtipo = int(Nexp*Rmax/ rtipo)

    freqs = np.fft.fftfreq(len(bins)-1)+0.000001

    pmax=0.0
    dmax = None
    smax = None

    ms = []
    mods=[]
    for i in range(len(PP)):
         # print i

         ft=get_fft(PP[i],bins ,irtipo )
         m = ft.max()
         ms.append(m)


         fm = np.argmax(np.abs(ft[:len(ft)/2]) )
         mods.append(fm)


         
         # if m> pmax:
         #      pmax = m
         #      dmax = directions[i]
         #      smax = ft

    potabile = [1]*len(ms)
    found_axes_set=[]
    assi = []
    # FIRST AXIS
    axe = 0

    pmax = 0
    imax=0
    iloc = 0
    mloc = []
    dloc=[]
    modloc=[]

    idir=[]
    
    for i in range(len(PP)):
        if potabile[i] and angle_compatible(assi,i, directions, cc, cb,ca):
            m = ms[i]
            
            for k in nearest[i] :
                if m<ms[k]  and angle_compatible(assi,k, directions, cc, cb,ca)  :
                    break
            else:
                iloc += 1
                mloc.append(m)
                dloc.append( directions[i]  )
                modloc.append(mods[i] )
                idir.append(i)
                
            if m> pmax:
                for k in nearest[i] :
                    if m<ms[k]  and angle_compatible(assi,k, directions, cc, cb,ca)  :
                        break
                else:
                    pmax = m
                    imax = i


    if lim_aa is not None:                    
        order = np.argsort(mloc) [::-1]
        dloc = np.array(dloc)[order]
        mloc = np.array(mloc)[order]
        modloc = (np.array(modloc)[order])*(1.0/(2*Nexp*Rmax))
        idir = np.array(idir)[order]
        for k in range(len(order)):
            if modloc[k]>=lim_aa[0] and modloc[k]<=lim_aa[1]:
                imax = idir[k]
                break
        else:
            raise Exception( " nodirection found with AA within limits")
    
    # print mloc[:10]
    # print mloc[-10:]
    # print dloc[-10:]
    # print (1.0/(2*Nexp*Rmax))*modloc[:]


                    
    assi.append(imax)
    nonpotabili=set([imax])
    for layers in range(4):
        for i in list(nonpotabili):
            for k in nearest[i]:
                nonpotabili.add(k)
    for i in nonpotabili:
        potabile[i]=0


    # SECOND AXIS
    axe = 1
    
    pmax=0
    imax=0
    
    for i in range(len(PP)):
        if potabile[i] and angle_compatible(assi,i, directions, cc, cb,ca):
            m = ms[i]
            if m> pmax:
                for k in nearest[i] :
                   if m<ms[k]  and angle_compatible(assi,k, directions, cc, cb,ca)  :
                       break
                else:
                    pmax = m
                    imax = i
#     if 1:
                assi_bis = copy.copy(assi)
                assi_bis.append(imax)
                
                nonpotabili=set([imax])
                
                for layers in range(4):
                    for inp in list(nonpotabili):
                        for k in nearest[inp]:
                            nonpotabili.add(k)
                
                potabile_bis = copy.copy(potabile)
                for inp in nonpotabili:
                    potabile_bis[inp]=0

                # third AXIS
                axe = 2
                pmax = 0
                imax=0
                for ibis in range(len(PP)):
                    if potabile_bis[ibis] and angle_compatible(assi_bis,ibis, directions, cc, cb,ca):
                        m = ms[ibis]
                        if m> pmax:
                            for k in nearest[ibis] :
                                if m<ms[k]  and angle_compatible(assi_bis, k, directions, cc, cb,ca)  :
                                    break
                            else:
                                pmax = m
                                imax = ibis
                assi_bis.append(imax)
                found_axes_set.append(assi_bis)


               
    dmax_s_list = []
    smax_s_list = []
    abc_list = []

    for assi in found_axes_set:
        fmod = []
        plots = []
        for i in assi:
             ft = get_fft(PP[i],bins ,irtipo )
             fm = np.argmax(np.abs(ft[:len(ft)/2]) )
             # fm = np.argmax(np.abs(ft[:]) )
             fmod.append(fm)
             print( " FMOD " , fm)
             plots.append(ft)

        order = np.argsort(fmod) # il c , piu lungo dovrebbe essere ultimo 
        order =  [0,1,2]  # il c , piu lungo dovrebbe essere ultimo 

        dmax_s = np.array([ directions[assi[i] ] for i in  order ])
        smax_s = np.array([ get_fft(PP[assi[i] ],bins ,irtipo ) for i in order   ])
        abc    = np.array([ (1.0/(2*Nexp*Rmax))* fmod[i] for i in order ])



        det = np.linalg.det( [directions[assi[i] ] for i in  order ] )

        # QUA    dmax_s[2] = -dmax_s[2]
        # if det <0:
        #      tmp      = order[0]
        #      order[0] = order[1]
        #      order[1] = tmp
        dmax_s_list .append( dmax_s)
        smax_s_list .append( smax_s)
        abc_list    .append( abc   )
    return dmax_s_list, smax_s_list,abc_list


# print " LOADING " 
# points = numpy.loadtxt(str(sys.argv[2]))
# print " chiamo gfourier " 
# ds,ss, abc  = Fourier(points,0,0,0)
























    
USAGE = """ USAGE :
    tds2el inputfile

input file containing


"""
print( __name__ )

# if __name__ == "__main__":

if(sys.argv[0][-12:]!="sphinx-build"):

    if len(sys.argv) < 2:
        print( USAGE)
    else:
        main( )

    # sys.exit()


