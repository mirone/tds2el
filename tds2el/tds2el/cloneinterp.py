#!/usr/bin/env python
# -*- coding: utf-8 -*-
###################################################################################
# Fit of elastic constants from diffused thermal scattering 
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#             Routines for rotating the data to locate them in reciprocal 3D
#             are from the first stages of 3DRSR project of Gael Goret
###################################################################################
import glob
import math

import h5py
import numpy
import os
import traceback
import string
import datetime
import time
from scipy.interpolate import interp1d
import scipy.signal
from PyMca5.PyMcaIO import EdfFile


def get_timeintensity_interpolator():
    times = []
    ints  = []
    
    s=open("TIMEINTENSITY.txt","r").read()
    sl = string.split(s,"\n")
    for l in sl:
        l=string.split(l)
        if len(l)==0: break
        day,month, year = string.split(l[0],"/")
        hours,mins,secs = string.split(l[1],":")
        dt =  datetime.datetime(int(year), int(month), int(day), int(hours), int(mins), int(secs))
        
        s = time.mktime(dt.timetuple())
        
        times.append(s)
        ints.append(float( l[2] ))
    return interp1d(times, ints) 

# timeintensity_interpolator = get_timeintensity_interpolator()
import os



from mpi4py import MPI
myrank = MPI.COMM_WORLD.Get_rank()
nprocs = MPI.COMM_WORLD.Get_size()
procnm = MPI.Get_processor_name()
comm = MPI.COMM_WORLD

import fabio

import numpy as np
import sys, time


if(sys.argv[0][-12:]!="sphinx-build"):
    import spotpicker_cy



#        images_prefix = None
#        file2becloned = None
#        clonefile     = None

def main(params):
    flist = glob.glob(params.images_prefix+"*.cbf" )
    flist.sort()
    h5=h5py.File( params.file2becloned , "r")

    hkls_start = np.array(h5["harvest/hkls_start"][:])
    np4hkl     = np.array(h5["harvest/np4hkl" ][:])
    correction =   np.array(h5["harvest/correction" ][:])

    
    target = np.zeros([hkls_start[-1]],"f") 
    if myrank==0:
        targetres = np.zeros([hkls_start[-1]],"f") 

    interp_prostart       = np.array(h5["interp/interp_prostart"][:])
    interp_compacted_data = np.array(h5["interp/interp_compacted_data"][:])

    
    Ntot_images = len(flist)
    if params.increment is not None:
        FFs = numpy.loadtxt(params.increment)[:,1]+1

    nblockfl = Ntot_images/nprocs+1

    hkl_max = int(h5["harvest/H"].value)
    qmax = h5["harvest/Qmax"].value   
    spotpicker = spotpicker_cy.PySpotPicker(hkl_max, qmax, do_elastic=1)


    Filter_fname = params.filter_file
    
    print( 'Loading images filter ...')
    if(Filter_fname[-4:]==".edf"):
        Filter=EdfFile.EdfFile(Filter_fname,"r").GetData(0).astype(np.float32)
        # assert( Filter.shape ==  (dim1,dim2) ) 
    else:
        f = open(Filter_fname,'rb')
        raw_data = f.read()
        Filter = np.fromstring(raw_data, np.uint8).reshape((dim1,dim2)).astype(np.float32)

    dim1,dim2 = Filter.shape


    if params.badradius>0.0  or params.airscattering is not None:
        DIM1=1
        while DIM1<dim1*2:
            DIM1*=2
        DIM2=1
        while DIM2<dim2*2:
            DIM2*=2

        import fftw3f
        ry = numpy.fft.fftfreq(DIM1,1.0/DIM1) .astype("f")
        rx = numpy.fft.fftfreq(DIM2,1.0/DIM2) .astype("f")

        fftw_direct=  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")
        fftw_rec   =  fftw3f.create_aligned_array( (len(ry), len(rx) )  ,"F")

        fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['measure'])
        fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['measure'])
        # fft_plan_F  =  fftw3f.Plan( fftw_direct, fftw_rec , direction='forward', flags=['estimate'])
        # fft_plan_B  =  fftw3f.Plan( fftw_rec , fftw_direct, direction='backward', flags=['estimate'])

    if params.badradius>0.0:

        disco = (ry*ry)[:,None] + (rx*rx)
        disco = (numpy.less( disco, params.badradius* params.badradius   )).astype("f")

        fftw_direct[:] = disco
        fft_plan_F()
        disco_reci = numpy.array(fftw_rec)






        
    
    for iim, fname in enumerate(flist):
        
        if (iim/nblockfl)%nprocs != myrank:
            continue

            
        print( 'Working on image %s' % fname)
        Newimg = fabio.open(fname)
        data = Newimg.data.astype(np.float32)  # *Filter
        if params.increment is not None:
            print( " INCREMENTO ", FFs[iim])
            data[:] = data*FFs[iim]




        if params.normalisation_file is  None:
            data[:]=data / correction
        else:
            data[:]=data/ params.normalisation[ fname ]    / correction





        data[ numpy.equal(0, Filter) ] = 0

        
        if params.startfrom is not None and iim < params.startfrom:
            data[: ] = numpy.nan
        elif  params.threshold>0.0:
            strongs = numpy.less( params.threshold  ,data)
            if strongs.sum() :
                if params.badradius>0:
                    fftw_direct[:]=0.0
                    fftw_direct[  : data.shape[0]  ,  : data.shape[1]] =  strongs
                    fft_plan_F()
                    fftw_rec[:] = fftw_rec * disco_reci
                    fft_plan_B()
                    size =  data.size
                    extramask = numpy.less(0.1, abs(fftw_direct [  : data.shape[0]  ,  : data.shape[1]] )/size   )
                    print( " SETTO ", extramask.sum() , "  to NAN " )
                    data[extramask ] = numpy.nan
                    # data[extramask ] = -data[extramask ]
                else:
                    print (" N hot spots " ,  strongs.sum())
                    print( " ALL TO NAN ")
                    data[: ] = numpy.nan

        data[ numpy.equal(0, Filter) ] = numpy.nan
 

        
        
        spotpicker_cy.clona(hkls_start , target,  iim , interp_prostart, interp_compacted_data, data )

    comm.Barrier()
    print( target.shape)
    if myrank==0:
        comm.Reduce( [target, target.size, MPI.FLOAT] , [targetres, target.size, MPI.FLOAT] , op=MPI.SUM, root=0)
    else:
        comm.Reduce([target,target.size, MPI.FLOAT], None, op=MPI.SUM, root=0)
    comm.Barrier()

    if   nprocs>1:
        if myrank>0:
            print(  myrank, "  RITORNA " )
            return

    h5clone=h5py.File( params.clonefile , "w")
    if("harvest" not in h5clone):
        h5clone.create_group("harvest")

    h5clone["harvest/hkls_start"]  = np.array( h5["harvest/hkls_start"][:] )
    h5clone["harvest/np4hkl"]  = np.array( h5["harvest/np4hkl"][:] )
    h5clone["harvest/H"]  =  h5["harvest/H"].value
    h5clone["harvest/H2p1"]  = h5["harvest/H2p1"].value
    h5clone["harvest/Qmax"]  = h5["harvest/Qmax"].value
    h5clone["harvest/qcube"]  = h5["harvest/qcube"].value
    
    h5clone["harvest/cellvectors"]  = np.array( h5["harvest/cellvectors"][:] )
    h5clone["harvest/Q0"]  = np.array( h5["harvest/Q0"][:] )
    
    tmp = np.array( h5["harvest/compacted_data"][:] )
    tmp[3::7] = targetres
    h5clone["harvest/compacted_data"]  = tmp
    
    h5     .close()
    h5clone.close()


if(sys.argv[0][-12:]!="sphinx-build"):

    class Parameters:
        images_prefix = None
        file2becloned = None
        clonefile     = None
        increment = None
        normalisation_file = None
        airscattering = None
        startfrom=None
        exec(open(sys.argv[1]).read())

    paramsInput = Parameters()
    if paramsInput.normalisation_file  is not None:
        paramsInput.normalisation={}
        sl = open(paramsInput.normalisation_file ,"r").read().split("\n")
        for  l in sl:
            fnv = l.split()
            if len(fnv):
                fn,v = fnv
            paramsInput.normalisation[fn]=float(v)
        
    main( paramsInput    )



