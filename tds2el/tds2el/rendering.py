
#/*##########################################################################
# Copyright (C) 2011-2017 European Synchrotron Radiation Facility
#
#              TDS2EL
# Author : 
#             Alessandro Mirone, Bjorn Wehinger
#             
#  European Synchrotron Radiation Facility, Grenoble,France
#
# TDS2EL is  developed at
# the ESRF by the Scientific Software  staff.
# Principal author for TDS2EL: Alessandro Mirone, Bjorn Wehinger
#
# This program is free software; you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option) 
# any later version.
#
# TDS2EL is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# TDS2EL; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307, USA.
#
# TDS2EL follows the dual licensing model of Trolltech's Qt and Riverbank's PyQt
# and cannot be used as a free plugin for a non-free program. 
#
# Please contact the ESRF industrial unit (industry@esrf.fr) if this license 
# is a problem for you.
#############################################################################*/

if 1:
     from pymol.cgo import *
     from pymol import cmd
     from pymol.opengl.gl import *
import math

import numpy
import sys


import numpy as np

def sphere_sampling ( N ):
  #  Reference:
  #
  #    Richard Swinbank, James Purser,
  #    Fibonacci grids: A novel approach to global modelling,
  #    Quarterly Journal of the Royal Meteorological Society,
  #    Volume 132, Number 619, July 2006 Part B, pages 1769-1793.
  #
  
  numoro = 1.618033988749895

  ps =  np.linspace(0.0, 1.0, num=N, endpoint=False)

  zs = 1-2*ps
  r=np.sqrt(  1-zs*zs   )
  xs = np.cos( numoro * 2 * np.pi * ps*N ) *r
  ys = np.sin( numoro * 2 * np.pi * ps*N ) *r

  res = np.zeros([N,3],"f")
  res[:,2]=zs
  res[:,1]=ys
  res[:,0]=xs
  return res





def get_fft(PP, bins, irtipo ):
     irtipo=irtipo
     freqs = np.fft.fftfreq(len(bins)-1)+0.000001
     h = numpy.histogram(  PP  ,  bins=bins      )[0]
     ft = np.fft.fft(h) 
     ft[:irtipo ] = 0
     ft[-irtipo: ] = 0
     ft  =  np.abs(ft / np.sqrt(np.abs(freqs)))

     return ft
     

def angle_compatible(assi,i, directions, cc, cb,ca):
     if len(assi)==0:
          return True
     if len(assi)==1:
          if cc is None:
               return True
          else:
               return abs( np.dot(directions[assi[0]], directions[i])-cc )<0.05
          
     if len(assi)==2:
          
          if cb is not None:
               if abs( np.dot(directions[assi[0]], directions[i])-cb )>0.05:
                    return False
               
          if ca is not None:
               if abs( np.dot(directions[assi[1]], directions[i])-ca )>0.05:
                    return False

          return True
               

def Fourier(points, cc=None, cb = None, ca=None):

     dsum=0
     for i1 in range(len(points)-1):
          dmin = 1.0e37
          for i2 in range(i1+1, len(points)):
               
               d = np.linalg.norm(points[i2]-points[i1])
               if dmin>d:
                    dmin=d
          # print dmin
          dsum+=dmin

     rtipo = dsum / len(points)
               
     N = 10000
     directions = sphere_sampling( N  )

     nearest = []
     for i,d in enumerate(directions):
          diff = directions-d
          diff = np.linalg.norm(diff, axis=-1)
          order = np.argsort(diff)

          diff = directions+d
          diff = np.linalg.norm(diff, axis=-1)
          order2 = np.argsort(diff)
          # print order2
          nearest.append( np.concatenate([order[1:6],order2[:6]]) )
          print "nearests " , i
     
     Rmax  = np.linalg.norm( points ,axis=-1).max()
     Nexp = 2

     PP =    np.tensordot(directions , points, [[-1],[-1]]) 

     bins = np.linspace(-Nexp*Rmax, Nexp*Rmax, num=2015 , endpoint = True )

     # print Nexp*Rmax, rtipo
     
     irtipo = int(Nexp*Rmax/ rtipo)

     freqs = np.fft.fftfreq(len(bins)-1)+0.000001

     pmax=0.0
     dmax = None
     smax = None

     ms = []
     for i in range(len(PP)):
          print i

          ft=get_fft(PP[i],bins ,irtipo )
          m = ft.max()
          ms.append(m)
          
          # if m> pmax:
          #      pmax = m
          #      dmax = directions[i]
          #      smax = ft

     potabile = [1]*len(ms)
     assi = []
     for axe in range(3):
          pmax = 0
          imax=0
          for i in range(len(PP)):
               if potabile[i] and angle_compatible(assi,i, directions, cc, cb,ca):
                    m = ms[i]
                    if m> pmax:
                         for k in nearest[i] :
                              if m<ms[k]  and angle_compatible(assi,k, directions, cc, cb,ca)  :
                                   break
                         else:
                              pmax = m
                              imax = i
                         
          assi.append(imax)

          nonpotabili=set([imax])
          for layers in range(4):
               for i in list(nonpotabili):
                    for k in nearest[i]:
                         nonpotabili.add(k)
          for i in nonpotabili:
               potabile[i]=0
          
     fmod = []
     plots = []
     for i in assi:
          ft = get_fft(PP[i],bins ,irtipo )
          fm = np.argmax(np.abs(ft[:len(ft)/2]) )
          # fm = np.argmax(np.abs(ft[:]) )
          fmod.append(fm)
          print " FMOD " , fm
          plots.append(ft)
          
     order = np.argsort(fmod) # il c , piu lungo dovrebbe essere ultimo 

     dmax_s = [ directions[assi[i] ] for i in  order ]
     smax_s = [ get_fft(PP[assi[i] ],bins ,irtipo ) for i in order   ]
     abc    = [ (1.0/(2*Nexp*Rmax))* fmod[i] for i in order ]
     
     det = np.linalg.det( [directions[assi[i] ] for i in  order ] )
     
     dmax_s[2] = -dmax_s[2]
     # if det <0:
     #      tmp      = order[0]
     #      order[0] = order[1]
     #      order[1] = tmp

     return dmax_s, smax_s,abc


print " LOADING " 
points = numpy.loadtxt(str(sys.argv[2]))
print points
# for i  in range(len(points)):
#      dm=1.0e38
#      for j in range(0, len(points)):
#           if i==j :
#                continue
#           d = points[i]-points[j]
#           d=math.sqrt((d*d).sum())
#           if d<dm:
#                dm=d
#     print i, dm




if 0:
     print " chiamo gfourier " 
     ds,ss, abc  = Fourier(points,0,0,0)
     print " Ds " , ds
     print "ABC", abc
     for i,s in enumerate(ss):
          np.savetxt("smax%d.txt"%i, s)

     # ds[2] = np.cross(ds[0],ds[1]    )
     # ds[2]=ds[2]/np.linalg.norm(ds[2])

     print np.dot(ds[0],ds[1])
     print np.dot(ds[0],ds[2])
     print np.dot(ds[1],ds[2])
else:
     ds=np.zeros([3,3],"f")
     ds[2] = [1,0,0]
     ds[0] = [0,1,0]
     ds[1] = [0,0,1]
     
dists = numpy.sqrt( (points*points).sum(axis=-1))

# inds = numpy.argsort(dists)
# points=points[inds[:40]]

obj = []

obj.extend( [ COLOR ]+ [1.0,1.0,1.0] )
for p in points:
   radius = 0.03
   obj.extend( [ SPHERE ]+p.tolist() +[radius] )
cmd.load_cgo(obj,'cgo03',1)


obj = []
obj.extend( [ COLOR ]+ [1.0,0.0,0.0] )
radius = 0.04
obj.extend( [ SPHERE ]+[0,0,0] +[radius] )
cmd.load_cgo(obj,'centro',1)

obj = []
obj.extend( [ COLOR ]+ [0.0,1.0,0.0] )
radius = 0.04
# obj.extend( [ SPHERE ]+[0,0,1] +[radius] )
obj.extend( [ SPHERE ]+  ds[0].tolist()+[radius] )
cmd.load_cgo(obj,'Z',1)

obj = []
obj.extend( [ COLOR ]+ [0.0,0.0,1.0] )
radius = 0.04
# obj.extend( [ SPHERE ]+[1,0,0] +[radius] )
obj.extend( [ SPHERE ]+  ds[2].tolist()  +[radius] )
cmd.load_cgo(obj,'X',1)

obj = []
obj.extend( [ COLOR ]+ [0.0,1.0,1.0] )
radius = 0.04
# obj.extend( [ SPHERE ]+[0,1,0] +[radius] )
obj.extend( [ SPHERE ]+ ds[1].tolist()  +[radius] )
cmd.load_cgo(obj,'Y',1)


cmd.reset()
cmd.zoom('cgo03',3.0)

cmd.clip('far',-12.0)

if 0:
   cmd.turn('z',30)
   cmd.turn('x',-60)

cmd.mplay()
   
        



