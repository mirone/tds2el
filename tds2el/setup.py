#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#    Project: from tds to elastic constants Python + OpenCL
#

"""
Installer script for tds2el
"""



__authors__ = ["Alessandro Mirone from a setup.py written by Jerome Kieffer"]
__contact__ = "mirone@esrf.fr"
__license__ = "a voir"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"
__date__ = "2013-12-2"
__status__ = "beta"
__license__ = """
a voir
"""


import os, sys, glob, shutil
from distutils.core import setup, Extension, Command
from numpy.distutils.misc_util import get_numpy_include_dirs
from distutils.sysconfig import get_python_lib
import string
import stat

from shutil import copyfile

try:
    import numpy
except ImportError:
    text = "You must have numpy installed.\n"
    raise Exception(ImportError, text)

jn = os.sep.join
mpi_dirs = ["/usr/include/openmpi/"]

cmdclass = {}
ext_modules = []
internal_clibraries = []

version = [eval(l.split("=")[1]) for l in open(os.path.join(os.path.dirname(
    os.path.abspath(__file__)), "tds2el", "__init__.py"))
    if l.strip().startswith("version")][0]


packages=["tds2el"+version]
    
installDir = os.path.join(get_python_lib(), "td2el")
data_files =  glob.glob("opencl/*.cl") + glob.glob("tds2el/*.ui")

from distutils.command.install_data import install_data

if sys.platform == "win32":
    # This is for mingw32/gomp?
    data_files[0][1].append(os.path.join("dll", "pthreadGC2.dll"))
    root = os.path.dirname(os.path.abspath(__file__))
    tocopy_files = []
    script_files = []
    for i in os.listdir(os.path.join(root, "scripts")):
        if os.path.isfile(os.path.join(root, "scripts", i)):
            if i.endswith(".py"):
                script_files.append(os.path.join("scripts", i))
            else:
                tocopy_files.append(os.path.join("scripts", i))
    for i in tocopy_files:
        filein = os.path.join(root, i)
        if (filein + ".py") not in script_files:
            shutil.copyfile(filein, filein + ".py")
            script_files.append(filein + ".py")
else:
    script_files = glob.glob("tds2el/scripts/*")




    
def customize_compiler_for_nvcc(self):
        """inject deep into distutils to customize how the dispatch
        to gcc/nvcc works.

        If you subclass UnixCCompiler, it's not trivial to get your subclass
        injected in, and still have the right customizations (i.e.
        distutils.sysconfig.customize_compiler) run on it. So instead of going
        the OO route, I have this. Note, it's kindof like a wierd functional
        subclassing going on."""

        # tell the compiler it can processes .cu
        self.src_extensions.append('.cu')

        # save references to the default compiler_so and _comple methods
        default_compiler_so = self.compiler_so
        super_comp = self._compile

        # now redefine the _compile method. This gets executed for each
        # object but distutils doesn't have the ability to change compilers
        # based on source extension: we add it.
        def _compile(obj, src, ext, cc_args, extra_postargs, pp_opts):
            if os.path.splitext(src)[1] == '.cu':
                # use the cuda for .cu files
                self.set_executable('compiler_so', CUDA['nvcc'])
                # use only a subset of the extra_postargs, which are 1-1 translated
                # from the extra_compile_args in the Extension class

                
                postargs = extra_postargs['nvcc']

                if "USECLANG4NVCC" in os.environ and os.environ ["USECLANG4NVCC"]=="YES":
                    postargs = postargs + ["-ccbin clang-3.8"]
            else:
                self.set_executable('compiler_so', "mpicc")
                postargs = extra_postargs['gcc']
                # postargs = extra_postargs['gcc']

            super_comp(obj, src, ext, cc_args, postargs, pp_opts)
            # reset the default compiler_so, which we might have changed for cuda
            self.compiler_so = default_compiler_so

        # inject our redefined _compile method into the class
        self._compile = _compile


    
try:
    from Cython.Distutils import build_ext
    CYTHON = True
    print(" CYTHON TRUE")
except ImportError:
    CYTHON = False

    
if CYTHON:
    cython_c_ext = ".pyx"
else:
    cython_c_ext = ".cpp"
    from distutils.command.build_ext import build_ext





# We subclass the build_ext class in order to handle compiler flags
# for openmp and opencl etc in a cross platform way
translator = {
    # Compiler
    # name, compileflag, linkflag
    'msvc' : {
        'openmp' : ('/openmp', ' '),
        'debug'  : ('/Zi', ' '),
        'OpenCL' : 'OpenCL',
    },
    'mingw32':{
        'openmp' : ('-fopenmp', '-fopenmp'),
        'debug'  : ('-g', '-g'),
        'stdc++' : 'stdc++',
        'OpenCL' : 'OpenCL'
    },
    'default':{
        'openmp' : ('-fopenmp', '-fopenmp'),
        'debug'  : ('-g', '-g'),
        'stdc++' : 'stdc++',
        'OpenCL' : 'OpenCL'
    }
}

# run the customize_compiler
class custom_build_ext(build_ext):
    """
        Cuda aware builder with nvcc compiler support
        """
    def build_extensions(self):
        customize_compiler_for_nvcc(self.compiler)
        build_ext.build_extensions(self)
        
cmdclass['build_ext'] = custom_build_ext


class smart_install_data(install_data):
    def run(self):
        global  version
        install_cmd = self.get_finalized_command('install')
        self.install_dir = os.path.join(getattr(install_cmd, 'install_lib'),"tds2el"+version)
        print( "DATA to be installed in %s" %  self.install_dir)
        pdir = os.path.join(getattr(install_cmd, 'install_lib'),"tds2el"+version)


            
        return install_data.run(self)


###########################################################################################################
from distutils.command.install_scripts import install_scripts
class smart_install_scripts(install_scripts):
    def run (self):
        global  version
        
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_scripts')
        self.install_lib_dir = getattr(install_cmd, 'install_lib')
            
        self.outfiles = []
        for filein in glob.glob('tds2el/scripts/*'):
            if filein in glob.glob('tds2el/scripts/*~'): continue
            
            print ("INSTALLO ", filein)
            
            if not os.path.exists(self.install_dir):
                os.makedirs(self.install_dir)

            filedest = os.path.join(self.install_dir, os.path.basename(filein+version))
            print( os.path.basename(filein+version))

            if os.path.exists(filedest):
                os.remove(filedest)
                
            text = open(filein, 'r').read()
            text=text.replace("import tds2el", "import tds2el"+version)
            text=text.replace("python", sys.executable)

            f=open(filedest, 'w')
            f.write(text)
            f.close()
            self.outfiles.append(filedest)

        if os.name == 'posix':
            # Set the executable bits (owner, group, and world) on
            # all the scripts we just installed.
            for file in self.get_outputs():
                if self.dry_run:
                    pass
                else:
                    mode = ((os.stat(file)[stat.ST_MODE]) | 0o555) & 0o7777
                    os.chmod(file, mode)







# We subclass the build_ext class in order to handle compiler flags
# for openmp and opencl etc in a cross platform way
translator = {
        # Compiler
            # name, compileflag, linkflag
        'msvc' : {
            'openmp' : ('/openmp', ' '),
            'debug'  : ('/Zi', ' '),
            'OpenCL' : 'OpenCL',
            },
        'mingw32':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            },
        'default':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            }
        }


cmdclass = {}



# run the customize_compiler
class custom_build_ext(build_ext):
    """
    Cuda aware builder with nvcc compiler support
    """
    def build_extensions(self):
        customize_compiler_for_nvcc(self.compiler)
        build_ext.build_extensions(self)

cmdclass['build_ext'] = custom_build_ext


def customize_compiler_for_nvcc(self):
    """inject deep into distutils to customize how the dispatch
    to gcc/nvcc works.

    If you subclass UnixCCompiler, it's not trivial to get your subclass
    injected in, and still have the right customizations (i.e.
    distutils.sysconfig.customize_compiler) run on it. So instead of going
    the OO route, I have this. Note, it's kindof like a wierd functional
    subclassing going on."""

    # tell the compiler it can processes .cu
    self.src_extensions.append('.cu')

    # save references to the default compiler_so and _comple methods
    default_compiler_so = self.compiler_so
    super_comp = self._compile

    # now redefine the _compile method. This gets executed for each
    # object but distutils doesn't have the ability to change compilers
    # based on source extension: we add it.
    def _compile(obj, src, ext, cc_args, extra_postargs, pp_opts):
        print( " EXTRA ", extra_postargs) 
        if os.path.splitext(src)[1] == '.cu':
            # use the cuda for .cu files
            self.set_executable('compiler_so', CUDA['nvcc'])
            # use only a subset of the extra_postargs, which are 1-1 translated
            # from the extra_compile_args in the Extension class


            postargs = extra_postargs['nvcc']

            if "USECLANG4NVCC" in os.environ and os.environ ["USECLANG4NVCC"]=="YES":
                postargs = postargs + ["-ccbin clang-3.8"]
        else:
            self.set_executable('compiler_so', "mpicc")
            postargs = extra_postargs['gcc']
            # postargs = extra_postargs['gcc']

        super_comp(obj, src, ext, cc_args, postargs, pp_opts)
        # reset the default compiler_so, which we might have changed for cuda
        self.compiler_so = default_compiler_so

    # inject our redefined _compile method into the class
    self._compile = _compile



try:
    from Cython.Distutils import build_ext, Extension
    CYTHON = True
except ImportError:
    CYTHON = False

if CYTHON:
    cython_c_ext = ".pyx"
else:
    cython_c_ext = ".cpp"
    from distutils.command.build_ext import build_ext


# We subclass the build_ext class in order to handle compiler flags
# for openmp and opencl etc in a cross platform way
translator = {
        # Compiler
            # name, compileflag, linkflag
        'msvc' : {
            'openmp' : ('/openmp', ' '),
            'debug'  : ('/Zi', ' '),
            'OpenCL' : 'OpenCL',
            },
        'mingw32':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            },
        'default':{
            'openmp' : ('-fopenmp', '-fopenmp'),
            'debug'  : ('-g', '-g'),
            'stdc++' : 'stdc++',
            'OpenCL' : 'OpenCL'
            }
        }

cmdclass = {}

# cmdclass['build_ext'] = build_ext_tds2el


# run the customize_compiler
class custom_build_ext(build_ext):
    """
    Cuda aware builder with nvcc compiler support
    """
    def build_extensions(self):
        customize_compiler_for_nvcc(self.compiler)
        build_ext.build_extensions(self)

cmdclass['build_ext'] = custom_build_ext









class build_ext_tds2el(build_ext):
    def build_extensions(self):

        customize_compiler_for_nvcc(self.compiler)


        # print( self.compiler.compiler_type)

        if self.compiler.compiler_type in translator:
            trans = translator[self.compiler.compiler_type]
        else:
            trans = translator['default']


        for e in self.extensions:
            e.extra_compile_args = [ trans[a][0] if a in trans else a
                                    for a in e.extra_compile_args]
            e.extra_link_args = [ trans[a][1] if a in trans else a
                                 for a in e.extra_link_args]

            if    e.libraries !=[ "fftw3f"] and e.libraries !=[ "mpi"]:
                e.libraries = filter(None, [ trans[a] if a in trans else None
                                             for a in e.libraries])

            # If you are confused look here:
            # print( e, e.libraries)
            # print( e.extra_compile_args)
            # print( e.extra_link_args)
        build_ext.build_extensions(self)

# cmdclass['build_ext'] = build_ext_tds2el


# run the customize_compiler
class custom_build_ext(build_ext):
    """
    Cuda aware builder with nvcc compiler support
    """
    def build_extensions(self):
        customize_compiler_for_nvcc(self.compiler)
        build_ext.build_extensions(self)

cmdclass['build_ext'] = custom_build_ext


# class build_ext_tds2el(build_ext):
#     def build_extensions(self):
#         # print ( build_ext )
#         # raise
#         # customize_compiler_for_nvcc(self.compiler)

#         if self.compiler.compiler_type in translator:
#             trans = translator[self.compiler.compiler_type]
#         else:            
#             trans = translator['default']

 

#         for e in self.extensions:
#             e.extra_compile_args = [ trans[a][0] if a in trans else a
#                                     for a in e.extra_compile_args]
#             e.extra_link_args = [ trans[a][1] if a in trans else a
#                                  for a in e.extra_link_args]

#             if    e.libraries !=[ "fftw3f"] and e.libraries !=[ "mpi_cxx"]:
#                 e.libraries = filter(None, [ trans[a] if a in trans else None
#                                              for a in e.libraries])



#             # If you are confused look here:
#             # print e, e.libraries
#             # print e.extra_compile_args
#             # print e.extra_link_args
#         build_ext.build_extensions(self)

# cmdclass['build_ext'] = build_ext_tds2el

class PyTest(Command):
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import sys, subprocess
        os.chdir("test")
        errno = subprocess.call([sys.executable, 'test_all.py'])
        if errno != 0:
            raise SystemExit(errno)
        else:
            os.chdir("..")
cmdclass['test'] = PyTest


#######################
# build_doc commandes #
#######################

try:
    import sphinx
    import sphinx.util.console
    sphinx.util.console.color_terminal = lambda: False
    from sphinx.setup_command import BuildDoc
except ImportError:
    sphinx = None

if sphinx:
    class build_doc(BuildDoc):

        def run(self):

            # make sure the python path is pointing to the newly built
            # code so that the documentation is built on this and not a
            # previously installed version

            build = self.get_finalized_command('build')
            sys.path.insert(0, os.path.abspath(build.build_lib))

            # Build the Users Guide in HTML and TeX format
            for builder in ('html', 'latex'):
                self.builder = builder
                self.builder_target_dir = os.path.join(self.build_dir, builder)
                self.mkpath(self.builder_target_dir)
                builder_index = 'index_{0}.txt'.format(builder)
                BuildDoc.run(self)
            sys.path.pop(0)
    cmdclass['build_doc'] = build_doc
cmdclass['install_data'] = smart_install_data
cmdclass['install_scripts'] = smart_install_scripts
 
#############################################################################
jn = os.sep.join

c_sorgenti_tmp = ["spotpicker_cy"+cython_c_ext, "spotpicker.cc", "fillPlanVolume.cc","Blob.cc", "Geo.cc", "Tensor.cc", "PeakFinder.cc"]
c_sorgenti = [jn(['tds2el', 'tds2el_c', tok]) for tok in c_sorgenti_tmp]

depends_tmp = [ "spotpicker.h", "Blob.hh","Geo.hh","Tensor.hh","PeakFinder.hh"]
depends = [jn(['tds2el', 'tds2el_c', tok]) for tok in depends_tmp]



cython_ext2 = Extension(name="spotpicker_cy",
                        sources=c_sorgenti,
                        depends=depends,
                        libraries=[ "mpi_cxx"],
                        include_dirs=get_numpy_include_dirs()+["/usr/include/openmpi/"],
                        language="c++",             # generate C++ code
                        # extra_compile_args=['-fopenmp'] ,
                        # this syntax is specific to this build system
                        # we're only going to use certain compiler args with nvcc and not with gcc
                        # the implementation of this trick is in customize_compiler() below
                        extra_compile_args={'gcc': ["-g","-fPIC", "-O3","-fopenmp"] },
                                            # 'nvcc': CUDA["arch"] + [ "--compiler-options",  "-fPIC", "-O3", "-g","-D_FORCE_INLINES" ]},
                        extra_link_args=['-fopenmp'] ,
                        )

ext_modules.append(cython_ext2)

print (ext_modules)
#############################################################################


setup(name='tds2el',
      version=version,
      author="Alessandro Mirone",
      author_email="mirone@esrf.fr",
      description='descrizione da farsi qui',
      url="",
      download_url="",
      ext_package="tds2el"+version,
      scripts=script_files,
      # ext_modules=[Extension(**dico) for dico in ext_modules],
      packages=packages,
      package_dir={"tds2el"+version:"tds2el"  },
      test_suite="test",
      cmdclass=cmdclass,
      data_files=data_files,
      ext_modules=ext_modules
      )



try:
    import pyopencl
except ImportError:
    print("""sprsaocl can use pyopencl to run on parallel accelerators like GPU; this is an optional dependency.
This python module can be found on:
http://pypi.python.org/pypi/pyopencl
""")

















